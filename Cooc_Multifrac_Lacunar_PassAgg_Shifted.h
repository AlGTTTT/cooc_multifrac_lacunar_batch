
#define INITIAL_TEST_TRAN_DATASETS_INCLUDED 

	#define USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

//#define INITIAL_ORTHONORMALIOZATION
//#define NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt

//#define EXIT_AFTER_TRAINING
///////////////////////////////////////////////
#define nLengthOneLineMax 200 //120000 //50000

//#define nLarge 1000000
//#define fLarge 1.0E+12 //9

#define NO_VecInit
//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-9


#define fFeaMin (-5000.0)
#define fFeaMax 5000.0

#define nInputLineLengthMax 100
#define nSubstringLenMax 20
///////////////////////////////////////////////////

#define nNumVecTrainTot (nNumOfVecs_Normal + nNumOfVecs_Malignant) //4178 // for svmguide1_train_2000_2178.txt
//#define nNumVecTrainTot 3089 // for svmguide1_train.txt

//??
	#define nNumVecTestTot 4000

////////////////////////////////////////////////////////////////////
//#define nDim (nDimSelecMax) //4 
#define nDim_WithConst (nDimSelecMax + 1) //8000

#define nDim_D (nDimSelecMax)
#define nDim_D_WithConst (nDimSelecMax + 1)

#define nProdTrainTot (nDimSelecMax*nNumVecTrainTot)
#define nProdTestTot (nDimSelecMax*nNumVecTestTot)

#define nProd_WithConstTrainTot (nDim_WithConst*nNumVecTrainTot)
#define nProd_WithConstTestTot (nDim_WithConst*nNumVecTestTot)
///////////////////////////////////////////////////////////////////////////
//#define nDim_H 1024
//#define nDim_H 512
//#define nDim_H 256
//#define nDim_H 128 //dimension of the nonlinear/transformed space
//#define nDim_H 64 //5 //dimension of the nonlinear/transformed space
//#define nDim_H 32
#define nDim_H 16
//#define nDim_H 8
//#define nDim_H 4
//#define nDim_H 3
//#define nDim_H 2

//#define nNumOfHyperplanes 7200  
//#define nNumOfHyperplanes 3600  
//#define nNumOfHyperplanes 1800  
//#define nNumOfHyperplanes 960  

//#define nNumOfHyperplanes 480  
//#define nNumOfHyperplanes 240  
//#define nNumOfHyperplanes 120  
//#define nNumOfHyperplanes 80  
//#define nNumOfHyperplanes 50  
//#define nNumOfHyperplanes 30  
//#define nNumOfHyperplanes 15  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 8  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 4 //3 // the number of hyperplanes per one nonlinear fea
#define nNumOfHyperplanes 3 // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 2 //3 // the number of hyperplanes per one nonlinear fea
#define nK (nNumOfHyperplanes) // the number of hyperplanes

//#define nNumOfItersOfTrainingTot 6400
//#define nNumOfItersOfTrainingTot 3200
//#define nNumOfItersOfTrainingTot 1600
//#define nNumOfItersOfTrainingTot 800 //99.8%/8.25% at 8-15-800 with srand(3);
//#define nNumOfItersOfTrainingTot 400 //?
//#define nNumOfItersOfTrainingTot 200 //?
//#define nNumOfItersOfTrainingTot 100 //?
//#define nNumOfItersOfTrainingTot 60 //?

//#define nNumOfItersOfTrainingTot 40 //?
//#define nNumOfItersOfTrainingTot 20 //
//#define nNumOfItersOfTrainingTot 10 //25 //15 
//#define nNumOfItersOfTrainingTot 6 

//#define nNumOfItersOfTrainingTot 4 
//#define nNumOfItersOfTrainingTot 2 //3 //10#define nNumOfItersOfTrainingTot 1 //10 //5 
#define nNumOfItersOfTrainingTot 1
//////////////////////////////////////////////////////////////////
//#define fW_Init_Min (-2.0) 
//#define fW_Init_Max 2.0

//#define fW_Init_Min (-1.0) 
//#define fW_Init_Max 1.0

#define fW_Init_Min (-0.1) //(-1.0) 
#define fW_Init_Max 0.1 //(1.0) 

#define nDim_U (nDim_D_WithConst*nDim_H*nK)

#define fU_Init_Min (-4.0) 
#define fU_Init_Max 4.0 
//#define fU_Init_Min (-2.0) 
//#define fU_Init_Max 2.0 

//#define fU_Init_Min (-1.0) 
//#define fU_Init_Max 1.0 

//#define fU_Init_Min (-0.5) //(-1.0) 
//#define fU_Init_Max 0.5 //(1.0) 

//#define fU_Init_Min (-0.1) //(-1.0) 
//#define fU_Init_Max 0.1 //(1.0) 

//#define INCLUDE_BIAS_POS_TO_NEG
// change 'nNumVecTrainTot' as well
//#define fBiasPosOrNeg (1.0) //1.0 -- no bias,  > 1.0 -- a positive bias; < 1.0 -- negative bias

#define fBiasForClassifByLossFunction 0.0
//#define fBiasForClassifByLossFunction (-0.1)

//#define fBiasForClassifByLossFunction 0.1
//#define fBiasForClassifByLossFunction 0.2
//#define fBiasForClassifByLossFunction 0.3

#define fFeaConstInit 1.0 //0.5?
////////////////////////////////////////

#define fAlpha 0.9 //const float fAlphaf, // < 1.0
#define fEpsilon 0.05 //?? -- linearly depends on variance

#define fCr 0.125
#define fC 0.125

#define nSrandInit 2
///////////////////////////////////////////////////////////////////

#define fScalarProdOfNormalizedHyperplanesMax 0.1

///////////////////////////
#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_Golden_Search = 0.01; // 0.005
const float eps_thesame = 1.0E-5; //-2; // precision of the position
/////////////////////////////////////

typedef struct
{
	int nNumOfPosit_Y_Totf;
	int	nNumOfNegat_Y_Totf;

	int	nNumOfCorrect_Y_Totf;
	int	nNumOfVecs_Totf;

	int	nNumOfPositCorrect_Y_Totf;
	int	nNumOfNegatCorrect_Y_Totf;

	float fPercentageOfCorrectTotf;
	float fPercentageOfCorrect_Positf;
	float fPercentageOfCorrect_Negatf;

} PAS_AGG_MAX_OUT_RESUTS;

/*
const int nDim_D_WithConstf, // = dimension of the original space
const int nDim_Hf, //dimension of the nonlinear/transformed space

const int nKf, //nNumOfHyperplanes
const int nDim_Uf, //(nDim_D_WithConst*nDim_H*nK)

///////////////////////
const float fW_Train_Arrf[],
const float fU_Train_Arrf[], //[nDim_U],

*/

typedef struct
{
	int nDim_D_WithConstf;
	int	nDim_Hf;

	int	nKf;
	int nDim_Uf; //(nDim_D_WithConst*nDim_H*nK)
	float *fW_Train_Arrf; // [nDim_D_WithConstf];
	float *fU_Train_Arrf; // [nDim_Uf];

} MODEL_PARAMETERS;


