
//delete 'return SUCCESSFUL_RETURN;' after //premature exit for testing!

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
//#include "Multifractal_SelectedFeas_2.h"
#include "Cooc_Multifrac_Lacunar_Batch.h"

using namespace imago;

FILE *fout_lr;
FILE *fout_PrintFeatures;

int
nNumOfOneFea_Spectrum_AdjustedGlob = -1,
nNumOfSpectrumFea_Glob = -1,

iFea_Glob,
iIntensity_Interval_1_Glob,
iIntensity_Interval_2_Glob;

#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.cpp"
//#include "Cooc_Multifrac_Lacunar_PassAgg_Shifted.cpp"

int doMultifractalSpectrumOf_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,
	COLOR_IMAGE *sColor_Image,

	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

#ifdef HISTOGRAM_EQUALIZATION_APPLIED
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);
#endif //#ifdef HISTOGRAM_EQUALIZATION_APPLIED

	int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

		//const int nDim_2pNf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;

	float
		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot]; //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

	////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fout_lr = fopen("wMain_Multifractal_Lacunar.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


//initialization
	for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)
	{
		fOneDim_Multifractal_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)

/////////////////////////////////////////////////////////
	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nImageWidth, nImageHeight, nNumOfMultifractalFeasFor_OneDimTot);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nImageWidth, nImageHeight, nNumOfMultifractalFeasFor_OneDimTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
		getchar(); exit(1);

		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image height:nImageHeight = %d > nWidMax = %d", nImageHeight, nWidMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image height: nImageHeight = %d > nWidMax = %d...", nImageHeight, nWidMax);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	//Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	//COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		//&sColor_Image); // COLOR_IMAGE *sColor_Imagef);
		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		//if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			//sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
/*
		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;
*/
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in'Initializing_Color_To_CurSize':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in'Initializing_Color_To_CurSize':");
		getchar(); exit(1);

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
				getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)

//premature exit for testing
	//return SUCCESSFUL_RETURN;
#ifdef HISTOGRAM_EQUALIZATION_APPLIED
	nRes = HistogramEqualization_ForColorFormatOfGray(

		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

#endif //#ifdef HISTOGRAM_EQUALIZATION_APPLIED


///////////////////////////////////////////////////////////////////////////
	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	//printf("\n 4"); getchar();

	///////////////////////////////////////////////////////////////////////
	nRes = GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
		sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf, // float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOfFeasForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf, //float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf); // float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':");
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

//premature exit for testing
	//return SUCCESSFUL_RETURN;
	//fprintf(fout_PrintFeatures, "\n\n nNumOfFeasForMultifractalTot = %d, nNumOfProcessedFiles_NormalTot_Glob = %d\n", nNumOfFeasForMultifractalTot, nNumOfProcessedFiles_NormalTot_Glob);

	for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)
	{
		fOneDim_Multifractal_Arrf[iFeaf] = fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf];

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n fOneDim_Multifractal_Arrf[%d] = %E, nNumOfProcessedFiles_NormalTot_Glob == -1", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	}//for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)

	for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2 * nNumOfFeasForMultifractalTot; iFeaf++)
	{
		fOneDim_Multifractal_Arrf[iFeaf] = fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iFeaf - nNumOfFeasForMultifractalTot];

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n fOneDim_Multifractal_Arrf[%d] = %E, nNumOfProcessedFiles_NormalTot_Glob == -1", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	}//for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2*nNumOfFeasForMultifractalTot; iFeaf++)

	for (iFeaf = 2 * nNumOfFeasForMultifractalTot; iFeaf < 3 * nNumOfFeasForMultifractalTot; iFeaf++)
	{
		fOneDim_Multifractal_Arrf[iFeaf] = fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf - (2 * nNumOfFeasForMultifractalTot)];

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n fOneDim_Multifractal_Arrf[%d] = %E,  nNumOfProcessedFiles_NormalTot_Glob == -1", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	}//for (iFeaf = 2*nNumOfFeasForMultifractalTot; iFeaf < 3*nNumOfFeasForMultifractalTot; iFeaf++)

	//nNumOfMultifractalFeasFor_OneDimTot

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n All OneDim multifractal feas:");
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)
	{
		fprintf(fout_lr, "\n fOneDim_Multifractal_Arrf[%d] = %E", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);
	}//for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)

	fprintf(fout_lr, "\n\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////
	/*
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

	*/
	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doMultifractalSpectrumOf_ColorImage(...
////////////////////////////////////////////////////////////////////

int doOneFea_OfMultifractal(
	const int nNumOfOneFeaf,

	//const Image& image_in,
	const COLOR_IMAGE *sColor_Image,

	//	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
	float &fOneFeaf)
{

	int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrGenerDimf);

	int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrGenerDimf);

	int OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrCrowdingIndexf);

	int
		nNumOfOneFea_Adjustedf,
		nRes;

	float
		fOneFea_FrGenerDimf,
		fOneFea_FrSpectrumf,
		fOneFea_FrCrowdingIndexf;
	////////////////////////////////////////////////////////////////////////

	printf("\n\n 'doOneFea_OfMultifractal'");
	if (nNumOfOneFeaf < 0 || nNumOfOneFeaf >= 3 * nNumOfFeasForMultifractalTot) // == nNumOfMultifractalFeasFor_OneDimTot
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The feature number %d is out of the range of 0 and %d", nNumOfOneFeaf, 3 * nNumOfFeasForMultifractalTot);
		fprintf(fout_lr, "\n\n The feature number %d is out of the range of 0 and %d", nNumOfOneFeaf, 3 * nNumOfFeasForMultifractalTot);

		fflush(fout_lr);  getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFeaf < 0 || nNumOfOneFeaf >= 3 * nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////////////////////

	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	///////////////////////////////////////////////////////////////////////
	if (nNumOfOneFeaf < nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (generalized dim): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'doOneFea_OfMultifractal' (generalized dim): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrGenerDimf); // float &fOneFea_FrGenerDimf) 

		fOneFeaf = fOneFea_FrGenerDimf;
	}//if (nNumOfOneFeaf < nNumOfFeasForMultifractalTot)
	else if (nNumOfOneFeaf >= nNumOfFeasForMultifractalTot && nNumOfOneFeaf < 2 * nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf - nNumOfFeasForMultifractalTot;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (spectrum): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'doOneFea_OfMultifractal' (spectrum): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrSpectrumf); // float &fOneFea_FrSpectrumf) 

		fOneFeaf = fOneFea_FrSpectrumf;

	}//else if (nNumOfOneFeaf >= nNumOfFeasForMultifractalTot  && nNumOfOneFeaf < 2*nNumOfFeasForMultifractalTot)
	else if (nNumOfOneFeaf >= 2 * nNumOfFeasForMultifractalTot && nNumOfOneFeaf < 3 * nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf - (2 * nNumOfFeasForMultifractalTot);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (crowding index): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'doOneFea_OfMultifractal' (crowding index): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrCrowdingIndexf); // float &fOneFea_FrCrowdingIndexf);

		fOneFeaf = fOneFea_FrCrowdingIndexf;
	}//else if (nNumOfOneFeaf >= 2*nNumOfFeasForMultifractalTot && nNumOfOneFeaf < 3 * nNumOfFeasForMultifractalTot)


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'doOneFea_OfMultifractal': nNumOfOneFeaf = %d, fOneFeaf = %E", nNumOfOneFeaf, fOneFeaf);
	fprintf(fout_lr, "\n nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d", nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////
/*
	delete[] sColor_Image->nRed_Arr;
	delete[] sColor_Image->nGreen_Arr;
	delete[] sColor_Image->nBlue_Arr;

	delete[] sColor_Image->nLenObjectBoundary_Arr;
	delete[] sColor_Image->nIsAPixelBackground_Arr;
*/
/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doOneFea_OfMultifractal(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatMax; // -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

int Initializing_Embedded_Image(
	const int nDim_2pNf,

	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]
{
	int
		nIndexOfPixelInEmbeddedImagef,
		nImageSizeCurf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf;

	sImageEmbeddedf_BlackWhitef->nWidth = nDim_2pNf;
	sImageEmbeddedf_BlackWhitef->nLength = nDim_2pNf;

	sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr = new int[nImageSizeCurf];

	if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		fprintf(fout_lr, "\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = -1; //invalid

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Embedded_Image(...
  ///////////////////////////////////////////////////////////////////////

int Dim_2powerN(
	const int nLengthf,
	const int nWidthf,

	int &nScalef,
	int &nDim_2pNf)
{
	int
		i,
		nTempf,
		nLargerDimInitf;

	if (nLengthf >= nWidthf)
		nLargerDimInitf = nLengthf;
	else
		nLargerDimInitf = nWidthf;

	nTempf = 2;
	for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
	{
		nTempf = nTempf * 2;
		if (nTempf >= nLargerDimInitf)
		{
			nDim_2pNf = nTempf;
			break;
		}//if (nTempf >= nLargerDimInitf)
	}//for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
/////////////////////////////////

	if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max - 1)
	{
		printf("\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max - 1)
	else
	{
		nScalef = i;
		return SUCCESSFUL_RETURN;
	} //
}//int Dim_2powerN(...
/////////////////////////////////////////////////////////////////////////////

int Embedding_Image_Into_2powerN_ForHausdorff(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForHausdorff'");
	fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");
		printf("\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		printf("\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");

		fprintf(fout_lr, "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax)
					nIntensityAverf = nIntensityStatMax;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForHausdorff': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForHausdorff(...

//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int NumOfObjectSquaresAndLogPoint_WithResolution(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	int &nNumOfObjectSquaresTotf,
	float &fLogPointf)

{
	int
		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImageTotf, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	float
		fRatioOfSquaresf;
	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfObjectSquaresTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'NumOfObjectSquaresAndLogPoint_WithResolution'\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;

		if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
		{
			continue;
		}//if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)

		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength)
			{
				continue;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength)

			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				//for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				for (iLenf = nLenOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nNumOfObjectSquaresTotf += 1;

						goto MarkContinueForiLenSquares;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

		MarkContinueForiLenSquares: continue;
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	fRatioOfSquaresf = (float)(nNumOfObjectSquaresTotf) / (float)(nNumOfSquaresInImageTotf);

	if (nNumOfObjectSquaresTotf > 1)
	{
		//fLogPointf = log(nNumOfObjectSquaresTotf) / (-log(nLenOfSquaref));
		fLogPointf = (float)(log(nNumOfObjectSquaresTotf));
	}
	else
		fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);

	fprintf(fout_lr, "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int NumOfObjectSquaresAndLogPoint_WithResolution(...
////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity --https://aip.scitation.org/doi/full/10.1063/1.4966539
int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	int &nNumOfSquaresInImageTotf,

	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf
	int &nMassOfImageTotf, //<= 

	int nMassesOfSquaresArrf[]) //[nNumOfSquaresTotf]


{
	int
		nIndexOfSquareCurf,

		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nMassOfASquaref,
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfNonZero_ObjectSquaresTotf = 0;
	nMassOfImageTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	//nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,
	fprintf(fout_lr, "\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare' nDim_2pNf = %d, nNumOfSquaresInImageSidef = %d, nMassOfASquareMaxf = %d",
		nDim_2pNf, nNumOfSquaresInImageSidef, nMassOfASquareMaxf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nNumOfProcessedFiles_NormalTot_Glob == -1)
	{
		fprintf(fout_PrintFeatures, "\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare' nDim_2pNf = %d, nNumOfSquaresInImageSidef = %d, nMassOfASquareMaxf = %d",
			nDim_2pNf, nNumOfSquaresInImageSidef, nMassOfASquareMaxf);
		fprintf(fout_PrintFeatures, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
		{
			fprintf(fout_PrintFeatures, "\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
		}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)

	} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nIndexOfSquareCurf = iLenSquaresf + iWidSquaresf * nNumOfSquaresInImageSidef;
			if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
			{
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
				printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)

			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
			{
				nMassOfASquaref = 0;
				goto MarkContinueForiLenSquares;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)

			nMassOfASquaref = 0;
			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nLenOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
						printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
						printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nMassOfASquaref += 1;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nLenOfSquareMinf

			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

			nMassOfImageTotf += nMassOfASquaref;

		MarkContinueForiLenSquares: nMassesOfSquaresArrf[nIndexOfSquareCurf] = nMassOfASquaref;
			if (nMassOfASquaref > 0)
			{
				nNumOfNonZero_ObjectSquaresTotf += 1;
			}//if (nMassOfASquaref > 0)

			if (nMassOfASquaref > nMassOfASquareMaxf)
			{
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
				printf("\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
				printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
				fprintf(fout_lr, "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref > nMassOfASquareMaxf)
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	//if (nMassOfImageTotf <= 0)
	if (nMassOfImageTotf < 0)
	{
		printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf = %d < 0", nMassOfImageTotf);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nMassOfImageTotf = %d < 0", nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} // if (nMassOfImageTotf < 0)

	if (nMassOfImageTotf == 0)
	{
		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			printf("\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf == 0, returning (-2)");
			fprintf(fout_PrintFeatures, "\n\n  'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf == 0, returning (-2)");
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nMassOfImageTotf = %d <= 0", nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return (-2);
	} // if (nMassOfImageTotf == 0)

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		//nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	//printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);

	fprintf(fout_lr, "\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////////////////////////////////

//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
void SlopeOfAStraightLine(
	const int nDimf,

	const float fX_Arrf[],
	const float fY_Arrf[],

	float &fSlopef)
{
	int
		i;
	float
		fX_Diff,
		fX_Averf = 0.0,
		fY_Averf = 0.0,

		fSumForNumeratorf = 0.0,
		fSumForDenominatorf = 0.0;

	for (i = 0; i < nDimf; i++)
	{
		fX_Averf += fX_Arrf[i];
		fY_Averf += fY_Arrf[i];

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'SlopeOfAStraightLine' 1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine'   1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (i = 0; i < nDimf; i++)

	fX_Averf = fX_Averf / nDimf;
	fY_Averf = fY_Averf / nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	//	printf("\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);
	fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	for (i = 0; i < nDimf; i++)
	{
		fX_Diff = fX_Arrf[i] - fX_Averf;

		fSumForNumeratorf += fX_Diff * (fY_Arrf[i] - fY_Averf);

		fSumForDenominatorf += fX_Diff * fX_Diff;
#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (i = 0; i < nDimf; i++)

	if (fSumForDenominatorf > 0.0)
	{
		fSlopef = fSumForNumeratorf / fSumForDenominatorf;
	}//if (fSumForDenominatorf > 0.0)
	else
		fSlopef = fLarge;

}//void SlopeOfAStraightLine(...
  ////////////////////////////////////////////////////////////////////////

int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	// fQf = -10, -9, ...,9,10
	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,

float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf)
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf,
		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	float PowerOfAFloatNumber(
		const int nIntOrFloatf, //1 -- int, 0 -- float
		const int nPowerf,
		const float fPowerf,

		const float fFloatInitf); //fFloatInitf != 0.0
	int
		iSquaref,

		nPowerf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nNumOfSquaresInImageTotf,
		//nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf,
		nMassOfASquaref,

		nMassOfImageTotf,
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nResf;

	float
		fMassProportionForASquaref,
		fPowerf,

		fSumOfMomentsf = 0.0,

		fMomentOf_Q_OrderAtFixedLenCurf;
	/////////////////////////////
	if (nIntOrFloatf == 1)
	{
		nPowerf = (int)(fQf);
		fPowerf = -fLarge;
	} //if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		nPowerf = -nLarge;
		fPowerf = fQf;
	}//else if (nIntOrFloatf == 0)
	else
	{
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nIntOrFloatf = %d", nIntOrFloatf);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nIntOrFloatf = %d", nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//else

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)
//////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf,nIntOrFloatf);
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf, nIntOrFloatf);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	


////////////////////////////////////////////////////////////////
	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf,

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' by 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nResf == -2)
	{
		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			//printf("\n\n nResf == -2: zero masses in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' by 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
			fprintf(fout_PrintFeatures, "\n\n nResf == -2: zero masses in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' by 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare'");

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		delete[] nMassesOfSquaresArrf;
		return(-2);
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfNonZero_ObjectSquaresTotMin == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfNonZero_ObjectSquaresTotf = %d <= 1", nNumOfNonZero_ObjectSquaresTotf);

		fprintf(fout_lr, "\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfNonZero_ObjectSquaresTotf = %d <= 1", nNumOfNonZero_ObjectSquaresTotf);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)
//////////////////////////////////////////////
	//printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
		//nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
	//printf("\n  nMassOfImageTotf = %d", nMassOfImageTotf);

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
	fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);

		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	//printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' 1:");
	///////////////////////////////////////////////////////////////
	float *fMomentsOfSquaresArrf;
	fMomentsOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsOfSquaresArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsOfSquaresArrf == nullptr)

	float *fMomentsNormalizedOfSquaresArrf;
	fMomentsNormalizedOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsNormalizedOfSquaresArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsNormalizedOfSquaresArrf == nullptr)

/////////////////////////////////////////////////////////////	
	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': before the loop for iSquaref, nIntOrFloatf = %d, nPowerf = %d, fPowerf = %E",
		nIntOrFloatf, nPowerf, fPowerf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			fMassProportionForASquaref = (float)(nMassOfASquaref) / (float)(nMassOfImageTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, fMassProportionForASquaref = %E, nMassOfASquaref = %d, nMassOfImageTotf = %d",
				iSquaref, fMassProportionForASquaref, nMassOfASquaref, nMassOfImageTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)
			{
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");
				printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;

				delete[] fMomentsOfSquaresArrf;
				delete[] fMomentsNormalizedOfSquaresArrf;

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)

			fMomentOf_Q_OrderAtFixedLenCurf = PowerOfAFloatNumber(
				nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
				nPowerf, //const int nPowerf,
				fPowerf, //const float fPowerf,

				fMassProportionForASquaref); // const float fFloatInitf); //fFloatInitf != 0.0

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf += fMomentOf_Q_OrderAtFixedLenCurf;

			fMomentsOfSquaresArrf[iSquaref] = fMomentOf_Q_OrderAtFixedLenCurf; //to be normalized later

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': iSquaref = %d, fMomentOf_Q_OrderAtFixedLenCurf = %E, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
				iSquaref, fMomentOf_Q_OrderAtFixedLenCurf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			fprintf(fout_lr, "\n fMomentsOfSquaresArrf[%d] = %E, nPowerf = %d, fPowerf = %E",
				iSquaref, fMomentsOfSquaresArrf[iSquaref], nPowerf, fPowerf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			//if (fMomentsOfSquaresArrf[iSquaref] <= 0.0)
			if (fMomentsOfSquaresArrf[iSquaref] < 0.0)
			{
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);
				printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);

				//printf("\n\n Please press any key:"); getchar();
				printf("\n\n Please press any key to exit:"); fflush(fout_lr);  getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsOfSquaresArrf[iSquaref] < 0.0)

		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

	//printf("\n\n  'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' 2:");

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' (before normalization): nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, nNumOfSquaresInImageTotf = %d",
		nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, nNumOfSquaresInImageTotf);

	//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf <= fCloseToZeroLimitForSumOfMomentsOfSquares)
	{
		/*
		#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
					nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
					nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);

				printf("\n\n nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
					nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);

				fprintf(fout_lr, "\n\n nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
					nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);

		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
		*/

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n Exit in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E < fCloseToZeroLimitForSumOfMomentsOfSquares = %E",
			//fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fCloseToZeroLimitForSumOfMomentsOfSquares);

		fprintf(fout_lr, "\n\n  Exit in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E < fCloseToZeroLimitForSumOfMomentsOfSquares = %E",
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fCloseToZeroLimitForSumOfMomentsOfSquares);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		delete[] fMomentsNormalizedOfSquaresArrf;

		return (-2);
	}//if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf <= fCloseToZeroLimitForSumOfMomentsOfSquares)

	//printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' 3:");

///////////////////////////////////////////////////////////////
//for spectrum and crowding index

	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
	fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			if (fMomentsOfSquaresArrf[iSquaref] < fCloseToZeroLimitForMomentsOfSquares)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Continue for iSquaref = %d, fMomentsOfSquaresArrf[iSquaref] = %E < fCloseToZeroLimitForMomentsOfSquares = %E",
					iSquaref, fMomentsOfSquaresArrf[iSquaref], fCloseToZeroLimitForMomentsOfSquares);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				continue;
			} // if (fMomentsOfSquaresArrf[iSquaref] < fCloseToZeroLimitForMomentsOfSquares)
//normalization
			fMomentsNormalizedOfSquaresArrf[iSquaref] = fMomentsOfSquaresArrf[iSquaref] / fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Normalization: iSquaref = %d, fMomentsNormalizedOfSquaresArrf[iSquaref] = %E, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
				iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref], fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (fMomentsNormalizedOfSquaresArrf[iSquaref] <= 0.0)
				//if (fMomentsNormalizedOfSquaresArrf[iSquaref] < 0.0)
			{
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsNormalizedOfSquaresArrf[%d] = %E <= 0.0", iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref]);
				printf("\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsNormalizedOfSquaresArrf[%d] = %E", iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref]);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsNormalizedOfSquaresArrf[iSquaref] < 0.0)

			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref] * log(fMomentsNormalizedOfSquaresArrf[iSquaref]);

			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref] * log(fMomentsOfSquaresArrf[iSquaref]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': iSquaref = %d, fMomentsNormalizedOfSquaresArrf[iSquaref] = %E, fMomentsOfSquaresArrf[iSquaref] = %E",
				iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref], fMomentsOfSquaresArrf[iSquaref]);

			fprintf(fout_lr, "\n fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			//fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	printf("\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d",
		nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	fprintf(fout_lr, "\n\n The end of 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d",
		nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	//the end for spectrum and crowding index
	/////////////////////////////////

	delete[] nMassesOfSquaresArrf;
	delete[] fMomentsOfSquaresArrf;

	delete[] fMomentsNormalizedOfSquaresArrf;

	//printf("\n\n The end of 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");

	return SUCCESSFUL_RETURN;
} // int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(...
//////////////////////////////////////////////////////////////////////

int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	// fQf = -10, -9, ...,9,10
	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
float &fGenerDim_Order_Qf,
float &fSpectrum_Order_Qf,

float &fCrowdingIndex_Order_Qf)
{
	int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		//fMomentNormalizedf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoint_GenerDimf = -1.0,
		fLogPoint_Spectrumf = -1.0,
		fLogPoint_CrowdingIndexf = -1.0;

	///////////////////////////////////////
	nNumOfSpectrumFea_Glob += 1;

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

		fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	// fQf = -10, -9, ...,9,10
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_GenerDimOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)

///////////////////////
	fDiff = (float)(1.0) - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension
	} // if (fDiff < feps && fDiff > -feps)

	///////////////////////////////////////////
Mark_GenerDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	fSumOfMomentsf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
		nNumOfSpectrumFea_Glob, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nNumOfProcessedFiles_NormalTot_Glob == -1)
	{
		fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
			nNumOfSpectrumFea_Glob, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

		if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
		{
			fprintf(fout_PrintFeatures, "\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
		}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)

	} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			// fQf = -10, -9, ...,9,10
			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	//printf("\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order' (after 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'): nResf = %d", nResf);

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order' (after 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'): nResf = %d", nResf);
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		fMoments_Arrf[iScalef] = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fQf = %E, nNumOfLogPointsf = %d",
			nNumOfNonZero_ObjectSquaresTotf, fQf, nNumOfLogPointsf);

		fprintf(fout_lr, "\n fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fprintf(fout_lr, "\n  nNumOfSpectrumFea_Glob = %d, iScalef = %d, nLenOfSquaref = %d", nNumOfSpectrumFea_Glob, iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order' by 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
			//	fprintf(fout_lr, "\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

			printf("\n\n Please press any key:"); getchar();

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fQf = %E, nNumOfLogPointsf = %d",
				nNumOfNonZero_ObjectSquaresTotf, fQf, nNumOfLogPointsf);

			fprintf(fout_PrintFeatures, "\n fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
				fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			fprintf(fout_PrintFeatures, "\n  nNumOfSpectrumFea_Glob = %d, iScalef = %d, nLenOfSquaref = %d", nNumOfSpectrumFea_Glob, iScalef, nLenOfSquaref);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		if (nResf == -2)
		{
			//printf("\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order', nResf == -2: continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order', nResf == -2: continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nNumOfProcessedFiles_NormalTot_Glob == -1)
			{
				fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order', nResf == -2: continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
			} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

			continue;
		}//if (nResf == -2)

		fSumOfMomentsf += fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_GenerDimf = log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_GenerDimf = 0.0;
			}//else
/////////////////
			//	fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_Spectrumf = log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				if (nNumOfProcessedFiles_NormalTot_Glob == -1)
				{
					fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
						iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
				} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

			} //
			else
			{
				fLogPoint_Spectrumf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
					fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

				fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E", iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				if (nNumOfProcessedFiles_NormalTot_Glob == -1)
				{
					fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
						fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

					fprintf(fout_PrintFeatures, "\n iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E", iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
				} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

			}//else
///////////////////
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_CrowdingIndexf = log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_CrowdingIndexf = 0.0;
			}//else

		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
				printf("\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				if (nNumOfProcessedFiles_NormalTot_Glob == -1)
				{
					fprintf(fout_PrintFeatures, "\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)


				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPoint_GenerDimf = log((float)(nNumOfNonZero_ObjectSquaresTotf));

			fLogPoint_Spectrumf = fLogPoint_GenerDimf;
			fLogPoint_CrowdingIndexf = fLogPoint_GenerDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 1, iScalef = %d, nLenOfSquaref = %d, nNumOfNonZero_ObjectSquaresTotf = %d, fLogPoint_Spectrumf = %E",
				iScalef, nLenOfSquaref, nNumOfNonZero_ObjectSquaresTotf, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_GenerDimf = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf * log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_GenerDimf = 0.0;
			}//else
/////////////////
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_Spectrumf = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf * log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_1f == 1 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //
			else
			{
				fLogPoint_Spectrumf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf <= 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			}//else
////////////////////
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_CrowdingIndexf = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf * log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_CrowdingIndexf = 0.0;
			}//else

		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPoint_GenerDimf;

		fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fLogPoint_Spectrumf; ////fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;

		fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fLogPoint_CrowdingIndexf; ////fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n nNumOfLogPointsf = %d, fLogPoints_Spectrum_Arrf[%d]  = %E", nNumOfLogPointsf, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

			fprintf(fout_PrintFeatures, "\n nNumOfLogPointsf = %d, fLogPoints_Spectrum_Arrf[%d]  = %E", nNumOfLogPointsf, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf]);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
		nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	


	if (nNumOfProcessedFiles_NormalTot_Glob == -1)
	{
		fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
			nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
	} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

////////////////////////////////////////////////////////////////////////////////

	if (nNumOfLogPointsf <= 1)
	{
		fGenerDim_Order_Qf = 0.0; // -1.0;

		fSpectrum_Order_Qf = 0.0; // -1.0;
		fCrowdingIndex_Order_Qf = 0.0; // -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, no 'SlopeOfAStraightLine', fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E",
			nNumOfLogPointsf, fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n  The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, no 'SlopeOfAStraightLine', fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E",
				nNumOfLogPointsf, fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	}//if (nNumOfLogPointsf <= 1)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fGenerDim_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n  'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fGenerDim_Order_Qf, nNumOfLogPointsf = %d", nNumOfLogPointsf);
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

			fGenerDim_Order_Qf); // float &fSlopef);

		fDiff = (float)(1.0) - fQf; //above

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n  fGenerDim_Order_Qf = %E, fDiff = %E, fQf = %E, nNumOfLogPointsf = %d", fGenerDim_Order_Qf, fDiff, fQf, nNumOfLogPointsf);
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)


		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
		} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
//////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fSpectrum_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fSpectrum_Order_Qf, nNumOfLogPointsf = %d", nNumOfLogPointsf);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

			fSpectrum_Order_Qf); // float &fSlopef);

///////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fCrowdingIndex_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

			fCrowdingIndex_Order_Qf); // float &fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, fQf = %E, fDiff = %E",
			nNumOfLogPointsf, fQf, fDiff);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nScalef = %d, fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E",
			nDim_2pNf, nScalef, fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, fQf = %E, fDiff = %E",
				nNumOfLogPointsf, fQf, fDiff);

			fprintf(fout_PrintFeatures, "\n nDim_2pNf = %d, nScalef = %d, fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E",
				nDim_2pNf, nScalef, fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	} // else

#ifndef COMMENT_OUT_ALL_PRINTS	
	fprintf(fout_lr, "\n nNumOfSpectrumFea_Glob = %d", nNumOfSpectrumFea_Glob);
	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(...
//////////////////////////////////////////////

int GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	float fSpectrum_Of_All_Orders_Arrf[],
	float fCrowdingIndex_Of_All_Orders_Arrf[])
{
	int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf,
		float &fSpectrum_Order_Qf,

		float &fCrowdingIndex_Order_Qf);
	int
		nResf,
		iQf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fGenerDim_Order_Qf,
		fSpectrum_Order_Qf,
		fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1); // 20/20 == 1

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);

	fprintf(fout_lr, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfProcessedFiles_NormalTot_Glob == -1)
	{
		fprintf(fout_PrintFeatures, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
			fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
		fprintf(fout_PrintFeatures, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)
//////////////////
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;

		// fQ_Curf == -10, -9, ...,9,10
		fQ_Curf = (float)(nQs_Min)+(iQf * fWidthOfQ_Stepf);

		nResf = GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fGenerDim_Order_Qf, // float &fGenerDim_Order_Qf);
			fSpectrum_Order_Qf, //float &fSpectrum_Order_Qf,

			fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{

			printf("\n\n An error in 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange' by 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order'");
			//	fprintf(fout_lr, "\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

			printf("\n\n Please press any key:"); getchar();

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;
		fSpectrum_Of_All_Orders_Arrf[iQf] = fSpectrum_Order_Qf;
		fCrowdingIndex_Of_All_Orders_Arrf[iQf] = fCrowdingIndex_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf( "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange':  iQf = %d, fQ_Curf = %E", iQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': iQf = %d, nNumOf_Qs_Tot = %d, fQ_Curf = %E", iQf, nNumOf_Qs_Tot, fQ_Curf);

		fprintf(fout_lr, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E",
			iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nNumOfProcessedFiles_NormalTot_Glob == -1)
		{
			fprintf(fout_PrintFeatures, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': iQf = %d, nNumOf_Qs_Tot = %d, fQ_Curf = %E", iQf, nNumOf_Qs_Tot, fQ_Curf);

			fprintf(fout_PrintFeatures, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E",
				iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

			fprintf(fout_PrintFeatures, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	return SUCCESSFUL_RETURN;
}// GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(...
////////////////////////////////////////////////////////////////////////////

int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

	//const int nDim_2pNf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

{
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
		float fSpectrum_Of_All_Orders_Arrf[],
		float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 256/64 == 4
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		iIntensity_Intervalf,

		iQf,

		iIndexf,
		nTempf,

		//nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractalTot,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	float
		fGenerDim_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fSpectrum_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fCrowdingIndex_Of_All_Orders_Arrf[nNumOf_Qs_Tot];


	/////////////////////////////////////////////////
	//initialization
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractalTot; iIntensity_Intervalf++)
	{
		nTempf = iIntensity_Intervalf * nNumOf_Qs_Tot;
		for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
		{
			iIndexf = iQf + nTempf;

			if (iIndexf >= nProductf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (iIndexf >= nProductf)

			fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;

		}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	}//for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractalTot; iIntensity_Intervalf++)

//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
	fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				printf("\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

			nThresholdForIntensitiesMaxf = iIntensity_Interval_2f * nLenOfIntensityIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "\n\n 1: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges' by 'Embedding_Image_Into_2powerN_ForHausdorff', nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS	
			fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
				nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

			fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fGenerDim_Of_All_Orders_Arrf, // float fGenerDim_Of_All_Orders_Arrf[]); //[nNumOf_Qs_Tot]

				fSpectrum_Of_All_Orders_Arrf, //float fSpectrum_Of_All_Orders_Arrf[],
				fCrowdingIndex_Of_All_Orders_Arrf); // float fCrowdingIndex_Of_All_Orders_Arrf[])

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges' by 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange'");
				printf("\n\n nScalef = %d, nDim_2pNf = %d", nScalef, nDim_2pNf);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges' by 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
			for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
			{
				iIndexf = iQf + nTempf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iQf = %d, iIndexf = %d, nTempf = %d", iQf, iIndexf, nTempf);
				fprintf(fout_lr, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E",
					iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				if (iIndexf >= nNumOfFeasForMultifractalTot)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

					delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (iIndexf >= nNumOfFeasForMultifractalTot)

				fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fGenerDim_Of_All_Orders_Arrf[iQf];
				fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fSpectrum_Of_All_Orders_Arrf[iQf];
				fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fCrowdingIndex_Of_All_Orders_Arrf[iQf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n iIndexf = %d, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E",
					iIndexf, iIndexf, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);

				fprintf(fout_lr, "\n fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
				fprintf(fout_lr, "\n fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nNumOfProcessedFiles_NormalTot_Glob == -1)
				{
					printf("\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfProcessedFiles_NormalTot_Glob = %d",
						nNumOfProcessedFiles_NormalTot_Glob);

					printf("\n iQf = %d, iIntensity_Interval_2f = %d, iIntensity_Interval_1f = %d, nNumOfIntensity_IntervalsForMultifractal = %d",
						iQf, iIntensity_Interval_2f, iIntensity_Interval_1f, nNumOfIntensity_IntervalsForMultifractal);

					printf("\n\n iIndexf = %d, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E",
						iIndexf, iIndexf, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);

					printf("\n fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
					printf("\n fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
					printf("\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);


					fprintf(fout_PrintFeatures, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfProcessedFiles_NormalTot_Glob = %d",
						nNumOfProcessedFiles_NormalTot_Glob);

					fprintf(fout_PrintFeatures, "\n iQf = %d, iIntensity_Interval_2f = %d, iIntensity_Interval_1f = %d, nNumOfIntensity_IntervalsForMultifractal = %d",
						iQf, iIntensity_Interval_2f, iIntensity_Interval_1f, nNumOfIntensity_IntervalsForMultifractal);

					fprintf(fout_PrintFeatures, "\n\n iIndexf = %d, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E",
						iIndexf, iIndexf, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);

					fprintf(fout_PrintFeatures, "\n fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
					fprintf(fout_PrintFeatures, "\n fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf, fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
					fprintf(fout_PrintFeatures, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

					fflush(fout_PrintFeatures);

				} // if (nNumOfProcessedFiles_NormalTot_Glob == -1)

			}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	printf("\n\n The end of 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges' ");

	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
	return SUCCESSFUL_RETURN;
}//int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(...


//////////////////////////////////////////////////////////////
float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;

	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
		{
			return 0.0; //instead of 1.0
			//return SUCCESSFUL_RETURN; -- should return a float number
		} //
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(
//////////////////////////////////////////////////////////

int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrGenerDimf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrGenerDimf);
	//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	//float fSpectrum_Of_All_Orders_Arrf[],
	//float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		nQf,
		//iIndexf,
		nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf / nNumOf_Qs_Tot,

		nIntensitiesFoundf = 0, //not yet

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	//////////////////////////////////////////////////////////////

	if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	

		fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

				if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':  nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);

					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
					//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

				goto MarkExitOf_2LoopsInOneFea_GenerDim;
			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

/*				nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
				for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
				{
					iIndexf = iQf + nTempf;
				} //
*/
		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':  the intensities have not been found");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': the intensities have not been found");

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

/////////////////////////////////////////////////
//initialization
MarkExitOf_2LoopsInOneFea_GenerDim:		nResf = Dim_2powerN(
	sColor_ImageInitf->nLength, //const int nLengthf,
	sColor_ImageInitf->nWidth, //const int nWidthf,

	nScalef, //int &nScalef,

	nDim_2pNf); // int &nDim_2pNf);

										if (nResf == UNSUCCESSFUL_RETURN)
										{
											//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
										printf("\n\n 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
											nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
										fprintf(fout_lr, "\n\n 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
											nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

										fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

										if (nScalef <= 1)
										{
#ifndef COMMENT_OUT_ALL_PRINTS
											fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

											printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
											printf("\n\n Please press any key:"); getchar();
											//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nScalef <= 1)
										//////////////////////////////////////////////
										EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

										nResf = Initializing_Embedded_Image(
											nDim_2pNf, //const int nDim_2pNf,

											&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

										if (nResf == UNSUCCESSFUL_RETURN)
										{
											delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nResf == UNSUCCESSFUL_RETURN)

										///////////////////////////////////////
										nNumOfIntensity_IntervalsForMultifractalTotf = 0;
										//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
										{
											nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

											iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
											//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
											{
												iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

												nNumOfIntensity_IntervalsForMultifractalTotf += 1;

												if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
												{
#ifndef COMMENT_OUT_ALL_PRINTS

													fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
														nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

													fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
													printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
														nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

													printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
													printf("\n\n Please press any key:"); getchar();

													delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

												nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

												nResf = Embedding_Image_Into_2powerN_ForHausdorff(
													nDim_2pNf, //const int nDim_2pNf,

													nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
													nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

													sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

													sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
													&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

												if (nResf == UNSUCCESSFUL_RETURN)
												{
#ifndef COMMENT_OUT_ALL_PRINTS
													fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
													printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
													printf("\n\n Please press any key:"); getchar();

													delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												}//if (nResf == UNSUCCESSFUL_RETURN)

												nResf = OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

													nQf,//const int nQf,
													nDim_2pNf, //const int nDim_2pNf,
													nScalef, //const int nScalef,

													sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

													&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

													fOneFea_FrGenerDimf); // float &fOneFea_FrGenerDimf);

											} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

										}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

										delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
										return SUCCESSFUL_RETURN;
}//int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
///////////////////////////////////////////////////////////////////////////

int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrSpectrumf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

//	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(
	int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrSpectrumf);
	//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	//float fSpectrum_Of_All_Orders_Arrf[],
	//float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		nQf,
		//iIndexf,
		nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf / nNumOf_Qs_Tot,

		nIntensitiesFoundf = 0, //not yet

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	//EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
//////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	

	fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d, nIndexOfIntensitiesCurf = %d",
		nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot, nIndexOfIntensitiesCurf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	nNumOfOneFea_Spectrum_AdjustedGlob = nNumOfOneFea_Adjustedf;

	if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	

		fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

#ifndef COMMENT_OUT_ALL_PRINTS	

				fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...':  nQf = %d, nNumOf_Qs_Tot = %d, nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d, nNumOfOneFea_Adjustedf = %d",
					nQf, nNumOf_Qs_Tot, nIntensity_Interval_1f, nIntensity_Interval_2f, nNumOfOneFea_Adjustedf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);

					fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...':  nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

				goto MarkExitOf_2LoopsInOneFea_Spectrum;

			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

/*				nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
				for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
				{
					iIndexf = iQf + nTempf;
				} //
*/
		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': the intensities have not been found");

		fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...':  the intensities have not been found");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

/////////////////////////////////////////////////
//initialization
MarkExitOf_2LoopsInOneFea_Spectrum:		nResf = Dim_2powerN(
	sColor_ImageInitf->nLength, //const int nLengthf,
	sColor_ImageInitf->nWidth, //const int nWidthf,

	nScalef, //int &nScalef,

	nDim_2pNf); // int &nDim_2pNf);

										if (nResf == UNSUCCESSFUL_RETURN)
										{
											//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
										printf("\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
											nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
										fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
											nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

										fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

										fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

										if (nScalef <= 1)
										{
#ifndef COMMENT_OUT_ALL_PRINTS
											printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
											fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
											printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
											///	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nScalef <= 1)
										//////////////////////////////////////////////
										EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

										nResf = Initializing_Embedded_Image(
											nDim_2pNf, //const int nDim_2pNf,

											&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

										if (nResf == UNSUCCESSFUL_RETURN)
										{
											//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
											return UNSUCCESSFUL_RETURN;
										}//if (nResf == UNSUCCESSFUL_RETURN)

										///////////////////////////////////////
										nNumOfIntensity_IntervalsForMultifractalTotf = 0;
										//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
										{
											nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

											iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
											//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
											{
												iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

												nNumOfIntensity_IntervalsForMultifractalTotf += 1;

												if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
												{
#ifndef COMMENT_OUT_ALL_PRINTS
													printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
														nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

													printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

													fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
														nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

													fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
													printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

													delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

												nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

												nResf = Embedding_Image_Into_2powerN_ForHausdorff(
													nDim_2pNf, //const int nDim_2pNf,

													nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
													nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

													sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

													sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
													&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

												if (nResf == UNSUCCESSFUL_RETURN)
												{
#ifndef COMMENT_OUT_ALL_PRINTS
													printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);

													fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
													printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

													delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												}//if (nResf == UNSUCCESSFUL_RETURN)

												nResf = OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

													nQf,//const int nQf,
													nDim_2pNf, //const int nDim_2pNf,
													nScalef, //const int nScalef,

													sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

													&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

													fOneFea_FrSpectrumf); // float &fOneFea_FrSpectrumf);

											} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

										}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

										delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
										return SUCCESSFUL_RETURN;
}//int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
///////////////////////////////////////////////////////////////////////////

int OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrCrowdingIndexf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

//	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(
//	int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(
	int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrCrowdingIndexf);
	//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	//float fSpectrum_Of_All_Orders_Arrf[],
	//float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		nQf,
		//iIndexf,
		nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf / nNumOf_Qs_Tot,

		nIntensitiesFoundf = 0, //not yet

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	//////////////////////////////////////////////////////////////

	if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

				if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);

					fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':  nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

				goto MarkExitOf_2LoopsInOneFea_CrowdingIndex;
			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': the intensities have not been found");

		fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':  the intensities have not been found");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

/////////////////////////////////////////////////
//initialization
MarkExitOf_2LoopsInOneFea_CrowdingIndex:		nResf = Dim_2powerN(
	sColor_ImageInitf->nLength, //const int nLengthf,
	sColor_ImageInitf->nWidth, //const int nWidthf,

	nScalef, //int &nScalef,

	nDim_2pNf); // int &nDim_2pNf);

												if (nResf == UNSUCCESSFUL_RETURN)
												{
													//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
												printf("\n\n 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
													nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
												fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
													nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

												fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

												if (nScalef <= 1)
												{
#ifndef COMMENT_OUT_ALL_PRINTS
													printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
													fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
													printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
													//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												}//if (nScalef <= 1)
												//////////////////////////////////////////////
												EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

												nResf = Initializing_Embedded_Image(
													nDim_2pNf, //const int nDim_2pNf,

													&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

												if (nResf == UNSUCCESSFUL_RETURN)
												{
													delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
													return UNSUCCESSFUL_RETURN;
												}//if (nResf == UNSUCCESSFUL_RETURN)

												///////////////////////////////////////
												nNumOfIntensity_IntervalsForMultifractalTotf = 0;
												//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
												{
													nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

													iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
													//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
													{
														iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

														nNumOfIntensity_IntervalsForMultifractalTotf += 1;

														if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
														{
#ifndef COMMENT_OUT_ALL_PRINTS
															printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
																nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

															printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

															fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
																nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

															fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
															printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
															delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
															return UNSUCCESSFUL_RETURN;
														} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

														nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

														nResf = Embedding_Image_Into_2powerN_ForHausdorff(
															nDim_2pNf, //const int nDim_2pNf,

															nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
															nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

															sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

															sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
															&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

														if (nResf == UNSUCCESSFUL_RETURN)
														{
#ifndef COMMENT_OUT_ALL_PRINTS
															printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);

															fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
															printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

															delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
															return UNSUCCESSFUL_RETURN;
														}//if (nResf == UNSUCCESSFUL_RETURN)

														nResf = OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

															nQf,//const int nQf,
															nDim_2pNf, //const int nDim_2pNf,
															nScalef, //const int nScalef,

															sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

															&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

															fOneFea_FrCrowdingIndexf); // float &fOneFea_FrCrowdingIndexf);

													} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

												}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

												delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
												return SUCCESSFUL_RETURN;
} // OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(...
////////////////////////////////////////////////////////////////////

int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

	const int nQf,
	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float &fOneFea_FrGenerDimf)
{
	int OneFea_GenerDimOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf);
	//float &fSpectrum_Order_Qf,
	//float &fCrowdingIndex_Order_Qf);
	int
		nResf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fGenerDim_Order_Qf;
	//fSpectrum_Order_Qf,
	//fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	//////////////////
	//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
		fQ_Curf = (float)(nQs_Min)+(nQf * fWidthOfQ_Stepf);

		nResf = OneFea_GenerDimOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fGenerDim_Order_Qf); // float &fGenerDim_Order_Qf);
		//	fSpectrum_Order_Qf, //float &fSpectrum_Order_Qf,
		//	fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fGenerDim_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fGenerDim_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fGenerDim_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fGenerDim_Order_Qf, nQf, fQ_Curf);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	fOneFea_FrGenerDimf = fGenerDim_Order_Qf;
	return SUCCESSFUL_RETURN;
} //int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(...
////////////////////////////////////////////////////////////////////

int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

	const int nQf,
	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float &fOneFea_FrSpectrumf)
{
	//	int OneFea_GenerDimOf_Q_order(
	int OneFea_SpectrumOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fSpectrum_Order_Qf);
	//float &fCrowdingIndex_Order_Qf);

	int
		nResf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fSpectrum_Order_Qf;

	//fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

	//////////////////
	//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
		fQ_Curf = (float)(nQs_Min)+(nQf * fWidthOfQ_Stepf);

#ifndef COMMENT_OUT_ALL_PRINTS

		//nNumOfOneFea_Spectrum_AdjustedGlob
		fprintf(fout_lr, "\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange': nNumOfOneFea_Spectrum_AdjustedGlob = %d, nQf = %d, nDim_2pNf = %d, nScalef = %d",
			nNumOfOneFea_Spectrum_AdjustedGlob, nQf, nDim_2pNf, nScalef);

		fprintf(fout_lr, "\n fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, fQ_Curf = %E",
			fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, fQ_Curf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//nResf = OneFea_GenerDimOf_Q_order(
		nResf = OneFea_SpectrumOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fSpectrum_Order_Qf); // float &fSpectrum_Order_Qf);
	//	fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange': fSpectrum_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fSpectrum_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange':fSpectrum_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fSpectrum_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	fOneFea_FrSpectrumf = fSpectrum_Order_Qf;
	return SUCCESSFUL_RETURN;
} //int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(...
///////////////////////////////////////////////////////////////

//OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange
int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

	const int nQf,
	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float &fOneFea_FrCrowdingIndexf)
{
	//	int OneFea_GenerDimOf_Q_order(
	int OneFea_CrowdingIndexOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fCrowdingIndex_Order_Qf);

	int
		nResf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	//////////////////
	//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
		fQ_Curf = (float)(nQs_Min)+(nQf * fWidthOfQ_Stepf);

		//nResf = OneFea_GenerDimOf_Q_order(
		nResf = OneFea_CrowdingIndexOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fCrowdingIndex_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fCrowdingIndex_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange':fCrowdingIndex_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fCrowdingIndex_Order_Qf, nQf, fQ_Curf);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	fOneFea_FrCrowdingIndexf = fCrowdingIndex_Order_Qf;
	return SUCCESSFUL_RETURN;
} //int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(...
///////////////////////////////////////////////////////

int OneFea_GenerDimOf_Q_order(

	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
float &fGenerDim_Order_Qf)
//float &fSpectrum_Order_Qf,
//float &fCrowdingIndex_Order_Qf)
{
	int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		// fQf = -10, -9, ...,9,10
		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
	/*
	//an option to make it more specific
			int OneFea_GenerDim_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				const int nDim_2pNf,

				const int nLenOfSquaref,

				const COLOR_IMAGE *sColor_ImageInitf,

				const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				const float fQf,
				const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				float &fSumOfMomentOf_Q_OrderAtFixedLenf);
	*/
	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],

		//fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		//fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPointf;

	///////////////////////////////////////

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

		//fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		//fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_OneFea_GenerDimOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)
///////////////////////
	fDiff = (float)(1.0) - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension
	} // if (fDiff < feps && fDiff > -feps)

	///////////////////////////////////////////
Mark_OneFea_GenerDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
		iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			// fQf = -10, -9, ...,9,10
			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fMoments_Arrf[iScalef] = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d",
			nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf);

		fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			continue;
		}//if (nResf == -2)

		fSumOfMomentsf += fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));
		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf * log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

		//fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
		//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_GenerDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
		nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	////////////////////////////////////////////////////////////////////////////////
	if (nNumOfLogPointsf <= 1)
	{
		//fGenerDim_Order_Qf = -1.0;
		fGenerDim_Order_Qf = 0.0;

		//fSpectrum_Order_Qf = -1.0;
		//fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d is insufficent, fGenerDim_Order_Qf = %E", nNumOfLogPointsf, fGenerDim_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//if (nNumOfLogPointsf <= 1)
	else
	{
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

			fGenerDim_Order_Qf); // float &fSlopef);

		fDiff = (float)(1.0) - fQf; //above

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
		} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
//////////////
/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

				fSpectrum_Order_Qf); // float &fSlopef);
	///////////////////////////
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

				fCrowdingIndex_Order_Qf); // float &fSlopef);
*/
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d, fGenerDim_Order_Qf = %E, fQf = %E, fDiff = %E",
			nNumOfLogPointsf, fGenerDim_Order_Qf, fQf, fDiff);

		//	fprintf(fout_lr, "\n fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	} // else
////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int OneFea_GenerDimOf_Q_order(...
//////////////////////////////////////////////////////////////////////////

int OneFea_SpectrumOf_Q_order(

	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
float &fSpectrum_Order_Qf)

//float &fCrowdingIndex_Order_Qf)
{

	int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
	/*
	//an option to make it more specific
			int OneFea_GenerDim_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				const int nDim_2pNf,

				const int nLenOfSquaref,

				const COLOR_IMAGE *sColor_ImageInitf,

				const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				const float fQf,
				const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				float &fSumOfMomentOf_Q_OrderAtFixedLenf);
	*/
	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		//fSumOfMomentOf_Q_OrderAtFixedLenf,
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		//fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		//fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPointf;

	///////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d,  nDim_2pNf = %d, nScalef = %d, fQf = %E, nIntOrFloatf = %d",
		nNumOfOneFea_Spectrum_AdjustedGlob, nDim_2pNf, nScalef, fQf, nIntOrFloatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		//fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

		fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		//fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_SpectrumDimOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)
///////////////////////
	fDiff = (float)(1.0) - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension
	} // if (fDiff < feps && fDiff > -feps)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': fDiff = %E, nIsfQ_0f = %d, nIsfQ_1f = %d", fDiff, nIsfQ_0f, nIsfQ_1f);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	///////////////////////////////////////////
Mark_SpectrumDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, nIsfQ_0f = %d, nIsfQ_1f = %d",
		nNumOfOneFea_Spectrum_AdjustedGlob, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, nIsfQ_0f, nIsfQ_1f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentOf_Q_OrderAtFixedLenf);
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fMoments_Arrf[iScalef] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
		fSumOfMomentsf += fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d",
			nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf);

		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		fprintf(fout_lr, "\n iScalef = %d, nScalef = %d, nLenOfSquaref = %d, fMoments_Arrf[%d] = %E", iScalef, nScalef, nLenOfSquaref, iScalef, fMoments_Arrf[iScalef]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			continue;
		}//if (nResf == -2)

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nIsfQ_0f == 0 && nIsfQ_1f == 0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E > 0.0, fLogPointf = %E",
					iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fLogPointf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //
			else
			{
				fLogPointf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nIsfQ_0f == 0 && nIsfQ_1f == 0, fLogPointf = 0.0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
					iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			}//else
		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n nIsfQ_0f == 1, iScalef = %d, nLenOfSquaref = %d, fLogPointf = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
				iScalef, nLenOfSquaref, fLogPointf, nNumOfNonZero_ObjectSquaresTotf);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf * log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nIsfQ_1f == 1, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E > 0.0, fLogPointf = %E",
					iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fLogPointf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //
			else
			{
				fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nIsfQ_1f == 1, fLogPointf = 0.0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
					iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			}//else
		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		//fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

		fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fLogPointf; // fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
		//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_SpectrumOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Spectrum_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Spectrum_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E, nNumOfLogPointsf = %d",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf], nNumOfLogPointsf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
		nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	////////////////////////////////////////////////////////////////////////////////
	if (nNumOfLogPointsf <= 1)
	{
		//fGenerDim_Order_Qf = -1.0;

		//fSpectrum_Order_Qf = -1.0;
		fSpectrum_Order_Qf = 0.0;
		//fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_SpectrumOf_Q_order': nNumOfLogPointsf = %d is insufficient, fSpectrum_Order_Qf = %E", nNumOfLogPointsf, fSpectrum_Order_Qf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//if (nNumOfLogPointsf <= 1)
	else
	{
		/*
					SlopeOfAStraightLine(
						nNumOfLogPointsf, //const int nDimf,

						fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
						fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

						fGenerDim_Order_Qf); // float &fSlopef);

					fDiff = 1.0 - fQf; //above

					if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
					{
						fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
					} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		*/
		//////////////

		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

			fSpectrum_Order_Qf); // float &fSlopef);
/*
	///////////////////////////
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

				fCrowdingIndex_Order_Qf); // float &fSlopef);
*/
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfLogPointsf = %d, fSpectrum_Order_Qf = %E, fQf = %E",
			nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfLogPointsf, fSpectrum_Order_Qf, fQf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	} // else
////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int OneFea_SpectrumOf_Q_order(...
//////////////////////////////////////////////////////////////////////

int OneFea_CrowdingIndexOf_Q_order(

	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
float &fCrowdingIndex_Order_Qf)
{

	int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		// fQf = -10, -9, ...,9,10
		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
	/*
	//an option to make it more specific
			int OneFea_CrowdingIndex_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				const int nDim_2pNf,

				const int nLenOfSquaref,

				const COLOR_IMAGE *sColor_ImageInitf,

				const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				const float fQf,
				const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				float &fSumOfMomentOf_Q_OrderAtFixedLenf);
	*/
	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		//fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],
		//fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPointf;

	///////////////////////////////////////

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		//fLogPoints_GenerDim_Arrf[iIterf] = -1.0;
		//fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_CrowdingIndexOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)
///////////////////////
	fDiff = (float)(1.0) - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': fDiff = %E, nIsfQ_0f = %d, nIsfQ_1f = %d", fDiff, nIsfQ_0f, nIsfQ_1f);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // if (fDiff < feps && fDiff > -feps)

	///////////////////////////////////////////
Mark_CrowdingIndexOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
		iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			// fQf = -10, -9, ...,9,10
			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fMoments_Arrf[iScalef] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;
		fSumOfMomentsf += fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d, fSumOfMomentsf = %E",
			nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf, fSumOfMomentsf);

		fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			continue;
		}//if (nResf == -2)

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_CrowdingIndxOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndxOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));

		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf * log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		//fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

		//fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
		//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf; //?
		fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fLogPointf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_CrowdingIndxOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_CrowdingIndex_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_CrowdingIndex_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
		nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	////////////////////////////////////////////////////////////////////////////////
	if (nNumOfLogPointsf <= 1)
	{
		//fGenerDim_Order_Qf = -1.0;

		//fSpectrum_Order_Qf = -1.0;
		//fCrowdingIndex_Order_Qf = -1.0;
		//fSpectrum_Order_Qf = -1.0;
		fCrowdingIndex_Order_Qf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d is insufficient, fCrowdingIndex_Order_Qf = %E", nNumOfLogPointsf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//if (nNumOfLogPointsf <= 1)
	else
	{
		/*
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

			fGenerDim_Order_Qf); // float &fSlopef);

		fDiff = 1.0 - fQf; //above

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
		} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		*/
		//////////////
/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

				fSpectrum_Order_Qf); // float &fSlopef);
*/

///////////////////////////
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

			fCrowdingIndex_Order_Qf); // float &fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d, fCrowdingIndex_Order_Qf = %E, fQf = %E",
			nNumOfLogPointsf, fCrowdingIndex_Order_Qf, fQf);

		//	fprintf(fout_lr, "\n fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	} // else
////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int  int OneFea_CrowdingIndxOf_Q_order((...
//////////////////////////////////////////////////////

int SetOfLogPoints_ForFractal_Dim(
	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	int &nNumOfLogPointsf,

	float fNegLogOfLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max_La]
	float fLogPoints_Arrf[]) // [nNumOfIters_ForDim_2powerN_Max_La]
{
	int Dim_2powerN(
		const int nLengthf,
		const int nWidthf,

		int &nScalef,

		int &nDim_2pNf);

	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int NumOfObjectSquaresAndLogPoint_WithResolution(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfObjectSquaresTotf,
		float &fLogPointf);

	int
		nResf,
		iScalef,
		nScalef = 1,

		nNumOfObjectSquaresTotf,

		nDim_2pNf,
		nLenOfSquaref;

	float
		fLogPointf;

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Dim_2powerN'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Dim_2powerN'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//	delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'SetOfLogPoints_ForFractal_Dim': nScalef = %d, nDim_2pNf = %d", nScalef, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim': nScalef = %d, nDim_2pNf = %d", nScalef, nDim_2pNf);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
		printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim': nScalef = %d", nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//	delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Initializing_Embedded_Image'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Initializing_Embedded_Image'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n 5"); getchar();

	nResf = Embedding_Image_Into_2powerN_ForHausdorff(
		nDim_2pNf, //const int nDim_2pNf,

		nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

		sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Embedding_Image_Into_2powerN_ForHausdorff'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'Embedding_Image_Into_2powerN_ForHausdorff'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////////////

	//printf("\n 6"); getchar();

	nNumOfLogPointsf = 0; // nScalef;
	nLenOfSquaref = nDim_2pNf;

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = NumOfObjectSquaresAndLogPoint_WithResolution(
			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			nNumOfObjectSquaresTotf, //int &nNumOfObjectSquaresTotf,
			fLogPointf); // float &fLogPointf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'NumOfObjectSquaresAndLogPoint_WithResolution'");
			printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'SetOfLogPoints_ForFractal_Dim' by 'NumOfObjectSquaresAndLogPoint_WithResolution'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		nNumOfLogPointsf += 1;

		fNegLogOfLenOfSquare_Arrf[iScalef] = -log((float)(nLenOfSquaref));
		fLogPoints_Arrf[iScalef] = fLogPointf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SetOfLogPoints_ForFractal_Dim': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Arrf[%d] = %E", iScalef, nLenOfSquaref, iScalef, fLogPoints_Arrf[iScalef]);
		fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Arrf[%d] = %E", iScalef, nLenOfSquaref, iScalef, fLogPoints_Arrf[iScalef]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (iScalef = 0; iScalef < nScalef; iScalef++)

	delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

	return SUCCESSFUL_RETURN;
}//int SetOfLogPoints_ForFractal_Dim(..
//////////////////////////////////////////////////////

int Embedding_Image_Into_2powerN_ForLacunarity(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf, //>=
	const int nThresholdForIntensitiesMaxf, // <

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d, nDim_2pNf = %d, sColor_ImageInitf->nWidth = %d",
		sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth); //

	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d, nDim_2pNf = %d, sColor_ImageInitf->nWidth = %d",
		sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth); //

	fflush(fout_lr);// getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': the color weights are wrong");

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': the color weights are wrong");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else


	//printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity' 1"); getchar();

	if (sColor_ImageInitf->nLength > nDim_2pNf || sColor_ImageInitf->nWidth > nDim_2pNf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d > nDim_2pNf = %d || sColor_ImageInitf->nWidth = %d > nDim_2pNf",
			sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForLacunarity': sColor_ImageInitf->nLength = %d > nDim_2pNf = %d || sColor_ImageInitf->nWidth = %d > nDim_2pNf",
			sColor_ImageInitf->nLength, nDim_2pNf, sColor_ImageInitf->nWidth);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;

	}//if (sColor_ImageInitf->nLength > nDim_2pNf || sColor_ImageInitf->nWidth > nDim_2pNf)

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity': iWidf = %d", iWidf);
		fprintf(fout_lr, "\n 'Embedding_Image_Into_2powerN_ForLacunarity': iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax_La)
					nIntensityAverf = nIntensityStatMax_La;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf < nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf < nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		///	fprintf(fout_lr, "\n iWidf = %d, iLenf = %d, sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[%d] = %d",
			//	iWidf, iLenf, nIndexOfPixelInEmbeddedImagef,sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef]);
			//fflush(fout_lr);

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)

	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	//printf("\n 'Embedding_Image_Into_2powerN_ForLacunarity' 2"); getchar();

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'Embedding_Image_Into_2powerN_ForLacunarity': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForLacunarity': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
	//printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForLacunarity(...
///////////////////////////////////////////////////////////////////////////////

int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

	const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	//const int nNumOfSquaresTotf,
///////////////////////////////////////

float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]) //[nNumOfSquareOccurrence_Intervals_Laf]
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf, //<= 

		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	int
		iSquareOccurrenceIntervalf,
		iSquaref,

		//		iWidSquaresf,
		//		iLenSquaresf,
		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,
		nMassOfImageTotf,

		nNumOfSquaresInImageTotf,
		nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf,
		nMassOfASquaref,

		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nMassPerIntervalf,
		nIntervalOffSquareOccurrencef,

		nResf;
	/////////////////////////////

	nMassPerIntervalf = nMassOfASquareMaxf / nNumOfSquareOccurrence_Intervals_Laf;

	if (nMassPerIntervalf == 0)
	{
		//fprintf(fout_PrintFeatures, "\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquareMaxf = %d < nNumOfSquareOccurrence_Intervals_Laf = %d", 
			//nMassOfASquareMaxf, nNumOfSquareOccurrence_Intervals_Laf);
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquareMaxf = %d < nNumOfSquareOccurrence_Intervals_Laf = %d",
			nMassOfASquareMaxf, nNumOfSquareOccurrence_Intervals_Laf);

		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return (-2);
	} //if (nMassPerIntervalf == 0)

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
		printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		fprintf(fout_lr, "\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)

	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf, //<= 

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' by 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		printf("\n\n Please press any key:"); getchar();

		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin_La) //nNumOfSquareOccurrence_Intervals_Laf)
	{

		//fprintf(fout_PrintFeatures, "\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1", nNumOfNonZero_ObjectSquaresTotf);
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1", nNumOfNonZero_ObjectSquaresTotf);

		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin_La) //nNumOfSquareOccurrence_Intervals_Laf)

	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)
	{
		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = 0.0;
	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'ProbOfMasses...': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d, nMassPerIntervalf = %d",
			//nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf, nMassPerIntervalf);

	fprintf(fout_PrintFeatures, "\n\n 'ProbOfMasses...': nDim_2pNf = %d, nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d, nMassPerIntervalf = %d",
		nDim_2pNf, nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf, nMassPerIntervalf);
	fflush(fout_PrintFeatures);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{
		printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ProbOfMasses...': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	///////////////////////////////////////////////////////////////
	nNumOfNonZero_ObjectSquaresCurf = 0;
	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			nIntervalOffSquareOccurrencef = nMassOfASquaref / nMassPerIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, nNumOfSquaresInImageTotf = %d, nMassOfASquaref = %d, nIntervalOffSquareOccurrencef = %d, nMassPerIntervalf = %d",
				iSquaref, nNumOfSquaresInImageTotf, nMassOfASquaref, nIntervalOffSquareOccurrencef, nMassPerIntervalf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef > nNumOfSquareOccurrence_Intervals_Laf)
			{
				printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIntervalOffSquareOccurrencef = %d > nNumOfSquareOccurrence_Intervals_Laf = %d, nLenOfSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervals_Laf, nLenOfSquaref);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'ProbOfMasses...': nIntervalOffSquareOccurrencef = %d > nNumOfSquareOccurrence_Intervals_Laf = %d, nLenOfSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervals_Laf, nLenOfSquaref);
				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef > nNumOfSquareOccurrence_Intervals_Laf)

			if (nIntervalOffSquareOccurrencef == nNumOfSquareOccurrence_Intervals_Laf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'ProbOfMasses...': nIntervalOffSquareOccurrencef = %d == nNumOfSquareOccurrence_Intervals_Laf = %d, iSquaref = %d",
					nIntervalOffSquareOccurrencef, nNumOfSquareOccurrence_Intervals_Laf, iSquaref);

				//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nNumOfSquareOccurrence_Intervals_Laf - 1] += 1.0;
				continue;
			}//if (nIntervalOffSquareOccurrencef < 0 || nIntervalOffSquareOccurrencef >= nNumOfSquareOccurrence_Intervals_Laf)

			fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nIntervalOffSquareOccurrencef] += 1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_PrintFeatures, "\n iSquaref = %d, nNumOfSquaresInImageTotf = %d, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nLenOfSquaref = %d",
				iSquaref, nNumOfSquaresInImageTotf, nIntervalOffSquareOccurrencef, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[nIntervalOffSquareOccurrencef], nLenOfSquaref);
			fflush(fout_PrintFeatures);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

/////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)
	{
		fprintf(fout_lr, "\n\n Before normalizing: fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nLenOfSquaref = %d, nNumOfSquareOccurrence_Intervals_Laf = %d, nNumOfNonZero_ObjectSquaresTotf = %d",
			iSquareOccurrenceIntervalf, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nLenOfSquaref, nNumOfSquareOccurrence_Intervals_Laf, nNumOfNonZero_ObjectSquaresTotf);
		fflush(fout_lr);
	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	


	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)
	{
		//fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] / (float)(nNumOfSquaresInImageTotf);
		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] / (float)(nNumOfNonZero_ObjectSquaresTotf);

		if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] < 0.0 || fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] > 1.0)
		{
			printf("\n\n An error in 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
				iSquareOccurrenceIntervalf, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nNumOfNonZero_ObjectSquaresTotf);
			printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[%d] = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
				iSquareOccurrenceIntervalf, fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf], nNumOfNonZero_ObjectSquaresTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			delete[] nMassesOfSquaresArrf;
			return UNSUCCESSFUL_RETURN;
		}//if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] < 0.0 || fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf] > 1.0)

	} //for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)

	//printf("\n\n The end of 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
	//printf("\n\n Please press any key:"); getchar();
	delete[] nMassesOfSquaresArrf;
	return SUCCESSFUL_RETURN;
} // int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

	const int nNumOfSquareOccurrence_Intervals_Laf,//4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

float &fFirstMomentf,
float &fSecondMomentf,

float &fLacunarityf)

{
	int ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		const int nNumOfSquareOccurrence_Intervals_Laf,// 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////

		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]); //[nNumOfSquareOccurrence_Intervals_Laf]

	int
		iSquareOccurrenceIntervalf,
		nResf;
	//////////////////////////////

	fFirstMomentf = 0.0;
	fSecondMomentf = 0.0;

	/*
	//nNumOfSquareOccurrence_Intervals_Laf == 4
		float *fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf = new float[nNumOfSquareOccurrence_Intervals_Laf];

		if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
		{
	#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
			fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
			printf("\n\n Please press any key:"); getchar();
	#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
	*/

	nResf = ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]
		///////////////////////////////////////

		fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf); // float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[]); //[nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' : after 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' by 'ProbOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
		printf("\n\n Please press any key:"); getchar();

		//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nResf == -2)
	{
		//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return (-2);
	}//if (nResf == -2)
	/////////////////////

	for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)
	{
		//midpoint of the interval
		fFirstMomentf += (float)((float)(iSquareOccurrenceIntervalf)+0.5)*fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf];
		fSecondMomentf += (float)((float)(iSquareOccurrenceIntervalf)+0.5)*(float)((float)(iSquareOccurrenceIntervalf)+0.5)*fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iSquareOccurrenceIntervalf];
	}//for (iSquareOccurrenceIntervalf = 0; iSquareOccurrenceIntervalf < nNumOfSquareOccurrence_Intervals_Laf; iSquareOccurrenceIntervalf++)

//	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': 1");

	if (fFirstMomentf <= feps)
	{
		printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fFirstMomentf = %E <= feps", fFirstMomentf);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': fFirstMomentf = %E <= feps", fFirstMomentf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
		return UNSUCCESSFUL_RETURN;
	}//if (fFirstMomentf <= feps)

	fLacunarityf = fSecondMomentf / (fFirstMomentf*fFirstMomentf);

	//delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	return SUCCESSFUL_RETURN;
} //int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
 //////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

	const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nNumOfLensOfSquareTot_Actualf,

	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

float fLacunarity_Arrf[]) //[nNumOfLensOfSquareTot_Actualf]

{
	int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

		float &fFirstMomentf,
		float &fSecondMomentf,

		float &fLacunarityf); //

	int
		nLenOfSquareCurf,

		nIndexOfSquaref,
		iNumOfSquaresf,

		nIndexOfProbMaxf = nNumOfLensOfSquareTot_Actualf * nNumOfSquareOccurrence_Intervals_Laf,
		nTempf,
		iLenOfSquaref,
		nResf;

	float
		fFirstMomentf,
		fSecondMomentf,
		fLacunarityf;
	//////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);

	fprintf(fout_PrintFeatures, "\n\n 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfLensOfSquareTot_Actualf = %d, nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfLensOfSquareTot_Actualf, nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nLenOfSquareMin_Laf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	float *fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;//  
	fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf = new float[nNumOfSquareOccurrence_Intervals_Laf];

	if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf' in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\nAn error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf' in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)

	nLenOfSquareCurf = nDim_2pNf / nDivisorForLenOfSquareMax_Init_La;
	for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		nTempf = nDim_2pNf / nLenOfSquareCurf; // == 2*nDivisorForLenOfSquareMax_Init_La = 2*(nNumOfSquareOccurrence_Intervals_La/2) == nNumOfSquareOccurrence_Intervals_La
		if (nNumOfSquareOccurrence_Intervals_Laf > nTempf*nTempf) //<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)
		{
			printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d > nTempf * nTempf = %d",
				nNumOfSquareOccurrence_Intervals_Laf, nTempf* nTempf);
			printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d > nTempf * nTempf = %d",
				nNumOfSquareOccurrence_Intervals_Laf, nTempf* nTempf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nNumOfSquareOccurrence_Intervals_Laf > nTempf*nTempf)

		//printf("\n Before 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");// getchar();
		//fprintf(fout_PrintFeatures, "\n\n After 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		//printf("\n\n Please press any key:"); fflush(fout_PrintFeatures); getchar();

		nResf = LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

			nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquareCurf, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////
			fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

			fFirstMomentf, //float &fFirstMomentf,
			fSecondMomentf, //float &fSecondMomentf,

			fLacunarityf); // float &fLacunarityf); //[nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
			iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

		fprintf(fout_PrintFeatures, "\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
			iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

		printf("\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nResf = %d", nResf);// getchar();
		fflush(fout_PrintFeatures);

		//printf("\n\n Please press any key:"); fflush(fout_PrintFeatures); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare' by 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
			printf("\n\n Please press any key:"); getchar();

			delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
				iLenOfSquaref, nLenOfSquareCurf);

			fprintf(fout_lr, "\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
				iLenOfSquaref, nLenOfSquareCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			fFirstMoment_Arrf[iLenOfSquaref] = 0.0; // -1.0; //[nNumOfLensOfSquareTot_Actualf]
			fSecondMoment_Arrf[iLenOfSquaref] = 0.0; // -1.0; //[nNumOfLensOfSquareTot_Actualf]

			fLacunarity_Arrf[iLenOfSquaref] = 0.0; // -1.0;  //[nNumOfLensOfSquareTot_Actualf

/////////////////////////////////////////
			for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
			{
				//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax_La);
				nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

				if (nIndexOfSquaref >= nIndexOfProbMaxf)
				{
					printf("\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' 1: nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
					printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare' 1: nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
					return UNSUCCESSFUL_RETURN;
				} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

				fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref] = 0.0; // -1.0; //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n iLenOfSquaref = %d, iNumOfSquaresf = %d, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[%d] = %E",
					iLenOfSquaref, iNumOfSquaresf, nIndexOfSquaref, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref]);
				//fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)

			continue;
		}//if (nResf == -2)

		fFirstMoment_Arrf[iLenOfSquaref] = fFirstMomentf; //[nNumOfLensOfSquareTot_Actualf]
		fSecondMoment_Arrf[iLenOfSquaref] = fSecondMomentf; //[nNumOfLensOfSquareTot_Actualf]

		fLacunarity_Arrf[iLenOfSquaref] = fLacunarityf;  //[nNumOfLensOfSquareTot_Actualf

/////////////////////////////////////////////////////////////

		for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
		{
			//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax_La);
			nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

			if (nIndexOfSquaref >= nIndexOfProbMaxf)
			{
				printf("\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

			fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iNumOfSquaresf]; //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS

			fprintf(fout_PrintFeatures, "\n\ iLenOfSquaref = %d, iNumOfSquaresf = %d, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[%d] = %E",
				iLenOfSquaref, iNumOfSquaresf, nIndexOfSquaref, fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref]);
			fflush(fout_PrintFeatures);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
/////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n iLenOfSquaref = %d, fFirstMoment_Arrf[%d] = %E, fSecondMoment_Arrf[%d] = %E, fLacunarity_Arrf[%d] = %E",
			iLenOfSquaref, iLenOfSquaref, fFirstMoment_Arrf[iLenOfSquaref],
			iLenOfSquaref, fSecondMoment_Arrf[iLenOfSquaref],
			iLenOfSquaref, fLacunarity_Arrf[iLenOfSquaref]);

		fprintf(fout_PrintFeatures, "\n iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d, fFirstMoment_Arrf[%d] = %E, fSecondMoment_Arrf[%d] = %E, fLacunarity_Arrf[%d] = %E",
			iLenOfSquaref, nNumOfLensOfSquareTot_Actualf,
			iLenOfSquaref, fFirstMoment_Arrf[iLenOfSquaref],
			iLenOfSquaref, fSecondMoment_Arrf[iLenOfSquaref],
			iLenOfSquaref, fLacunarity_Arrf[iLenOfSquaref]);
		fflush(fout_PrintFeatures);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nLenOfSquareCurf == nLenOfSquareMin_Laf)
		{
			if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)
			{
				printf("\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMin_Laf",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf - 1);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMin_Laf",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf - 1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
				return UNSUCCESSFUL_RETURN;
			} // if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)

			break;
		} //if (nLenOfSquareCurf == nLenOfSquareMin_Laf)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n The end of 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
	//fprintf(fout_PrintFeatures, "\n The end of 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
	//printf("\n\n Please press any key:"); fflush(fout_PrintFeatures); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	return SUCCESSFUL_RETURN;
} //int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(...

//////////////////////////////////////////////////////////////////////////
	//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
  //LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range
int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

	const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf, //>=
	const int nThresholdForIntensitiesMaxf, // <

	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,
	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

int &nNumOfLensOfSquareTot_Actualf)
{

	int LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nNumOfLensOfSquareTot_Actualf,

		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

		float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
		float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

		float fLacunarity_Arrf[]); //[nNumOfLensOfSquareTot_Actualf]


	int
		nResf,

		iFeaf,
		nFeaf,

		nIndexOfSquaref,
		iNumOfSquaresf, // = nDim_2pNf / nLenOfSquaref, 

//		nNumOfSquareOccurrence_Intervals_LaCurf,
		//nDim_2pNf,

//		nScalef,
nLenOfSquareCurf,

nNumOfLenOfSquareCurf = 0,

nIndexOfProbMaxf,
nNumOfLensOfSquareTot_ForTestingf,
iLenOfSquaref;

	//////////////////////////////////

	if (nDim_2pNf > nDim_2pN_Max_La)
	{
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the image is too big, nDim_2pNf = %d > nDim_2pN_Max_La = %d",
			nDim_2pNf, nDim_2pN_Max_La);
		printf("\n\n Please increase 'nDim_2pN_Max_La'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the image is too big, nDim_2pNf = %d > nDim_2pN_Max_La = %d",
			nDim_2pNf, nDim_2pN_Max_La);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

		return UNSUCCESSFUL_RETURN;
	} //if (nDim_2pNf > nDim_2pN_Max_La)

	//initialization
	for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
	{
		fFeaAll_AtIntensity_Range_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)

////////////////////////////////////////////////////////
//verifying that 'nLenOfSquareCurf' == nNumOfLenOfSquareMax_La for 'nDim_2pN_Max_La'

	nLenOfSquareCurf = nDim_2pN_Max_La / nDivisorForLenOfSquareMax_Init_La;

	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max_La; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMin_Laf) //nLenOfSquareMin_La == 2
		{
			nNumOfLensOfSquareTot_ForTestingf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);

			fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max_La; iLenOfSquaref++)

	//printf("\n 3"); getchar();

	if (nNumOfLensOfSquareTot_ForTestingf != nNumOfLenOfSquareMax_La)
	{
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax_La = %d",
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax_La);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax_La = %d",
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax_La);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nLenOfSquareCurf != nNumOfLenOfSquareMax_La)
//////////////////////////////////
	//finding 'nNumOfLensOfSquareTot_Actualf'
	nNumOfLensOfSquareTot_Actualf = 0;

	nLenOfSquareCurf = nDim_2pNf / nDivisorForLenOfSquareMax_Init_La; //nDivisorForLenOfSquareMax_Init_La == 2
	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMin_Laf)
		{
			nNumOfLensOfSquareTot_Actualf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);
			fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)

	if (nNumOfLensOfSquareTot_Actualf == 0)
	{
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");
		printf("\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");
		fprintf(fout_lr, "\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfLensOfSquareTot_Actualf == 0)

	if (nNumOfLensOfSquareTot_Actualf > nNumOfLenOfSquareMax_La)
	{
		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf = %d > nNumOfLenOfSquareMax_La = %d", nNumOfLensOfSquareTot_Actualf, nNumOfLenOfSquareMax_La);
		printf("\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf = %d > nNumOfLenOfSquareMax_La = %d",
			nNumOfLensOfSquareTot_Actualf, nNumOfLenOfSquareMax_La);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfLensOfSquareTot_Actualf > nNumOfLenOfSquareMax_La)

///////////////////////////////////////

	float *fFirstMoment_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fFirstMoment_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fFirstMoment_Arrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFirstMoment_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fFirstMoment_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr);  getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fFirstMoment_Arrf == nullptr)
///////////////////////////////////////////
	float *fSecondMoment_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fSecondMoment_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fSecondMoment_Arrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fSecondMoment_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fSecondMoment_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fSecondMoment_Arrf == nullptr)
///////////////////////////////////////////////////

	float *fLacunarity_Arrf; //[nNumOfLensOfSquareTot_Actualf]
	fLacunarity_Arrf = new float[nNumOfLensOfSquareTot_Actualf];

	if (fLacunarity_Arrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fLacunarity_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fLacunarity_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		delete[] fSecondMoment_Arrf;
		return UNSUCCESSFUL_RETURN;
	} //if (fLacunarity_Arrf == nullptr)
//////////////////////////////////////////////////

	nIndexOfProbMaxf = nNumOfLensOfSquareTot_Actualf * nNumOfSquareOccurrence_Intervals_Laf;

	float *fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf; //
	fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf = new float[nIndexOfProbMaxf];

	if (fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf' in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf' in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
		printf("\n\n Please press any key:"); fflush(fout_lr);  getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fFirstMoment_Arrf;
		delete[] fSecondMoment_Arrf;
		delete[] fLacunarity_Arrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf == nullptr)
//////////////////////////////////////////////
	//printf("\n 5"); //getchar();
	//printf("\n\n Please press any key:"); fflush(fout_PrintFeatures); getchar();

	nResf = LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

		nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquareCurf)*(nDim_2pNf / nLenOfSquareCurf)

		nDim_2pNf, //const int nDim_2pNf,

		nNumOfLensOfSquareTot_Actualf, //const int nNumOfLensOfSquareTot_Actualf,

		nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

		fFirstMoment_Arrf, //float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
		fSecondMoment_Arrf, //float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

		fLacunarity_Arrf); // float fLacunarity_Arrf[]); //[nNumOfLensOfSquareTot_Actualf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] fFirstMoment_Arrf;
		delete[] fSecondMoment_Arrf;
		delete[] fLacunarity_Arrf;
		delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

		printf("\n\n An error in 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range' by 'LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		printf("\n\n Please press any key:"); getchar();

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the feas\n");
		//printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	//	printf("\n 6");// getchar();
		//fprintf(fout_PrintFeatures, "\n\n After 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
	//	printf("\n\n Please press any key:"); fflush(fout_PrintFeatures); getchar();
///////////////////////////////////////////////////////////////
//fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

	nFeaf = 0;
	for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	{
		fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fFirstMoment_Arrf[iLenOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nFeaf += 1;
	} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
/////////////////////////////////////
	nFeaf = nNumOfLenOfSquareMax_La;

	for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	{
		fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fSecondMoment_Arrf[iLenOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nFeaf += 1;
	} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
///////////////////////////////////////////
	nFeaf = 2 * nNumOfLenOfSquareMax_La;

	for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	{
		fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fLacunarity_Arrf[iLenOfSquaref];
#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nFeaf += 1;
	} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
/////////////////////////////////////////
	nFeaf = 3 * nNumOfLenOfSquareMax_La; // nNumOfLenOfSquareMax_La == 7
//fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

	for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
	{
		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax_La);
			nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

			if (nIndexOfSquaref >= nIndexOfProbMaxf)
			{
				printf("\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fFirstMoment_Arrf;
				delete[] fSecondMoment_Arrf;
				delete[] fLacunarity_Arrf;
				delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

			if (nFeaf >= nNumOfFeasForLacunar_AtIntensity_Range)
			{
				printf("\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nFeaf = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d",
					nFeaf, nNumOfFeasForLacunar_AtIntensity_Range);
				printf("\n\n iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d, iNumOfSquaresf = %d, nNumOfSquareOccurrence_Intervals_Laf = %d",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf, iNumOfSquaresf, nNumOfSquareOccurrence_Intervals_Laf);

				printf("\n\n nNumOfFeasForLacunar_AtIntensity_Range = %d = (nNumOfLenOfSquareMax_La = %d * 3) + (nNumOfLenOfSquareMax_La = %d* nNumOfSquareOccurrence_Intervals_La = %d",
					nNumOfFeasForLacunar_AtIntensity_Range, nNumOfLenOfSquareMax_La, nNumOfLenOfSquareMax_La, nNumOfSquareOccurrence_Intervals_La);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nFeaf = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d",
					nFeaf, nNumOfFeasForLacunar_AtIntensity_Range);

				fprintf(fout_lr, "\n\n iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d, iNumOfSquaresf = %d, nNumOfSquareOccurrence_Intervals_Laf = %d",
					iLenOfSquaref, nNumOfLensOfSquareTot_Actualf, iNumOfSquaresf, nNumOfSquareOccurrence_Intervals_Laf);

				fprintf(fout_lr, "\n\n nNumOfFeasForLacunar_AtIntensity_Range = %d = (nNumOfLenOfSquareMax_La = %d * 3) + (nNumOfLenOfSquareMax_La = %d* nNumOfSquareOccurrence_Intervals_La = %d",
					nNumOfFeasForLacunar_AtIntensity_Range, nNumOfLenOfSquareMax_La, nNumOfLenOfSquareMax_La, nNumOfSquareOccurrence_Intervals_La);

				printf("\n\n Please press any key to exit:"); fflush(fout_lr);  getchar(); exit(1);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				delete[] fFirstMoment_Arrf;
				delete[] fSecondMoment_Arrf;
				delete[] fLacunarity_Arrf;
				delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

				return UNSUCCESSFUL_RETURN;
			}//if (nFeaf >= nNumOfFeasForLacunar_AtIntensity_Range)

			fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref];

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "%d:%E \n", nFeaf, fFeaAll_AtIntensity_Range_Arrf[nFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nFeaf += 1;
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)

	}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)

	//printf("\n 7"); getchar();

///////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);

	fprintf(fout_lr, "\n\n 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);
	fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	delete[] fFirstMoment_Arrf;
	delete[] fSecondMoment_Arrf;
	delete[] fLacunarity_Arrf;

	delete[] fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf;

#ifndef COMMENT_OUT_ALL_PRINTS	
	//printf("\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': please press any key");
	//fprintf(fout_lr, "\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': please press any key"); fflush(fout_lr); getchar();

	printf("\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");

	fprintf(fout_lr, "\n\n The end of 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'"); fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(...
////////////////////////////////////////////////////////////////////////////

int LacunarityOfMasses_At_All_Intensity_Ranges(
	const int nNumOfSquareOccurrence_Intervals_Laf,// == 16 // == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	//const int nDim_2pNf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,
	//const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
float fOneDim_Lacunar_Arrf[]) //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot
{
	int Dim_2powerN(
		const int nLengthf,
		const int nWidthf,

		int &nScalef,

		int &nDim_2pNf);

	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForLacunarity(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,
		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

		int &nNumOfLensOfSquareTot_Actualf);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax_La + 1) / nNumOfIntensity_IntervalsForLacunar, // 256/32 = 8 //256/8 = 32

		nScalef,
		nDim_2pNf,

		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf,

		nNumOfLensOfSquareTot_Actualf,

		nTempf,
		iFeaf,
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nFea_Curf,
		nNumOfIntensity_IntervalsForLacunarTotf;

	float
		fFeaAll_AtIntensity_Range_Arrf[nNumOfFeasForLacunar_AtIntensity_Range];
	/////////////////////////////

	//initialization
	for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
	{
		fOneDim_Lacunar_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'Dim_2powerN'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'Dim_2powerN'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fprintf(fout_lr, "\n\n 'LacunarityOfMasses_At_All_Intensity_Ranges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
		printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////
	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'Initializing_Embedded_Image'");
		printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'Initializing_Embedded_Image'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n The loop in 'LacunarityOfMasses_At_All_Intensity_Ranges' (normal): please press any key to continue"); fflush(fout_PrintFeatures);  getchar();

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForLacunarTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForLacunarTotf += 1;

			if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)
			{
				printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				printf("\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

				//#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				fprintf(fout_PrintFeatures, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)

			nThresholdForIntensitiesMaxf = iIntensity_Interval_2f * nLenOfIntensityIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS	
			fprintf(fout_PrintFeatures, "\n\n 1: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

			fprintf(fout_PrintFeatures, "\n\n Before 'Embedding_Image_Into_2powerN_ForHausdorff': nNumOfIntensity_IntervalsForLacunarTotf = %d, iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d",
				nNumOfIntensity_IntervalsForLacunarTotf, iIntensity_Interval_1f, iIntensity_Interval_2f);
			fflush(fout_PrintFeatures);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			//fprintf(fout_PrintFeatures, "\n\n Before 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
			//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);
			//fflush(fout_PrintFeatures);

			nResf = LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

				nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf, //>=
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf, // <

				nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			///////////////////////////////////////
				fFeaAll_AtIntensity_Range_Arrf, // float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

				nNumOfLensOfSquareTot_Actualf); // int &nNumOfLensOfSquareTot_Actualf);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
				printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges' by 'LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

	////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	

			fprintf(fout_PrintFeatures, "\n\n nNumOfIntensity_IntervalsForLacunarTotf = %d, iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d",
				nNumOfIntensity_IntervalsForLacunarTotf, iIntensity_Interval_1f, iIntensity_Interval_2f);
			fflush(fout_PrintFeatures);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nTempf = (nNumOfIntensity_IntervalsForLacunarTotf - 1) * nNumOfFeasForLacunar_AtIntensity_Range;
			for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
			{
				nFea_Curf = iFeaf + nTempf;

#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout_lr, "\n fFeaAll_AtIntensity_Range_Arrf[%d] = %E, nFea_Curf = %d, nTempf = %d", iFeaf, fFeaAll_AtIntensity_Range_Arrf[iFeaf], nFea_Curf, nTempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)
				{
					printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d", nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d",
						nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

					return UNSUCCESSFUL_RETURN;
				}//if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)

				fOneDim_Lacunar_Arrf[nFea_Curf] = fFeaAll_AtIntensity_Range_Arrf[iFeaf];

#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout_PrintFeatures, "\n fOneDim_Lacunar_Arrf[%d] = %E, nFea_Curf = %d, nTempf = %d", nFea_Curf, fOneDim_Lacunar_Arrf[nFea_Curf], nFea_Curf, nTempf);
				fflush(fout_PrintFeatures);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			}//for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)

			//fprintf(fout_lr, "\n\n4: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)

///////////////
	delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
	return SUCCESSFUL_RETURN;
}//int LacunarityOfMasses_At_All_Intensity_Ranges(...
///////////////////////////////////////////////////////////////////////////

int doLacunarityOf_ColorImage(
	//const Image& image_in,

	const COLOR_IMAGE *sColor_Imagef,
	//[nNumOfLacunarFeasFor_OneDimTot]
	float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

	float &fFractal_Dimension)
{
	int SetOfLogPoints_ForFractal_Dim(
		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		int &nNumOfLogPointsf,
		float fNegLogOfLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max_La]

		float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max_La]

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	////////////////////////////////////////////////////////////////////////
//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
	int LacunarityOfMasses_At_All_Intensity_Ranges(
		const int nNumOfSquareOccurrence_Intervals_Laf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,

		///////////////////////////////////////
		float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,

		iFeaf,
#ifndef COMMENT_OUT_ALL_PRINTS
		iLogf,
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,
		nNumOfLogPointsf,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		//		nNumOfLensOfSquareTot_Actualf, //
		nImageWidth,
		nImageHeight;

	float
		fSlope,//
		fNegLogOfLenOfSquare_Arr[nNumOfIters_ForDim_2powerN_Max_La], //[]

		fLogPoints_Arr[nNumOfIters_ForDim_2powerN_Max_La];

	////////////////////////////////////////////////////////////////////////
//initialization
	for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)
	{
		fOneDim_Lacunar_Arrf[iFeaf] = 0.0;
	}//for (iFeaf = 0; iFeaf < nNumOfLacunarFeasFor_OneDimTot; iFeaf++)

	//////////////////////////////////////////////////////////
	  // size of image
	nImageWidth = sColor_Imagef->nWidth; //image_in.width();
	nImageHeight = sColor_Imagef->nLength; //image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax_La || nImageWidth < nLenMin_La)
	{
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax_La || nImageWidth < nLenMin_La)

	if (nImageHeight > nWidMax_La || nImageHeight < nWidMin_La)
	{
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax_La || nImageHeight < nWidMin_La)
///////////////////////////////////////////////////////////////////////////

	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr_La;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr_La;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr_La;

	//printf("\n 4"); getchar();

	nRes = SetOfLogPoints_ForFractal_Dim(
		nThresholdForIntensities_ForFractalDimMin_La, //const int nThresholdForIntensitiesMinf,
		nThresholdForIntensities_ForFractalDimMax_La, //const int nThresholdForIntensitiesMaxf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
		sColor_Imagef, //const COLOR_IMAGE *sColor_ImageInitf,

		nNumOfLogPointsf, //int &nNumOfLogPointsf,
		fNegLogOfLenOfSquare_Arr, //int nLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max_La]

		fLogPoints_Arr); // float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max_La]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;

		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n An error in 'doLacunarityOf_ColorImage' by 'SetOfLogPoints_ForFractal_Dim'");
		printf("\n Please press any key:");  getchar();

		//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doLacunarityOf_ColorImage' by 'SetOfLogPoints_ForFractal_Dim'");
		getchar(); exit(1);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	fprintf(fout_lr, "\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	//getchar();

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	///////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	for (iLogf = 0; iLogf < nNumOfLogPointsf; iLogf++)
	{
		printf("\n\n 'doLacunarityOf_ColorImage' 2: iLogf = %d, fNegLogOfLenOfSquare_Arr[iLogf] = %E, fLogPoints_Arr[iLogf] = %E", iLogf, fNegLogOfLenOfSquare_Arr[iLogf], fLogPoints_Arr[iLogf]);
		fprintf(fout_lr, "\n\n 'doLacunarityOf_ColorImage'  2: iLogf = %d, fNegLogOfLenOfSquare_Arr[iLogf] = %E, fLogPoints_Arr[iLogf] = %E",
			iLogf, fNegLogOfLenOfSquare_Arr[iLogf], fLogPoints_Arr[iLogf]);
	}//for (iLogf = 0; iLogf < nNumOfLogPointsf; iLogf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	SlopeOfAStraightLine(
		nNumOfLogPointsf, //const int nDimf,

		fNegLogOfLenOfSquare_Arr, //const float fX_Arrf[],
		fLogPoints_Arr, //const float fY_Arrf[],

		fSlope); // float &fSlopef);

	fFractal_Dimension = fSlope;
	/////////////////////////////////////////////////////////////////////

	nRes = LacunarityOfMasses_At_All_Intensity_Ranges(
		nNumOfSquareOccurrence_Intervals_La, //const int nNumOfSquareOccurrence_Intervals_Laf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		nLenOfSquareMin_La, //const int nLenOfSquareMin_Laf,

		//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
		sColor_Imagef, //const COLOR_IMAGE *sColor_ImageInitf,

		///////////////////////////////////////
		fOneDim_Lacunar_Arrf); // float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;

		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n An error in 'doLacunarityOf_ColorImage' by 'LacunarityOfMasses_At_All_Intensity_Ranges'");
		printf("\n Please press any key:");  getchar();

		//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doLacunarityOf_ColorImage' by 'SetOfLogPoints_ForFractal_Dim'");
		getchar(); exit(1);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)


//////////////////////////////////////////////////////////
/*
	delete[] sColor_Imagef->nRed_Arr;
	delete[] sColor_Imagef->nGreen_Arr;
	delete[] sColor_Imagef->nBlue_Arr;

	delete[] sColor_Imagef->nLenObjectBoundary_Arr;
	delete[] sColor_Imagef->nIsAPixelBackground_Arr;
*/

#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doLacunarityOf_ColorImage(...
////////////////////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
int OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

	const int nNumTypeOfFeaf, // 1 -- 1st moment, 2 -- 2nd moment, 3 -- lacunarity, 4 -- probability
	const int nNumOfOneFea_Adjusted_2f,

	const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nNumOfLensOfSquareTot_Actualf,

	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
float &fOneFea_Lacunar_AtIntensity_Rangef) //
{
	int LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

		float &fFirstMomentf,
		float &fSecondMomentf,

		float &fLacunarityf); //

	int
		nLenOfSquareCurf,

		nIndexOfSquaref,
		iNumOfSquaresf,

		nIndexFoundf = -1, //not yet
		nIndexOfProbMaxf = nNumOfLensOfSquareTot_Actualf * nNumOfSquareOccurrence_Intervals_Laf,
		nTempf,
		iLenOfSquaref,
		nResf;

	float
		fFirstMomentf,
		fSecondMomentf,
		fLacunarityf;
	//////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);

	fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	float *fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
	fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf = new float[nNumOfSquareOccurrence_Intervals_Laf];

	if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf == nullptr)

////////////////////////////////////////////////////
	if (nNumTypeOfFeaf < 4)
	{
		nLenOfSquareCurf = nDim_2pNf / nDivisorForLenOfSquareMax_Init_La;

		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

			if (iLenOfSquaref == nNumOfOneFea_Adjusted_2f)
			{
				nIndexFoundf = 1; //found
				nTempf = nDim_2pNf / nLenOfSquareCurf;
				if (nNumOfSquareOccurrence_Intervals_Laf > nTempf*nTempf) //<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d > nTempf * nTempf = %d",
						nNumOfSquareOccurrence_Intervals_Laf, nTempf* nTempf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumOfSquareOccurrence_Intervals_Laf = %d > nTempf * nTempf = %d",
						nNumOfSquareOccurrence_Intervals_Laf, nTempf* nTempf);
					printf("\n\n Please press any key:"); getchar();

					delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
					return UNSUCCESSFUL_RETURN;
				}//if (nNumOfSquareOccurrence_Intervals_Laf > nTempf*nTempf)

				nResf = LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

					nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

					nDim_2pNf, //const int nDim_2pNf,

					nLenOfSquareCurf, //const int nLenOfSquaref,

					sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

					sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				///////////////////////////////////////
					fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

					fFirstMomentf, //float &fFirstMomentf,
					fSecondMomentf, //float &fSecondMomentf,

					fLacunarityf); // float &fLacunarityf); //[nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
					iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

				fprintf(fout_lr, "\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
					iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
					printf("\n\n Please press any key:"); getchar();

					delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
					return UNSUCCESSFUL_RETURN;
				}//if (nResf == UNSUCCESSFUL_RETURN)

				if (nResf == -2)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
						iLenOfSquaref, nLenOfSquareCurf);

					fprintf(fout_lr, "\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
						iLenOfSquaref, nLenOfSquareCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					//fFirstMoment_Arrf[iLenOfSquaref] = -1.0; //[nNumOfLensOfSquareTot_Actualf]
					//fSecondMoment_Arrf[iLenOfSquaref] = -1.0; //[nNumOfLensOfSquareTot_Actualf]
					//fLacunarity_Arrf[iLenOfSquaref] = -1.0;  //[nNumOfLensOfSquareTot_Actualf

					fOneFea_Lacunar_AtIntensity_Rangef = 0.0; // -1.0; //for 1st, 2nd moment or lacunarity
					break;
					//continue;
				}//if (nResf == -2)

//////////////////////////////////////////////////////////
			//	fFirstMoment_Arrf[iLenOfSquaref] = fFirstMomentf; //[nNumOfLensOfSquareTot_Actualf]
			//	fSecondMoment_Arrf[iLenOfSquaref] = fSecondMomentf; //[nNumOfLensOfSquareTot_Actualf]
			//	fLacunarity_Arrf[iLenOfSquaref] = fLacunarityf;  //[nNumOfLensOfSquareTot_Actualf

				if (nNumTypeOfFeaf == 1)
				{
					fOneFea_Lacunar_AtIntensity_Rangef = fFirstMomentf;
				}
				else if (nNumTypeOfFeaf == 2)
				{
					fOneFea_Lacunar_AtIntensity_Rangef = fSecondMomentf;
				}
				else if (nNumTypeOfFeaf == 3)
				{
					fOneFea_Lacunar_AtIntensity_Rangef = fLacunarityf;
				}

			}//if (iLenOfSquaref == nNumOfOneFea_Adjusted_2f)

		} // for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)

	} //if (nNumTypeOfFeaf < 4)
//////////////////////////////////////////////////////////////////////////////////

	if (nNumTypeOfFeaf == 4) //prob
	{
		nLenOfSquareCurf = nDim_2pNf / nDivisorForLenOfSquareMax_Init_La;
		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

			for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
			{
				//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax_La);
				nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

				if (nIndexOfSquaref >= nIndexOfProbMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);

					fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nIndexOfSquaref = %d >= nIndexOfProbMaxf = %d", nIndexOfSquaref, nIndexOfProbMaxf);
					printf("\n\n Please press any key:"); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
					return UNSUCCESSFUL_RETURN;
				} //if (nIndexOfSquaref >= nIndexOfProbMaxf)

				//fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref] = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iNumOfSquaresf]; //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]
				if (nIndexOfSquaref == nNumOfOneFea_Adjusted_2f)
				{
					nIndexFoundf = 1; //found
					nResf = LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare(

						nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

						nDim_2pNf, //const int nDim_2pNf,

						nLenOfSquareCurf, //const int nLenOfSquaref,

						sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

						sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

					///////////////////////////////////////
						fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[], //[nNumOfSquareOccurrence_Intervals_Laf]

						fFirstMomentf, //float &fFirstMomentf,
						fSecondMomentf, //float &fSecondMomentf,

						fLacunarityf); // float &fLacunarityf); //[nNumOfSquareOccurrence_Intervals_Laf]

#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
						iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

					fprintf(fout_lr, "\n\n After 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare', iLenOfSquaref = %d, nLenOfSquareCurf = %d, fFirstMomentf = %E, fSecondMomentf = %E, fLacunarityf = %E",
						iLenOfSquaref, nLenOfSquareCurf, fFirstMomentf, fSecondMomentf, fLacunarityf);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					if (nResf == UNSUCCESSFUL_RETURN)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

						printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': 'LacunarityOfMassesOfAll_ObjectSquares_AtFixedLenOfSquare'");
						printf("\n\n Please press any key:"); getchar();

						delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;
						return UNSUCCESSFUL_RETURN;
					}//if (nResf == UNSUCCESSFUL_RETURN)

					if (nResf == -2)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
							iLenOfSquaref, nLenOfSquareCurf);
						fprintf(fout_lr, "\n\n Just one or no squares contain object pixels -- continue: iLenOfSquaref = %d, nLenOfSquareCurf = %d",
							iLenOfSquaref, nLenOfSquareCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						fOneFea_Lacunar_AtIntensity_Rangef = 0.0; // -1.0;
						break;
						//continue;
					}//if (nResf == -2)

					fOneFea_Lacunar_AtIntensity_Rangef = fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf[iNumOfSquaresf];
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n iLenOfSquaref = %d, iNumOfSquaresf = %d, fOneFea_Lacunar_AtIntensity_Rangef = %E",
						iLenOfSquaref, iNumOfSquaresf, fOneFea_Lacunar_AtIntensity_Rangef);
					//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nIndexOfSquaref == nNumOfOneFea_Adjusted_2f)

			}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
	/////////////////////////////////////////////////////////////

/*
			if (nLenOfSquareCurf == nLenOfSquareMin_Laf)
			{
				if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMin_Laf",
						iLenOfSquaref, nNumOfLensOfSquareTot_Actualf - 1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': iLenOfSquaref = %d != nNumOfLensOfSquareTot_Actualf - 1 = %d at nLenOfSquareCurf == nLenOfSquareMin_Laf",
						iLenOfSquaref, nNumOfLensOfSquareTot_Actualf - 1);

					printf("\n\n Please press any key:"); getchar();
					delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;

					return UNSUCCESSFUL_RETURN;
				} // if (iLenOfSquaref != nNumOfLensOfSquareTot_Actualf - 1)

				break;
			} //if (nLenOfSquareCurf == nLenOfSquareMin_Laf)
*/
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
	} // if (nNumTypeOfFeaf == 4) //prob

	if (nIndexFoundf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nIndexFoundf == UNSUCCESSFUL_RETURN, 'nNumOfOneFea_Adjusted_2f' has not been found");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//printf("\n\n An error in 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nIndexFoundf == UNSUCCESSFUL_RETURN, 'nNumOfOneFea_Adjusted_2f' has not been found");
	//	printf("\n\n 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nIndexFoundf == UNSUCCESSFUL_RETURN, 'nNumOfOneFea_Adjusted_2f' has not been found");

		//printf("\n\n Please press any key to exit"); getchar(); exit(1);
		delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;

		fOneFea_Lacunar_AtIntensity_Rangef = 0.0;

		//return UNSUCCESSFUL_RETURN;
		return SUCCESSFUL_RETURN;
	} //if (nIndexFoundf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n The end of 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d, fOneFea_Lacunar_AtIntensity_Rangef = %E",
		nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f, fOneFea_Lacunar_AtIntensity_Rangef);
	fprintf(fout_lr, "\n The end of 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare': nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d, fOneFea_Lacunar_AtIntensity_Rangef = %E",
		nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f, fOneFea_Lacunar_AtIntensity_Rangef);
	fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fProbOfAll_Mass_Intervals_AtFixedLenOfSquare_Arrf;

	return SUCCESSFUL_RETURN;
} //int OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(...

////////////////////////////////////////////////////////////////////////////
int OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(
	const int nNumOfOneFea_Adjusted_1f,
	const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf, //>=
	const int nThresholdForIntensitiesMaxf, // <

	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,
	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

///////////////////////////////////////
	//float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] =
	float &fOneFea_Lacunar_AtIntensity_Rangef, //

	int &nNumOfLensOfSquareTot_Actualf)
{
	int OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

		const int nNumTypeOfFeaf, // 1 -- 1st moment, 2 -- 2nd moment, 3 -- lacunarity, 4 -- probability
		const int nNumOfOneFea_Adjusted_2f,

		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nNumOfLensOfSquareTot_Actualf,

		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		float &fOneFea_Lacunar_AtIntensity_Rangef); //

	int
		nResf,

		nLenOfSquareCurf,

		nNumOfLenOfSquareCurf = 0,

		nNumTypeOfFeaf,
		nNumOfOneFea_Adjusted_2f,

		nNumOfLensOfSquareTot_ForTestingf,
		iLenOfSquaref;

	if (nDim_2pNf > nDim_2pN_Max_La)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the image is too big, nDim_2pNf = %d > nDim_2pN_Max_La = %d",
			nDim_2pNf, nDim_2pN_Max_La);
		printf("\n\n Please increase 'nDim_2pN_Max_La'");

		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': the image is too big, nDim_2pNf = %d > nDim_2pN_Max_La = %d",
			nDim_2pNf, nDim_2pN_Max_La);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	} //if (nDim_2pNf > nDim_2pN_Max_La)
////////////////////////////////////////////////////////
//verifying that 'nLenOfSquareCurf' == nNumOfLenOfSquareMax_La for 'nDim_2pN_Max_La'

	nLenOfSquareCurf = nDim_2pN_Max_La / nDivisorForLenOfSquareMax_Init_La;
	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max_La; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMin_Laf) //nLenOfSquareMin_La == 2
		{
			nNumOfLensOfSquareTot_ForTestingf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);

			fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_ForTestingf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_ForTestingf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pN_Max_La; iLenOfSquaref++)

	if (nNumOfLensOfSquareTot_ForTestingf != nNumOfLenOfSquareMax_La)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax_La = %d",
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax_La);
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_ForTestingf = %d != nNumOfLenOfSquareMax_La = %d",
			nNumOfLensOfSquareTot_ForTestingf, nNumOfLenOfSquareMax_La);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (nLenOfSquareCurf != nNumOfLenOfSquareMax_La)
//////////////////////////////////
	//finding 'nNumOfLensOfSquareTot_Actualf'
	nNumOfLensOfSquareTot_Actualf = 0;

	nLenOfSquareCurf = nDim_2pNf / nDivisorForLenOfSquareMax_Init_La; //nDivisorForLenOfSquareMax_Init_La == 2
	for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)
	{
		nLenOfSquareCurf = nLenOfSquareCurf / 2; //divisible by 2

		if (nLenOfSquareCurf == nLenOfSquareMin_Laf)
		{
			nNumOfLensOfSquareTot_Actualf = iLenOfSquaref + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);

			fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': iLenOfSquaref = %d, nNumOfLensOfSquareTot_Actualf = %d",
				iLenOfSquaref, nNumOfLensOfSquareTot_Actualf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			break;
		} //if (nLenOfSquareCurf == 1)

	} //for (iLenOfSquaref = 0; iLenOfSquaref < nDim_2pNf; iLenOfSquaref++)

	if (nNumOfLensOfSquareTot_Actualf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");

		fprintf(fout_lr, "\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfLensOfSquareTot_Actualf == 0");
		printf("\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d", nDim_2pNf, nLenOfSquareMin_Laf);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfLensOfSquareTot_Actualf == 0)
///////////////////////////////////////

	if (nNumOfOneFea_Adjusted_1f < nNumOfLenOfSquareMax_La)
	{
		nNumTypeOfFeaf = 1; // 1st moment
		nNumOfOneFea_Adjusted_2f = nNumOfOneFea_Adjusted_1f;
	} // 
	else if (nNumOfOneFea_Adjusted_1f >= nNumOfLenOfSquareMax_La && nNumOfOneFea_Adjusted_1f < 2 * nNumOfLenOfSquareMax_La)
	{
		nNumTypeOfFeaf = 2; // 2nd moment
		nNumOfOneFea_Adjusted_2f = nNumOfOneFea_Adjusted_1f - nNumOfLenOfSquareMax_La;
	}//
	//else if (nNumOfOneFea_Adjusted_1f >= 3 * nNumOfLenOfSquareMax_La && nNumOfOneFea_Adjusted_1f < 3 * nNumOfLenOfSquareMax_La)
	else if (nNumOfOneFea_Adjusted_1f >= 2 * nNumOfLenOfSquareMax_La && nNumOfOneFea_Adjusted_1f < 3 * nNumOfLenOfSquareMax_La)
	{
		nNumTypeOfFeaf = 3; // lacunarity
		nNumOfOneFea_Adjusted_2f = nNumOfOneFea_Adjusted_1f - (2 * nNumOfLenOfSquareMax_La);
	} // 
	else if (nNumOfOneFea_Adjusted_1f >= 3 * nNumOfLenOfSquareMax_La) // && nNumOfOneFea_Adjusted_1f < 3 * nNumOfLenOfSquareMax_La)
	{
		nNumTypeOfFeaf = 4; // probability
		nNumOfOneFea_Adjusted_2f = nNumOfOneFea_Adjusted_1f - (3 * nNumOfLenOfSquareMax_La);
	} //
/*
	nResf = LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(

		nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquareCurf)*(nDim_2pNf / nLenOfSquareCurf)

		nDim_2pNf, //const int nDim_2pNf,

		nNumOfLensOfSquareTot_Actualf, //const int nNumOfLensOfSquareTot_Actualf,

		nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

		fFirstMoment_Arrf, //float fFirstMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]
		fSecondMoment_Arrf, //float fSecondMoment_Arrf[], //[nNumOfLensOfSquareTot_Actualf]

		fLacunarity_Arrf); // float fLacunarity_Arrf[]); //[nNumOfLensOfSquareTot_Actualf]
*/

	nResf = OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare(
		nNumTypeOfFeaf, //const int nNumTypeOfFeaf, // 1 -- 1st moment, 2 -- 2nd moment, 3 -- lacunarity, 4 -- probability

		nNumOfOneFea_Adjusted_2f, //const int nNumOfOneFea_Adjusted_2f,
		nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		nDim_2pNf, //const int nDim_2pNf,

		nNumOfLensOfSquareTot_Actualf, //const int nNumOfLensOfSquareTot_Actualf,

		nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		fOneFea_Lacunar_AtIntensity_Rangef); // float &fOneFea_Lacunar_AtIntensity_Rangef); //

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': 'OneFea_LacunarityOfMassesOfAll_ObjectSquares_ForAll_LensOfSquare'");

		fprintf(fout_lr, "\n nDim_2pNf = %d, nLenOfSquareMin_Laf = %d, nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d", nDim_2pNf, nLenOfSquareMin_Laf, nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Ran  nDim_2pNf = %d, nLenOfSquareMin_Laf = %d, nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d",
			nDim_2pNf, nLenOfSquareMin_Laf, nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

///////////////////////////////////////////////////////////////
//fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

/*
//fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf, //float fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[], //[nNumOfLensOfSquareTot_Actualf*nNumOfSquareOccurrence_Intervals_Laf]

	for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
	{
		for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)
		{
			//nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLenOfSquareMax_La);
			nIndexOfSquaref = iLenOfSquaref + (iNumOfSquaresf * nNumOfLensOfSquareTot_Actualf);

			fFeaAll_AtIntensity_Range_Arrf[nFeaf] = fProbOfAll_Mass_Intervals_ForAllLensOfSquare_Arrf[nIndexOfSquaref];

			nFeaf += 1;
		} //for (iLenOfSquaref = 0; iLenOfSquaref < nNumOfLensOfSquareTot_Actualf; iLenOfSquaref++)

	}//for (iNumOfSquaresf = 0; iNumOfSquaresf < nNumOfSquareOccurrence_Intervals_Laf; iNumOfSquaresf++)
*/
///////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);
	printf("\n nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d, fOneFea_Lacunar_AtIntensity_Rangef = %E", nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f, fOneFea_Lacunar_AtIntensity_Rangef);


	fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': nNumOfSquareOccurrence_Intervals_Laf = %d, nDim_2pNf = %d nNumOfLensOfSquareTot_Actualf = %d, nLenOfSquareMin_Laf = %d",
		nNumOfSquareOccurrence_Intervals_Laf, nDim_2pNf, nNumOfLensOfSquareTot_Actualf, nLenOfSquareMin_Laf);
	fprintf(fout_lr, "\n nNumTypeOfFeaf = %d, nNumOfOneFea_Adjusted_2f = %d, fOneFea_Lacunar_AtIntensity_Rangef = %E", nNumTypeOfFeaf, nNumOfOneFea_Adjusted_2f, fOneFea_Lacunar_AtIntensity_Rangef);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

#ifndef COMMENT_OUT_ALL_PRINTS	
	printf("\n\n The end of 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'");
	fprintf(fout_lr, "\n\n The end of 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range'"); fflush(fout_lr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	//printf("\n\n The end of 'OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range': please press any key"); fflush(fout_lr); getchar();

	return SUCCESSFUL_RETURN;
} //int OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(...


/////////////////////////////////////////////////////////////////////////////////////////

int OneFea_LacunarityOfMasses_At_All_Intensity_Ranges(
	const int nNumOfLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

	const int nNumOfSquareOccurrence_Intervals_Laf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)
	//const int nDim_2pNf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
	const int nLenOfSquareMin_Laf,

	const COLOR_IMAGE *sColor_ImageInitf,

	//float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot
	float &fOne_Lacunar_Feaf)
{
	int Dim_2powerN(
		const int nLengthf,
		const int nWidthf,

		int &nScalef,

		int &nDim_2pNf);

	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForLacunarity(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(
		const int nNumOfOneFea_Adjusted_1f,
		const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf, //>=
		const int nThresholdForIntensitiesMaxf, // <

		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,
		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	///////////////////////////////////////
		//float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] =
		float &fOneFea_Lacunar_AtIntensity_Rangef, //

		int &nNumOfLensOfSquareTot_Actualf);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax_La + 1) / nNumOfIntensity_IntervalsForLacunar, // 8

		nScalef,
		nDim_2pNf,

		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf,

		nNumOfLensOfSquareTot_Actualf,

		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nIntensitiesFoundf = 0,

		nIndexOfIntensitiesCurf = nNumOfLacunarityFeaf / nNumOfFeasForLacunar_AtIntensity_Range,

		nNumOfOneFea_Adjusted_1f,
		nNumOfIntensity_IntervalsForLacunarTotf;

	float
		fOneFea_Lacunar_AtIntensity_Rangef = -fLarge;
	//fFeaAll_AtIntensity_Range_Arrf[nNumOfFeasForLacunar_AtIntensity_Range];
/////////////////////////////

/////////////////////////////////////////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForLacunarTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////
	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_ForLacunarityBlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)
///////////////////////////////////////////////////////////////////////////////////////

	nNumOfIntensity_IntervalsForLacunarTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForLacunarTotf += 1;

			if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				printf("\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nNumOfIntensity_IntervalsForLacunarTotf = %d > nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfIntensity_IntervalsForLacunarTotf, nNumOfIntensity_IntervalsForLacunarTot);

				printf("\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

				delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForLacunarTotf > nNumOfIntensity_IntervalsForLacunarTot)
///////////////////////////////////////////////////////////////////////

			//nNumOfLacunarityFeaf/ nNumOfFeasForLacunar_AtIntensity_Range,
			if (nNumOfIntensity_IntervalsForLacunarTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nNumOfOneFea_Adjusted_1f = nNumOfLacunarityFeaf - (nIndexOfIntensitiesCurf*nNumOfFeasForLacunar_AtIntensity_Range);

				if (nNumOfOneFea_Adjusted_1f < 0 || nNumOfOneFea_Adjusted_1f > nNumOfFeasForLacunar_AtIntensity_Range - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':  nNumOfOneFea_Adjusted_1f = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d || ...",
						nNumOfOneFea_Adjusted_1f, nNumOfFeasForLacunar_AtIntensity_Range);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjusted_1f = %d >= nNumOfFeasForLacunar_AtIntensity_Range = %d || ...",
						nNumOfOneFea_Adjusted_1f, nNumOfFeasForLacunar_AtIntensity_Range);

					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

					delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

					return UNSUCCESSFUL_RETURN;
				}//if (nNumOfOneFea_Adjusted_1f < 0 || nNumOfOneFea_Adjusted_1f > nNumOfFeasForLacunar_AtIntensity_Range - 1)

				goto MarkExitOf_2LoopsInOneFea_Lacunar;

			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges':  the intensities have not been found");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': the intensities have not been found");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

		delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

///////////////////////////////////////
	//nNumOfIntensity_IntervalsForLacunarTotf = 0;
	//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)
	{
	MarkExitOf_2LoopsInOneFea_Lacunar:		nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

		//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)
	{
		//////////////////////////////////////////////////////////////////////
		nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS	
		fprintf(fout_lr, "\n\n 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nIntensity_Interval_1f = %d, nThresholdForIntensitiesMinf = %d, sWeightsOfColorsf->fWeightOfBlue = %E",
			nIntensity_Interval_1f, nThresholdForIntensitiesMinf, sWeightsOfColorsf->fWeightOfBlue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nResf = Embedding_Image_Into_2powerN_ForHausdorff(
			nDim_2pNf, //const int nDim_2pNf,

			nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
			nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

			sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
			&sImageEmbeddedf_ForLacunarityBlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges' by 'LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);

#ifndef COMMENT_OUT_ALL_PRINTS

			fprintf(fout_lr, "\n\n An error in 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': nScalef = %d", nScalef);
			printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

	///	fprintf(fout_lr, "\n\n2: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
		//	sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

/*
			nResf = LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

				nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf, //>=
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf, // <

				nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			///////////////////////////////////////
				fFeaAll_AtIntensity_Range_Arrf, // float fFeaAll_AtIntensity_Range_Arrf[], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

				nNumOfLensOfSquareTot_Actualf); // int &nNumOfLensOfSquareTot_Actualf);
*/
		nResf = OneFea_LacunarityOfMasses_ForAll_LensOfSquare_AtIntensity_Range(

			nNumOfOneFea_Adjusted_1f, // const int nNumOfOneFea_Adjusted_1f,
			nNumOfSquareOccurrence_Intervals_Laf, //const int nNumOfSquareOccurrence_Intervals_Laf,//<= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

			nDim_2pNf, //const int nDim_2pNf,

			nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf, //>=
			nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf, // <

			nLenOfSquareMin_Laf, //const int nLenOfSquareMin_Laf,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
			&sImageEmbeddedf_ForLacunarityBlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		///////////////////////////////////////
			fOneFea_Lacunar_AtIntensity_Rangef, // float &fOneFea_Lacunar_AtIntensity_Rangef,

			nNumOfLensOfSquareTot_Actualf); // int &nNumOfLensOfSquareTot_Actualf

////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	

		fprintf(fout_lr, "\n\n nNumOfIntensity_IntervalsForLacunarTotf = %d, nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d",
			nNumOfIntensity_IntervalsForLacunarTotf, nIntensity_Interval_1f, nIntensity_Interval_2f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		/*
					nTempf = (nNumOfIntensity_IntervalsForLacunarTotf - 1) * nNumOfFeasForLacunar_AtIntensity_Range;
					for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
					{
						nFea_Curf = iFeaf + nTempf;

		#ifndef COMMENT_OUT_ALL_PRINTS
						//fprintf(fout_lr, "\n fFeaAll_AtIntensity_Range_Arrf[%d] = %E, nFea_Curf = %d, nTempf = %d", iFeaf, fFeaAll_AtIntensity_Range_Arrf[iFeaf], nFea_Curf, nTempf);
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)
						{
		#ifndef COMMENT_OUT_ALL_PRINTS
							printf("\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d", nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
							fprintf(fout_lr, "\n\n An error in 'LacunarityOfMasses_At_All_Intensity_Ranges': nFea_Curf = %d >= nNumOfLacunarFeasFor_OneDimTot = %d",
								nFea_Curf, nNumOfLacunarFeasFor_OneDimTot);
							printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							return UNSUCCESSFUL_RETURN;
						}//if (nFea_Curf >= nNumOfLacunarFeasFor_OneDimTot)

						//fOneDim_Lacunar_Arrf[nFea_Curf] = fFeaAll_AtIntensity_Range_Arrf[iFeaf];

		#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n fOneFea_Lacunar_AtIntensity_Rangef = %E, nFea_Curf = %d, nTempf = %d", fOneFea_Lacunar_AtIntensity_Rangef, nFea_Curf, nTempf);
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					}//for (iFeaf = 0; iFeaf < nNumOfFeasForLacunar_AtIntensity_Range; iFeaf++)
		*/

	} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForLacunar; iIntensity_Interval_1f++)

#ifndef COMMENT_OUT_ALL_PRINTS	
	fprintf(fout_lr, "\n\n The end of 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges': fOneFea_Lacunar_AtIntensity_Rangef = %E, nNumOfLensOfSquareTot_Actualf = %d",
		fOneFea_Lacunar_AtIntensity_Rangef, nNumOfLensOfSquareTot_Actualf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	fOne_Lacunar_Feaf = fOneFea_Lacunar_AtIntensity_Rangef;
	delete[] sImageEmbeddedf_ForLacunarityBlackWhitef.nEmbeddedImage_Arr;

	return SUCCESSFUL_RETURN;
}//int OneFea_LacunarityOfMasses_At_All_Intensity_Ranges(

//////////////////////////////////////////////////////////////////////////////
int doOne_Fea_OfLacunarityOf_ColorImage(
	const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

	const Image& image_in,

	const COLOR_IMAGE *sColor_Imagef,
	float &fOne_SelectedLacunar_Feaf) //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

{
	int SetOfLogPoints_ForFractal_Dim(
		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		int &nNumOfLogPointsf,
		float fNegLogOfLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max_La]

		float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max_La]

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity, https://aip.scitation.org/doi/full/10.1063/1.4966539
	int OneFea_LacunarityOfMasses_At_All_Intensity_Ranges(
		const int nNumOfLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

		const int nNumOfSquareOccurrence_Intervals_Laf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		const int nLenOfSquareMin_Laf,

		const COLOR_IMAGE *sColor_ImageInitf,

		///////////////////////////////////////
		float &fOne_Lacunar_Feaf);
	//float fOneDim_Lacunar_Arrf[]); //[nNumOfLacunarFeasFor_OneDimTot] //nNumOfLacunarFeasFor_OneDimTot = nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,
		i,
		//		j,

				//		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,
		nNumOfLogPointsf,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		//		nNumOfLensOfSquareTot_Actualf, //
		nImageWidth,
		nImageHeight;

	float
		fSlope,//
		fNegLogOfLenOfSquare_Arr[nNumOfIters_ForDim_2powerN_Max_La], //[]

		fLogPoints_Arr[nNumOfIters_ForDim_2powerN_Max_La];

	////////////////////////////////////////////////////////////////////////

// size of image
//	nImageWidth = image_in.width();
	//nImageHeight = image_in.height();
	nImageWidth = sColor_Imagef->nWidth; //image_in.width();
	nImageHeight = sColor_Imagef->nLength; //image_in.height();


#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax_La || nImageWidth < nLenMin_La)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax_La || nImageWidth < nLenMin_La)

	if (nImageHeight > nWidMax_La || nImageHeight < nWidMin_La)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax_La || nImageHeight < nWidMin_La)

	int bytesOfWidth = image_in.pitchInBytes();

	//Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	nSizeOfImage = nImageWidth * nImageHeight;

	//////////////////////////////////////////////////////////////////////////
	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr_La;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr_La;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr_La;

	nRes = SetOfLogPoints_ForFractal_Dim(
		nThresholdForIntensities_ForFractalDimMin_La, //const int nThresholdForIntensitiesMinf,
		nThresholdForIntensities_ForFractalDimMax_La, //const int nThresholdForIntensitiesMaxf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
		sColor_Imagef, //const COLOR_IMAGE *sColor_ImageInitf,

		nNumOfLogPointsf, //int &nNumOfLogPointsf,
		fNegLogOfLenOfSquare_Arr, //int nLenOfSquare_Arrf[], //[nNumOfIters_ForDim_2powerN_Max_La]

		fLogPoints_Arr); // float fLogPoints_Arrf[]); // [nNumOfIters_ForDim_2powerN_Max_La]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doOne_Fea_OfLacunarityOf_ColorImage' by 'SetOfLogPoints_ForFractal_Dim'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doOne_Fea_OfLacunarityOf_ColorImage' by 'SetOfLogPoints_ForFractal_Dim'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key:"); getchar();
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	fprintf(fout_lr, "\n\n  After 'SetOfLogPoints_ForFractal_Dim': nNumOfLogPointsf = %d", nNumOfLogPointsf);
	//getchar();

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	///////////////////////////////////////////////////////
	for (i = 0; i < nNumOfLogPointsf; i++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 2: i = %d, fNegLogOfLenOfSquare_Arr[i] = %E, fLogPoints_Arr[i] = %E", i, fNegLogOfLenOfSquare_Arr[i], fLogPoints_Arr[i]);
		fprintf(fout_lr, "\n\n 'SetOfLogPoints_ForFractal_Dim'  2: i = %d, fNegLogOfLenOfSquare_Arr[i] = %E, fLogPoints_Arr[i] = %E", i, fNegLogOfLenOfSquare_Arr[i], fLogPoints_Arr[i]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//for (i = 0; i < nDimf; i++)

	SlopeOfAStraightLine(
		nNumOfLogPointsf, //const int nDimf,

		fNegLogOfLenOfSquare_Arr, //const float fX_Arrf[],
		fLogPoints_Arr, //const float fY_Arrf[],

		fSlope); // float &fSlopef);
/////////////////////////////////////////////////////////////////////

	nRes = OneFea_LacunarityOfMasses_At_All_Intensity_Ranges(
		nNumOfSelectedLacunarityFeaf, //const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

		nNumOfSquareOccurrence_Intervals_La, //const int nNumOfSquareOccurrence_Intervals_Laf,// == 4 <= nNumOfSquaresInImageTotf = (nDim_2pNf / nLenOfSquaref)*(nDim_2pNf / nLenOfSquaref)

		//const int nDim_2pNf,

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
		nLenOfSquareMin_La, //const int nLenOfSquareMin_Laf,

		sColor_Imagef, //const COLOR_IMAGE *sColor_ImageInitf,

		///////////////////////////////////////
		fOne_SelectedLacunar_Feaf); //float &fOne_Lacunar_Feaf);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doOne_Fea_OfLacunarityOf_ColorImage' by 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges'");
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doOne_Fea_OfLacunarityOf_ColorImage' by 'OneFea_LacunarityOfMasses_At_All_Intensity_Ranges'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key:"); getchar();

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

//////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doOne_Fea_OfLacunarityOf_ColorImage(...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cooc feas
int Initializing_Grayscale_Image(
	//const int nImageWidthf,
	//const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef,
	GRAYSCALE_IMAGE *sGrayscale_Imagef)
{
	int
		nIndexOfPixelCurf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,
		iWidf,
		iLenf,

		nRedf,
		nGreenf,
		nBluef,

		nImageSizeCurf = nImageWidthf * nImageLengthf;

	sGrayscale_Imagef->nWidth = nImageWidthf;
	sGrayscale_Imagef->nLength = nImageLengthf;

	//printf("\n Initializing_Grayscale_Image: 1, please press any key"); getchar();

	////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n ///////////////////////////////////////////////////////////////////");
	printf("\n 'Initializing_Grayscale_Image': nImageWidthf = %d, nImageLengthf = %d\n", nImageWidthf, nImageLengthf);

	//fprintf(fout_lr, "\n\n ///////////////////////////////////////////////////////////////////");
	//fprintf(fout_lr, "\n 'Initializing_Grayscale_Image':\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	sGrayscale_Imagef->nPixelArr = new int[nImageSizeCurf];

	if (sGrayscale_Imagef->nPixelArr == nullptr)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		//fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		printf("\n\n Please press any key:"); getchar();
		//#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (sGrayscale_Imagef->nPixelArr == nullptr)

	//printf("\n Initializing_Grayscale_Image: 2, please press any key"); getchar();

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//printf("\n iWidf = %d, nImageWidthf = %d", iWidf, nImageWidthf); getchar();
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			//the average of colors
			sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef) / 3;
			/*
						if (nRedf > sColor_Imagef->nIntensityOfBackground_Red || nGreenf > sColor_Imagef->nIntensityOfBackground_Green ||
							nBluef > sColor_Imagef->nIntensityOfBackground_Blue)
						{
							sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = nRedf; // 1; // nGrayLevelIntensityMax; // nRed;

						} //if (nRed > sColor_Image.nIntensityOfBackground_Red || nGreen > sColor_Image.nIntensityOfBackground_Green ||
						else
						{
							sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = 0; //background
						}//else
			*/
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	//printf("\n Initializing_Grayscale_Image: the end, please press any key"); getchar();
	return SUCCESSFUL_RETURN;
}//int Initializing_Grayscale_Image(...
//////////////////////////////////////////////////////////////////////////////

void Initializing_Cooc_OrigImage(

	const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
						   //1--0 degrees, 2--45 degrees, 3--90 degrees

	const int nDistOfCoocf, //distance of cooccurrence

	COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef)
{

	int
		iFeaf;
	///////////////////////////////////////////////
	sCooc_Of_Orig_Imagef->nIndicOfDirection = nIndicOfDirectionf;

	sCooc_Of_Orig_Imagef->nDistOfCooc = nDistOfCoocf;

	sCooc_Of_Orig_Imagef->nLen_IntensityCoocCur = nGrayLevelIntensityMax;

	sCooc_Of_Orig_Imagef->nWid_IntensityCoocCur = nGrayLevelIntensityMax;

	for (iFeaf = 0; iFeaf < nCoocSizeMax; iFeaf++)
	{
		sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[iFeaf] = 0;

		sCooc_Of_Orig_Imagef->fAverOfCoocArr[iFeaf] = 0.0;
	} //for (iFeaf = 0; iLenScaleCoocf < nGrayIntensity_ScaleCoocMaxf; iLenScaleCoocf++)

		//return SUCCESSFUL_RETURN;
} //void Initializing_Cooc_OrigImage(...
//////////////////////////////////////////////////////////////////////////////

int CoocOfOrigImage(
	const int nIndicOfDirectionf, // <= nNumOfAnglesTot == 8

	const int nDistOfCoocf,

	const GRAYSCALE_IMAGE *sGrayscale_Imagef,
	COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef) //[nCoocSizeMax]
{
	int
		nImageWidthf = sGrayscale_Imagef->nWidth,
		nImageLengthf = sGrayscale_Imagef->nLength,

		nPositionWidInitf,
		nPositionLenInitf,

		nPositionWidFinalf,
		nPositionLenFinalf,

		//Position of the left (destination) point
		nPositionWid_Leftf,
		nPositionLen_Leftf,

		//Position of the right (destination) point
		nPositionWid_Rightf,
		nPositionLen_Rightf,

		iLenf,
		iWidf,
		iWidIntensityf,
		iLenIntensityf,

		nIndexPositionInImage_Leftf,
		nIndexPositionInImage_Rightf,

		////////////////////////////////////////////////////
		nIntensity_Leftf,

		nIntensity_Rightf,

		nIndexPositionOfIntensityf,

		nAdjustedProdOfLenAndWidf,

		nNumOfValidObjectPixelsTotf = 0,
		nProdOfLenAndWidf,

		//nIndexPositionOfIntensity_Rightf,

		nSumf = 0;
	///////////////////////////////////////////////////////////////////////////

	nProdOfLenAndWidf = nImageLengthf * nImageWidthf;
	//////////////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n /////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'CoocOfOrigImage': nIndicOfDirectionf = %d, nDistOfCoocf = %d, nImageLengthf = %d, nImageWidthf = %d",
		nIndicOfDirectionf, nDistOfCoocf, nImageLengthf, nImageWidthf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//	fprintf(fout_PrintFeatures, "\n\n /////////////////////////////////////////////////////////////");
		//fprintf(fout_PrintFeatures, "\n 'CoocOfOrigImage': nIndicOfDirectionf = %d, nDistOfCoocf = %d, nImageLengthf = %d, nImageWidthf = %d",
			//nIndicOfDirectionf, nDistOfCoocf, nImageLengthf, nImageWidthf);

		/*//already specified in 'Initializing_Cooc_OrigImage'

			sCooc_Of_Orig_Imagef->nIndicOfDirection = nIndicOfDirectionf;
			sCooc_Of_Orig_Imagef->nDistOfCooc = nDistOfCoocf;

			sCooc_Of_Orig_Imagef->nLen_IntensityCoocCur = nGrayLevelIntensityMax;

			sCooc_Of_Orig_Imagef->nWid_IntensityCoocCur = nGrayLevelIntensityMax;
		*/

	if (nIndicOfDirectionf < nIndicOfDirectionMin || nIndicOfDirectionf > nIndicOfDirectionMax ||
		nDistOfCoocf < nDistOfCoocMin || nDistOfCoocf > nDistOfCoocMax)
	{
		printf("\n\n An error in 'CoocOfOrigImage': nIndicOfDirectionf = %d or nDistOfCoocf = %d is wrong",
			nIndicOfDirectionf, nDistOfCoocf);
		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nIndicOfDirectionf = %d or nDistOfCoocf = %d is wrong",
			nIndicOfDirectionf, nDistOfCoocf);	fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return UNSUCCESSFUL_RETURN; //-1;
	} //if (nIndicOfDirectionf < nIndicOfDirectionMin ||...


	if (nImageWidthf < nWidMin || nImageLengthf < nLenMin)
	{
		printf("\n\n An error in 'CoocOfOrigImage': nImageWidthf = %d or nImageLengthf = %d is wrong",
			nImageWidthf, nImageLengthf);
		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nImageWidthf = %d or nImageLengthf = %d is wrong",
			nImageWidthf, nImageLengthf);

		fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN; // -1;
	} //if (nImageWidthf < nWidMin || ...

	  ///////////////////////////////////////////////////////////////////////
	nPositionLenInitf = 0; //always
	if (nIndicOfDirectionf == 0) // (-45 degrees)
	{
		/*
				nPositionWidInitf = nDistOfCoocf;

				nPositionWidFinalf = nImageWidthf;

				nPositionLenFinalf = nImageLengthf - nDistOfCoocf;
		*/
		nPositionWidInitf = 0;

		nPositionWidFinalf = nImageWidthf - nDistOfCoocf;

		nPositionLenFinalf = nImageLengthf - nDistOfCoocf;

		nAdjustedProdOfLenAndWidf = (nImageWidthf - nDistOfCoocf)*(nImageLengthf - nDistOfCoocf);

	} //
	else if (nIndicOfDirectionf == 1) // (0 degrees; horizontal)
	{
		nPositionWidInitf = 0;

		nPositionWidFinalf = nImageWidthf;

		nPositionLenFinalf = nImageLengthf - nDistOfCoocf;
		nAdjustedProdOfLenAndWidf = (nImageWidthf)*(nImageLengthf - nDistOfCoocf);

	} //
	else if (nIndicOfDirectionf == 2) //(45 degrees)
	{
		/*
				nPositionWidInitf = 0;

				nPositionWidFinalf = nImageWidthf - nDistOfCoocf;

				nPositionLenFinalf = nImageLengthf - nDistOfCoocf;
		*/
		nPositionWidInitf = nDistOfCoocf;

		nPositionWidFinalf = nImageWidthf;

		nPositionLenFinalf = nImageLengthf - nDistOfCoocf;

		nAdjustedProdOfLenAndWidf = (nImageWidthf - nDistOfCoocf)*(nImageLengthf - nDistOfCoocf);

	} //
	else if (nIndicOfDirectionf == 3) // (90 degrees)
	{
		nPositionWidInitf = 0;

		nPositionWidFinalf = nImageWidthf - nDistOfCoocf;

		nPositionLenFinalf = nImageLengthf;
		nAdjustedProdOfLenAndWidf = (nImageWidthf - nDistOfCoocf)*(nImageLengthf);

	} //
/////////////////////////////////////////////////////

	  //Initialization
	for (iWidIntensityf = 0; iWidIntensityf < nGrayLevelIntensityMax; iWidIntensityf++)
	{

		for (iLenIntensityf = 0; iLenIntensityf < nGrayLevelIntensityMax; iLenIntensityf++)
		{
			nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);

			sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[nIndexPositionOfIntensityf] = 0;

			sCooc_Of_Orig_Imagef->fAverOfCoocArr[nIndexPositionOfIntensityf] = 0.0;
		} //for (iLenIntensityf = 0; iLenIntensityf < nGrayLevelIntensityMax; iLenIntensityf++)

	} //for (iWidIntensityf = 0; iWidIntensityf < nGrayLevelIntensityMax; iWidIntensityf++)

	if (nPositionWidFinalf <= 0 || nPositionLenFinalf <= 0 || nPositionWidFinalf <= nPositionWidInitf || nPositionLenFinalf <= nPositionLenInitf)
	{
		fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage':  nPositionWidFinalf = %d <= 0 || nPositionLenFinalf = %d <= 0 || ..., nPositionWidInitf = %d, nPositionLenInitf = %d",
			nPositionWidFinalf, nPositionLenFinalf, nPositionWidInitf, nPositionLenInitf);
		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'CoocOfOrigImage': exit by nPositionWidFinalf = %d <= 0 || nPositionLenFinalf = %d <= 0 || ...", nPositionWidFinalf, nPositionLenFinalf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//return EXIT_FOR_IMAGE_SIZE_CONSTRAINTS;
		return UNSUCCESSFUL_RETURN;
	} //if (nPositionWidFinalf <= 0 || nPositionLenFinalf <= 0 || ...)
/////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'CoocOfOrigImage': nPositionWidInitf = %d, nPositionWidFinalf = %d, nPositionLenInitf = %d, nPositionLenFinalf = %d",
		nPositionWidInitf, nPositionWidFinalf, nPositionLenInitf, nPositionLenFinalf);

	fprintf(fout_lr, "\n\n 'CoocOfOrigImage': nPositionWidInitf = %d, nPositionWidFinalf = %d, nPositionLenInitf = %d, nPositionLenFinalf = %d",
		nPositionWidInitf, nPositionWidFinalf, nPositionLenInitf, nPositionLenFinalf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//	fprintf(fout_PrintFeatures, "\n\n 'CoocOfOrigImage': nPositionWidInitf = %d, nPositionWidFinalf = %d, nPositionLenInitf = %d, nPositionLenFinalf = %d\n",
		//	nPositionWidInitf, nPositionWidFinalf, nPositionLenInitf, nPositionLenFinalf);

		///////////////////////////////////////////////////////////////////
	for (iWidf = nPositionWidInitf; iWidf < nPositionWidFinalf; iWidf++)
	{
		for (iLenf = nPositionLenInitf; iLenf < nPositionLenFinalf; iLenf++)
		{
			//reading intensity of the left point first
			nPositionWid_Leftf = iWidf;
			nPositionLen_Leftf = iLenf;

			//enumerating length first at a fixed width
			nIndexPositionInImage_Leftf = nPositionLen_Leftf + nPositionWid_Leftf * nImageLengthf;

			if (nIndexPositionInImage_Leftf < 0 || nIndexPositionInImage_Leftf >= nProdOfLenAndWidf)
			{
				printf("\n\n An error in 'CoocOfOrigImage': nIndexPositionInImage_Leftf = %d >= nProdOfLenAndWidf = %d, iWidf = %d and iLenf = %d",
					nIndexPositionInImage_Leftf, nProdOfLenAndWidf, iWidf, iLenf);
				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nIndexPositionInImage_Leftf = %d >= nProdOfLenAndWidf = %d, iWidf = %d and iLenf = %d",
					nIndexPositionInImage_Leftf, nProdOfLenAndWidf, iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN; //-1;
			} // if (nIndexPositionInImage_Leftf < 0 || nIndexPositionInImage_Leftf >= nProdOfLenAndWidf)

			nIntensity_Leftf = sGrayscale_Imagef->nPixelArr[nIndexPositionInImage_Leftf];

			if (nIntensity_Leftf < 0 || nIntensity_Leftf > nGrayLevelIntensityMax)
			{
				printf("\n\n An error in 'CoocOfOrigImage': nIntensity_Leftf = %d at iWidf = %d and iLenf = %d",
					nIntensity_Leftf, iWidf, iLenf);
				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nIntensity_Leftf = %d at iWidf = %d and iLenf = %d",
					nIntensity_Leftf, iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN; // -1;
			} //if (nIntensity_Leftf < 0 || nIntensity_Leftf > nGrayLevelIntensityMax)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'CoocOfOrigImage': iLenf = %d, iWidf = %d, nIndexPositionInImage_Leftf = %d, nIntensity_Leftf = %d",
				iLenf, iWidf, nIndexPositionInImage_Leftf, nIntensity_Leftf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//	fprintf(fout_PrintFeatures, "\n\n 'CoocOfOrigImage' left: nPositionLen_Leftf = %d, nPositionWid_Leftf = %d, nIndexPositionInImage_Leftf = %d, nIntensity_Leftf = %d",
				//	nPositionLen_Leftf, nPositionWid_Leftf, nIndexPositionInImage_Leftf, nIntensity_Leftf);

			if (nIntensity_Leftf == nIntensityOfBackground)
			{
				//fprintf(fout_PrintFeatures, "\n\n 'CoocOfOrigImage' left: 'continue' by 'nIntensity_Leftf == nIntensityOfBackground' "); 
				//fprintf(fout_PrintFeatures, "\n iWidf = %d, nPositionWidInitf = %d, nPositionWidFinalf = %d, iLenf = %d, nPositionLenInitf = %d, nPositionLenFinalf = %d\n",
					//iWidf,nPositionWidInitf, nPositionWidFinalf, iLenf,nPositionLenInitf, nPositionLenFinalf);

				continue;
			} //
			////////////////////////////////////////////////////////////////////
			if (nIndicOfDirectionf == 0) // (-45 degrees)
			{
				/*
								nPositionWid_Rightf = iWidf - nDistOfCoocf;
								nPositionLen_Rightf = iLenf + nDistOfCoocf;
				*/
				nPositionWid_Rightf = iWidf + nDistOfCoocf;
				nPositionLen_Rightf = iLenf + nDistOfCoocf;
			} //
			else if (nIndicOfDirectionf == 1) // (0 degrees; horizontal)
			{
				nPositionWid_Rightf = iWidf; // the same horizontal position
				nPositionLen_Rightf = iLenf + nDistOfCoocf;

			} //
			else if (nIndicOfDirectionf == 2) //(45 degrees)
			{
				/*
								nPositionWid_Rightf = iWidf + nDistOfCoocf;
								nPositionLen_Rightf = iLenf + nDistOfCoocf;
				*/
				nPositionWid_Rightf = iWidf - nDistOfCoocf;
				nPositionLen_Rightf = iLenf + nDistOfCoocf;
			} //
			else if (nIndicOfDirectionf == 3) // (90 degrees)
			{
				nPositionWid_Rightf = iWidf + nDistOfCoocf;
				nPositionLen_Rightf = iLenf; // the same vertical position

			} //else if (nIndicOfDirectionf == 3) // (90 degrees)

			  /////////////////////////////////////////////////////

	//reading intensity of the right point second
			nIndexPositionInImage_Rightf = nPositionLen_Rightf + nPositionWid_Rightf * nImageLengthf;

			if (nIndexPositionInImage_Rightf < 0 || nIndexPositionInImage_Rightf >= nProdOfLenAndWidf)
			{
				printf("\n\n An error in 'CoocOfOrigImage': nIndexPositionInImage_Rightf = %d >= nProdOfLenAndWidf = %d, iWidf = %d and iLenf = %d",
					nIndexPositionInImage_Rightf, nProdOfLenAndWidf, iWidf, iLenf);

				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nIndexPositionInImage_Rightf = %d >= nProdOfLenAndWidf = %d, iWidf = %d and iLenf = %d",
					nIndexPositionInImage_Rightf, nProdOfLenAndWidf, iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN; //-1;
			} // if (nIndexPositionInImage_Rightf < 0 || nIndexPositionInImage_Rightf >= nProdOfLenAndWidf)

			nIntensity_Rightf = sGrayscale_Imagef->nPixelArr[nIndexPositionInImage_Rightf];

			if (nIntensity_Rightf < 0 || nIntensity_Rightf > nGrayLevelIntensityMax)
			{
				printf("\n\n An error in 'CoocOfOrigImage': nIntensity_Rightf = %d at iWidf = %d and iLenf = %d",
					nIntensity_Rightf, iWidf, iLenf);
				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'CoocOfOrigImage': nIntensity_Rightf = %d at iWidf = %d and iLenf = %d",
					nIntensity_Rightf, iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN; // -1;
			} //if (nIntensity_Rightf < 0 || nIntensity_Rightf > nGrayLevelIntensityMax)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n iLenf = %d, iWidf = %d, nIndexPositionInImage_Rightf = %d, nIntensity_Rightf = %d",
				iLenf, iWidf, nIndexPositionInImage_Rightf, nIntensity_Rightf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//	fprintf(fout_PrintFeatures, "\n\n 'CoocOfOrigImage' right: nPositionLen_Rightf = %d, nPositionWid_Rightf = %d, nIndexPositionInImage_Leftf = %d, nIntensity_Leftf = %d",
				//	nPositionLen_Rightf, nPositionWid_Rightf, nIndexPositionInImage_Leftf, nIntensity_Leftf);

			if (nIntensity_Rightf == nIntensityOfBackground)
			{
				//fprintf(fout_PrintFeatures, "\n\n 'CoocOfOrigImage' right: 'continue' by 'nIntensity_Rightf == nIntensityOfBackground' ");
				//fprintf(fout_PrintFeatures, "\n iWidf = %d, nPositionWidInitf = %d, nPositionWidFinalf = %d, iLenf = %d, nPositionLenInitf = %d, nPositionLenFinalf = %d\n",
					//iWidf, nPositionWidInitf, nPositionWidFinalf, iLenf, nPositionLenInitf, nPositionLenFinalf);

				continue;
			} //

			nIndexPositionOfIntensityf = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);

			if (nIndexPositionOfIntensityf < 0 || nIndexPositionOfIntensityf >= nCoocSizeMax)
			{
				printf("\n\n An error in 'CoocOfOrigImage': nIndexPositionOfIntensityf = %d >= nCoocSizeMax = %d, iWidf = %d and iLenf = %d",
					nIndexPositionOfIntensityf, nCoocSizeMax, iWidf, iLenf);
				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in CoocOfOrigImage': nIndexPositionOfIntensityf = %d >= nCoocSizeMax = %d, iWidf = %d and iLenf = %d",
					nIndexPositionOfIntensityf, nCoocSizeMax, iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN; //-1;
			} // if (nIndexPositionOfIntensityf < 0 || nIndexPositionOfIntensityf >= nCoocSizeMax)

			nNumOfValidObjectPixelsTotf += 1;

			sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[nIndexPositionOfIntensityf] += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'CoocOfOrigImage': a new nNumOfValidObjectPixelsTotf = %d, iLenf = %d, iWidf = %d, sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[%d] = %d",
				nNumOfValidObjectPixelsTotf, iLenf, iWidf, nIndexPositionOfIntensityf, sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[nIndexPositionOfIntensityf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iLenf = nPositionLenInitf; iLenf < nPositionLenFinalf; iLenf++)

	} //for (iWidf = nPositionWidInitf; iWidf < nPositionWidFinalf; iWidf++)
	  ////////////////////////////////////////////////////////////////////////////////

	if (nNumOfValidObjectPixelsTotf <= 0)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n An error in 'CoocOfOrigImage': nNumOfValidObjectPixelsTotf = %d <= 0", nNumOfValidObjectPixelsTotf);
		printf("\n\n 'CoocOfOrigImage': exit by nNumOfValidObjectPixelsTotf = %d", nNumOfValidObjectPixelsTotf);

		fprintf(fout_lr, "\n\n 'CoocOfOrigImage': exit by nNumOfValidObjectPixelsTotf = %d", nNumOfValidObjectPixelsTotf);
		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//return EXIT_FOR_IMAGE_SIZE_CONSTRAINTS;
		//return UNSUCCESSFUL_RETURN;

		return (-2);
	} // if (nNumOfValidObjectPixelsTotf <= 0)

	  //The averages
	for (iWidIntensityf = 0; iWidIntensityf < nGrayLevelIntensityMax; iWidIntensityf++)
	{

		for (iLenIntensityf = 0; iLenIntensityf < nGrayLevelIntensityMax; iLenIntensityf++)
		{
			nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);

			sCooc_Of_Orig_Imagef->fAverOfCoocArr[nIndexPositionOfIntensityf] =
				(float)(sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[nIndexPositionOfIntensityf]) / (float)(nNumOfValidObjectPixelsTotf);

			//	fprintf(fout_lr, "\n 'CoocOfOrigImage': sCooc_Of_Orig_Imagef->fAverOfCoocArr[%d] = %E, iLenIntensityf = %d, iWidIntensityf = %d", 
				//	nIndexPositionOfIntensityf,sCooc_Of_Orig_Imagef->fAverOfCoocArr[nIndexPositionOfIntensityf], iLenIntensityf, iWidIntensityf);

		} //for (iLenIntensityf = 0; iLenIntensityf < nGrayLevelIntensityMax; iLenIntensityf++)

	} //for (iWidIntensityf = 0; iWidIntensityf < nGrayLevelIntensityMax; iWidIntensityf++)

	return SUCCESSFUL_RETURN; // 1;
} //int CoocOfOrigImage(...
////////////////////////////////////////////////////

int MeansAndStDev_OfRowsAndCols_OrigImage(
	const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

	float &fMeanForRowsf,
	float &fMeanForColsf,

	float &fStDevForRowsf,
	float &fStDevForColsf)
{
	int
		nWidCurf = sCooc_Of_Orig_Image->nWid_IntensityCoocCur, // == nGrayLevelIntensityMax
		nLenCurf = sCooc_Of_Orig_Image->nLen_IntensityCoocCur, // == nGrayLevelIntensityMax

		nIndexPositionOfIntensityf, // = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);
		iLenf,
		iWidf,
		nSumf = 0;


	fMeanForRowsf = 0;
	fMeanForColsf = 0;

	fStDevForRowsf = 0;
	fStDevForColsf = 0;

	for (iWidf = 0; iWidf < nWidCurf; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenCurf; iLenf++)
		{
			//nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);
			nIndexPositionOfIntensityf = iLenf + (iWidf * nGrayLevelIntensityMax); //or nWidCurf = nGrayLevelIntensityMax

			fMeanForRowsf += iWidf * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];
			fMeanForColsf += iLenf * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

		} // for (iLenf = 0; iLenf < nLenCurf; iLenf++)

	} //for (iWidf = 0; iWidf < nWidCurf; iWidf++)

	for (iWidf = 0; iWidf < nWidCurf; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenCurf; iLenf++)
		{
			nIndexPositionOfIntensityf = iWidf + (iLenf * nGrayLevelIntensityMax); //or nWidCurf = nGrayLevelIntensityMax

			fStDevForRowsf += (iWidf - fMeanForRowsf) * (iWidf - fMeanForRowsf) * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

			fStDevForColsf += (iLenf - fMeanForColsf) * (iLenf - fMeanForColsf) * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

		} // for (iLenf = 0; iLenf < nLenCurf; iLenf++)

	} //for (iWidf = 0; iWidf < nWidCurf; iWidf++)

	  //fflush(fout_lr);

	return SUCCESSFUL_RETURN;
} //int MeansAndStDev_OfRowsAndCols_OrigImage(...

int FeaturesOfOrigCooc_OrigImage(
	const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

	FEATURES_OF_COOC *sFeasOfCooc)
{

	int MeansAndStDev_OfRowsAndCols_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

		float &fMeanForRowsf,
		float &fMeanForColsf,

		float &fStDevForRowsf,
		float &fStDevForColsf);

	int
		nResf,
		nWidCurf = sCooc_Of_Orig_Image->nWid_IntensityCoocCur, // == nGrayLevelIntensityMax
		nLenCurf = sCooc_Of_Orig_Image->nLen_IntensityCoocCur, // == nGrayLevelIntensityMax

		nTempf,
		nIndexPositionOfIntensityf, // = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);
		iLenf,
		iWidf,
		nSumf = 0;

	float
		fTempf,
		fMeanForRowsf,
		fMeanForColsf,

		fStDevForRowsf,
		fStDevForColsf;

	//////////////////////////////////////////////

	nResf = MeansAndStDev_OfRowsAndCols_OrigImage(
		sCooc_Of_Orig_Image, //const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

		fMeanForRowsf, //float &fMeanForRowsf,
		fMeanForColsf, //float &fMeanForColsf,

		fStDevForRowsf, //float &fStDevForRowsf,
		fStDevForColsf); // float &fStDevForColsf);

						 ///////////////////////////////////////////////
	sFeasOfCooc->fEnergy = 0.0;
	sFeasOfCooc->fContrast = 0.0;

	sFeasOfCooc->fCorrelation = 0.0;
	sFeasOfCooc->fHomogeneity = 0.0;

	sFeasOfCooc->fEntropy = 0.0;
	sFeasOfCooc->fAutocorrelation = 0.0;

	sFeasOfCooc->fDissimilarity = 0.0;
	sFeasOfCooc->fClusterShade = 0.0;
	sFeasOfCooc->fClusterProminence = 0.0;

	sFeasOfCooc->fMaxProb = -1.0; //impossible
	//sFeasOfCooc->fMaxProb_At_HighIntensities = -1.0; //impossible

	for (iWidf = 0; iWidf < nWidCurf; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenCurf; iLenf++)
		{
			//nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);
			nIndexPositionOfIntensityf = iLenf + (iWidf * nGrayLevelIntensityMax); //or nWidCurf = nGrayLevelIntensityMax

			if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] < 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'FeaturesOfOrigCooc_OrigImage': ->fAverOfCoocArr = %E < 0.0 at iWidf = %d and iLenf = %d",
					sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf], iWidf, iLenf);

				fprintf(fout_lr, "\n\n An error in 'FeaturesOfOrigCooc_OrigImage': ->fAverOfCoocArr = %E < 0.0 at iWidf = %d and iLenf = %d",
					sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf], iWidf, iLenf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] < 0.0)

			  //Energy
			sFeasOfCooc->fEnergy += (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf])*(sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf]);

			//Correlation is below, after calculating autocorrelation 

						//Homogeneity
			sFeasOfCooc->fHomogeneity += (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf])*(1.0 + ((iWidf - iLenf)*(iWidf - iLenf)));

			//Entropy
			if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] > feps)
				sFeasOfCooc->fEntropy += (-sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf]) * log10f(sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf]);

			//Autocorrelation
			sFeasOfCooc->fAutocorrelation += (iLenf * iWidf) * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

			//Dissimilarity
			if (iWidf > iLenf)
				nTempf = iWidf - iLenf;
			else
				nTempf = iLenf - iWidf;

			if (nTempf > 0)
				sFeasOfCooc->fDissimilarity += nTempf * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

			//Cluster shade
			//3rd order
			fTempf = ((float)(iLenf)+(float)(iWidf)-fMeanForRowsf - fMeanForColsf) * ((float)(iLenf)+(float)(iWidf)-fMeanForRowsf - fMeanForColsf) *
				((float)(iLenf)+(float)(iWidf)-fMeanForRowsf - fMeanForColsf);

			sFeasOfCooc->fClusterShade += fTempf * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

			// Cluster prominance 
			//4th order
			fTempf = fTempf * ((float)(iLenf)+(float)(iWidf)-fMeanForRowsf - fMeanForColsf);

			sFeasOfCooc->fClusterProminence += fTempf * sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

			//Max probabilities
			if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] > sFeasOfCooc->fMaxProb)
			{
				sFeasOfCooc->fMaxProb = sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

				sFeasOfCooc->nIntensityLength_MaxProb = iLenf;
				sFeasOfCooc->nIntensityWidth_MaxProb = iWidf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n A new sFeasOfCooc->fMaxProb = %E, nIndexPositionOfIntensityf = %d", sFeasOfCooc->fMaxProb, nIndexPositionOfIntensityf);

				fprintf(fout_lr, "\n sFeasOfCooc->nIntensityLength_MaxProb = %d, sFeasOfCooc->nIntensityWidth_MaxProb = %dE", sFeasOfCooc->nIntensityLength_MaxProb, sFeasOfCooc->nIntensityWidth_MaxProb);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} //if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] > sFeasOfCooc->fMaxProb)

/*
			if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] > sFeasOfCooc->fMaxProb_At_HighIntensities &&
				iLenf >= nGrayLevel_HighIntensityThreshold && iWidf >= nGrayLevel_HighIntensityThreshold)

			{
				sFeasOfCooc->fMaxProb_At_HighIntensities = sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf];

				fprintf(fout_lr, "\n\n A new sFeasOfCooc->fMaxProb_At_HighIntensities = %E, nIndexPositionOfIntensityf = %d", sFeasOfCooc->fMaxProb_At_HighIntensities, nIndexPositionOfIntensityf);

			//	fprintf(fout_lr, "\n sFeasOfCooc->nIntensityLength_MaxProb = %d, sFeasOfCooc->nIntensityWidth_MaxProb = %dE",sFeasOfCooc->nIntensityLength_MaxProb, sFeasOfCooc->nIntensityWidth_MaxProb);

			} //if (sCooc_Of_Orig_Image->fAverOfCoocArr[nIndexPositionOfIntensityf] > sFeasOfCooc->fMaxProb && ...)
*/
		} // for (iLenf = 0; iLenf < nLenCurf; iLenf++)

	} //for (iWidf = 0; iWidf < nWidCurf; iWidf++)

	  //Correlation after calculating autocorrelation 
	if (fStDevForRowsf * fStDevForColsf > 0.0)
	{
		sFeasOfCooc->fCorrelation = (sFeasOfCooc->fAutocorrelation - fMeanForRowsf * fMeanForColsf) / (fStDevForRowsf * fStDevForColsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Correlation 1: sFeasOfCooc->fCorrelation = %E", sFeasOfCooc->fCorrelation);
		fprintf(fout_lr, "\n sFeasOfCooc->fAutocorrelation = %E, fMeanForRowsf = %E, fMeanForColsf = %E, fStDevForRowsf = %E, fStDevForColsf = %E",
			sFeasOfCooc->fAutocorrelation, fMeanForRowsf, fMeanForColsf, fStDevForRowsf, fStDevForColsf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'FeaturesOfOrigCooc_OrigImage': fStDevForRowsf = %E or fStDevForColsf = %E <= 0.0", fStDevForRowsf, fStDevForColsf);

		fprintf(fout_lr, "\n\n An error in 'FeaturesOfOrigCooc_OrigImage': fStDevForRowsf = %E or fStDevForColsf = %E <= 0.0", fStDevForRowsf, fStDevForColsf);

		fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //else
	  ////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int FeaturesOfOrigCooc_OrigImage(...
///////////////////////////////////////////////////////////////////////////////

int ScaleCooc_OrigImage(
	const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef,

	const int nDimOfSquaref,

	int &nNumOfScaleCooc_OrigImage_FeasCurf,
	SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef)
{

	int
		nGrayIntensity_ScaleCoocMaxf,

		nLenIn256_GrayscaleCurf, // = sCooc_Of_Orig_Imagef->nLen_IntensityCoocCur, // == nGrayLevelIntensityMax
		nWidIn256_GrayscaleCurf, // = sCooc_Of_Orig_Imagef->nWid_IntensityCoocCur, // == nGrayLevelIntensityMax

		nIndexOfPositionOfScaleCoocf,
		nIndexPositionOfIntensityf, // = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);

		iLenScaleCoocf, //
		iWidScaleCoocf,

		iLen_IntensitySquaref,
		iWid_IntensitySquaref,

		nSumOfCoocWithinSquaref,

		nSumf = 0;

	float
		fSumOfProbWithinSquaref;

	//nDimOfSquaref = 16, 32, 64

	if (nDimOfSquaref < 2 || nDimOfSquaref > nDimOfSquare_ScaleCoocMax || (nDimOfSquaref / 2) * 2 != nDimOfSquaref)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'ScaleCooc_OrigImage': nDimOfSquaref = %d, nDimOfSquare_ScaleCoocMax = %d",
			nDimOfSquaref, nDimOfSquare_ScaleCoocMax);

		fprintf(fout_lr, "\n\n An error in 'ScaleCooc_OrigImage': nDimOfSquaref = %d, nDimOfSquare_ScaleCoocMax = %d",
			nDimOfSquaref, nDimOfSquare_ScaleCoocMax);

		fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nDimOfSquaref < 2 || nDimOfSquaref >= nDimOfSquare_ScaleCoocMax || ...)

///////////////////////////////////////////////////////////////////////////////////
	nGrayIntensity_ScaleCoocMaxf = nNumOfGrayLevelIntensities / nDimOfSquaref; //256/16, 256/32, 256/64, ...

	if (nGrayIntensity_ScaleCoocMaxf < 1 || (nGrayIntensity_ScaleCoocMaxf / 2) * 2 != nGrayIntensity_ScaleCoocMaxf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'ScaleCooc_OrigImage': nGrayIntensity_ScaleCoocMaxf = %d, nDimOfSquare_ScaleCoocMax = %d",
			nGrayIntensity_ScaleCoocMaxf, nDimOfSquare_ScaleCoocMax);

		fprintf(fout_lr, "\n\n An error in 'ScaleCooc_OrigImage': nGrayIntensity_ScaleCoocMaxf = %d, nDimOfSquare_ScaleCoocMax = %d",
			nGrayIntensity_ScaleCoocMaxf, nDimOfSquare_ScaleCoocMax);

		fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nGrayIntensity_ScaleCoocMaxf < 1 ||  ...

	nNumOfScaleCooc_OrigImage_FeasCurf = nGrayIntensity_ScaleCoocMaxf * nGrayIntensity_ScaleCoocMaxf;
	///////////////////////////////////////////////
	sScale_Cooc_Of_Orig_Imagef->nIndicOfClass = sCooc_Of_Orig_Imagef->nIndicOfClass;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'ScaleCooc_OrigImage': nGrayIntensity_ScaleCoocMaxf = %d, nDimOfSquaref = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d",
		nGrayIntensity_ScaleCoocMaxf, nDimOfSquaref, nNumOfScaleCooc_OrigImage_FeasCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iLenScaleCoocf = 0; iLenScaleCoocf < nGrayIntensity_ScaleCoocMaxf; iLenScaleCoocf++) //nGrayIntensity_ScaleCoocMaxf = 256/2, 256/4, 256/8, ...
	{
		for (iWidScaleCoocf = 0; iWidScaleCoocf < nGrayIntensity_ScaleCoocMaxf; iWidScaleCoocf++)
		{
			nIndexOfPositionOfScaleCoocf = iLenScaleCoocf + (iWidScaleCoocf * nGrayIntensity_ScaleCoocMaxf); // for sScale_Cooc_Of_Orig_Imagef

			//fprintf(fout_lr, "\n\n iLenScaleCoocf = %d, iWidScaleCoocf = %d, nIndexOfPositionOfScaleCoocf = %d",
//				iLenScaleCoocf, iWidScaleCoocf, nIndexOfPositionOfScaleCoocf);

			if (nIndexOfPositionOfScaleCoocf < 0 || nIndexOfPositionOfScaleCoocf >= nScaleCoocSizeMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'ScaleCooc_OrigImage': nIndexOfPositionOfScaleCoocf = %d >= nScaleCoocSizeMax = %d, iLenScaleCoocf = %d, iWidScaleCoocf = %d",
					nIndexOfPositionOfScaleCoocf, nScaleCoocSizeMax, iLenScaleCoocf, iWidScaleCoocf);
				fprintf(fout_lr, "\n\n An error in 'ScaleCooc_OrigImage': nIndexOfPositionOfScaleCoocf = %d >= nScaleCoocSizeMax = %d, iLenScaleCoocf = %d, iWidScaleCoocf = %d",
					nIndexOfPositionOfScaleCoocf, nScaleCoocSizeMax, iLenScaleCoocf, iWidScaleCoocf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nIndexOfPositionOfScaleCoocf < 0 || nIndexOfPositionOfScaleCoocf >= nScaleCoocSizeMax)

			nSumOfCoocWithinSquaref = 0;
			fSumOfProbWithinSquaref = 0.0;

			for (iWid_IntensitySquaref = 0; iWid_IntensitySquaref < nDimOfSquaref; iWid_IntensitySquaref++)
			{
				for (iLen_IntensitySquaref = 0; iLen_IntensitySquaref < nDimOfSquaref; iLen_IntensitySquaref++)
				{

					nLenIn256_GrayscaleCurf = iLen_IntensitySquaref + (iLenScaleCoocf * nDimOfSquaref);
					nWidIn256_GrayscaleCurf = iWid_IntensitySquaref + (iWidScaleCoocf * nDimOfSquaref);

					//nIndexPositionOfIntensityf = nLenIn256_GrayscaleCurf + (nWidIn256_GrayscaleCurf * nGrayLevelIntensityMax); //or nWidCurf = nGrayLevelIntensityMax
					nIndexPositionOfIntensityf = nLenIn256_GrayscaleCurf + (nWidIn256_GrayscaleCurf * nGrayLevelIntensityMax); //or nWidCurf = nGrayLevelIntensityMax

					if (nIndexPositionOfIntensityf < 0 || nIndexPositionOfIntensityf > nCoocSizeMax - 1) //#define nCoocSizeMax (nNumOfGrayLevelIntensities * nNumOfGrayLevelIntensities)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'ScaleCooc_OrigImage': nIndexPositionOfIntensityf = %d, nCoocSizeMax = %d, at iWid_IntensitySquaref = %d and iLen_IntensitySquaref = %d",
							nIndexPositionOfIntensityf, nCoocSizeMax, iWid_IntensitySquaref, iLen_IntensitySquaref);

						printf("\n iLenScaleCoocf = %d, iWidScaleCoocf = %d, nLenIn256_GrayscaleCurf = %d, nWidIn256_GrayscaleCurf = %d, nGrayIntensity_ScaleCoocMax = %d, nDimOfSquaref = %d",
							iLenScaleCoocf, iWidScaleCoocf, nLenIn256_GrayscaleCurf, nWidIn256_GrayscaleCurf, nGrayIntensity_ScaleCoocMax, nDimOfSquaref);

						fprintf(fout_lr, "\n\n An error in 'ScaleCooc_OrigImage': nIndexPositionOfIntensityf = %d, nCoocSizeMax = %d, at iWid_IntensitySquaref = %d and iLen_IntensitySquaref = %d",
							nIndexPositionOfIntensityf, nCoocSizeMax, iWid_IntensitySquaref, iLen_IntensitySquaref);

						fprintf(fout_lr, "\n iLenScaleCoocf = %d, iWidScaleCoocf = %d, nLenIn256_GrayscaleCurf = %d, nWidIn256_GrayscaleCurf = %d, nGrayIntensity_ScaleCoocMax = %d, nDimOfSquaref = %d",
							iLenScaleCoocf, iWidScaleCoocf, nLenIn256_GrayscaleCurf, nWidIn256_GrayscaleCurf, nGrayIntensity_ScaleCoocMax, nDimOfSquaref);

						fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					}// if (nIndexPositionOfIntensityf < 0 || nIndexPositionOfIntensityf > nCoocSizeMax - 1)


					nSumOfCoocWithinSquaref += sCooc_Of_Orig_Imagef->nCoocOfOrigImageArr[nIndexPositionOfIntensityf];

					fSumOfProbWithinSquaref += sCooc_Of_Orig_Imagef->fAverOfCoocArr[nIndexPositionOfIntensityf];

					if (iWidScaleCoocf == nGrayIntensity_ScaleCoocMaxf - 1) // && iLenScaleCoocf == nGrayIntensity_ScaleCoocMaxf - 1)
					{
						//fprintf(fout_lr, "\n\n nSumOfCoocWithinSquaref = %d, fSumOfProbWithinSquaref = %E, nIndexPositionOfIntensityf = %d", 
							//nSumOfCoocWithinSquaref, fSumOfProbWithinSquaref,nIndexPositionOfIntensityf);

						//fprintf(fout_lr, "\n  iLen_IntensitySquaref = %d, iWid_IntensitySquaref = %d, nLenIn256_GrayscaleCurf = %d, nWidIn256_GrayscaleCurf = %d, iLenScaleCoocf = %d, iWidScaleCoocf = %d",
							//iLen_IntensitySquaref, iWid_IntensitySquaref, nLenIn256_GrayscaleCurf, nWidIn256_GrayscaleCurf,iLenScaleCoocf, iWidScaleCoocf);

					}// if (iWidScaleCoocf == nGrayIntensity_ScaleCoocMaxf - 1 && iLenScaleCoocf == nGrayIntensity_ScaleCoocMaxf - 1)

				} // for (iLen_IntensitySquaref = 0; iLen_IntensitySquaref < nDimOfSquaref; iLen_IntensitySquaref++)

			} //for (iWid_IntensitySquaref = 0; iWid_IntensitySquaref < nDimOfSquaref; iWid_IntensitySquaref++)

			sScale_Cooc_Of_Orig_Imagef->nScaleCoocOfOrigImageArr[nIndexOfPositionOfScaleCoocf] = nSumOfCoocWithinSquaref;

			sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexOfPositionOfScaleCoocf] = fSumOfProbWithinSquaref;

			//fprintf(fout_lr, "\n ... ->nScaleCoocOfOrigImageArr[%d] = %d, ...->fAverOfScaleCoocArr[%d] = %E",
				//nIndexOfPositionOfScaleCoocf, sScale_Cooc_Of_Orig_Imagef->nScaleCoocOfOrigImageArr[nIndexOfPositionOfScaleCoocf],
					//nIndexOfPositionOfScaleCoocf, sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexOfPositionOfScaleCoocf]);

		} // for (iWidScaleCoocf = 0; iWidScaleCoocf < nGrayIntensity_ScaleCoocMaxf; iWidScaleCoocf++)

	} //for (iLenScaleCoocf = 0; iLenScaleCoocf < nGrayIntensity_ScaleCoocMaxf; iLenScaleCoocf++)

	sScale_Cooc_Of_Orig_Imagef->nNumOfScaleCooc_OrigImage_FeasCurf = nNumOfScaleCooc_OrigImage_FeasCurf;
	////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int ScaleCooc_OrigImage(...

  //////////////////////////////////////////////////////////////////
int Initializing_ScaleCooc_OrigImage(

	const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
								  //1--0 degrees, 2--45 degrees, 3--90 degrees

	const int nDistOfCoocf, //distance of cooccurrence

	const int nDimOfSquaref,
	SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef)
{
	int
		iFeaf;

	//nDimOfSquaref =  4, 8, ...,2^n

	sScale_Cooc_Of_Orig_Imagef->nNumOfScaleCooc_OrigImage_FeasCurf = 0;

	sScale_Cooc_Of_Orig_Imagef->nDimOfSquare = nDimOfSquaref;

	if (nDimOfSquaref < nDimOfSquare_ScaleCoocMin || nDimOfSquaref > nDimOfSquare_ScaleCoocMax || (nDimOfSquaref / 2) * 2 != nDimOfSquaref)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Initializing_ScaleCooc_OrigImage': nDimOfSquaref = %d, nDimOfSquare_ScaleCoocMax = %d",
			nDimOfSquaref, nDimOfSquare_ScaleCoocMax);

		fprintf(fout_lr, "\n\n An error in 'Initializing_ScaleCooc_OrigImage': nDimOfSquaref = %d, nDimOfSquare_ScaleCoocMax = %d",
			nDimOfSquaref, nDimOfSquare_ScaleCoocMax);

		fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nDimOfSquaref < Initializing_ScaleCooc_OrigImage || nDimOfSquaref > nDimOfSquare_ScaleCoocMax || ...)

	  ///////////////////////////////////////////////
	sScale_Cooc_Of_Orig_Imagef->nDistOfCooc = nDistOfCoocf;

	sScale_Cooc_Of_Orig_Imagef->nIndicOfDirection = nIndicOfDirectionf;

	sScale_Cooc_Of_Orig_Imagef->nLen_IntensityScaleCoocCur = nGrayIntensity_ScaleCoocMax;
	sScale_Cooc_Of_Orig_Imagef->nWid_IntensityScaleCoocCur = nGrayIntensity_ScaleCoocMax;

	for (iFeaf = 0; iFeaf < nScaleCoocSizeMax; iFeaf++)
	{
		sScale_Cooc_Of_Orig_Imagef->nScaleCoocOfOrigImageArr[iFeaf] = 0;

		sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[iFeaf] = 0.0;

	} //for (iFeaf = 0; iFeaf < nScaleCoocSizeMax; iFeaf++)

	  ////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int Initializing_ScaleCooc_OrigImage(...

int Initializing_FeasAllOfCooc(

	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{
	int
		iCoocScalef,

		iDistf,

		nIndexDistScalef,
		nNumOfScalesAndDistMaxf = nNumOfScalesAndDistTot - 1,

		iFeaf;

	//////////////////////////////////////////////////////////////
	for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc; iFeaf++)
	{
		sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf] = 0.0;

	} // for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc; iFeaf++)

	for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc - 2; iFeaf++)
	{
		sFeasAllOfCoocf->fPercentagesAboveThresholds_InHistogramArrf[iFeaf] = 0.0;

	} // for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc - 2; iFeaf++)

	//for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax - 1; iFeaf++)
	for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)
	{
		sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[iFeaf] = 0.0;

	} // for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)

//////////////////////////////////////////////////////////////////////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		sFeasAllOfCoocf->fEnergyArr[iFeaf] = -fLarge;
		sFeasAllOfCoocf->fContrastArr[iFeaf] = -fLarge;

		sFeasAllOfCoocf->fCorrelationArr[iFeaf] = -fLarge;
		sFeasAllOfCoocf->fHomogeneityArr[iFeaf] = -fLarge;

		sFeasAllOfCoocf->fEntropyArr[iFeaf] = -fLarge;
		sFeasAllOfCoocf->fAutocorrelationArr[iFeaf] = -fLarge;

		sFeasAllOfCoocf->fDissimilarityArr[iFeaf] = -fLarge;
		sFeasAllOfCoocf->fClusterShadeArr[iFeaf] = -fLarge;

		sFeasAllOfCoocf->fClusterProminenceArr[iFeaf] = -fLarge;
		sFeasAllOfCoocf->fMaxProbArr[iFeaf] = -fLarge;

		//sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesArr[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	  ////////////////////////////////////////////////////////////////

	for (iDistf = 0; iDistf < nNumOfDistances; iDistf++)
	{

		for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++)
		{

			nIndexDistScalef = iDistf + iCoocScalef * nNumOfDistances;

			if (nIndexDistScalef < 0 || nIndexDistScalef > nNumOfScalesAndDistMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Initializing_FeasAllOfCooc': nIndexDistScalef = %d and nNumOfScalesAndDistMaxf = %d",
					nIndexDistScalef, nNumOfScalesAndDistMaxf);

				fprintf(fout_lr, "\n\n An error in 'Initializing_FeasAllOfCooc': nIndexDistScalef = %d and nNumOfScalesAndDistMaxf = %d",
					nIndexDistScalef, nNumOfScalesAndDistMaxf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexDistScalef < 0 || nIndexDistScalef > nNumOfScalesAndDistMaxf)

			sFeasAllOfCoocf->fEnergyAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fContrastAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fCorrelationAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fHomogeneityAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fEntropyAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fAutocorrelationAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fDissimilarityAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fClusterShadeAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fClusterProminenceAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fMaxProbAverOverDirecArr[nIndexDistScalef] = 0.0;

			//sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesAverOverDirecArr[nIndexDistScalef] = 0.0;

		} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++)

	} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_FeasAllOfCooc(...

  ////////////////////////////////////////////////////////////
int MeansAndStDev_OfRowsAndCols_ScaleCoocImage(
	const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Image,

	float &fMeanForRowsf,
	float &fMeanForColsf,

	float &fStDevForRowsf,
	float &fStDevForColsf)
{
	int
		nWidCurf = sScale_Cooc_Of_Orig_Image->nWid_IntensityScaleCoocCur, // == nGrayLevelIntensityMax
		nLenCurf = sScale_Cooc_Of_Orig_Image->nLen_IntensityScaleCoocCur, // == nGrayLevelIntensityMax

		nIndexPositionOfIntensityf, // = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);
		iLenf,
		iWidf,
		nSumf = 0;


	fMeanForRowsf = 0;
	fMeanForColsf = 0;

	fStDevForRowsf = 0;
	fStDevForColsf = 0;

	for (iWidf = 0; iWidf < nWidCurf; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenCurf; iLenf++)
		{
			//nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);
			nIndexPositionOfIntensityf = iLenf + (iWidf * nLenCurf); //or nWidCurf = nGrayLevelIntensityMax

			fMeanForRowsf += iWidf * sScale_Cooc_Of_Orig_Image->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];
			fMeanForColsf += iLenf * sScale_Cooc_Of_Orig_Image->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

		} // for (iLenf = 0; iLenf < nLenCurf; iLenf++)

	} //for (iWidf = 0; iWidf < nWidCurf; iWidf++)

	for (iWidf = 0; iWidf < nWidCurf; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenCurf; iLenf++)
		{
			nIndexPositionOfIntensityf = iWidf + (iLenf * nLenCurf); //or nWidCurf = nGrayLevelIntensityMax

			fStDevForRowsf += (iWidf - fMeanForRowsf) * (iWidf - fMeanForRowsf) * sScale_Cooc_Of_Orig_Image->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

			fStDevForColsf += (iLenf - fMeanForColsf) * (iLenf - fMeanForColsf) * sScale_Cooc_Of_Orig_Image->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

		} // for (iLenf = 0; iLenf < nLenCurf; iLenf++)

	} //for (iWidf = 0; iWidf < nWidCurf; iWidf++)

	  //fflush(fout_lr);

	return SUCCESSFUL_RETURN;
} //int MeansAndStDev_OfRowsAndCols_ScaleCoocImage(...

  ///////////////////////////////////////////////////////////////////
int FeaturesOfScaleCooc_OrigImage(
	const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

	FEATURES_OF_COOC *sFeasOfCoocf)
{

	int MeansAndStDev_OfRowsAndCols_ScaleCoocImage(
		const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

		float &fMeanForRowsf,
		float &fMeanForColsf,

		float &fStDevForRowsf,
		float &fStDevForColsf);

	int
		nResf,
		nWidCoocf = sScale_Cooc_Of_Orig_Imagef->nWid_IntensityScaleCoocCur, // == nGrayLevelIntensityMax
		nLenCoocf = sScale_Cooc_Of_Orig_Imagef->nLen_IntensityScaleCoocCur, // == nGrayLevelIntensityMax

		nTempf,
		nIndexPositionOfIntensityf, // = nIntensity_Leftf + (nIntensity_Rightf * nGrayLevelIntensityMax);

		iLenCoocf,
		iWidCoocf,

		nSumf = 0;

	float
		fTempf,
		fMeanForRowsf,
		fMeanForColsf,

		fStDevForRowsf,
		fStDevForColsf;

	nResf = MeansAndStDev_OfRowsAndCols_ScaleCoocImage(
		sScale_Cooc_Of_Orig_Imagef,

		fMeanForRowsf,
		fMeanForColsf,

		fStDevForRowsf,
		fStDevForColsf);

	//////////////////////////////////////////////

	sFeasOfCoocf->fEnergy = 0.0;

	sFeasOfCoocf->fContrast = 0.0;

	sFeasOfCoocf->fCorrelation = 0.0;
	sFeasOfCoocf->fHomogeneity = 0.0;
	sFeasOfCoocf->fEntropy = 0.0;
	sFeasOfCoocf->fAutocorrelation = 0.0;
	sFeasOfCoocf->fDissimilarity = 0.0;
	sFeasOfCoocf->fClusterShade = 0.0;
	sFeasOfCoocf->fClusterProminence = 0.0;

	sFeasOfCoocf->fMaxProb = -1.0; //impossible
	//sFeasOfCoocf->fMaxProb_At_HighIntensities = -1.0; //impossible

	for (iWidCoocf = 0; iWidCoocf < nWidCoocf; iWidCoocf++)
	{
		for (iLenCoocf = 0; iLenCoocf < nLenCoocf; iLenCoocf++)
		{
			//nIndexPositionOfIntensityf = iLenIntensityf + (iWidIntensityf * nGrayLevelIntensityMax);
			nIndexPositionOfIntensityf = iLenCoocf + (iWidCoocf * nLenCoocf); //or nWidCoocf = nGrayLevelIntensityMax

			if (nIndexPositionOfIntensityf >= nScaleCoocSizeMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'FeaturesOfScaleCooc_OrigImage': nIndexPositionOfIntensityf = %d >= nScaleCoocSizeMax = %d, at iWidCoocf = %d and iLenCoocf = %d",
					nIndexPositionOfIntensityf, nScaleCoocSizeMax, iWidCoocf, iLenCoocf);

				fprintf(fout_lr, "\n\n An error in 'FeaturesOfScaleCooc_OrigImage': nIndexPositionOfIntensityf = %d >= nScaleCoocSizeMax = %d, at iWidCoocf = %d and iLenCoocf = %d",
					nIndexPositionOfIntensityf, nScaleCoocSizeMax, iWidCoocf, iLenCoocf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexPositionOfIntensityf >= nScaleCoocSizeMax)

			if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] < 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'FeaturesOfScaleCooc_OrigImage': ->fAverOfScaleCoocArr = %E < 0.0 at iWidCoocf = %d and iLenCoocf = %d",
					sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf], iWidCoocf, iLenCoocf);

				printf("\n  nIndexPositionOfIntensityf = %d, nScaleCoocSizeMax = %d, nLenCoocf = %d, nWidCoocf = %d", nIndexPositionOfIntensityf, nScaleCoocSizeMax, nLenCoocf, nWidCoocf);

				fprintf(fout_lr, "\n\n An error in 'FeaturesOfScaleCooc_OrigImage': ->fAverOfScaleCoocArr = %E < 0.0 at iWidCoocf = %d and iLenCoocf = %d",
					sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf], iWidCoocf, iLenCoocf);

				fprintf(fout_lr, "\n nIndexPositionOfIntensityf = %d, nScaleCoocSizeMax = %d, nLenCoocf = %d, nWidCoocf = %d", nIndexPositionOfIntensityf, nScaleCoocSizeMax, nLenCoocf, nWidCoocf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] < 0.0)

			  //Energy
			sFeasOfCoocf->fEnergy += (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf])*(sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf]);

			//Contrast
			sFeasOfCoocf->fContrast += (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf]) * ((iWidCoocf - iLenCoocf)*(iWidCoocf - iLenCoocf));

			//Homogeneity
				//sFeasOfCoocf->fHomogeneity += (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf])*(1.0 + ((iWidCoocf - iLenCoocf)*(iWidCoocf - iLenCoocf)));
			sFeasOfCoocf->fHomogeneity += (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf]) / (1.0 + ((iWidCoocf - iLenCoocf)*(iWidCoocf - iLenCoocf)));

			//Entropy
			if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] > feps)
				sFeasOfCoocf->fEntropy += (-sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf]) * log10f(sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf]);

			//Autocorrelation
			sFeasOfCoocf->fAutocorrelation += (iLenCoocf * iWidCoocf) * sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

			//Dissimilarity
			if (iWidCoocf > iLenCoocf)
				nTempf = iWidCoocf - iLenCoocf;
			else
				nTempf = iLenCoocf - iWidCoocf;

			if (nTempf > 0)
				sFeasOfCoocf->fDissimilarity += nTempf * sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

			//Cluster shade
			//3rd order
			fTempf = ((float)(iLenCoocf)+(float)(iWidCoocf)-fMeanForRowsf - fMeanForColsf) * ((float)(iLenCoocf)+(float)(iWidCoocf)-fMeanForRowsf - fMeanForColsf) *
				((float)(iLenCoocf)+(float)(iWidCoocf)-fMeanForRowsf - fMeanForColsf);

			sFeasOfCoocf->fClusterShade += fTempf * sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

			// Cluster prominance 
			//4th order
			fTempf = fTempf * ((float)(iLenCoocf)+(float)(iWidCoocf)-fMeanForRowsf - fMeanForColsf);

			sFeasOfCoocf->fClusterProminence += fTempf * sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

			//Max probabilities
			if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] > sFeasOfCoocf->fMaxProb)
			{
				sFeasOfCoocf->fMaxProb = sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];

				sFeasOfCoocf->nIntensityLength_MaxProb = iLenCoocf;
				sFeasOfCoocf->nIntensityWidth_MaxProb = iWidCoocf;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n A new sFeasOfCoocf->fMaxProb = %E, nIndexPositionOfIntensityf = %d", sFeasOfCoocf->fMaxProb, nIndexPositionOfIntensityf);

				fprintf(fout_lr, "\n sFeasOfCoocf->nIntensityLength_MaxProb = %d, sFeasOfCooc->nIntensityWidth_MaxProb = %d", sFeasOfCoocf->nIntensityLength_MaxProb, sFeasOfCoocf->nIntensityWidth_MaxProb);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} //if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] > sFeasOfCoocf->fMaxProb)

/*
			if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] > sFeasOfCoocf->fMaxProb_At_HighIntensities &&
				iLenCoocf >= nGrayLevel_HighIntensityThreshold && iWidCoocf >= nGrayLevel_HighIntensityThreshold)

			{
				sFeasOfCoocf->fMaxProb_At_HighIntensities = sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf];
				fprintf(fout_lr, "\n\n A new sFeasOfCoocf->fMaxProb_At_HighIntensities = %E, nIndexPositionOfIntensityf = %d", sFeasOfCoocf->fMaxProb_At_HighIntensities, nIndexPositionOfIntensityf);

				//sFeasOfCoocf->nIntensityLength_MaxProb = iLenCoocf;
				//sFeasOfCoocf->nIntensityWidth_MaxProb = iWidCoocf;

			} //if (sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nIndexPositionOfIntensityf] > sFeasOfCoocf->fMaxProb && ...)
*/
		} // for (iLenCoocf = 0; iLenCoocf < nLenCoocf; iLenCoocf++)

	} //for (iWidCoocf = 0; iWidCoocf < nWidCoocf; iWidCoocf++)

	  //Correlation after calculating autocorrelation 
	if (fStDevForRowsf * fStDevForColsf > 0.0)
	{
		sFeasOfCoocf->fCorrelation = (sFeasOfCoocf->fAutocorrelation - fMeanForRowsf * fMeanForColsf) / (fStDevForRowsf * fStDevForColsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Correlation 2: sFeasOfCoocf->fCorrelation = %E", sFeasOfCoocf->fCorrelation);
		fprintf(fout_lr, "\n sFeasOfCooc->fAutocorrelation = %E, fMeanForRowsf = %E, fMeanForColsf = %E, fStDevForRowsf = %E, fStDevForColsf = %E",
			sFeasOfCoocf->fAutocorrelation, fMeanForRowsf, fMeanForColsf, fStDevForRowsf, fStDevForColsf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (fStDevForRowsf * fStDevForColsf > 0.0)
	else
	{

		sFeasOfCoocf->fCorrelation = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		//		printf("\n\n 'FeaturesOfScaleCooc_OrigImage': fStDevForRowsf = %E or fStDevForColsf = %E <= 0.0 at iWidCoocf = %d and iLenCoocf = %d",
			//		fStDevForRowsf, fStDevForColsf, iWidCoocf, iLenCoocf);

		fprintf(fout_lr, "\n\n 'FeaturesOfScaleCooc_OrigImage': fStDevForRowsf = %E or fStDevForColsf = %E <= 0.0 at iWidCoocf = %d and iLenCoocf = %d",
			fStDevForRowsf, fStDevForColsf, iWidCoocf, iLenCoocf);

		//fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//return EXIT_FOR_IMAGE_SIZE_CONSTRAINTS;
	} //else

	return SUCCESSFUL_RETURN;
} //int FeaturesOfScaleCooc_OrigImage(...
  ////////////////////////////////////////////////////////////////////

int FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage(
	const int nNumOfDirectionsf,
	const int nIndicOfDirectionArrf[],

	const int nNumOfDistancesf,
	const int nDistancesArrf[],

	const int nScaleCoocTotf,
	const int nDimOfSquare_ScaleCoocArrf[],

	const GRAYSCALE_IMAGE *sGrayscale_Imagef,

	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{
	void Initializing_Cooc_OrigImage(

		const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
									  //1--0 degrees, 2--45 degrees, 3--90 degrees

		const int nDistOfCoocf, //distance of cooccurrence

		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

	int CoocOfOrigImage(
		const int nIndicOfDirectionf,
		const int nDistOfCoocf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,
		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

	int FeaturesOfOrigCooc_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

		FEATURES_OF_COOC *sFeasOfCooc);

	int FeaturesOfScaleCooc_OrigImage(
		const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

		FEATURES_OF_COOC *sFeasOfCoocf);

	int ScaleCooc_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef,

		const int nDimOfSquaref,

		int &nNumOfScaleCooc_OrigImage_FeasCurf,

		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

	int Initializing_ScaleCooc_OrigImage(

		const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
									  //1--0 degrees, 2--45 degrees, 3--90 degrees

		const int nDistOfCoocf, //distance of cooccurrence

		const int nDimOfSquaref,
		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);


	int Initializing_FeasAllOfCooc(

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Histogram_Statistics_Grayscale_Image(

		const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageSizeMax]

		float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		float fPercentagesAboveThresholds_InHistogramArrf[],

		float &fMeanf,
		float &fStDevf,
		float &fSkewnessf,
		float &fKurtosisf); //[nNumOfHistogramIntervals_ForCooc - 2]

	void Copying_HistogramFeatures_To_sFeasAllOfCoocf(

		const float fMeanf,
		const float fStDevf,
		const float fSkewnessf,
		const float fKurtosisf,

		const float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		const float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf(

		const int nNumOfScaleCooc_OrigImage_FeasCurf,

		const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

		int &nNumOfScaleCooc_OrigImage_FeasTotSoFarf,

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Features_AverOverDirections_AllDistAndScaleCooc_OrigImage(
		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	////////////////////////////////////////////////

	int
		iDirecf,
		iDistf,
		iCoocScalef,

		nIndex3Dimf,

		nNumOfDirecAndDistAndScaleMaxf = nNumOfDirecAndDistAndScaleTot - 1,

		nNumOfScaleCooc_OrigImage_FeasTotSoFarf = 0,

		nDimOfSquareCurf,

		nNumOfScaleCooc_OrigImage_FeasCurf,

		nDistCurf,
		nResf;

	float
		fMeanf,
		fStDevf,
		fSkewnessf,
		fKurtosisf,

		fPercentsOfHistogramIntervalsArrf[nNumOfHistogramIntervals_ForCooc],//[]

		fPercentagesAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2]; //[nNumOfHistogramIntervals_ForCooc - 1]

	COOC_OF_ORIG_IMAGE
		sCooc_Of_Orig_Imagef;

	SCALE_COOC_OF_ORIG_IMAGE
		sScale_Cooc_Of_Orig_Imagef;

	FEATURES_OF_COOC
		sFeasOfCoocf;

	/////////////////////////////////////////////////////////////
	nResf = Initializing_FeasAllOfCooc(

		//&sFeaOf_AllCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
		sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
	//////////////////////////////////////////////////////////////////////////////

	nResf = Histogram_Statistics_Grayscale_Image(

		sGrayscale_Imagef, //GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageSizeMax]

		fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		fPercentagesAboveThresholds_InHistogramArrf,
		fMeanf, //float &fMeanf,
		fStDevf, //float &fStDevf,
		fSkewnessf, //float &fSkewnessf,
		fKurtosisf); // float &fKurtosisf); // float fPercentagesAboveThresholds_InHistogramArrf[]); //[nNumOfHistogramIntervals_ForCooc - 2]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Histogram_Statistics_Grayscale_Image' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Histogram_Statistics_Grayscale_Image' "); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

	Copying_HistogramFeatures_To_sFeasAllOfCoocf(

		fMeanf, //const float fMeanf,
		fStDevf, //const float fStDevf,
		fSkewnessf, //const float fSkewnessf,
		fKurtosisf, //const float fKurtosisf,

		fPercentsOfHistogramIntervalsArrf, //const float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		fPercentagesAboveThresholds_InHistogramArrf, //const float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)

/*
	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Copying_HistogramFeatures_To_sFeasAllOfCoocf' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Copying_HistogramFeatures_To_sFeasAllOfCoocf' "); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)
*/
//////////////////////////////////////////////////////////////////////
	//for (iDirecf = 0; iDirecf < nNumOfAnglesTot; iDirecf++)
	for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++)
	{
		for (iDistf = 0; iDistf < nNumOfDistancesf; iDistf++)
		{
			nDistCurf = nDistancesArr[iDistf];

			Initializing_Cooc_OrigImage(

				iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
											  //1--0 degrees, 2--45 degrees, 3--90 degrees

				nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

				&sCooc_Of_Orig_Imagef); //		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef)

			nResf = CoocOfOrigImage(
				iDirecf, //const int nIndicOfDirectionf,

				nDistCurf, //const int nDistOfCoocf,

				sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
				&sCooc_Of_Orig_Imagef); // COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n After 'CoocOfOrigImage': nResf = %d, iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d",
				nResf, iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
			fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//if (nResf == UNSUCCESSFUL_RETURN)
			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'CoocOfOrigImage' ");

				printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'CoocOfOrigImage' ");
				fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
				fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

				return UNSUCCESSFUL_RETURN; // -1;
			}// if (nResf == UNSUCCESSFUL_RETURN)


			for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //3
			{
				nDimOfSquareCurf = nDimOfSquare_ScaleCoocArr[iCoocScalef];

				//Impossible values
				nResf = Initializing_ScaleCooc_OrigImage(

					iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
												  //1--0 degrees, 2--45 degrees, 3--90 degrees

					nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

					nDimOfSquareCurf,
					&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);


				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Initializing_ScaleCooc_OrigImage' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Initializing_ScaleCooc_OrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

/////////////////////////////////////////////////////
				nResf = ScaleCooc_OrigImage(
					&sCooc_Of_Orig_Imagef, //const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

					nDimOfSquareCurf, //const int nDimOfSquaref,

					nNumOfScaleCooc_OrigImage_FeasCurf,

					&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);


				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'ScaleCooc_OrigImage' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'ScaleCooc_OrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Before 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
					iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

				fprintf(fout_PrintFeatures, "\n\n Before 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				fprintf(fout_PrintFeatures, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
					iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				nResf = Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf(

					nNumOfScaleCooc_OrigImage_FeasCurf, //const int nNumOfScaleCooc_OrigImage_FeasCurf,

					&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

					nNumOfScaleCooc_OrigImage_FeasTotSoFarf, //int &nNumOfScaleCooc_OrigImage_FeasTotSoFarf,

					sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				printf("\n iDistf = %d, iCoocScalef = %d, iDirecf = %d", iDistf, iCoocScalef, iDirecf);

				fprintf(fout_PrintFeatures, "\n\n After 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				fprintf(fout_PrintFeatures, "\n iDistf = %d, iCoocScalef = %d, iDirecf = %d", iDistf, iCoocScalef, iDirecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				///////////////////////////////////////////////////////
				nResf = FeaturesOfScaleCooc_OrigImage(
					&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

					&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'FeaturesOfScaleCooc_OrigImage' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'FeaturesOfScaleCooc_OrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)


				//nIndex3Dimf = iDirecf + iDistf*nNumOfAnglesTot + iCoocScalef*(nNumOfDistances*nNumOfAnglesTot);
				nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);

				if (nIndex3Dimf < 0 || nIndex3Dimf > nNumOfDirecAndDistAndScaleMaxf)
				{
					printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage': nIndex3Dimf = %d and nNumOfDirecAndDistAndScaleMaxf = %d",
						nIndex3Dimf, nNumOfDirecAndDistAndScaleMaxf);

#ifndef COMMENT_OUT_ALL_PRINTS

					fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage': nIndex3Dimf = %d and nNumOfDirecAndDistAndScaleMaxf = %d",
						nIndex3Dimf, nNumOfDirecAndDistAndScaleMaxf);

					fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} //if (nIndex3Dimf < 0 || nIndex3Dimf > nNumOfDirecAndDistAndScaleMaxf)

//nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);
				sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf] = sFeasOfCoocf.fEnergy;

#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_PrintFeatures, "\n\n 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage':  sFeasAllOfCoocf->fEnergyArr[%d] = %E",
					//nIndex3Dimf, sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf]);

				//nIndex3Dimf = iDirecf + (iDistf * nNumOfDirectionsTot) + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);
				//fprintf(fout_PrintFeatures, "\n nIndex3Dimf = %d, iDirecf = %d, iDistf = %d, iCoocScalef = %d", nIndex3Dimf, iDirecf, iDistf, iCoocScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				sFeasAllOfCoocf->fContrastArr[nIndex3Dimf] = sFeasOfCoocf.fContrast;

				sFeasAllOfCoocf->fCorrelationArr[nIndex3Dimf] = sFeasOfCoocf.fCorrelation;

				sFeasAllOfCoocf->fHomogeneityArr[nIndex3Dimf] = sFeasOfCoocf.fHomogeneity;

				sFeasAllOfCoocf->fEntropyArr[nIndex3Dimf] = sFeasOfCoocf.fEntropy;
				sFeasAllOfCoocf->fAutocorrelationArr[nIndex3Dimf] = sFeasOfCoocf.fAutocorrelation;

				sFeasAllOfCoocf->fDissimilarityArr[nIndex3Dimf] = sFeasOfCoocf.fDissimilarity;
				sFeasAllOfCoocf->fClusterShadeArr[nIndex3Dimf] = sFeasOfCoocf.fClusterShade;

				sFeasAllOfCoocf->fClusterProminenceArr[nIndex3Dimf] = sFeasOfCoocf.fClusterProminence;
				sFeasAllOfCoocf->fMaxProbArr[nIndex3Dimf] = sFeasOfCoocf.fMaxProb;

			} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //z

		} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++) //y

	} //for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++) //x

	nResf = Features_AverOverDirections_AllDistAndScaleCooc_OrigImage(
		sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Features_AverOverDirections_AllDistAndScaleCooc_OrigImage' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' by 'Features_AverOverDirections_AllDistAndScaleCooc_OrigImage' ");
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	}// if (nResf == UNSUCCESSFUL_RETURN)

	return SUCCESSFUL_RETURN;
} //int FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage(...

/////////////////////////////////////////////////////////////////
int Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf(

	const int nNumOfScaleCooc_OrigImage_FeasCurf,

	const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

	int &nNumOfScaleCooc_OrigImage_FeasTotSoFarf,

	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{

	int
		iFeaf,
		nFeaCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_PrintFeatures, "\n\n 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d, nAllScales_andCoocSizeMax = %d",
		nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf, nAllScales_andCoocSizeMax);//
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//new 
	if (nNumOfScaleCooc_OrigImage_FeasCurf > nScaleCoocSizeMax)
	{
		printf("\n\n An error in 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d > nScaleCoocSizeMax = %d",
			nNumOfScaleCooc_OrigImage_FeasCurf, nScaleCoocSizeMax);
		printf("\n\n Please press any key to exit"); getchar();	exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d > nScaleCoocSizeMax = %d",
			nNumOfScaleCooc_OrigImage_FeasCurf, nScaleCoocSizeMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfScaleCooc_OrigImage_FeasCurf > nScaleCoocSizeMax)

	for (iFeaf = 0; iFeaf < nNumOfScaleCooc_OrigImage_FeasCurf; iFeaf++)
	{
		nFeaCurf = iFeaf + nNumOfScaleCooc_OrigImage_FeasTotSoFarf; // for sScale_Cooc_Of_Orig_Imagef

		//nScaleCoocSizeMax
		if (nFeaCurf >= nAllScales_andCoocSizeMax)
		{
			printf("\n\n An error in 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nFeaCurf = %d >= nAllScales_andCoocSizeMax = %d",
				nFeaCurf, nAllScales_andCoocSizeMax);

			printf("\n iFeaf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
				iFeaf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

			fprintf(fout_PrintFeatures,"\n\n An error in 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nFeaCurf = %d >= nAllScales_andCoocSizeMax = %d",
				nFeaCurf, nAllScales_andCoocSizeMax);

			fprintf(fout_PrintFeatures,"\n iFeaf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d", 
				iFeaf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);
			
			printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar();	exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nFeaCurf = %d >= nAllScales_andCoocSizeMax = %d",
				nFeaCurf, nAllScales_andCoocSizeMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nFeaCurf >= nAllScales_andCoocSizeMax)

//sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nScaleCoocSizeMax] //64*64 = 4096
		sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[nFeaCurf] = sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[iFeaf];

		//#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_PrintFeatures, "\n 'Copying_AversOf_sScale...':  ... ->sFeasAllOfCoocf[%d] = %E, nFeaCurf = %d, iFeaf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d",
					//nFeaCurf, sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[nFeaCurf], nFeaCurf, iFeaf, nNumOfScaleCooc_OrigImage_FeasCurf);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // for (iFeaf = 0; iFeaf < nNumOfScaleCooc_OrigImage_FeasCurf; iFeaf++)

	nNumOfScaleCooc_OrigImage_FeasTotSoFarf += nNumOfScaleCooc_OrigImage_FeasCurf;

	return SUCCESSFUL_RETURN;
} // int Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf(...

//////////////////////////////////////////////////////////////////////
int Features_AverOverDirections_AllDistAndScaleCooc_OrigImage(
	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{

	int
		iDistf,
		iCoocScalef,

		iDirecf,
		nIndexDistScalef,

		nNumOfScalesAndDistMaxf = nNumOfScalesAndDistTot - 1,
		nIndex3Dimf;

	float
		fSumf = 0.0;

	////////////////////////////////////////////////////////////////
	for (iDistf = 0; iDistf < nNumOfDistances; iDistf++)
	{
		for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++)
		{
			nIndexDistScalef = iDistf + iCoocScalef * nNumOfDistances;

			sFeasAllOfCoocf->fEnergyAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fContrastAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fCorrelationAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fHomogeneityAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fEntropyAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fAutocorrelationAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fDissimilarityAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fClusterShadeAverOverDirecArr[nIndexDistScalef] = 0.0;

			sFeasAllOfCoocf->fClusterProminenceAverOverDirecArr[nIndexDistScalef] = 0.0;
			sFeasAllOfCoocf->fMaxProbAverOverDirecArr[nIndexDistScalef] = 0.0;

			//sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesAverOverDirecArr[nIndexDistScalef] = 0.0;

			if (nIndexDistScalef < 0 || nIndexDistScalef > nNumOfScalesAndDistMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Features_AverOverDirections_AllDistAndScaleCooc_OrigImage': nIndexDistScalef = %d and nNumOfScalesAndDistMaxf = %d",
					nIndexDistScalef, nNumOfScalesAndDistMaxf);

				fprintf(fout_lr, "\n\n An error in 'Features_AverOverDirections_AllDistAndScaleCooc_OrigImage': nIndexDistScalef = %d and nNumOfScalesAndDistMaxf = %d",
					nIndexDistScalef, nNumOfScalesAndDistMaxf);

				fflush(fout_lr); getchar();	exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} //if (nIndexDistScalef < 0 || nIndexDistScalef > nNumOfScalesAndDistMaxf)

//////////////////////
			//for (iDirecf = 0; iDirecf < nNumOfAnglesTot; iDirecf++)
			for (iDirecf = 0; iDirecf < nNumOfDirectionsTot; iDirecf++)
			{

				//nIndex3Dimf = iDirecf + iDistf*nNumOfAnglesTot + iCoocScalef*(nNumOfDistances*nNumOfAnglesTot);
				nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);

				sFeasAllOfCoocf->fEnergyAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf];

				sFeasAllOfCoocf->fContrastAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fContrastArr[nIndex3Dimf];

				sFeasAllOfCoocf->fCorrelationAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fCorrelationArr[nIndex3Dimf];
				sFeasAllOfCoocf->fHomogeneityAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fHomogeneityArr[nIndex3Dimf];

				sFeasAllOfCoocf->fEntropyAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fEntropyArr[nIndex3Dimf];
				sFeasAllOfCoocf->fAutocorrelationAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fAutocorrelationArr[nIndex3Dimf];

				sFeasAllOfCoocf->fDissimilarityAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fDissimilarityArr[nIndex3Dimf];
				sFeasAllOfCoocf->fClusterShadeAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fClusterShadeArr[nIndex3Dimf];

				sFeasAllOfCoocf->fClusterProminenceAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fClusterProminenceArr[nIndex3Dimf];
				sFeasAllOfCoocf->fMaxProbAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fMaxProbArr[nIndex3Dimf];

				//sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesAverOverDirecArr[nIndexDistScalef] += sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesArr[nIndex3Dimf];

			} //for (iDirecf = 0; iDirecf < nNumOfDirectionsTot; iDirecf++)

		} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++)

	} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++)

	return SUCCESSFUL_RETURN;
} //int Features_AverOverDirections_AllDistAndScaleCooc_OrigImage(...
///////////////////////////////////////////////////////////////////////////////////////////

int Histogram_Statistics_Grayscale_Image(

	const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageSizeMax]

	float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

	float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

	float &fMeanf,
	float &fStDevf,
	float &fSkewnessf,
	float &fKurtosisf)
{
	int
		nImageWidthf = sGrayscale_Imagef->nWidth,
		nImageLengthf = sGrayscale_Imagef->nLength,

		nHistogramArrf[nNumOfGrayLevelIntensities], //[nNumOfHistogramBinsStat]

		nHistInBinsArrf[nNumOfHistogramIntervals_ForCooc],
		nAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2], // -1]

		nAreaOfTheWholeImagef,

		//nDenseAreaf = 0,
		//nWidthOfABinInHistogramf = (nNumOfGrayLevelIntensities - nIntensityStatMin) / nNumOfHistogramBinsStat,
		nWidthOfABinInHistogramf = nNumOfGrayLevelIntensities / nNumOfHistogramIntervals_ForCooc,

		nNumOfHistogramBinCurf,

		nNumOfValidObjectPixelsf = 0,
		nIntensityCurf,
		nIndexOfPixelf,

		iHistf,
		iHistogramBinf,

		iHistThresholdf,

		nSumOfHistBinsf,
		iLenf,
		iWidf;

	float
		fTempf,
		fVariancef = 0.0,
		fHistogramProbabArrf[nNumOfGrayLevelIntensities];
	/////////////////////////////////////////////////

	nAreaOfTheWholeImagef = nImageWidthf * nImageLengthf;

	if (nAreaOfTheWholeImagef <= 0)
	{
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_Grayscale_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nAreaOfTheWholeImagef <= 0)

	fMeanf = 0.0;
	fStDevf = 0.0;
	fSkewnessf = 0.0;
	fKurtosisf = 0.0;

	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		nHistogramArrf[iHistf] = 0;

	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	//memset(nHistogramArrf, 0, sizeof(nHistogramArrf));
	//memset(fPercentagesInHistogramArrf, 0.0, sizeof(fPercentagesInHistogramArrf));

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	{
		nHistInBinsArrf[iHistogramBinf] = 0;
		fPercentsOfHistogramIntervalsArrf[iHistogramBinf] = 0.0;

		if (iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 2)
		{
			nAboveThresholds_InHistogramArrf[iHistogramBinf] = 0;

			fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf] = 0.0;
		} //if (iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 2)

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	  //////////////////////////////////////////////////////

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//fprintf(fout_lr, "\n\n iWidf = %d: ", iWidf);

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelf = iLenf + (iWidf*nImageLengthf);

			nIntensityCurf = sGrayscale_Imagef->nPixelArr[nIndexOfPixelf];

			if (nIntensityCurf == nIntensityOfBackground)
				continue;

			nNumOfValidObjectPixelsf += 1;
			//fprintf(fout_lr, " %d:%d %d,", iWidf, iLenf, nIntensityCurf);

			nHistogramArrf[nIntensityCurf] += 1;

			nNumOfHistogramBinCurf = nIntensityCurf / nWidthOfABinInHistogramf;

			nHistInBinsArrf[nNumOfHistogramBinCurf] += 1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
////////////////////////////////////////////////////////////////////////

	if (nNumOfValidObjectPixelsf <= 0)
	{
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': nNumOfValidObjectPixelsf = %d <= 0", nNumOfValidObjectPixelsf);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': nNumOfValidObjectPixelsf = %d <= 0", nNumOfValidObjectPixelsf);

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_Grayscale_Image': nNumOfValidObjectPixelsf = %d <= 0", nNumOfValidObjectPixelsf);
		printf("\n\n Please press any key to exit:");
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nNumOfValidObjectPixelsf <= 0)

	//////////////////////////////////////////////////////////////////////////////
//I.Pitas
//Digital image processing algorithms and applications, p.304

//fMeanf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fHistogramProbabArrf[iHistf] = (float)(nHistogramArrf[iHistf]) / (float)(nNumOfValidObjectPixelsf);

		fMeanf += fHistogramProbabArrf[iHistf] * (float)(iHistf);
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

//fMeanf
	if (fMeanf < 0.0 || fMeanf >(float)(nNumOfGrayLevelIntensities - 1))
	{
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': fMeanf = %E", fMeanf);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': fMeanf = %E", fMeanf);

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_Grayscale_Image': fMeanf = %E", fMeanf);
		printf("\n\n Please press any key to exit:");
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (fMeanf < 0.0 || fMeanf > (float)(nNumOfGrayLevelIntensities - 1))

///////////////////////////////////////////////////////////////////////////////	  
//fVariancef
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fVariancef += fTempf * fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

//fVariancef
	if (fVariancef <= 0.0)
	{
		printf("\n\n An error in 'Histogram_Statistics_Grayscale_Image': fVariancef = %E <= 0.0", fVariancef);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n An error in 'Histogram_Statistics_Grayscale_Image': fVariancef = %E <= 0.0", fVariancef);
		printf("\n\n Please press any key to exit:");
		fflush(fout_lr); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (fVariancef <= 0.0)

	fStDevf = sqrtf(fVariancef);
	///////////////////////////////////////////////////////////////////////

	//fSkewnessf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fSkewnessf += fTempf * fTempf*fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	fSkewnessf = fSkewnessf / (fVariancef * fStDevf);
	///////////////////////////////////////////////////////////////////////

	//fKurtosisf
	for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)
	{
		fTempf = (float)(iHistf)-fMeanf;
		fKurtosisf += fTempf * fTempf*fTempf*fTempf*fHistogramProbabArrf[iHistf];
	} //for (iHistf = 0; iHistf < nNumOfGrayLevelIntensities; iHistf++)

	fKurtosisf = (fKurtosisf / (fVariancef * fVariancef)) - 3.0;
	///////////////////////////////////////////////////////////////////////////////////
	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)
	{
		fPercentsOfHistogramIntervalsArrf[iHistogramBinf] = (float)(nHistInBinsArrf[iHistogramBinf]) / (float)(nNumOfValidObjectPixelsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'Histogram_Statistics_Grayscale_Image': fPercentsOfHistogramIntervalsArrf[%d] = %E", iHistogramBinf, fPercentsOfHistogramIntervalsArrf[iHistogramBinf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramIntervals_ForCooc; iHistogramBinf++)

	for (iHistogramBinf = 1; iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 1; iHistogramBinf++)
	{
		nSumOfHistBinsf = 0;

		for (iHistThresholdf = iHistogramBinf; iHistThresholdf < nNumOfHistogramIntervals_ForCooc; iHistThresholdf++)
		{
			nSumOfHistBinsf += nHistInBinsArrf[iHistThresholdf];

		} //for (iHistThresholdf = iHistThresholdf; iHistThresholdf < nNumOfHistogramIntervals_ForCooc; iHistThresholdf++)

		nAboveThresholds_InHistogramArrf[iHistogramBinf - 1] = nSumOfHistBinsf;

		fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf - 1] = (float)(nSumOfHistBinsf) / (float)(nNumOfValidObjectPixelsf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'Histogram_Statistics_Grayscale_Image': fPercentagesAboveThresholds_InHistogramArrf[%d] = %E", iHistogramBinf - 1, fPercentagesAboveThresholds_InHistogramArrf[iHistogramBinf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //for (iHistogramBinf = 1; iHistogramBinf < nNumOfHistogramIntervals_ForCooc - 1; iHistogramBinf++)

	return SUCCESSFUL_RETURN;
} //Histogram_Statistics_Grayscale_Image(...
/////////////////////////////////////////////////////////////////

void Copying_HistogramFeatures_To_sFeasAllOfCoocf(

	const float fMeanf,
	const float fStDevf,
	const float fSkewnessf,
	const float fKurtosisf,

	const float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

	const float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{

	int
		iFeaf,
		nFeaCurf;

	sFeasAllOfCoocf->fMeanf = fMeanf;
	sFeasAllOfCoocf->fStDevf = fStDevf;
	sFeasAllOfCoocf->fSkewnessf = fSkewnessf;
	sFeasAllOfCoocf->fKurtosisf = fKurtosisf;

	for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc; iFeaf++)
	{
		sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf] = fPercentsOfHistogramIntervalsArrf[iFeaf];
		if (iFeaf < nNumOfHistogramIntervals_ForCooc - 2)
		{
			sFeasAllOfCoocf->fPercentagesAboveThresholds_InHistogramArrf[iFeaf] = fPercentagesAboveThresholds_InHistogramArrf[iFeaf];
		} // if (iFeaf < nNumOfHistogramIntervals_ForCooc - 2)
	} // for (iFeaf = 0; iFeaf < nNumOfScaleCooc_OrigImage_FeasCurf; iFeaf++)

	//return SUCCESSFUL_RETURN;
} // void Copying_HistogramFeatures_To_sFeasAllOfCoocf(...
/////////////////////////////////////////////////////////////////

int doGenerating_CoocFeaturesInOneRow(
	const COLOR_IMAGE *sColor_Imagef,

	FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int Initializing_Grayscale_Image(
		const COLOR_IMAGE *sColor_Imagef,
		GRAYSCALE_IMAGE *sGrayscale_Imagef);

	int FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage(
		const int nNumOfDirectionsf,
		const int nIndicOfDirectionArrf[],

		const int nNumOfDistancesf,
		const int nDistancesArrf[],

		const int nScaleCoocTotf,
		const int nDimOfSquare_ScaleCoocArrf[],

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	void PrintingAll_CoocFeaturesAsAVector(
		const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int
		nRes;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		i,
		//	j,

		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nRed_Mask,
		//nGreen,
		//nBlue,

		nIntensityOfMaskImage,
		nIntensity_Read_Test_ImageMax = -nLarge,

		nImageWidth,
		nImageHeight;

	////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_Image_Features.txt", "w");

	if (fout_lr == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
*/

// size of image
	nImageWidth = sColor_Imagef->nWidth; //image_in.width();
	nImageHeight = sColor_Imagef->nLength; //image_in.height();

//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The total number of features: nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	printf("\n\n nCoocSizeMax = %d, nNumOfHistogramIntervals_ForCooc = %d, nNumOfDirecAndDistAndScaleTot = %d, nNumOfScalesAndDistTot = %d",
		nCoocSizeMax, nNumOfHistogramIntervals_ForCooc, nNumOfDirecAndDistAndScaleTot, nNumOfScalesAndDistTot);

	fprintf(fout_lr, "\n\n The total number of features: nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	fprintf(fout_lr, "\n\n nCoocSizeMax = %d, nNumOfHistogramIntervals_ForCooc = %d, nNumOfDirecAndDistAndScaleTot = %d, nNumOfScalesAndDistTot = %d",
		nCoocSizeMax, nNumOfHistogramIntervals_ForCooc, nNumOfDirecAndDistAndScaleTot, nNumOfScalesAndDistTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//printf("\n Testing: 1, please press any key"); getchar();
/////////////////////////////////////////////////////////////////////////////////////////////
	GRAYSCALE_IMAGE sGRAYSCALE_IMAGE;

	nRes = Initializing_Grayscale_Image(
		//nImageHeight, //const int nImageWidthf,
		//nImageWidth, //const int nImageLengthf,

		//&sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		&sGRAYSCALE_IMAGE); // GRAYSCALE_IMAGE *sGrayscale_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doGenerating_CoocFeaturesInOneRow' by 'Initializing_Grayscale_Image' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doGenerating_CoocFeaturesInOneRow' by 'Initializing_Grayscale_Image' ") fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sGRAYSCALE_IMAGE.nPixelArr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	//printf("\n Testing: 2, please press any key");  getchar();

////////////////////////////////////////////////
	int
		nDistOfCooc = nDistancesArr[nNumOfDistances - 1],
		iIndicOfDirection;

	COOC_OF_ORIG_IMAGE
		sCooc_Of_Orig_Image;

	/////////////////////////////////////////////////////////////////////////////////////////
	nRes = FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage(
		nNumOfDirectionsTot, //const int nNumOfDirectionsf,
		nIndicOfDirectionArr, //const int nIndicOfDirectionArrf[],

		nNumOfDistances, //const int nNumOfDistancesf,
		nDistancesArr, //const int nDistancesArrf[],

		nScaleCoocTot, //const int nScaleCoocTotf,
		nDimOfSquare_ScaleCoocArr, //const int nDimOfSquare_ScaleCoocArrf[],

		&sGRAYSCALE_IMAGE, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doGenerating_CoocFeaturesInOneRow' by 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doGenerating_CoocFeaturesInOneRow' by 'FeaturesOfAllDirectionsAnd_AllCoocScales_OrigImage' ") fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;
		/////////////////////////////////////////////////

		delete[] sGRAYSCALE_IMAGE.nPixelArr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	//printf("\n Testing: 3, please press any key");  getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
	PrintingAll_CoocFeaturesAsAVector(
		sFeasAllOfCoocf); // const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
in the 'Cooc_Multifrac_Lacunar.cpp'
	delete[] sColor_Imagef->nRed_Arr;
	delete[] sColor_Imagef->nGreen_Arr;
	delete[] sColor_Imagef->nBlue_Arr;

	delete[] sColor_Imagef->nLenObjectBoundary_Arr;
	delete[] sColor_Imagef->nIsAPixelBackground_Arr;
	/////////////////////////////////////////////////
*/
	delete[] sGRAYSCALE_IMAGE.nPixelArr;

	return SUCCESSFUL_RETURN;
} //int doGenerating_CoocFeaturesInOneRow...
  ///////////////////////////////////////////////////////////////////////

void PrintingAll_CoocFeaturesAsAVector(
	const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
{
	int
		nLimit_1f = nNumOfHistogramIntervals_ForCooc + 4,
		nLimit_2f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + 4,
		nLimit_3f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + 4,

		//nLimit_4f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (11*nNumOfDirecAndDistAndScaleTot),

		nNumOfIterOver_10_Feasf = nNumOfDirecAndDistAndScaleTot,

		nNumOfIterOver_AverOf_11_Feasf = nNumOfScalesAndDistTot,

		iIterOver_10_Feasf,

		iFeaf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector': nLimit_1f = %d, nLimit_2f = %d, nLimit_3f = %d", nLimit_1f, nLimit_2f, nLimit_3f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_PrintFeatures, "%d:%E, ", 0, sFeasAllOfCoocf->fMeanf);
	fprintf(fout_PrintFeatures, "%d:%E, ", 1, sFeasAllOfCoocf->fStDevf);
	fprintf(fout_PrintFeatures, "%d:%E, ", 2, sFeasAllOfCoocf->fSkewnessf);
	fprintf(fout_PrintFeatures, "%d:%E, ", 3, sFeasAllOfCoocf->fKurtosisf);

	//for (iFeaf = 0; iFeaf < nLimit_3f; iFeaf++)
	for (iFeaf = 4; iFeaf < nLimit_3f; iFeaf++)
	{
		if (iFeaf < nLimit_1f)
		{
			fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf]);
		} // if (iFeaf < nNumOfHistogramIntervals_ForCooc)

		else if (iFeaf >= nLimit_1f && iFeaf < nLimit_2f)
		{
			fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fPercentagesAboveThresholds_InHistogramArrf[iFeaf - nLimit_1f]);

		}// else if (iFeaf >= nLimit_1f && iFeaf < nLimit_3f)

		else if (iFeaf >= nLimit_2f && iFeaf < nLimit_3f)
		{

			fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[iFeaf - nLimit_2f]);

		}// else if (iFeaf >= nLimit_2f && iFeaf < nLimit_3f)

	}// for (iFeaf = 4; iFeaf < nLimit_3f; iFeaf++)

	iFeaf = nLimit_3f;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector' (after for (iFeaf = 0; iFeaf < nLimit_3f; iFeaf++) ): iFeaf = %d", iFeaf);
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector' : iFeaf = nLimit_3f = %d, nNumOfIterOver_10_Feasf = %d, nNumOfIterOver_AverOf_11_Feasf = %d", iFeaf, nNumOfIterOver_10_Feasf, nNumOfIterOver_AverOf_11_Feasf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////

	for (iIterOver_10_Feasf = 0; iIterOver_10_Feasf < nNumOfIterOver_10_Feasf; iIterOver_10_Feasf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector' 1: iIterOver_10_Feasf = %d, iFeaf = %d", iIterOver_10_Feasf, iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fEnergyArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fContrastArr[iIterOver_10_Feasf]);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector': iFeaf = %d, sFeasAllOfCoocf->fContrastArr[%d] = %E", iFeaf, iIterOver_10_Feasf, sFeasAllOfCoocf->fContrastArr[iIterOver_10_Feasf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fCorrelationArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fHomogeneityArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fEntropyArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fAutocorrelationArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fDissimilarityArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fClusterShadeArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fClusterProminenceArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fMaxProbArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		//fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesArr[iIterOver_10_Feasf]);
		//iFeaf += 1;

	} // for (iIterOver_10_Feasf = 0; iIterOver_10_Feasf < nNumOfIterOver_10_Feasf; iIterOver_10_Feasf++)

///////////////////////////////////////////////////////////////////////////////////////


	for (iIterOver_10_Feasf = 0; iIterOver_10_Feasf < nNumOfIterOver_AverOf_11_Feasf; iIterOver_10_Feasf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector' 2: iIterOver_10_Feasf = %d, iFeaf = %d", iIterOver_10_Feasf, iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fEnergyAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fContrastAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fCorrelationAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fHomogeneityAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fEntropyAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fAutocorrelationAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fDissimilarityAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fClusterShadeAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fClusterProminenceAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fMaxProbAverOverDirecArr[iIterOver_10_Feasf]);
		iFeaf += 1;

		//fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fMaxProb_At_HighIntensitiesAverOverDirecArr[iIterOver_10_Feasf]);
		//iFeaf += 1;
	} // for (iIterOver_10_Feasf = 0; iIterOver_10_Feasf < nNumOfIterOver_AverOf_11_Feasf; iIterOver_10_Feasf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end in 'PrintingAll_CoocFeaturesAsAVector': iIterOver_10_Feasf = %d, iFeaf = %d", iIterOver_10_Feasf, iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_PrintFeatures);

	//return 1;
} //void PrintingAll_CoocFeaturesAsAVector(...
/////////////////////////////////////////////////////////////

int Copying_CoocFeaturesAsAVector(
	const int nDimf, // == nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot

	const int nPositInitf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot // - 1
	const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

	float fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]
{
	int
		nLimit_1f = nNumOfHistogramIntervals_ForCooc + 4,
		nLimit_2f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + 4,
		nLimit_3f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + 4,

		//nLimit_4f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (11*nNumOfDirecAndDistAndScaleTot),

		nNumOfIterOver_10_Feasf = nNumOfDirecAndDistAndScaleTot,

		nNumOfIterOver_AverOf_11_Feasf = nNumOfScalesAndDistTot,

		iIterOver_10_Feasf,

		nFeaf,
		iFeaf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector': nLimit_1f = %d, nLimit_2f = %d, nLimit_3f = %d", nLimit_1f, nLimit_2f, nLimit_3f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//fprintf(fout_PrintFeatures, "%d:%E, ", 0, sFeasAllOfCoocf->fMeanf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 1, sFeasAllOfCoocf->fStDevf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 2, sFeasAllOfCoocf->fSkewnessf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 3, sFeasAllOfCoocf->fKurtosisf);

	nFeaf = nPositInitf;
	if (nFeaf >= nDimf)
	{
		printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 1: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaf >= nDimf)
	fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fMeanf;
	/////////////////////////////////
	nFeaf = nFeaf + 1;
	if (nFeaf >= nDimf)
	{
		printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 2: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaf >= nDimf)
	fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fStDevf;
	/////////////////////////////////

	nFeaf = nFeaf + 1;
	if (nFeaf >= nDimf)
	{
		printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 3: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaf >= nDimf)
	fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fSkewnessf;
	/////////////////////////////////
	nFeaf = nFeaf + 1;
	if (nFeaf >= nDimf)
	{
		printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 4: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaf >= nDimf)
	fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fKurtosisf;

	/////////////////////////////////////////////////////////////////////////////////////////////////////

	//	float fPercentsOfHistogramIntervalsArrf[nNumOfHistogramIntervals_ForCooc];
	for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc; iFeaf++)
	{
		//	fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf]);
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 5: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf];

	} // for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc; iFeaf++)
	//////////////////////////////////////

//	float fPercentagesAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2]
	for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc - 2; iFeaf++)
	{
		//	fprintf(fout_PrintFeatures, "%d:%E, ", iFeaf, sFeasAllOfCoocf->fPercentsOfHistogramIntervalsArrf[iFeaf]);
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 6: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fPercentagesAboveThresholds_InHistogramArrf[iFeaf];

	} // for (iFeaf = 0; iFeaf < nNumOfHistogramIntervals_ForCooc - 2; iFeaf++)

	//////////////////////////////////////

//	float fAverOf_AllScalesCoocArr[nAllScales_andCoocSizeMax];
	for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 7: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[iFeaf];

	} // for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)
///////////////////////////////////////////////////////////////////


//	float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
	//float fContrastArr[nNumOfDirecAndDistAndScaleTot];

	//float fCorrelationArr[nNumOfDirecAndDistAndScaleTot];
	//float fHomogeneityArr[nNumOfDirecAndDistAndScaleTot];

	//float fEntropyArr[nNumOfDirecAndDistAndScaleTot];
	//float fAutocorrelationArr[nNumOfDirecAndDistAndScaleTot];

	//	float fDissimilarityArr[nNumOfDirecAndDistAndScaleTot];
	//	float fClusterShadeArr[nNumOfDirecAndDistAndScaleTot];

	//	float fClusterProminenceArr[nNumOfDirecAndDistAndScaleTot];

	//	float fMaxProbArr[nNumOfDirecAndDistAndScaleTot];

	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 8: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fEnergyArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 9: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fContrastArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 10: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fCorrelationArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 11: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fHomogeneityArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 12: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fEntropyArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 13: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fAutocorrelationArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 14: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fDissimilarityArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 15: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fClusterShadeArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 16: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fClusterProminenceArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	///////////////////
	for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 17: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fMaxProbArr[iFeaf];

	} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//float fEnergyAverOverDirecArr[nNumOfScalesAndDistTot];
		//float fContrastAverOverDirecArr[nNumOfScalesAndDistTot];

		//float fCorrelationAverOverDirecArr[nNumOfScalesAndDistTot];
		//float fHomogeneityAverOverDirecArr[nNumOfScalesAndDistTot];

		//float fEntropyAverOverDirecArr[nNumOfScalesAndDistTot];
		//float fAutocorrelationAverOverDirecArr[nNumOfScalesAndDistTot];

		//float fDissimilarityAverOverDirecArr[nNumOfScalesAndDistTot];
		//float fClusterShadeAverOverDirecArr[nNumOfScalesAndDistTot];

		//float fClusterProminenceAverOverDirecArr[nNumOfScalesAndDistTot];
		//float fMaxProbAverOverDirecArr[nNumOfScalesAndDistTot];

	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 18: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fEnergyAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////

	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 19: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fContrastAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 20: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fCorrelationAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 21: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fHomogeneityAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 22: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fEntropyAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 23: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fAutocorrelationAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 24: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fDissimilarityAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 25: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fClusterShadeAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

		///////////////////
	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf >= nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 26: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf >= nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fClusterProminenceAverOverDirecArr[iFeaf];
	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
		///////////////////

	for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)
	{
		nFeaf = nFeaf + 1;
		if (nFeaf > nDimf)
		{
			printf("\n\n An error in 'Copying_CoocFeaturesAsAVector' 27: nFeaf = %d >= nDimf = %d", nFeaf, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} //if (nFeaf > nDimf)

		  //float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
		fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fMaxProbAverOverDirecArr[iFeaf];

	} // for (iFeaf = 0; iFeaf < nNumOfScalesAndDistTot; iFeaf++)

	if (nFeaf + 1 != nDimf)
	{
		printf("\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf + 1 = %d != nDimf = %d", nFeaf + 1, nDimf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Copying_CoocFeaturesAsAVector': nFeaf = %d >= nDimf = %d", nFeaf, nDimf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nFeaf + 1 != nDimf)

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end in 'Copying_CoocFeaturesAsAVector': nFeaf = %d", nFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_PrintFeatures);

	return SUCCESSFUL_RETURN;
} //int Copying_CoocFeaturesAsAVector(
////////////////////////////////////////////////////////

int doOneCoocFeature(
	const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

	const int nPosit_OfCoocFeaf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot // - 1

	const int nNumOfDirectionsf,
	const int nIndicOfDirectionArrf[],

	const int nNumOfDistancesf,
	const int nDistancesArrf[],

	const int nScaleCoocTotf,
	const int nDimOfSquare_ScaleCoocArrf[],

	//const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

	const COLOR_IMAGE *sColor_Imagef,

	float &fOneFeaf) //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]
{
	int CoocOfOrigImage(
		const int nIndicOfDirectionf,
		const int nDistOfCoocf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,
		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

	int FeaturesOfOrigCooc_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

		FEATURES_OF_COOC *sFeasOfCooc);

	int FeaturesOfScaleCooc_OrigImage(
		const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

		FEATURES_OF_COOC *sFeasOfCoocf);

	int ScaleCooc_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef,

		const int nDimOfSquaref,

		int &nNumOfScaleCooc_OrigImage_FeasCurf,

		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

	int Initializing_ScaleCooc_OrigImage(

		const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
									  //1--0 degrees, 2--45 degrees, 3--90 degrees

		const int nDistOfCoocf, //distance of cooccurrence

		const int nDimOfSquaref,
		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);


	int Initializing_FeasAllOfCooc(

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Histogram_Statistics_Grayscale_Image(

		const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageSizeMax]

		float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		float fPercentagesAboveThresholds_InHistogramArrf[],

		float &fMeanf,
		float &fStDevf,
		float &fSkewnessf,
		float &fKurtosisf); //[nNumOfHistogramIntervals_ForCooc - 2]

	void Copying_HistogramFeatures_To_sFeasAllOfCoocf(

		const float fMeanf,
		const float fStDevf,
		const float fSkewnessf,
		const float fKurtosisf,

		const float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		const float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Features_AverOverDirections_AllDistAndScaleCooc_OrigImage(
		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	void iDistf_And_iCoocScalef_For_nIndexDistScalef(
		const int nIndexDistScalef,

		int &iDistf,
		int &iCoocScalef);

	int AverOverDirec_AtFixed_IDist_And_iCoocScale(
		const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

		const int nNumOfDirectionsf,

		const int nNumOfDistancesf,
		const int nDistancesArrf[],

		const int nScaleCoocTotf,

		const COLOR_IMAGE *sColor_Imagef,
		///////////////////////////////////////
	/*
	0 --	fEnergyAverOverDirecf = 0.0,
	1 --	fContrastAverOverDirecf = 0.0,

	2 --	fCorrelationAverOverDirecf = 0.0,
	3 --	fHomogeneityAverOverDirecf = 0.0,

	4 --	fEntropyAverOverDirecf = 0.0,
	5 --	fAutocorrelationAverOverDirecf = 0.0,

	6 --	fDissimilarityAverOverDirecf = 0.0,
	7 --	fClusterShadeAverOverDirecf = 0.0,

	8 --	fClusterProminenceAverOverDirecf = 0.0,
	9 --	fMaxProbAverOverDirecf = 0.0,
	*/
		const int nIndicOfFeaClassf,

		const int iDistf,
		const int iCoocScalef,

		float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]
	//////////////////////////////////////////////////////////////////

	int
		nResf,
		//nLimit_1f = nNumOfHistogramIntervals_ForCooc + 4,
		//nLimit_2f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + 4,
		//nLimit_3f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + 4,

		//nLimit_4f = 2 * nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (11*nNumOfDirecAndDistAndScaleTot),

		nNumOfIterOver_AverOf_11_Feasf = nNumOfScalesAndDistTot,

		iIterOver_10_Feasf,

		nFeaf,
		iFeaf;

	int
		nPosit_OfCoocFea_Prevf,
		iDirecf,
		iDistf,
		iCoocScalef,

		nIndex3Dimf,

		nNumOfDirecAndDistAndScaleMaxf = nNumOfDirecAndDistAndScaleTot - 1,

		nNumOfScaleCooc_OrigImage_FeasTotSoFarf = 0,

		nSumOfNumOfScaleCooc_OrigImage_FeasCurf = 0,
		nSumOfNumOfScaleCooc_OrigImage_FeasPrevf = 0,

		nDimOfSquareCurf,

		nNumOfScaleCooc_OrigImage_FeasCurf,

		nPosit_OfCoocFea_Internallyf,

		nNumOfDirecAndDistAndScale_Curf = 0,

		nNumOfDirecAndDistAndScale_MaxForCurf = nScaleCoocTot * nNumOfDistances*nNumOfDirectionsf,
		//////////////////////////////////////////////////////////
		//4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax,

				//	float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
				//nPosEnergyMinf = (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax) + nNumOfDirecAndDistAndScaleTot,
		nPosEnergyMinf = (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax),
		nPosEnergyMaxf = nPosEnergyMinf + nNumOfDirecAndDistAndScaleTot,

		//float fContrastArr[nNumOfDirecAndDistAndScaleTot];
		nPosContrastMinf = nPosEnergyMaxf,
		nPosContrastMaxf = nPosContrastMinf + nNumOfDirecAndDistAndScaleTot,

		//float fCorrelationArr[nNumOfDirecAndDistAndScaleTot];
		nPosCorrelationMinf = nPosContrastMaxf,
		nPosCorrelationMaxf = nPosCorrelationMinf + nNumOfDirecAndDistAndScaleTot,

		//float fHomogeneityArr[nNumOfDirecAndDistAndScaleTot];
		nPosHomogeneityMinf = nPosCorrelationMaxf,
		nPosHomogeneityMaxf = nPosHomogeneityMinf + nNumOfDirecAndDistAndScaleTot,

		//float fEntropyArr[nNumOfDirecAndDistAndScaleTot];
		nPosEntropyMinf = nPosHomogeneityMaxf,
		nPosEntropyMaxf = nPosEntropyMinf + nNumOfDirecAndDistAndScaleTot,

		//float fAutocorrelationArr[nNumOfDirecAndDistAndScaleTot];
		nPosAutocorrelationMinf = nPosEntropyMaxf,
		nPosAutocorrelationMaxf = nPosAutocorrelationMinf + nNumOfDirecAndDistAndScaleTot,

		//	float fDissimilarityArr[nNumOfDirecAndDistAndScaleTot];
		nPosDissimilarityMinf = nPosAutocorrelationMaxf,
		nPosDissimilarityMaxf = nPosDissimilarityMinf + nNumOfDirecAndDistAndScaleTot,

		//	float fClusterShadeArr[nNumOfDirecAndDistAndScaleTot];
		nPosClusterShadeMinf = nPosDissimilarityMaxf,
		nPosClusterShadeMaxf = nPosClusterShadeMinf + nNumOfDirecAndDistAndScaleTot,

		//	float fClusterProminenceArr[nNumOfDirecAndDistAndScaleTot];
		nPosClusterProminenceMinf = nPosClusterShadeMaxf,
		nPosClusterProminenceMaxf = nPosClusterProminenceMinf + nNumOfDirecAndDistAndScaleTot,

		//	float fMaxProbArr[nNumOfDirecAndDistAndScaleTot];
		nPosMaxProbMinf = nPosClusterProminenceMaxf,
		nPosMaxProbMaxf = nPosMaxProbMinf + nNumOfDirecAndDistAndScaleTot,
		/////////////////////////////////////////////////////////////////////////////////////////
		//
			//float fEnergyAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosEnergyAverOverDirecMinf = nPosMaxProbMaxf,
		nPosEnergyAverOverDirecMaxf = nPosEnergyAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fContrastAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosContrastAverOverDirecMinf = nPosEnergyAverOverDirecMaxf,
		nPosContrastAverOverDirecMaxf = nPosContrastAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fCorrelationAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosCorrelationAverOverDirecMinf = nPosContrastAverOverDirecMaxf,
		nPosCorrelationAverOverDirecMaxf = nPosCorrelationAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fHomogeneityAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosHomogeneityAverOverDirecMinf = nPosCorrelationAverOverDirecMaxf,
		nPosHomogeneityAverOverDirecMaxf = nPosHomogeneityAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fEntropyAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosEntropyAverOverDirecMinf = nPosHomogeneityAverOverDirecMaxf,
		nPosEntropyAverOverDirecMaxf = nPosEntropyAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fAutocorrelationAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosAutocorrelationAverOverDirecMinf = nPosEntropyAverOverDirecMaxf,
		nPosAutocorrelationAverOverDirecMaxf = nPosAutocorrelationAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fDissimilarityAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosDissimilarityAverOverDirecMinf = nPosAutocorrelationAverOverDirecMaxf,
		nPosDissimilarityAverOverDirecMaxf = nPosDissimilarityAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fClusterShadeAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosClusterShadeAverOverDirecMinf = nPosDissimilarityAverOverDirecMaxf,
		nPosClusterShadeAverOverDirecMaxf = nPosClusterShadeAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fClusterProminenceAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosClusterProminenceAverOverDirecMinf = nPosClusterShadeAverOverDirecMaxf,
		nPosClusterProminenceAverOverDirecMaxf = nPosClusterProminenceAverOverDirecMinf + nNumOfScalesAndDistTot,

		//float fMaxProbAverOverDirecArr[nNumOfScalesAndDistTot];
		nPosMaxProbAverOverDirecMinf = nPosClusterProminenceAverOverDirecMaxf,
		nPosMaxProbAverOverDirecMaxf = nPosMaxProbAverOverDirecMinf + nNumOfScalesAndDistTot,

		nDistCurf;

	float
		fMeanf,
		fStDevf,
		fSkewnessf,
		fKurtosisf,
		///////////////////////////////////////
		fEnergyAverOverDirecf = 0.0,
		fContrastAverOverDirecf = 0.0,

		fCorrelationAverOverDirecf = 0.0,
		fHomogeneityAverOverDirecf = 0.0,

		fEntropyAverOverDirecf = 0.0,
		fAutocorrelationAverOverDirecf = 0.0,

		fDissimilarityAverOverDirecf = 0.0,
		fClusterShadeAverOverDirecf = 0.0,

		fClusterProminenceAverOverDirecf = 0.0,
		fMaxProbAverOverDirecf = 0.0,
		//////////////////////////////////////
		fAverFeaType_OverDirecf,

		fPercentsOfHistogramIntervalsArrf[nNumOfHistogramIntervals_ForCooc],//[]
		fPercentagesAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2]; //[nNumOfHistogramIntervals_ForCooc - 1]

	COOC_OF_ORIG_IMAGE
		sCooc_Of_Orig_Imagef;

	SCALE_COOC_OF_ORIG_IMAGE
		sScale_Cooc_Of_Orig_Imagef;

	FEATURES_OF_COOC
		sFeasOfCoocf;

	//FEATURES_ALL_OF_COOC 
		//		sFeasAllOfCoocf;

//	nResf = Initializing_FeasAllOfCooc(
	//	&sFeasAllOfCoocf); // FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
//////////////////////////////////////////////////////////////////////////////////////////////////////

	if (nPosit_OfCoocFeaf >= nPosEnergyMinf && nPosit_OfCoocFeaf < nPosEnergyMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEnergyMinf;
	} //if (nPosit_OfCoocFeaf >= nPosEnergyMinf && nPosit_OfCoocFeaf < nPosEnergyMaxf)

	else if (nPosit_OfCoocFeaf >= nPosContrastMinf && nPosit_OfCoocFeaf < nPosContrastMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosContrastMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosContrastMinf && nPosit_OfCoocFeaf < nPosContrastMaxf)

	else if (nPosit_OfCoocFeaf >= nPosCorrelationMinf && nPosit_OfCoocFeaf < nPosCorrelationMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosCorrelationMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosCorrelationMinf && nPosit_OfCoocFeaf < nPosCorrelationMaxf)

	else if (nPosit_OfCoocFeaf >= nPosHomogeneityMinf && nPosit_OfCoocFeaf < nPosHomogeneityMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosHomogeneityMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosHomogeneityMinf && nPosit_OfCoocFeaf < nPosHomogeneityMaxf)

	else if (nPosit_OfCoocFeaf >= nPosEntropyMinf && nPosit_OfCoocFeaf < nPosEntropyMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEntropyMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosEntropyMinf && nPosit_OfCoocFeaf < nPosEntropyMaxf)

	else if (nPosit_OfCoocFeaf >= nPosAutocorrelationMinf && nPosit_OfCoocFeaf < nPosAutocorrelationMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosAutocorrelationMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosAutocorrelationMinf && nPosit_OfCoocFeaf < nPosAutocorrelationMaxf)

	else if (nPosit_OfCoocFeaf >= nPosDissimilarityMinf && nPosit_OfCoocFeaf < nPosDissimilarityMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosDissimilarityMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosDissimilarityMinf && nPosit_OfCoocFeaf < nPosDissimilarityMaxf)

	else if (nPosit_OfCoocFeaf >= nPosClusterShadeMinf && nPosit_OfCoocFeaf < nPosClusterShadeMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterShadeMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosClusterShadeMinf && nPosit_OfCoocFeaf < nPosClusterShadeMaxf)

	else if (nPosit_OfCoocFeaf >= nPosClusterProminenceMinf && nPosit_OfCoocFeaf < nPosClusterProminenceMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterProminenceMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosClusterProminenceMinf && nPosit_OfCoocFeaf < nPosClusterProminenceMaxf)

	else if (nPosit_OfCoocFeaf >= nPosMaxProbMinf && nPosit_OfCoocFeaf < nPosMaxProbMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosMaxProbMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosMaxProbMinf && nPosit_OfCoocFeaf < nPosMaxProbMaxf)
///////////////////////////////////////////////////////////////////////////////////////////////////
//	nPosEnergyAverOverDirecMinf = nPosMaxProbMaxf,
//	nPosEnergyAverOverDirecMaxf = nPosEnergyAverOverDirecMinf + nNumOfScalesAndDistTot,

	else if (nPosit_OfCoocFeaf >= nPosEnergyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEnergyAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosEnergyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosContrastAverOverDirecMinf && nPosit_OfCoocFeaf < nPosContrastAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosContrastAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosContrastAverOverDirecMinf && nPosit_OfCoocFeaf < nPosContrastAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosCorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosCorrelationAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosCorrelationAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosCorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosCorrelationAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosHomogeneityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosHomogeneityAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosHomogeneityAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosHomogeneityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosHomogeneityAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosEntropyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEntropyAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEntropyAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosEntropyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEntropyAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosAutocorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosAutocorrelationAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosAutocorrelationAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosAutocorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosAutocorrelationAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosDissimilarityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosDissimilarityAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosDissimilarityAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosDissimilarityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosDissimilarityAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosClusterShadeAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterShadeAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterShadeAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosClusterShadeAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterShadeAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosClusterProminenceAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterProminenceAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterProminenceAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosClusterProminenceAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterProminenceAverOverDirecMaxf)

	else if (nPosit_OfCoocFeaf >= nPosMaxProbAverOverDirecMinf && nPosit_OfCoocFeaf < nPosMaxProbAverOverDirecMaxf)
	{
		nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosMaxProbAverOverDirecMinf;
	} //else if (nPosit_OfCoocFeaf >= nPosMaxProbAverOverDirecMinf && nPosit_OfCoocFeaf < nPosMaxProbAverOverDirecMaxf)



#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector': nLimit_1f = %d, nLimit_2f = %d, nLimit_3f = %d", nLimit_1f, nLimit_2f, nLimit_3f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//fprintf(fout_PrintFeatures, "%d:%E, ", 0, sFeasAllOfCoocf->fMeanf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 1, sFeasAllOfCoocf->fStDevf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 2, sFeasAllOfCoocf->fSkewnessf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 3, sFeasAllOfCoocf->fKurtosisf);

	fOneFeaf = -fLarge; //initially
////////////////////////////////////////////////////////////////////////////////////
	nFeaf = nPosit_OfCoocFeaf;
	if (nFeaf >= nDimCoocf)
	{
		printf("\n\n An error in 'doOneCoocFeature': nFeaf = %d >= nDimCoocf = %d", nFeaf, nDimCoocf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': nFeaf = %d >= nDimCoocf = %d", nFeaf, nDimCoocf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaf >= nDimCoocf)

///////////////////////////////////
	GRAYSCALE_IMAGE sGrayscale_Imagef;
	nResf = Initializing_Grayscale_Image(
		//nImageHeight, //const int nImageWidthf,
		//nImageWidth, //const int nImageLengthf,

		//&sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		&sGrayscale_Imagef); // GRAYSCALE_IMAGE *sGrayscale_Imagef);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'doOneCoocFeature' by 'Initializing_Grayscale_Image' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'Initializing_Grayscale_Image' ") fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sGrayscale_Imagef.nPixelArr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

////////////////////////////
	//if (nPosit_OfCoocFeaf <= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))
	if (nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))
	{
		nResf = Histogram_Statistics_Grayscale_Image(

			&sGrayscale_Imagef, //GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageSizeMax]

			fPercentsOfHistogramIntervalsArrf, //float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

			fPercentagesAboveThresholds_InHistogramArrf,
			fMeanf, //float &fMeanf,
			fStDevf, //float &fStDevf,
			fSkewnessf, //float &fSkewnessf,
			fKurtosisf); // float &fKurtosisf); // float fPercentagesAboveThresholds_InHistogramArrf[]); //[nNumOfHistogramIntervals_ForCooc - 2]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'doOneCoocFeature' by 'Histogram_Statistics_Grayscale_Image' ");

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'Histogram_Statistics_Grayscale_Image' "); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			delete[] sColor_Imagef->nRed_Arr;
			delete[] sColor_Imagef->nGreen_Arr;
			delete[] sColor_Imagef->nBlue_Arr;

			delete[] sColor_Imagef->nLenObjectBoundary_Arr;
			delete[] sColor_Imagef->nIsAPixelBackground_Arr;

			/////////////////////////////////////////////////
			delete[] sGrayscale_Imagef.nPixelArr;

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		}// if (nResf == UNSUCCESSFUL_RETURN)

///////////////////////////////////////////////

		if (nPosit_OfCoocFeaf < 4)
		{
			nPosit_OfCoocFea_Prevf = 4;
			if (nPosit_OfCoocFeaf == 0)
			{
				fOneFeaf = fMeanf;

				delete[] sGrayscale_Imagef.nPixelArr;
				return SUCCESSFUL_RETURN;
			} // if (nPosit_OfCoocFeaf == 0)
			else if (nPosit_OfCoocFeaf == 1)
			{
				fOneFeaf = fStDevf;

				delete[] sGrayscale_Imagef.nPixelArr;
				return SUCCESSFUL_RETURN;
			} //else if (nPosit_OfCoocFeaf == 1)
			else if (nPosit_OfCoocFeaf == 2)
			{
				fOneFeaf = fSkewnessf;

				delete[] sGrayscale_Imagef.nPixelArr;
				return SUCCESSFUL_RETURN;
			} //else if (nPosit_OfCoocFeaf == 2)
			else if (nPosit_OfCoocFeaf == 3)
			{
				fOneFeaf = fKurtosisf;

				delete[] sGrayscale_Imagef.nPixelArr;
				return SUCCESSFUL_RETURN;
			} //else if (nPosit_OfCoocFeaf == 3)

		} //if (nPosit_OfCoocFeaf < 4)
		else if (nPosit_OfCoocFeaf >= 4 && nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc)
		{
			nPosit_OfCoocFea_Prevf = 4;

			fOneFeaf = fPercentsOfHistogramIntervalsArrf[nPosit_OfCoocFeaf - nPosit_OfCoocFea_Prevf];

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= 4 && nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc)

		else if (nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc && nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))
		{
			nPosit_OfCoocFea_Prevf = 4 + nNumOfHistogramIntervals_ForCooc;

			fOneFeaf = fPercentagesAboveThresholds_InHistogramArrf[nPosit_OfCoocFeaf - nPosit_OfCoocFea_Prevf];
			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		}//else if (nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc && nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))
	} //if (nPosit_OfCoocFeaf <= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))

	//else if (nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2))
	if (nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) && nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMinf)
	{

		/*
		//	float fAverOf_AllScalesCoocArr[nAllScales_andCoocSizeMax];
			for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)
			{
				nFeaf = nFeaf + 1;
				fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fAverOf_AllScalesCoocArr[iFeaf];

			} // for (iFeaf = 0; iFeaf < nAllScales_andCoocSizeMax; iFeaf++)
		*/
		//fAverOf_AllScalesCoocArr[
		nPosit_OfCoocFea_Prevf = 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2);

		for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++)
		{
			for (iDistf = 0; iDistf < nNumOfDistancesf; iDistf++)
			{
				nDistCurf = nDistancesArr[iDistf];

				Initializing_Cooc_OrigImage(

					iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
												  //1--0 degrees, 2--45 degrees, 3--90 degrees

					nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

					&sCooc_Of_Orig_Imagef); //		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef)

				nResf = CoocOfOrigImage(
					iDirecf, //const int nIndicOfDirectionf,

					nDistCurf, //const int nDistOfCoocf,

					&sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
					&sCooc_Of_Orig_Imagef); // COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n After 'CoocOfOrigImage': nResf = %d, iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d",
					nResf, iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
				fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//if (nResf == UNSUCCESSFUL_RETURN)
				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'doOneCoocFeature' by 'CoocOfOrigImage' ");
					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'CoocOfOrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN; // -1;
				}// if (nResf == UNSUCCESSFUL_RETURN)

				//nScaleCoocTot == 3
				for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //3
				{
					nIndex3Dimf = iDirecf + (iDistf * nNumOfDirectionsTot) + (iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot));

					nNumOfDirecAndDistAndScale_Curf += 1;

					nDimOfSquareCurf = nDimOfSquare_ScaleCoocArr[iCoocScalef];

					//Impossible values
					nResf = Initializing_ScaleCooc_OrigImage(

						iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
													  //1--0 degrees, 2--45 degrees, 3--90 degrees
						nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

						nDimOfSquareCurf,
						&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

					if (nResf == UNSUCCESSFUL_RETURN)
					{
						printf("\n\n An error in 'doOneCoocFeature' by 'Initializing_ScaleCooc_OrigImage' ");

						printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
							iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'Initializing_ScaleCooc_OrigImage' ");
						fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
							iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						printf("\n\n Please press any key to exit:");	getchar(); exit(1);

						return UNSUCCESSFUL_RETURN;
					}// if (nResf == UNSUCCESSFUL_RETURN)

	/////////////////////////////////////////////////////
					nResf = ScaleCooc_OrigImage(
						&sCooc_Of_Orig_Imagef, //const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

						nDimOfSquareCurf, //const int nDimOfSquaref,

						nNumOfScaleCooc_OrigImage_FeasCurf,

						//sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nScaleCoocSizeMax]
						&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

					if (nResf == UNSUCCESSFUL_RETURN)
					{
						printf("\n\n An error in 'doOneCoocFeature' by 'ScaleCooc_OrigImage' ");

						printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
							iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'ScaleCooc_OrigImage' ");
						fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
							iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						printf("\n\n Please press any key to exit:");	getchar(); exit(1);

						return UNSUCCESSFUL_RETURN;
					}// if (nResf == UNSUCCESSFUL_RETURN)

					nSumOfNumOfScaleCooc_OrigImage_FeasCurf += nNumOfScaleCooc_OrigImage_FeasCurf;
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n Before 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
						nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

					fprintf(fout_lr, "\n\n Before 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
						nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n After 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
						nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

					printf("\n iDistf = %d, iCoocScalef = %d, iDirecf = %d", iDistf, iCoocScalef, iDirecf);

					fprintf(fout_lr, "\n\n After 'Copying_AversOf_sScale_Cooc_Of_Orig_Imagef_To_sFeasAllOfCoocf': nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
						nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

					fprintf(fout_lr, "\n iDistf = %d, iCoocScalef = %d, iDirecf = %d", iDistf, iCoocScalef, iDirecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					/*
					//my prev code

										//nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) already
										if (nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax)
										{

											if (nPosit_OfCoocFeaf >= nSumOfNumOfScaleCooc_OrigImage_FeasPrevf && nPosit_OfCoocFeaf < nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
											{
					//nPosit_OfCoocFeaf is within the interval (nSumOfNumOfScaleCooc_OrigImage_FeasPrevf,nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
												nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nSumOfNumOfScaleCooc_OrigImage_FeasPrevf;

												//sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nScaleCoocSizeMax]
												if (nPosit_OfCoocFea_Internallyf < 0 || nPosit_OfCoocFea_Internallyf >= nScaleCoocSizeMax) // nScaleCoocSizeMax == 256
												{
													printf("\n\n An error in 'doOneCoocFeature': nPosit_OfCoocFea_Internallyf = %d >= nScaleCoocSizeMax = %d", nPosit_OfCoocFea_Internallyf, nScaleCoocSizeMax);

													printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
														iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

					#ifndef COMMENT_OUT_ALL_PRINTS
													fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature':  nPosit_OfCoocFea_Internallyf = %d >= nScaleCoocSizeMax = %d", nPosit_OfCoocFea_Internallyf, nScaleCoocSizeMax);
													fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
														iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
													fflush(fout_lr);
					#endif //#ifndef COMMENT_OUT_ALL_PRINTS

													printf("\n\n Please press any key to exit:");	getchar(); exit(1);
													return UNSUCCESSFUL_RETURN;
												} //if (nPosit_OfCoocFea_Internallyf < 0 || nPosit_OfCoocFea_Internallyf >= nScaleCoocSizeMax)

												//fAverOf_AllScalesCoocArr
												fOneFeaf = sScale_Cooc_Of_Orig_Imagef.fAverOfScaleCoocArr[nPosit_OfCoocFea_Internallyf];

													delete[] sGrayscale_Imagef.nPixelArr;
													return SUCCESSFUL_RETURN;
											} //if (nPosit_OfCoocFeaf >= nSumOfNumOfScaleCooc_OrigImage_FeasPrevf && nPosit_OfCoocFeaf < nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
											else
											{
					//nPosit_OfCoocFeaf is NOT within the interval (nSumOfNumOfScaleCooc_OrigImage_FeasPrevf,nSumOfNumOfScaleCooc_OrigImage_FeasCurf)

												continue;
											} //else

											nSumOfNumOfScaleCooc_OrigImage_FeasPrevf = nSumOfNumOfScaleCooc_OrigImage_FeasCurf;
											continue;
										} //if (nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax)
					*/

					if (nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax)
					{

						if (nPosit_OfCoocFeaf - nPosit_OfCoocFea_Prevf >= nSumOfNumOfScaleCooc_OrigImage_FeasPrevf && nPosit_OfCoocFeaf - nPosit_OfCoocFea_Prevf < nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
						{
							//nPosit_OfCoocFeaf is within the interval (nSumOfNumOfScaleCooc_OrigImage_FeasPrevf,nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
							nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosit_OfCoocFea_Prevf - nSumOfNumOfScaleCooc_OrigImage_FeasPrevf;

							//sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nScaleCoocSizeMax]
							if (nPosit_OfCoocFea_Internallyf < 0 || nPosit_OfCoocFea_Internallyf >= nScaleCoocSizeMax) // nScaleCoocSizeMax == 256
							{
								printf("\n\n An error in 'doOneCoocFeature': nPosit_OfCoocFea_Internallyf = %d >= nScaleCoocSizeMax = %d", nPosit_OfCoocFea_Internallyf, nScaleCoocSizeMax);

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature':  nPosit_OfCoocFea_Internallyf = %d >= nScaleCoocSizeMax = %d", nPosit_OfCoocFea_Internallyf, nScaleCoocSizeMax);
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");    getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nPosit_OfCoocFea_Internallyf < 0 || nPosit_OfCoocFea_Internallyf >= nScaleCoocSizeMax)

							//fAverOf_AllScalesCoocArr
							fOneFeaf = sScale_Cooc_Of_Orig_Imagef.fAverOfScaleCoocArr[nPosit_OfCoocFea_Internallyf];

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFeaf >= nSumOfNumOfScaleCooc_OrigImage_FeasPrevf && nPosit_OfCoocFeaf < nSumOfNumOfScaleCooc_OrigImage_FeasCurf)
						else
						{
							//nPosit_OfCoocFeaf is NOT within the interval (nSumOfNumOfScaleCooc_OrigImage_FeasPrevf,nSumOfNumOfScaleCooc_OrigImage_FeasCurf)

							//continue;
						} //else

						nSumOfNumOfScaleCooc_OrigImage_FeasPrevf = nSumOfNumOfScaleCooc_OrigImage_FeasCurf;
						continue;
					} //if (nPosit_OfCoocFeaf < 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
						nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);

						sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf] = sFeasOfCoocf.fEnergy; // nNumOfScalesAndDistTot,  already included
						sFeasAllOfCoocf->fContrastArr[nIndex3Dimf] = sFeasOfCoocf.fContrast;

						sFeasAllOfCoocf->fCorrelationArr[nIndex3Dimf] = sFeasOfCoocf.fCorrelation;

						sFeasAllOfCoocf->fHomogeneityArr[nIndex3Dimf] = sFeasOfCoocf.fHomogeneity;

						sFeasAllOfCoocf->fEntropyArr[nIndex3Dimf] = sFeasOfCoocf.fEntropy;
						sFeasAllOfCoocf->fAutocorrelationArr[nIndex3Dimf] = sFeasOfCoocf.fAutocorrelation;

						sFeasAllOfCoocf->fDissimilarityArr[nIndex3Dimf] = sFeasOfCoocf.fDissimilarity;
						sFeasAllOfCoocf->fClusterShadeArr[nIndex3Dimf] = sFeasOfCoocf.fClusterShade;

						sFeasAllOfCoocf->fClusterProminenceArr[nIndex3Dimf] = sFeasOfCoocf.fClusterProminence;
						sFeasAllOfCoocf->fMaxProbArr[nIndex3Dimf] = sFeasOfCoocf.fMaxProb;
*/


//nPosEnergyMinf = (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax),
	//nPosEnergyMaxf = nPosEnergyMinf + nNumOfDirecAndDistAndScaleTot,
					else if (nPosit_OfCoocFeaf >= nPosEnergyMinf && nPosit_OfCoocFeaf < nPosEnergyMaxf)
					{

						//	float fEnergyArr[nNumOfDirecAndDistAndScaleTot];

						//already specified above
												//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEnergyMinf;
											/*
													for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
													{
														nFeaf = nFeaf + 1;

															//float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
														fOneDim_Multifrac_Lacunar_CoocArrf[nFeaf] = sFeasAllOfCoocf->fEnergyArr[iFeaf];
													} // for (iFeaf = 0; iFeaf < nNumOfDirecAndDistAndScaleTot; iFeaf++)
													*/

													/*
													//nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);

														sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf] = sFeasOfCoocf.fEnergy;

																} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //z

															} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++) //y

														} //for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++) //x

													*/

													//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fEnergy;

							printf("\n\n 'doOneCoocFeature':  nNumOfSelected_CoocFea = %d, fOneFeaf = sFeasOfCoocf.fEnergy = %E, nPosit_OfCoocFea_Internallyf = %d, nNumOfDirecAndDistAndScale_Curf = %d",
								nNumOfSelected_CoocFea, fOneFeaf, nPosit_OfCoocFea_Internallyf, nNumOfDirecAndDistAndScale_Curf);

							printf("\n\n nIndex3Dimf = %d, iDirecf = %d, iDistf = %d, iCoocScalef = %d", nIndex3Dimf, iDirecf, iDistf, iCoocScalef);

							fprintf(fout_PrintFeatures, "\n\n 'doOneCoocFeature':  nNumOfSelected_CoocFea = %d, fOneFeaf = sFeasOfCoocf.fEnergy = %E, nPosit_OfCoocFea_Internallyf = %d, nNumOfDirecAndDistAndScale_Curf = %d",
								nNumOfSelected_CoocFea, fOneFeaf, nPosit_OfCoocFea_Internallyf, nNumOfDirecAndDistAndScale_Curf);

							//nIndex3Dimf = iDirecf + (iDistf * nNumOfDirectionsTot) + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);
							fprintf(fout_PrintFeatures, "\n\n nIndex3Dimf = %d, iDirecf = %d, iDistf = %d, iCoocScalef = %d", nIndex3Dimf, iDirecf, iDistf, iCoocScalef);

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an Energy fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an Energy fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosEnergyMinf && nPosit_OfCoocFeaf < nPosEnergyMaxf)
//////////////////////////////

					else if (nPosit_OfCoocFeaf >= nPosContrastMinf && nPosit_OfCoocFeaf < nPosContrastMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosContrastMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{
							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Contrast) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Contrast) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fContrast;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': a Contrast fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': a Contrast fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosContrastMinf && nPosit_OfCoocFeaf < nPosContrastMaxf)
					//////////////////
					else if (nPosit_OfCoocFeaf >= nPosCorrelationMinf && nPosit_OfCoocFeaf < nPosCorrelationMaxf)
					{
						//	nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosCorrelationMinf;

								//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{
							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Correlation) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Correlation) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fCorrelation;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': a Correlation fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': a Correlation fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosCorrelationMinf && nPosit_OfCoocFeaf < nPosCorrelationMaxf)
//////////////////
					else if (nPosit_OfCoocFeaf >= nPosHomogeneityMinf && nPosit_OfCoocFeaf < nPosHomogeneityMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosHomogeneityMinf;

						//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							//fOneFeaf = sFeasOfCoocf.fEnergy;
							fOneFeaf = sFeasOfCoocf.fHomogeneity;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an Energy fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an Energy fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosHomogeneityMinf && nPosit_OfCoocFeaf < nPosHomogeneityMaxf)

	////////////////
					else if (nPosit_OfCoocFeaf >= nPosEntropyMinf && nPosit_OfCoocFeaf < nPosEntropyMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEntropyMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Entropy) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Entropy) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fEntropy;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an Entropy fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an Entropy fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosEntropyMinf && nPosit_OfCoocFeaf < nPosEntropyMaxf)
//////////////////////////////////////////
					else if (nPosit_OfCoocFeaf >= nPosAutocorrelationMinf && nPosit_OfCoocFeaf < nPosAutocorrelationMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosAutocorrelationMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Autocorrelation) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Autocorrelation) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fAutocorrelation;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an Autocorrelation fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an Autocorrelation fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosAutocorrelationMinf && nPosit_OfCoocFeaf < nPosAutocorrelationMaxf)
////////////////////////////////////
					else if (nPosit_OfCoocFeaf >= nPosDissimilarityMinf && nPosit_OfCoocFeaf < nPosDissimilarityMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosDissimilarityMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Dissimilarity) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (Dissimilarity) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fDissimilarity;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an Dissimilarity fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an Dissimilarity fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosDissimilarityMinf && nPosit_OfCoocFeaf < nPosDissimilarityMaxf)
/////////////////////////////////////////
					else if (nPosit_OfCoocFeaf >= nPosClusterShadeMinf && nPosit_OfCoocFeaf < nPosClusterShadeMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterShadeMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (ClusterShade) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (ClusterShade) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fClusterShade;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an ClusterShade fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an ClusterShade fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosClusterShadeMinf && nPosit_OfCoocFeaf < nPosClusterShadeMaxf)
////////////////////////////////////////
					else if (nPosit_OfCoocFeaf >= nPosClusterProminenceMinf && nPosit_OfCoocFeaf < nPosClusterProminenceMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterProminenceMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (ClusterProminence) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (ClusterProminence) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fClusterProminence;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an ClusterProminence fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an ClusterProminence fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosClusterProminenceMinf && nPosit_OfCoocFeaf < nPosClusterProminenceMaxf)

					else if (nPosit_OfCoocFeaf >= nPosMaxProbMinf && nPosit_OfCoocFeaf < nPosMaxProbMaxf)
					{
						//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosMaxProbMinf;

							//if (nPosit_OfCoocFea_Internallyf == nNumOfDirecAndDistAndScale_Curf - 1)
						if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						{

							nResf = FeaturesOfScaleCooc_OrigImage(
								&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

								&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

							if (nResf == UNSUCCESSFUL_RETURN)
							{
								printf("\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (MaxProb) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature' by 'FeaturesOfScaleCooc_OrigImage' (MaxProb) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							}// if (nResf == UNSUCCESSFUL_RETURN)

							fOneFeaf = sFeasOfCoocf.fMaxProb;

							delete[] sGrayscale_Imagef.nPixelArr;
							return SUCCESSFUL_RETURN;
						} //if (nPosit_OfCoocFea_Internallyf == nIndex3Dimf)
						else
						{
							//if (nNumOfDirecAndDistAndScale_MaxForCurf == nNumOfDirecAndDistAndScale_MaxForCurf)
							if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)
							{
								printf("\n\n An error in 'doOneCoocFeature': an MaxProb fea has not been found) ");

								printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': an MaxProb fea has not been found) ");
								fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
									iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								printf("\n\n Please press any key to exit:");	getchar(); exit(1);
								return UNSUCCESSFUL_RETURN;
							} //if (nNumOfDirecAndDistAndScale_Curf >= nNumOfDirecAndDistAndScale_MaxForCurf)

							continue;
						}//else

					} //else if (nPosit_OfCoocFeaf >= nPosMaxProbMinf && nPosit_OfCoocFeaf < nPosMaxProbMaxf)

				} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //z

			} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++) //y

		} //for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++) //x

	} // else if (nPosit_OfCoocFeaf >= 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) && nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMinf)

	/////////////////////////////////////////////////////////////////////////////////////////////////////
/*
0 --	fEnergyAverOverDirecf = 0.0,
1 --	fContrastAverOverDirecf = 0.0,

2 --	fCorrelationAverOverDirecf = 0.0,
3 --	fHomogeneityAverOverDirecf = 0.0,

4 --	fEntropyAverOverDirecf = 0.0,
5 --	fAutocorrelationAverOverDirecf = 0.0,

6 --	fDissimilarityAverOverDirecf = 0.0,
7 --	fClusterShadeAverOverDirecf = 0.0,

8 --	fClusterProminenceAverOverDirecf = 0.0,
9 --	fMaxProbAverOverDirecf = 0.0,
*/
	else if (nPosit_OfCoocFeaf >= nPosEnergyAverOverDirecMinf) // && nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMaxf)
	{
		//switching to AverOverDirec
		iDistf_And_iCoocScalef_For_nIndexDistScalef(
			nPosit_OfCoocFea_Internallyf, //const int nIndexDistScalef,

			iDistf, //int &iDistf,
			iCoocScalef); // int &iCoocScalef)

		if (nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEnergyAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				0, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //if (nPosit_OfCoocFeaf < nPosEnergyAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosContrastAverOverDirecMinf && nPosit_OfCoocFeaf < nPosContrastAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosContrastAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				1, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosContrastAverOverDirecMinf && nPosit_OfCoocFeaf < nPosContrastAverOverDirecMaxf)


		else if (nPosit_OfCoocFeaf >= nPosCorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosCorrelationAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosCorrelationAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				2, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosCorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosCorrelationAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosHomogeneityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosHomogeneityAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosHomogeneityAverOverDirecMinf;
			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				3, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosHomogeneityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosHomogeneityAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosEntropyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEntropyAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosEntropyAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				4, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosEntropyAverOverDirecMinf && nPosit_OfCoocFeaf < nPosEntropyAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosAutocorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosAutocorrelationAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosAutocorrelationAverOverDirecMinf;
			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				5, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;

		} //else if (nPosit_OfCoocFeaf >= nPosAutocorrelationAverOverDirecMinf && nPosit_OfCoocFeaf < nPosAutocorrelationAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosDissimilarityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosDissimilarityAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosDissimilarityAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				6, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosDissimilarityAverOverDirecMinf && nPosit_OfCoocFeaf < nPosDissimilarityAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosClusterShadeAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterShadeAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterShadeAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				7, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosClusterShadeAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterShadeAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosClusterProminenceAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterProminenceAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosClusterProminenceAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				8, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosClusterProminenceAverOverDirecMinf && nPosit_OfCoocFeaf < nPosClusterProminenceAverOverDirecMaxf)

		else if (nPosit_OfCoocFeaf >= nPosMaxProbAverOverDirecMinf && nPosit_OfCoocFeaf < nPosMaxProbAverOverDirecMaxf)
		{
			//nPosit_OfCoocFea_Internallyf = nPosit_OfCoocFeaf - nPosMaxProbAverOverDirecMinf;

			nResf = AverOverDirec_AtFixed_IDist_And_iCoocScale(
				nDimCoocf, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				nNumOfDirectionsf, //const int nNumOfDirectionsf,

				nNumOfDistancesf, //const int nNumOfDistancesf,
				nDistancesArrf, //const int nDistancesArrf[],

				nScaleCoocTotf, //const int nScaleCoocTotf,

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,
				///////////////////////////////////////
				9, //const int nIndicOfFeaClassf,

				iDistf, //const int iDistf,
				iCoocScalef, //const int iCoocScalef,

				fAverFeaType_OverDirecf); // float &fAverFeaType_OverDirecf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

			fOneFeaf = fAverFeaType_OverDirecf;

			delete[] sGrayscale_Imagef.nPixelArr;
			return SUCCESSFUL_RETURN;
		} //else if (nPosit_OfCoocFeaf >= nPosMaxProbAverOverDirecMinf && nPosit_OfCoocFeaf < nPosMaxProbAverOverDirecMaxf)

//the end of AverOverDirec
	}//else if (nPosit_OfCoocFeaf >= nPosEnergyAverOverDirecMinf) 

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end in 'doOneCoocFeature': nFeaf = %d", nFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (fOneFeaf == -fLarge)
	{
		printf("\n\n An error in 'doOneCoocFeature': the feature nPosit_OfCoocFeaf = %d has not been found, nDimCoocf = %d", nPosit_OfCoocFeaf, nDimCoocf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'doOneCoocFeature': the feature nPosit_OfCoocFeaf = %d has not been found, nDimCoocf = %d", nPosit_OfCoocFeaf, nDimCoocf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fOneFeaf == -fLarge)

	return SUCCESSFUL_RETURN;
} //int doOneCoocFeature(...
/////////////////////////////////////////////////////////////


void iDistf_And_iCoocScalef_For_nIndexDistScalef(
	const int nIndexDistScalef,

	int &iDistf,
	int &iCoocScalef)
{
	int
		iCoocScale_1f,
		iDist_1f;

	////////////////////////////////////////////////////////////////
/*
	for (iDist_1f = 0; iDist_1f < nNumOfDistances; iDist_1f++)
	{
		for (iCoocScale_1f = 0; iCoocScale_1f < nScaleCoocTot; iCoocScale_1f++)
		{
			if (nIndexDistScalef - (iDist_1f + iCoocScale_1f * nNumOfDistances) == 0)
			{
				goto Mark_iDistf_And_iCoocScalef_Next;
			} //if (nIndexDistScalef - (iDistf + iCoocScale_1f * nNumOfDistances) == 0)

			//sFeasAllOfCoocf->fEnergyAverOverDirecArr[nIndexDistScalef]
		} //for (iCoocScale_1f = 0; iCoocScale_1f < nScaleCoocTot; iCoocScale_1f++)
	} //for (iDist_1f = 0; iDist_1f < nNumOfDistances; iDist_1f++)

Mark_iDistf_And_iCoocScalef_Next:	iCoocScalef = nIndexDistScalef / nNumOfDistances;
	iDistf = nIndexDistScalef - iCoocScalef * nNumOfDistances;

*/
	iCoocScalef = nIndexDistScalef / nNumOfDistances;
	iDistf = nIndexDistScalef - iCoocScalef * nNumOfDistances;
	/*
		if (iDistf != iDist_1f || iCoocScalef != iCoocScale_1f)
		{
			printf("\n\n An error in 'iDistf_And_iCoocScalef_For_nIndexDistScalef': iDistf = %d != iDist_1f = %d || iCoocScalef = %d != iCoocScale_1f = %d, nIndexDistScalef= %d",
				iDistf,iDist_1f, iCoocScalef,iCoocScale_1f, nIndexDistScalef);

			printf("\n\n Please press any key to exit:");	getchar(); exit(1);
		}//if (iDistf != iDist_1f || iCoocScalef != iCoocScale_1f)
	*/
	printf("\n\n 'iDistf_And_iCoocScalef_For_nIndexDistScalef': iDistf = %d, iCoocScalef = %d, nIndexDistScalef= %d",
		iDistf, iCoocScalef, nIndexDistScalef);

	//	printf("\n\n Please press any key");	getchar();
} //void iDistf_And_iCoocScalef_For_nIndexDistScalef(...
////////////////////////////////////////////////////////////

int AverOverDirec_AtFixed_IDist_And_iCoocScale(
	const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

	//const int nPosit_OfCoocFeaf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot // - 1

	const int nNumOfDirectionsf,
	//const int nIndicOfDirectionArrf[],

	const int nNumOfDistancesf,
	const int nDistancesArrf[],

	const int nScaleCoocTotf,
	//	const int nDimOfSquare_ScaleCoocArrf[],

	const COLOR_IMAGE *sColor_Imagef,
	///////////////////////////////////////
/*
0 --	fEnergyAverOverDirecf = 0.0,
1 --	fContrastAverOverDirecf = 0.0,

2 --	fCorrelationAverOverDirecf = 0.0,
3 --	fHomogeneityAverOverDirecf = 0.0,

4 --	fEntropyAverOverDirecf = 0.0,
5 --	fAutocorrelationAverOverDirecf = 0.0,

6 --	fDissimilarityAverOverDirecf = 0.0,
7 --	fClusterShadeAverOverDirecf = 0.0,

8 --	fClusterProminenceAverOverDirecf = 0.0,
9 --	fMaxProbAverOverDirecf = 0.0,
*/
const int nIndicOfFeaClassf,

const int iDistf,
const int iCoocScalef,

float &fAverFeaType_OverDirecf) //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]
{
	int CoocOfOrigImage(
		const int nIndicOfDirectionf,
		const int nDistOfCoocf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,
		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

	int FeaturesOfScaleCooc_OrigImage(
		const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

		FEATURES_OF_COOC *sFeasOfCoocf);

	int ScaleCooc_OrigImage(
		const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef,

		const int nDimOfSquaref,

		int &nNumOfScaleCooc_OrigImage_FeasCurf,

		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

	int Initializing_ScaleCooc_OrigImage(

		const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
									  //1--0 degrees, 2--45 degrees, 3--90 degrees

		const int nDistOfCoocf, //distance of cooccurrence

		const int nDimOfSquaref,
		SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

	//////////////////////////////////////////////////////////////////

	int
		nResf,
		iDirecf,
		//iDistf,
		//iCoocScalef,

		nDimOfSquareCurf,

		nNumOfScaleCooc_OrigImage_FeasCurf,

		nPosit_OfCoocFea_Internallyf,

		nNumOfDirecAndDistAndScale_Curf = 0,

		nNumOfDirecAndDistAndScale_MaxForCurf = nScaleCoocTot * nNumOfDistances*nNumOfDirectionsf,
		//////////////////////////////////////////////////////////
		nDistCurf;

	COOC_OF_ORIG_IMAGE
		sCooc_Of_Orig_Imagef;

	SCALE_COOC_OF_ORIG_IMAGE
		sScale_Cooc_Of_Orig_Imagef;

	FEATURES_OF_COOC
		sFeasOfCoocf;
	//////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'PrintingAll_CoocFeaturesAsAVector': nLimit_1f = %d, nLimit_2f = %d, nLimit_3f = %d", nLimit_1f, nLimit_2f, nLimit_3f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//fprintf(fout_PrintFeatures, "%d:%E, ", 0, sFeasAllOfCoocf->fMeanf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 1, sFeasAllOfCoocf->fStDevf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 2, sFeasAllOfCoocf->fSkewnessf);
	//fprintf(fout_PrintFeatures, "%d:%E, ", 3, sFeasAllOfCoocf->fKurtosisf);

	fAverFeaType_OverDirecf = -fLarge; //initially
////////////////////////////////////////////////////////////////////////////////////
	//nFeaf = nPosit_OfCoocFeaf;
	if (nIndicOfFeaClassf < 0 || nIndicOfFeaClassf > 9)
	{
		printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale': nIndicOfFeaClassf = %d ", nIndicOfFeaClassf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale': nIndicOfFeaClassf = %d ", nIndicOfFeaClassf); fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nIndicOfFeaClassf < 0 || nIndicOfFeaClassf > 9)

	if (iDistf < 0 || iDistf >= nNumOfDistancesf)
	{
		printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale': iDistf = %d, nNumOfDistancesf = %d ", iDistf, nNumOfDistancesf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale': iDistf = %d, nNumOfDistancesf = %d ", iDistf, nNumOfDistancesf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (iDistf < 0 || iDistf >= nNumOfDistancesf)

	if (iCoocScalef < 0 || iCoocScalef >= nScaleCoocTotf)
	{
		printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale': iCoocScalef = %d, nScaleCoocTotf = %d ", iCoocScalef, nScaleCoocTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale':  iCoocScalef = %d, nScaleCoocTotf = %d ", iCoocScalef, nScaleCoocTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (iCoocScalef < 0 || iCoocScalef >= nScaleCoocTotf)
///////////////////////////////////////////////
	fAverFeaType_OverDirecf = 0.0;

	///////////////////////////////////
	GRAYSCALE_IMAGE sGrayscale_Imagef;
	nResf = Initializing_Grayscale_Image(

		//&sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		sColor_Imagef, // COLOR_IMAGE *sColor_Imageff);
		&sGrayscale_Imagef); // GRAYSCALE_IMAGE *sGrayscale_Imagef);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'Initializing_Grayscale_Image' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'Initializing_Grayscale_Image' ") fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit:");	getchar(); exit(1);

		delete[] sColor_Imagef->nRed_Arr;
		delete[] sColor_Imagef->nGreen_Arr;
		delete[] sColor_Imagef->nBlue_Arr;

		delete[] sColor_Imagef->nLenObjectBoundary_Arr;
		delete[] sColor_Imagef->nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sGrayscale_Imagef.nPixelArr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

////////////////////////////
	for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++)
	{
		//for (iDistf = 0; iDistf < nNumOfDistancesf; iDistf++)
		{
			nDistCurf = nDistancesArr[iDistf];

			Initializing_Cooc_OrigImage(

				iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
												//1--0 degrees, 2--45 degrees, 3--90 degrees

				nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

				&sCooc_Of_Orig_Imagef); //		COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef)

			nResf = CoocOfOrigImage(
				iDirecf, //const int nIndicOfDirectionf,

				nDistCurf, //const int nDistOfCoocf,

				&sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
				&sCooc_Of_Orig_Imagef); // COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n After 'CoocOfOrigImage': nResf = %d, iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d",
				nResf, iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
			fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//if (nResf == UNSUCCESSFUL_RETURN)
			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'CoocOfOrigImage' ");
				printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'CoocOfOrigImage' ");
				fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d", iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf);
				fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				printf("\n\n Please press any key to exit:");	getchar(); exit(1);

				delete[] sColor_Imagef->nRed_Arr;
				delete[] sColor_Imagef->nGreen_Arr;
				delete[] sColor_Imagef->nBlue_Arr;

				delete[] sColor_Imagef->nLenObjectBoundary_Arr;
				delete[] sColor_Imagef->nIsAPixelBackground_Arr;
				/////////////////////////////////////////////////
				delete[] sGrayscale_Imagef.nPixelArr;
				return UNSUCCESSFUL_RETURN; // -1;
			}// if (nResf == UNSUCCESSFUL_RETURN)

			//for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //3
			{

				nNumOfDirecAndDistAndScale_Curf += 1;

				nDimOfSquareCurf = nDimOfSquare_ScaleCoocArr[iCoocScalef];

				//Impossible values
				nResf = Initializing_ScaleCooc_OrigImage(

					iDirecf, //const int nIndicOfDirectionf, // // 0-- (-45 degrees from horizontal,
													//1--0 degrees, 2--45 degrees, 3--90 degrees
					nDistCurf, //const int nDistOfCoocf, //distance of cooccurrence

					nDimOfSquareCurf,
					&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'Initializing_ScaleCooc_OrigImage' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'Initializing_ScaleCooc_OrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] sColor_Imagef->nRed_Arr;
					delete[] sColor_Imagef->nGreen_Arr;
					delete[] sColor_Imagef->nBlue_Arr;

					delete[] sColor_Imagef->nLenObjectBoundary_Arr;
					delete[] sColor_Imagef->nIsAPixelBackground_Arr;
					/////////////////////////////////////////////////
					delete[] sGrayscale_Imagef.nPixelArr;
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

/////////////////////////////////////////////////////
				nResf = ScaleCooc_OrigImage(
					&sCooc_Of_Orig_Imagef, //const COOC_OF_ORIG_IMAGE *sCooc_Of_Orig_Image,

					nDimOfSquareCurf, //const int nDimOfSquaref,

					nNumOfScaleCooc_OrigImage_FeasCurf,

					//sScale_Cooc_Of_Orig_Imagef->fAverOfScaleCoocArr[nScaleCoocSizeMax]
					&sScale_Cooc_Of_Orig_Imagef); // SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'ScaleCooc_OrigImage' ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'ScaleCooc_OrigImage' ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] sColor_Imagef->nRed_Arr;
					delete[] sColor_Imagef->nGreen_Arr;
					delete[] sColor_Imagef->nBlue_Arr;

					delete[] sColor_Imagef->nLenObjectBoundary_Arr;
					delete[] sColor_Imagef->nIsAPixelBackground_Arr;
					/////////////////////////////////////////////////
					delete[] sGrayscale_Imagef.nPixelArr;
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Before 'FeaturesOfScaleCooc_OrigImage': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
					iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

				fprintf(fout_lr, "\n\n Before 'FeaturesOfScaleCooc_OrigImage': nDimOfSquareCurf = %d, nNumOfScaleCooc_OrigImage_FeasCurf = %d, nNumOfScaleCooc_OrigImage_FeasTotSoFarf = %d",
					nDimOfSquareCurf, nNumOfScaleCooc_OrigImage_FeasCurf, nNumOfScaleCooc_OrigImage_FeasTotSoFarf);

				fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
					iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				/*
				//nIndex3Dimf = iDirecf + iDistf * nNumOfDirectionsTot + iCoocScalef * (nNumOfDistances*nNumOfDirectionsTot);
				sFeasAllOfCoocf->fEnergyArr[nIndex3Dimf] = sFeasOfCoocf.fEnergy;

						} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //z

					} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++) //y

				} //for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++) //x

				*/

				nResf = FeaturesOfScaleCooc_OrigImage(
					&sScale_Cooc_Of_Orig_Imagef, //const SCALE_COOC_OF_ORIG_IMAGE *sScale_Cooc_Of_Orig_Imagef,

					&sFeasOfCoocf); // FEATURES_OF_COOC *sFeasOfCoocf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");

					printf("\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'AverOverDirec_AtFixed_IDist_And_iCoocScale' by 'FeaturesOfScaleCooc_OrigImage' (Energy) ");
					fprintf(fout_lr, "\n iDirecf = %d, nNumOfDirectionsf = %d, iDistf = %d, nNumOfDistancesf = %d, iCoocScalef = %d, nScaleCoocTot = %d",
						iDirecf, nNumOfDirectionsf, iDistf, nNumOfDistancesf, iCoocScalef, nScaleCoocTot);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					delete[] sColor_Imagef->nRed_Arr;
					delete[] sColor_Imagef->nGreen_Arr;
					delete[] sColor_Imagef->nBlue_Arr;

					delete[] sColor_Imagef->nLenObjectBoundary_Arr;
					delete[] sColor_Imagef->nIsAPixelBackground_Arr;
					/////////////////////////////////////////////////
					delete[] sGrayscale_Imagef.nPixelArr;
					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

/*
	fEnergyAverOverDirecf = 0.0,
	fContrastAverOverDirecf = 0.0,
	fCorrelationAverOverDirecf = 0.0,
	fHomogeneityAverOverDirecf = 0.0,

	fEntropyAverOverDirecf = 0.0,
	fAutocorrelationAverOverDirecf = 0.0,

	fDissimilarityAverOverDirecf = 0.0,
	fClusterShadeAverOverDirecf = 0.0,

	fClusterProminenceAverOverDirecf = 0.0,
	fMaxProbAverOverDirecf = 0.0;
*/
				if (nIndicOfFeaClassf == 0)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fEnergy;

				} //if (nIndicOfFeaClassf == 0)

				else if (nIndicOfFeaClassf == 1)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fContrast;

				} //else if (nIndicOfFeaClassf == 1)

				else if (nIndicOfFeaClassf == 2)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fCorrelation;

				} //else if (nIndicOfFeaClassf == 2)

				else if (nIndicOfFeaClassf == 3)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fHomogeneity;

				} //else if (nIndicOfFeaClassf == 3)

				else if (nIndicOfFeaClassf == 4)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fEntropy;

				} //else if (nIndicOfFeaClassf == 4)

				else if (nIndicOfFeaClassf == 5)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fAutocorrelation;

				} //else if (nIndicOfFeaClassf == 5)

				else if (nIndicOfFeaClassf == 6)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fDissimilarity;

				} //else if (nIndicOfFeaClassf == 6)

				else if (nIndicOfFeaClassf == 7)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fClusterShade;

				} //else if (nIndicOfFeaClassf == 7)

				else if (nIndicOfFeaClassf == 8)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fClusterProminence;

				} //else if (nIndicOfFeaClassf == 8)

				else if (nIndicOfFeaClassf == 9)
				{
					fAverFeaType_OverDirecf += sFeasOfCoocf.fMaxProb;

				} //else if (nIndicOfFeaClassf == 9)
/////////////////////////////

			} //for (iCoocScalef = 0; iCoocScalef < nScaleCoocTot; iCoocScalef++) //z

		} //for (iDistf = 0; iDistf < nNumOfDistances; iDistf++) //y

	} //for (iDirecf = 0; iDirecf < nNumOfDirectionsf; iDirecf++) //x

/////////////////////////////////////////////////////////////////////////////////////////////////////
	delete[] sGrayscale_Imagef.nPixelArr;

	return SUCCESSFUL_RETURN;
} //int AverOverDirec_AtFixed_IDist_And_iCoocScale(...
//////////////////////////////////////////////

void Initializing_AFloatVec_To_Zero(
	const int nDimf,
	float fVecArrf[]) //[nDimf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fVecArrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
}//void Initializing_AFloatVec_To_Zero(
////////////////////////////////////////////////////////////////////////

int	Converting_All_Feas_To_ARange_0_1(
	const int nDimf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	int &nNumOfFeasWithUnusualValuesf,
	int &nNumOfFeasWith_TheSameValuesf,

	int nFeaTheSameOrNorArrf[], //[nDimf] // 1-- the same, 0-- not
	float fFeas_InitMinArrf[], //[nDimf]
	float fFeas_InitMaxArrf[], //[nDimf]

	float fArr1stf[], //[nDimf*nNumVec1stf]
	float fArr2ndf[]) //[nDimf*nNumVec2ndf]
{

	int
		nNumOfFeasFilledf = 0,

		nIndexf,
		nTempf,
		iVecf,
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaRangeArrf[nDim],
		fFeaCurf;

	nNumOfFeasWithUnusualValuesf = 0;
	nNumOfFeasWith_TheSameValuesf = 0;
	//////////////////////////////////////

	fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d", nDimf, nNumVec1stf, nNumVec2ndf);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nFeaTheSameOrNorArrf[iFeaf] = 0; //not the same initially
		fFeaRangeArrf[iFeaf] = 0.0;

		fFeas_InitMinArrf[iFeaf] = fLarge;
		fFeas_InitMaxArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//printing all fea vecs+

	fprintf(fout_PrintFeatures, "\n\n Normal:");

	for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
	{
		fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 1 (init feas):  iVecf = %d, ", iVecf);
		nTempf = iVecf * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr1stf[nIndexf];

			fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

		}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

	fprintf(fout_PrintFeatures, "\n\n Malignant:");

	for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
	{
		fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 2 (init feas):  iVecf = %d, ", iVecf);
		nTempf = iVecf * nDimf;
		for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
		{

			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr2ndf[nIndexf];

			fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

		}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)


	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr1stf[nIndexf];

			if (iFeaf == nNumOfSelectedFea)
			{
				//fprintf(fout_PrintFeatures, "\n 1: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])

		} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
//////////////////////////////////////////////////////////
		//fprintf(fout_PrintFeatures, "\n\n");
		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr2ndf[nIndexf];

			if (iFeaf == nNumOfSelectedFea)
			{
				//	fprintf(fout_PrintFeatures, "\n 2: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//	fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)

			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;

				if (iFeaf == nNumOfSelectedFea)
				{
					//		fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
		} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)
		{
			nNumOfFeasWithUnusualValuesf += 1;
			printf("\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
			fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %d", iFeaf, fFeas_InitMinArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)

		if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
		{
			if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)
			{
				nNumOfFeasWithUnusualValuesf += 1;
				printf("\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %d", iFeaf, fFeas_InitMaxArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)
		}//if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
////////////////////////////////////////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fRangeCurf = fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf];

		if (iFeaf == nNumOfSelectedFea)
		{
			//		printf( "\n\n fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
				//		fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

			fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
				fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

		} //if (iFeaf == nNumOfSelectedFea)

		if (fRangeCurf < feps)
		{
			nFeaTheSameOrNorArrf[iFeaf] = 1; //the same 
			nNumOfFeasWith_TheSameValuesf += 1;

			fprintf(fout_PrintFeatures, "\n The next nNumOfFeasWith_TheSameValuesf = %d, iFeaf = %d", nNumOfFeasWith_TheSameValuesf, iFeaf);

		} //if (fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf] < feps)
		else
		{
			fFeaRangeArrf[iFeaf] = fRangeCurf;
		} //else
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	printf("\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);
	fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

	/////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (nFeaTheSameOrNorArrf[iFeaf] == 0)  //the feas are different 
		{
			//	fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr1stf[nIndexf] = (fArr1stf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

							//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 1: iFeaf = %d, iVecf = %d, fArr1stf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
								//iFeaf, iVecf,nIndexf,fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)

				if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					fflush(fout_PrintFeatures);

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				}//if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)

			} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

			//fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr2ndf[nIndexf] = (fArr2ndf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				}//if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 2: iFeaf = %d, iVecf = %d, fArr2ndf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//iFeaf, iVecf, nIndexf, fArr2ndf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)
			} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

			fflush(fout_PrintFeatures);
		} //if (nFeaTheSameOrNorArrf[iFeaf] == 0)  //the feas are different 

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	return SUCCESSFUL_RETURN;
} // int Converting_All_Feas_To_ARange_0_1(...
////////////////////////////////////////////////////////////////////////////////////////////

int ConvertingVecs_ToBest_SeparableFeas(
	const float fLargef,
	const  float fepsf,

	const int nDimf,
	const int nDimSelecMaxf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	const float fPrecisionOf_G_Const_Searchf,

	const  float fBorderBelf,
	const  float fBorderAbof,

	const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
	const float fEfficBestSeparOfOneFeaAcceptMaxf,

	float fArr1stf[], //[nDimf*nNumVec1stf] // after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	float fArr2ndf[], //[nDimf*nNumVec2ndf] //after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

int &nDimSelecf,

float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

float &fRatioBestMinf,

int &nPosOneFeaBestMaxf,

int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

float &fNumArr1stSeparBestMaxf,
float &fNumArr2ndSeparBestMaxf,

float fRatioBestArrf[], //nDimSelecMaxf

int nPosFeaSeparBestArrf[])	//nDimSelecMaxf
{
	int	Converting_All_Feas_To_ARange_0_1(
		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		int &nNumOfFeasWithUnusualValuesf,
		int &nNumOfFeasWith_TheSameValuesf,

		int nFeaTheSameOrNorArrf[], //[nDimf] // 1-- the same, 0-- not
		float fFeas_InitMinArrf[], //[nDimf]
		float fFeas_InitMaxArrf[], //[nDimf]

		float fArr1stf[], //[nDimf*nNumVec1stf]
		float fArr2ndf[]); //[nDimf*nNumVec2ndf]

	int SelectingBestFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

int &nDimSelecf,
//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

float &fRatioBestMinf,

int &nPosOneFeaBestMaxf,

int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

float &fNumArr1stSeparBestMaxf,
float &fNumArr2ndSeparBestMaxf,

float fRatioBestArrf[], //nDimSelecMaxf

int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

//////////////////////////////////////////
	int
		nResf,
		iFeaf,
		nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNorArrf[nDim]; //[nDimf] // 1-- the same, 0-- not

	float
		fFeas_InitMinArrf[nDim],
		fFeas_InitMaxArrf[nDim];

	nResf = Converting_All_Feas_To_ARange_0_1(
		nDimf, //const int nDimf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		nNumOfFeasWithUnusualValuesf, //int &nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf, //int &nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNorArrf, //int nFeaTheSameOrNorArrf[], //[nDimf] // 1-- the same, 0-- not
		fFeas_InitMinArrf, //float fFeas_InitMinArrf[], //[nDimf]
		fFeas_InitMaxArrf, //float fFeas_InitMaxArrf[], //[nDimf]

		fArr1stf, //float fArr1stf[], //[nDimf*nNumVec1stf]  //0 -- 1 now
		fArr2ndf); // float fArr2ndf[]); //[nDimf*nNumVec2ndf]  //0 -- 1 now

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	nResf = SelectingBestFeas(
		fLargef, //const float fLargef,
		fepsf, //const  float fepsf,

		nDimf, //const int nDimf,
		nDimSelecMaxf, //const int nDimSelecMaxf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMaxf, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1
		///////////////////////////////////

		nDimSelecf, //int &nDimSelecf,

		fRatioBestMinf, //float &fRatioBestMinf,

		nPosOneFeaBestMaxf, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMaxf, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestMaxf, //float &fNumArr1stSeparBestMaxf,
		fNumArr2ndSeparBestMaxf, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArrf, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArrf); // int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		fprintf(fout_lr, "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar(); // exit(1);
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	else if (nResf == SUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': the number of selected features  nDimSelecf = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax' results to get a larger number of the selected features; the range is (-2.0, -1.0) ");
		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':  the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);
		fprintf(fout_lr, "\n\n  Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//else if (nResf == SUCCESSFUL_RETURN)
	else if (nResf == 2)
	{
		printf("\n\n After 'SelectingBestFeas': the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);

		fprintf(fout_lr, "\n\n  Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//else if (nResf == 2)

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	printf("\n\n After 'SelectingBestFeas': fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, iniatal nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_lr, "\n\n After 'SelectingBestFeas':fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, iniatal nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/*
	if (nDimSelecf > nDimSelecMax)
	{
		printf("\n\n  Changing nDimSelecf = %d to nDimSelecMax = %d", nDimSelecf, nDimSelecMax);

	#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nDimSelecf = nDimSelecMax;
	}//if (nDimSelec > nDimSelecMax)
	*/

	fprintf(fout_PrintFeatures, "\n\n //////////////////////////////////////////////////////////////////////");

	//for (int iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)
	{
		printf("\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
		//fprintf(fout_lr, "\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			//iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);

		fprintf(fout_PrintFeatures, "\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
	} //for (iFea = 0; iFea < nDimSelecMax; iFea++)


#ifndef COMMENT_OUT_ALL_PRINTS
	for (iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)
	{
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas': fRatioBestArr[%d] = %E, nPosFeaSeparBestArr[%d] = %d",
			iFea, fRatioBestArr[iFea], iFea, nPosFeaSeparBestArr[iFea]);
	} //	for ( iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)

	printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////

	//float *fVecTrainSelec_1st = new float[nNumVecTrain_1st*nDimSelecf]; //0 -- 1 //[nDimSelecMaxf]
	nResf = Converting_Arr_To_Selec(
		nDim, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Normal, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr1stf, //const float fVecArr[],[nProdTrain_1st]
		fSelecArr1stf); //float fVecSelecArr[]) //[nNumVecTrain_1st*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

	nResf = Converting_Arr_To_Selec(
		nDim, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Malignant, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr2ndf, //const float fVecArr[],[nProdTrain_2nd]
		fSelecArr2ndf); //float fVecSelecArr[]) //[nNumVecTrain_2nd*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

	printf("\n\n The end in 'ConvertingVecs_ToBest_SeparableFeas'");
	printf("\n\n Please press any key"); getchar();

	return SUCCESSFUL_RETURN;
} //int ConvertingVecs_ToBest_SeparableFeas(...
//////////////////////////////////////////////////////////////

int HistogramEqualization_ForColorFormatOfGray(

	//GRAYSCALE_IMAGE *sGrayscale_Image)
	COLOR_IMAGE *sColor_Imagef)
{
	int
		nNumOfValidPixelsTotf = 0,

		nLookUptableArrf[nNumOfHistogramBinsStat],
		nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nCumulatNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat],

		nWidthImagef = sColor_Imagef->nWidth,
		nLenImagef = sColor_Imagef->nLength,

		nIndexOfPixelCurf,

		nIntensityCurf,

		nIntensityMinf = nLarge,
		nIntensityMaxf = -nLarge,

		iIntensityf,
		iWidf,
		iLenf;

	float
		fCumulProbOverIntensitiesArrf[nNumOfHistogramBinsStat];

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;
		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

		fCumulProbOverIntensitiesArrf[iIntensityf] = 0.0;

		nLookUptableArrf[iIntensityf] = 0;

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
	fprintf(fout_PrintFeatures, "\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

	//printf("\n\n Please press any key to continue:");
	//fflush(fout_PrintFeatures); getchar();
	//////////////////////////////////////////////////////////////////////////////
	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageSizeMax)
			{
				printf("\n\n An error: nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);

				fprintf(fout_PrintFeatures, "\n\n An error:  nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax)

//all colors have the same intensity
			//if (sColor_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
			if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] >= nIntensityOfBackground)
				continue;

			nIntensityCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)
			{
				printf("\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

				fprintf(fout_PrintFeatures, "\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
			} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat)

			if (nIntensityCurf < nIntensityMinf)
				nIntensityMinf = nIntensityCurf;

			if (nIntensityCurf > nIntensityMaxf)
				nIntensityMaxf = nIntensityCurf;

			nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)


	for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{

		nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] += nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf - 1] +
			nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

		nNumOfValidPixelsTotf += nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

	} // for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	printf("\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
	fprintf(fout_PrintFeatures, "\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);

	//printf("\n\n Please press any key to continue:");
//	fflush(fout_PrintFeatures); getchar();

	if (nNumOfValidPixelsTotf <= 0)
	{
		printf("\n\n An error: the number of valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);
		fprintf(fout_PrintFeatures, "\n\n An error: the number  valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);

		printf("\n\n Please press any key to exit:");
		fflush(fout_PrintFeatures); getchar(); exit(1);
	} //if (nNumOfValidPixelsTotf <= 0)

	for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)
	{
		fCumulProbOverIntensitiesArrf[iIntensityf] = (float)(nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);

		nLookUptableArrf[iIntensityf] = (int)((fCumulProbOverIntensitiesArrf[iIntensityf])*(float)(nIntensityStatMax));

		if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)
		{
			printf("\n\n An error in HistogramEqualization_ForColorFormatOfGray: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);
			fprintf(fout_PrintFeatures, "\n\n An error in HistogramEqualization_ForColorFormatOfGray: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);

			printf("\n\n Please press any key to exit:");
			fflush(fout_PrintFeatures); getchar(); exit(1);

		} // if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)

		//printf("\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
		//fprintf(fout_PrintFeatures, "\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);

	} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat; iIntensityf++)

	//printf("\n\n Please press any key to continue:");
//	fflush(fout_PrintFeatures); getchar();
///////////////////////////////////////////////////////////////////////////////////////////////////
	//printf( "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
	//fprintf(fout_PrintFeatures, "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);


	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLenMax);

			if (nIndexOfPixelCurf >= nImageSizeMax)
			{
				printf("\n\n An error 2: nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);

				fprintf(fout_PrintFeatures, "\n\n An error 2:  nIndexOfPixelCurf = %d >= nImageSizeMax = %d", nIntensityCurf, nImageSizeMax);
				printf("\n\n Please press any key to exit:");
				fflush(fout_PrintFeatures); getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax)

			//if (sColor_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
			if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] >= nIntensityOfBackground)
				continue;

			nIntensityCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nLookUptableArrf[nIntensityCurf];

			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			//fprintf(fout_PrintFeatures, "\n\n iWidf = %d, iLenf = %d, sColor_Imagef->nRed_Arr[%d] = %d",
				//iWidf, iLenf, nIndexOfPixelCurf, sColor_Imagef->nRed_Arr[nIndexOfPixelCurf]);

			//fprintf(fout_PrintFeatures, "\n nIntensityCurf = %d, nLookUptableArrf[%d] = %d", nIntensityCurf, nIntensityCurf, nLookUptableArrf[nIntensityCurf]);

		} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	//printf("\n\n The end of 'HistogramEqualization_ForColorFormatOfGray': please press any key to continue");getchar();
	fflush(fout_PrintFeatures);

	return SUCCESSFUL_RETURN;
} //int HistogramEqualization_ForColorFormatOfGray(....

///////////////////////////////////////////////////////////////////////////

/*
//From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
//From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
// Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HLS_Fr_RGB(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	HLS_IMAGE *sHLS_Imagef)
{
	int
		nImageLengthf = sColor_Imagef->nLength,
		nImageWidthf = sColor_Imagef->nWidth,

		//nIndexCurSizef, // = iLenf + (iWidf*nImageLengthf);
		nIndexOfPixelCurf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nColorMinf,
		nColorMaxf,

		iLenf,
		iWidf;

	float
		fCoefOfPixelIntensNormalizationf = 1.0 / (float)(nIntensityStatMax),

		fHueNotNormalisedArrf[nImageSizeMax],

		fPixelIntensitiesNormalizedArrf[nImageSizeMax],

		fOnePixelIntensNormalizedMinf,
		fOnePixelIntensNormalizedMaxf,

		fOnePixelIntensNormalizedRangef,

		fUNDEFINEDF = fHueUndefinedValue, //-1.0,

		fDiffCurf,

		fLightnessCurf,
		fSaturationCurf,
		fHueCurf,

		fRedNormCurf,
		fGreenNormCurf,
		fBlueNormCurf;

	/////////////////////////////////////////////////////////////////////////////////////////////

	sHLS_Imagef->nLength = nImageLengthf;
	sHLS_Imagef->nWidth = nImageWidthf;

	//printf("\n\n An error in 'HLS_Fr_RGB': please choose only one way to calulate Hue");

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//nIndexOfPixelCurf = iLenf + (iWidf*nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			fRedNormCurf = (float)(nRedCurf)*fCoefOfPixelIntensNormalizationf;

			fGreenNormCurf = (float)(nGreenCurf)*fCoefOfPixelIntensNormalizationf;
			fBlueNormCurf = (float)(nBlueCurf)*fCoefOfPixelIntensNormalizationf;

			//fprintf(fout, "\n\n 'HLS_Fr_RGB': nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", nRedCurf, nGreenCurf, nBlueCurf);
			//fprintf(fout, "\n iWidf = %d, iLenf = %d, fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", iWidf, iLenf, fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

//min
			if (nRedCurf <= nGreenCurf)
			{
				if (nRedCurf <= nBlueCurf)
				{
					nColorMinf = nRedCurf;
					fOnePixelIntensNormalizedMinf = fRedNormCurf;
				} // if (nRedCurf <= nBlueCurf)
				else
				{
					nColorMinf = nBlueCurf;
					fOnePixelIntensNormalizedMinf = fBlueNormCurf;

				} //else
			} // if (nRedCurf <= nGreenCurf)
			else if (nRedCurf > nGreenCurf)
			{
				if (nGreenCurf <= nBlueCurf)
				{
					nColorMinf = nGreenCurf;
					fOnePixelIntensNormalizedMinf = fGreenNormCurf;
				} // if (nGreenCurf <= nBlueCurf)
				else
				{
					nColorMinf = nBlueCurf;
					fOnePixelIntensNormalizedMinf = fBlueNormCurf;
				} // else

			} // else if (nRedCurf > nGreenCurf)


//Max
			if (nRedCurf >= nGreenCurf)
			{
				if (nRedCurf >= nBlueCurf)
				{
					nColorMaxf = nRedCurf;
					fOnePixelIntensNormalizedMaxf = fRedNormCurf;
				} // if (nRedCurf >= nBlueCurf)
				else
				{
					nColorMaxf = nBlueCurf;
					fOnePixelIntensNormalizedMaxf = fBlueNormCurf;

				} //else
			} // if (nRedCurf >= nGreenCurf)
			else if (nRedCurf < nGreenCurf)
			{
				if (nGreenCurf >= nBlueCurf)
				{
					nColorMaxf = nGreenCurf;
					fOnePixelIntensNormalizedMaxf = fGreenNormCurf;
				} // if (nGreenCurf >= nBlueCurf)
				else
				{
					nColorMaxf = nBlueCurf;
					fOnePixelIntensNormalizedMaxf = fBlueNormCurf;
				} // else

			} // else if (nRedCurf < nGreenCurf)

/////////////////////////////////////////////////////////////////////////////////////////////
			fLightnessCurf = (fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf) / 2.0;

			if (nColorMinf == nColorMaxf)
			{
				fSaturationCurf = 0.0;
				fHueCurf = fUNDEFINEDF;
			} // if (nColorMinf == nColorMaxf)
			else
			{

				fOnePixelIntensNormalizedRangef = fOnePixelIntensNormalizedMaxf - fOnePixelIntensNormalizedMinf;

				//if (fOnePixelIntensNormalizedRangef <= 0.0 || fOnePixelIntensNormalizedRangef > fLarge)
				if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)
				{
					//fOnePixelIntensNormalizedRangef = 0.0;

					printf("\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);
					fprintf(fout, "\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);

					printf("\n\n Please press any key to exit");
					fflush(fout); getchar(); exit(1);

				} //if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)

//saturation
				if (fLightnessCurf < 0.5)
				{
					fSaturationCurf = fOnePixelIntensNormalizedRangef / (fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf);
				} // if (fLightnessCurf < 0.5)
				else // fLightnessCurf >= 0.5
				{
					fDiffCurf = 2.0 - fOnePixelIntensNormalizedMinf - fOnePixelIntensNormalizedMaxf;

					if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)
					{
						fDiffCurf = 0.0;

						printf("\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);
						fprintf(fout, "\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);

						printf("\n\n Please press any key to exit");
						fflush(fout); getchar(); exit(1);


					} //if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)

					fSaturationCurf = fOnePixelIntensNormalizedRangef / fDiffCurf;
				}// else // if (fLightnessCurf >= 0.5)

//hue
				if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				{
					//fHueCurf = (fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef;
					fHueCurf = fmod((fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef, 6.0);

				}// if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				else // if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
				{

					if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
					{
						fHueCurf = 2.0 + (fBlueNormCurf - fRedNormCurf) / fOnePixelIntensNormalizedRangef;

					}// if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
					else
					{
						if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
						{
							fHueCurf = 4.0 + (fRedNormCurf - fGreenNormCurf) / fOnePixelIntensNormalizedRangef;

						}// if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
						else
						{

							printf("\n\n An error in 'HLS_Fr_RGB': a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
							printf("\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

							fprintf(fout, "\n\n An error in 'HLS_Fr_RGB':  a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
							fprintf(fout, "\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

							fprintf(fout, "\n iWidf = %d, nImageWidthf = %d, iLenf = %d, nImageLengthf = %d", iWidf, nImageWidthf, iLenf, nImageLengthf);

							printf("\n\n Please press any key to exit");
							fflush(fout); getchar(); exit(1);

						}// else

					} // else if (fGreenNormCurf != fOnePixelIntensNormalizedMaxf)

				} // else if (fRedNormCurf != fOnePixelIntensNormalizedMaxf)

				fHueCurf = fHueCurf * 60.0;

				if (fHueCurf < 0.0)
					fHueCurf = fHueCurf + 360.0;

			}// else if (nColorMinf != nColorMaxf)

			sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = fHueCurf;
			sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = fLightnessCurf;
			sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = fSaturationCurf;

			if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
			{

				fprintf(fout, "\n\n 'HLS_Fr_RGB': iWidf = %d, iLenf = %d", iWidf, iLenf);
				fprintf(fout, "\n fHueCurf = %E, fLightnessCurf = %E, fSaturationCurf = %E", fHueCurf, fLightnessCurf, fSaturationCurf);
			} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)

		} // for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} // int HLS_Fr_RGB(...
*/

///////////////////////////////////////////////////////////////////////////////
  //printf("\n\n Please press any key:"); getchar();



