


/*
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n Before 'SelectingBestFeas': please press any key"); fflush(fout); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


nResult = SelectingBestFeas(
					fLarge, //const float fLargef,
					feps, //const  float fepsf,

					nDim, //const int nDimf,
					nDimSelecMax, //const int nDimSelecMaxf,

					nNumVecTrain_1st, //const int nNumVec1stf,
					nNumVecTrain_2nd, //const int nNumVec2ndf,

					fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

					0.0, //const  float fBorderBelf,
					1.0, //const  float fBorderAbof,

					nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
					fEfficBestSeparOfOneFeaAcceptMax, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

					fVecTrain_1st, //const float fArr1stf[], //0 -- 1
					fVecTrain_2nd, //const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

					nDimSelec, //int &nDimSelecf,

					fRatioBestMin, //float &fRatioBestMinf,

					nPosOneFeaBestMax, //int &nPosOneFeaBestMaxf,

					nSeparDirectionBestMax, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparBestMax, //float &fNumArr1stSeparBestMaxf,
					fNumArr2ndSeparBestMax, //float &fNumArr2ndSeparBestMaxf,

					fRatioBestArr,
					nPosFeaSeparBestArr); //int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

	if (nResult == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		printf( "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");

		fprintf(fout, "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		fprintf(fout, "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nResult == UNSUCCESSFUL_RETURN)

	else if (nResult == SUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n After 'SelectingBestFeas': the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec,nDimSelecMax);
		printf("\n\n Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");

		fprintf(fout, "\n\n After 'SelectingBestFeas':  the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);

		fprintf(fout, "\n\n  Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//else if (nResult == SUCCESSFUL_RETURN)
	else if (nResult == 2)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n After 'SelectingBestFeas': the number of selected features nDimSelec = %d is greater than the specified number nDimSelecMax = %d",
			nDimSelec,nDimSelecMax);
		printf("\n\n Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");

		fprintf(fout, "\n\n After 'SelectingBestFeas':the number of selected features nDimSelec = %d is greater than the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);

		fprintf(fout, "\n\n  Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//else if (nResult == 2)

#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n After 'SelectingBestFeas': fRatioBestMin = %E, nPosOneFeaBestMax = %d, inital nDimSelec = %d",
	fRatioBestMin, nPosOneFeaBestMax, nDimSelec);
fprintf(fout,"\n\n After 'SelectingBestFeas': fRatioBestMin = %E, nPosOneFeaBestMax = %d, iniatal nDimSelec = %d",
	fRatioBestMin, nPosOneFeaBestMax, nDimSelec);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

if (nDimSelec > nDimSelecMax)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
	fprintf(fout, "\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nDimSelec = nDimSelecMax;
}//if (nDimSelec > nDimSelecMax)

#ifndef COMMENT_OUT_ALL_PRINTS
for (iFea = 0; iFea < nDimSelec; iFea++)
{
	printf("\n\n After 'SelectingBestFeas': fRatioBestArr[%d] = %E, nPosFeaSeparBestArr[%d] = %d",
		iFea, fRatioBestArr[iFea], iFea, nPosFeaSeparBestArr[iFea]);

	fprintf(fout,"\n\n After 'SelectingBestFeas': fRatioBestArr[%d] = %E, nPosFeaSeparBestArr[%d] = %d",
		iFea, fRatioBestArr[iFea], iFea, nPosFeaSeparBestArr[iFea]);
} //for (iFea = 0; iFea < nDimSelec; iFea++)

printf("\n\n Please press any key:"); fflush(fout); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
////////////////////////////////////////////////

float *fVecTrainSelec_1st = new float[nNumVecTrain_1st*nDimSelec]; //0 -- 1 //[nDimSelecMaxf]
if (fVecTrainSelec_1st == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n An error: fVecTrainSelec_1st == NULL ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

delete[] fVecTrain_1st;
delete[] fVecTrain_2nd;
//getchar(); exit(1);

return UNSUCCESSFUL_RETURN;
} // if (fVecTrainSelec_1st == NULL)

float *fVecTrainSelec_2nd = new float[nNumVecTrain_2nd*nDimSelec]; //0 -- 1 //[nDimSelecMaxf]
if (fVecTrainSelec_2nd == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n An error: fVecTrainSelec_2nd == NULL ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fVecTrain_1st;
	delete[] fVecTrain_2nd;
	delete[] fVecTrainSelec_1st;

	//getchar(); exit(1);
	return UNSUCCESSFUL_RETURN;
} // if (fVecTrainSelec_2nd == NULL)

nResult = Converting_Arr_To_Selec(
					nDim, //const int nDimf,
					nDimSelec, //const int nDimSelecf,

					nNumVecTrain_1st, //const int nNumVecf,

					nPosFeaSeparBestArr, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

					fVecTrain_1st, //const float fVecArr[],[nProdTrain_1st]
					fVecTrainSelec_1st); //float fVecSelecArr[]) //[nNumVecTrain_1st*nDimSelec]

if (nResult == UNSUCCESSFUL_RETURN)
{
	return UNSUCCESSFUL_RETURN;
} // if (nResult == UNSUCCESSFUL_RETURN)

nResult = Converting_Arr_To_Selec(
					nDim, //const int nDimf,
					nDimSelec, //const int nDimSelecf,

					nNumVecTrain_2nd, //const int nNumVecf,

					nPosFeaSeparBestArr, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

					fVecTrain_2nd, //const float fVecArr[],[nProdTrain_2nd]
					fVecTrainSelec_2nd); //float fVecSelecArr[]) //[nNumVecTrain_2nd*nDimSelec]

if (nResult == UNSUCCESSFUL_RETURN)
{
	return UNSUCCESSFUL_RETURN;
} // if (nResult == UNSUCCESSFUL_RETURN)

delete[] fVecTrainSelec_1st;
delete[] fVecTrainSelec_2nd;

*/
////////////////////////////////////////////////////////////

float fSclarProd(
				const int nDimf,
				 const float fArr1f[],
				 const float fArr2f[])
{
int
	i1;

float
	fScalarProdf = 0.0;

for (i1 = 0; i1 < nDimf; i1++)
{

fScalarProdf += fArr1f[i1]*fArr2f[i1];
} // for (i1 = 0; i1 < i1++)

return fScalarProdf;
} //float fSclarProd(
////////////////////////////////////////////////

void nNumArr1AboSepar_Arr2AboSepar(
					const int nDim1stf,
					const int nDim2ndf,
					const  float fepsf,

					const float fArr1stf[], //0 -- 1.0
					const  float fArr2ndf[], //0 -- 1.0

					const float fPosSeparf,

					int &nNumArr1stAboSeparf,
					int &nNumArr2ndAboSeparf,

					float &fNumArr1stAboSeparf,
					float &fNumArr2ndAboSeparf)
{

int 
	i1;

float
	fPosSeparMinf = fPosSeparf - fepsf,
	fPosSeparMaxf = fPosSeparf + fepsf;

nNumArr1stAboSeparf = 0;
nNumArr2ndAboSeparf = 0;

fNumArr1stAboSeparf = 0.0;
fNumArr2ndAboSeparf = 0.0;

for (i1 = 0; i1 < nDim1stf;  i1++)
{
	if (fArr1stf[i1] > fPosSeparMaxf)
	{
	nNumArr1stAboSeparf += 1;
	fNumArr1stAboSeparf += 1.0;
	} //if (fArr1stf[i1] > fPosSeparMaxf)
	else if (fArr1stf[i1] >= fPosSeparMinf && fArr1stf[i1] <= fPosSeparMaxf)
		fNumArr1stAboSeparf += 0.5;

} //for (i1 = 0; i1 < nDim1stf;  i1++)

for (i1 = 0; i1 < nDim2ndf;  i1++)
{
	if (fArr2ndf[i1] > fPosSeparMaxf)
	{
	nNumArr2ndAboSeparf += 1;
	fNumArr2ndAboSeparf += 1.0;
	} //if (fArr2ndf[i1] > fPosSeparMaxf)
	else if (fArr2ndf[i1] >= fPosSeparMinf && fArr2ndf[i1] <= fPosSeparMaxf)
		fNumArr2ndAboSeparf += 0.5;

	//if (fArr2ndf[i1] < fPosSeparf)
	//	nNumArr2ndAboSeparf += 1;
} //for (i1 = 0; i1 < nDim2ndf;  i1++)

//return SUCCESSFUL_RETURN;
} // void nNumArr1AboSepar_Arr2AboSepar(...
//////////////////////////////////////////////////////////

int FindingBestSeparByRatioOfOneFea(
					const float fLargef,
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_G_Const_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf) 					
{
	int EfficiencyOfSeparByRatio(
		const int nDim1stf,
		const int nDim2ndf,

		const  float fepsf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1

		const float fPosSeparf,

		int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
		int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparf,
		float &fNumArr2ndSeparf,

		float &fDiffSeparOfArr1stAndArr2ndf,
		float &fRatioOfSeparOfArr1stAndArr2ndf,  //fRatioOfSeparOfArr1stAndArr2ndf

		float &fEfficiencyOfSeparByRatiof);

int 
	i_Iterf = -2,
	nResf,
	nSeparDiffDirectionf,
	nSeparRatioDirectionf;
//////////////////////

float 

	fNumArr1stSeparf,
	fNumArr2ndSeparf,
	
	fBorderBelCurf = fBorderBelf,
	fBorderAboCurf = fBorderAbof,

	fEfficiencyOfSeparByRatiof,

	fDiffSeparOfArr1stAndArr2ndf,
	fRatioOfSeparOfArr1stAndArr2ndf,

//fPosSeparf,
/////////////////
	fX1f,
	fX2f,

	fArg_x1f, //x1,
	fArg_x2f;

//fC_Const,fR_Const
fEfficBestSeparOfOneFeaf = fLargef;

fArg_x1f = (float) ((fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf) );

nResf = EfficiencyOfSeparByRatio(
			nDim1stf, //const int nDim1stf,
			nDim2ndf, //const int nDim2ndf,

			fepsf, //const  float fepsf,

			fArr1stf, //const float fArr1stf[], //0 -- 1
			fArr2ndf, //const  float fArr2ndf[], //0 -- 1

			fArg_x1f, //const float fPosSeparf,

			nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
			nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

			fNumArr1stSeparf, //float &fNumArr1stSeparf,
			fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

			fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
			fRatioOfSeparOfArr1stAndArr2ndf, //float &fRatioOfSeparOfArr1stAndArr2ndf);

			fEfficiencyOfSeparByRatiof); // float &fEfficiencyOfSeparByRatiof);//

if (nResf == UNSUCCESSFUL_RETURN)
{
	return UNSUCCESSFUL_RETURN;
}//if (nResf == UNSUCCESSFUL_RETURN)

fX1f = fEfficiencyOfSeparByRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 'FindingBestSeparByRatioOfOneFea' 1: iFea_Glob = %d, fArg_x1f = %E, fX1f = %E", iFea_Glob, fArg_x1f, fX1f);
fprintf(fout, "\n fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E", 
	fNumArr1stSeparf, fNumArr2ndSeparf, fRatioOfSeparOfArr1stAndArr2ndf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

if (fX1f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX1f;

fPosSeparOfOneFeaBestf = fArg_x1f;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 1: a new fEfficBestSeparOfOneFeaf = %E, fPosSeparOfOneFeaBestf = %E, iFea_Glob = %d",
	fEfficBestSeparOfOneFeaf, fPosSeparOfOneFeaBestf, iFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

} //if (fX1f < fEfficBestSeparOfOneFeaf)

fArg_x2f = (float)( (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf) );

/*
fX2f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					fArg_x2f, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);
*/

nResf = EfficiencyOfSeparByRatio(
	nDim1stf, //const int nDim1stf,
	nDim2ndf, //const int nDim2ndf,

	fepsf, //const  float fepsf,

	fArr1stf, //const float fArr1stf[], //0 -- 1
	fArr2ndf, //const  float fArr2ndf[], //0 -- 1

	fArg_x2f, //const float fPosSeparf,

	nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
	nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

	fNumArr1stSeparf, //float &fNumArr1stSeparf,
	fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

	fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
	fRatioOfSeparOfArr1stAndArr2ndf, //float &fRatioOfSeparOfArr1stAndArr2ndf);

	fEfficiencyOfSeparByRatiof); // float &fEfficiencyOfSeparByRatiof);//

if (nResf == UNSUCCESSFUL_RETURN)
{
	return UNSUCCESSFUL_RETURN;
}//if (nResf == UNSUCCESSFUL_RETURN)

fX2f = fEfficiencyOfSeparByRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
//fprintf(fout,"\n\n1: fArg_x1f = %E, fX1f = %E, fArg_x2f = %E, fX2f = %E, fC_Const = %E",
//	   fArg_x1f, fX1f, fArg_x2f, fX2f,fC_Const); fflush(fout);

//printf("\n\n1: fArg_x1f = %E, fX1f = %E, fArg_x2f = %E, fX2f = %E, fC_Const = %E",
//	   fArg_x1f, fX1f, fArg_x2f, fX2f,fC_Const);  getchar();

fprintf(fout, "\n\n 'FindingBestSeparByRatioOfOneFea' 2: iFea_Glob = %d, fArg_x2f = %E, fX2f = %E", iFea_Glob, fArg_x2f, fX2f);
fprintf(fout, "\n fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E",
	fNumArr1stSeparf, fNumArr2ndSeparf, fRatioOfSeparOfArr1stAndArr2ndf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

if (fX2f < fEfficBestSeparOfOneFeaf)
{
fEfficBestSeparOfOneFeaf = fX2f;

fPosSeparOfOneFeaBestf = fArg_x2f;

nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

fNumArr1stSeparBestf = fNumArr1stSeparf;
fNumArr2ndSeparBestf = fNumArr2ndSeparf;

fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 2: a new fEfficBestSeparOfOneFeaf = %E, fPosSeparOfOneFeaBestf = %E, iFea_Glob = %d, i_Iterf = %d",
	fEfficBestSeparOfOneFeaf, fPosSeparOfOneFeaBestf, iFea_Glob, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

} //if (fX2f < fEfficBestSeparOfOneFeaf)

for (i_Iterf = 0; i_Iterf < nNumIterMaxForFindingBestSeparByRatioOfOneFeaf; i_Iterf++)
{
	if (fX1f < fX2f)
	{
	fBorderAboCurf = fArg_x2f;

	fArg_x2f = fArg_x1f;

	fX2f = fX1f;

	fArg_x1f = (float)( (fC_Const*fBorderBelCurf) + (fR_Const*fBorderAboCurf));

/*
	fX1f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					fArg_x1f, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);
*/

	nResf = EfficiencyOfSeparByRatio(
		nDim1stf, //const int nDim1stf,
		nDim2ndf, //const int nDim2ndf,

		fepsf, //const  float fepsf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1

		fArg_x1f, //const float fPosSeparf,

		nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparf, //float &fNumArr1stSeparf,
		fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

		fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
		fRatioOfSeparOfArr1stAndArr2ndf, //float &fRatioOfSeparOfArr1stAndArr2ndf);

		fEfficiencyOfSeparByRatiof); // float &fEfficiencyOfSeparByRatiof);//

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fX1f = fEfficiencyOfSeparByRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
//	fprintf(fout,"\n\n2: fArg_x1f = %E, fX1f = %E, fArg_x2f = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   fArg_x1f, fX1f, fArg_x2f, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

	fprintf(fout, "\n\n 'FindingBestSeparByRatioOfOneFea' 3: iFea_Glob = %d, fArg_x1f = %E, fX1f = %E", iFea_Glob, fArg_x1f, fX1f);
	fprintf(fout, "\n fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, i_Iterf = %d",
		fNumArr1stSeparf, fNumArr2ndSeparf, fRatioOfSeparOfArr1stAndArr2ndf, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = fArg_x1f;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 4: a new fEfficBestSeparOfOneFeaf = %E, fPosSeparOfOneFeaBestf = %E, iFea_Glob = %d, i_Iterf = %d",
			fEfficBestSeparOfOneFeaf, fPosSeparOfOneFeaBestf, iFea_Glob, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //if (fX1f < fEfficBestSeparOfOneFeaf)

	} //if (fX1f < fX2f)
	else
	{
	fBorderBelCurf = fArg_x1f;

	fArg_x1f = fArg_x2f;

	fX1f = fX2f;
	fArg_x2f = (float)( (fR_Const*fBorderBelCurf) + (fC_Const*fBorderAboCurf));
/*
	fX2f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					fArg_x2f, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);
*/

	nResf = EfficiencyOfSeparByRatio(
		nDim1stf, //const int nDim1stf,
		nDim2ndf, //const int nDim2ndf,

		fepsf, //const  float fepsf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1

		fArg_x2f, //const float fPosSeparf,

		nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparf, //float &fNumArr1stSeparf,
		fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

		fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
		fRatioOfSeparOfArr1stAndArr2ndf, //float &fRatioOfSeparOfArr1stAndArr2ndf);

		fEfficiencyOfSeparByRatiof); // float &fEfficiencyOfSeparByRatiof);//

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fX2f = fEfficiencyOfSeparByRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
//	fprintf(fout,"\n\n3: fArg_x1f = %E, fX1f = %E, fArg_x2f = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   fArg_x1f, fX1f, fArg_x2f, fX2f,fBorderBelCurf,fBorderAboCurf); fflush(fout);

//	printf("\n\n3:  fArg_x1f = %E, fX1f = %E, fArg_x2f = %E, fX2f = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
//	   fArg_x1f, fX1f, fArg_x2f, fX2f,fBorderBelCurf,fBorderAboCurf);fflush(fout);

	fprintf(fout, "\n\n 'FindingBestSeparByRatioOfOneFea' 5: iFea_Glob = %d, fArg_x2f = %E, fX2f = %E", iFea_Glob, fArg_x2f, fX2f);
	fprintf(fout, "\n fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, i_Iterf = %d",
		fNumArr1stSeparf, fNumArr2ndSeparf, fRatioOfSeparOfArr1stAndArr2ndf, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fX2f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX2f;

		fPosSeparOfOneFeaBestf = fArg_x2f;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 6: a new fEfficBestSeparOfOneFeaf = %E, fPosSeparOfOneFeaBestf = %E, iFea_Glob = %d, i_Iterf = %d",
			fEfficBestSeparOfOneFeaf, fPosSeparOfOneFeaBestf, iFea_Glob, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (fX2f < fEfficBestSeparOfOneFeaf)

	} //else

	if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_G_Const_Searchf)
	{
		fArg_x1f = (float)( (fBorderBelCurf + fBorderAboCurf)/2.0);

/*
	fX1f = EfficiencyOfSeparByRatio(
					nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1

					fArg_x1f, //const float fPosSeparf,

					nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					fNumArr1stSeparf, //float &fNumArr1stSeparf,
					fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

					fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
					fRatioOfSeparOfArr1stAndArr2ndf); //float &fRatioOfSeparOfArr1stAndArr2ndf);
*/

	nResf = EfficiencyOfSeparByRatio(
		nDim1stf, //const int nDim1stf,
		nDim2ndf, //const int nDim2ndf,

		fepsf, //const  float fepsf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1

		fArg_x1f, //const float fPosSeparf,

		nSeparDiffDirectionf, //int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionf, //int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparf, //float &fNumArr1stSeparf,
		fNumArr2ndSeparf, //float &fNumArr2ndSeparf,

		fDiffSeparOfArr1stAndArr2ndf, //float &fDiffSeparOfArr1stAndArr2ndf,
		fRatioOfSeparOfArr1stAndArr2ndf, //float &fRatioOfSeparOfArr1stAndArr2ndf);

		fEfficiencyOfSeparByRatiof); // float &fEfficiencyOfSeparByRatiof);//

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fX1f = fEfficiencyOfSeparByRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout, "\n\n 'FindingBestSeparByRatioOfOneFea' 7: iFea_Glob = %d, fArg_x1f = %E, fX1f = %E", iFea_Glob, fArg_x1f, fX1f);
	fprintf(fout, "\n fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, i_Iterf = %d",
		fNumArr1stSeparf, fNumArr2ndSeparf, fRatioOfSeparOfArr1stAndArr2ndf, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (fX1f < fEfficBestSeparOfOneFeaf)
		{
		fEfficBestSeparOfOneFeaf = fX1f;

		fPosSeparOfOneFeaBestf = fArg_x1f;

		nSeparDiffDirectionBestf = nSeparDiffDirectionf;  //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf = nSeparRatioDirectionf; // //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestf = fNumArr1stSeparf;
		fNumArr2ndSeparBestf = fNumArr2ndSeparf;

		fDiffSeparOfArr1stAndArr2ndBestf = fDiffSeparOfArr1stAndArr2ndf;
		fRatioOfSeparOfArr1stAndArr2ndBestf = fRatioOfSeparOfArr1stAndArr2ndf; 	

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout, "\n\n 8: a new fEfficBestSeparOfOneFeaf = %E, fPosSeparOfOneFeaBestf = %E, iFea_Glob = %d, i_Iterf = %d",
			fEfficBestSeparOfOneFeaf, fPosSeparOfOneFeaBestf, iFea_Glob, i_Iterf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //if (fX1f < fEfficBestSeparOfOneFeaf)
/*
	printf("\n\n//////////////////////////////////////////");
	printf("\n\nExit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_G_Const_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_G_Const_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);

	fprintf(fout,"\n\n//////////////////////////////////////////");

	fprintf(fout,"\n\n Exit by: fabs(fBorderAboCurf - fBorderBelCurf)= %E  < fPrecisionOf_G_Const_Searchf = %E, fBorderBelCurf = %E, fBorderAboCurf = %E",
	   fabs(fBorderAboCurf - fBorderBelCurf),fPrecisionOf_G_Const_Searchf,fBorderBelCurf,fBorderAboCurf); fflush(fout);
*/
//		return 1;
		return SUCCESSFUL_RETURN;
	} //if ( fabs(fBorderAboCurf - fBorderBelCurf) < fPrecisionOf_G_Const_Searchf)

} //for (i_Iterf = 0; i_Iterf < nNumIterMaxForFindingBestSeparByRatioOfOneFeaf; i_Iterf++)

#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n An error in 'FindingBestSeparByRatioOfOneFea'");
fprintf(fout,"\n\nAn error in 'FindingBestSeparByRatioOfOneFea'");

printf("\n\n fArg_x1f = %E, fArg_x2f = %E, fNumArr1stSeparf = %E, fNumArr2ndSeparf = %E",
	fArg_x1f, fArg_x2f,fNumArr1stSeparf, fNumArr2ndSeparf);

printf("\n\nnSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//getchar();	exit(1);
return UNSUCCESSFUL_RETURN;
} //int FindingBestSeparByRatioOfOneFea(
/////////////////////////////////////////////////////////////////

//float EfficiencyOfSeparByRatio(
	int EfficiencyOfSeparByRatio(
				const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					const float fPosSeparf,

					int &nSeparDiffDirectionf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparf,
					float &fNumArr2ndSeparf,

					float &fDiffSeparOfArr1stAndArr2ndf,
					float &fRatioOfSeparOfArr1stAndArr2ndf,  //fRatioOfSeparOfArr1stAndArr2ndf

					float &fEfficiencyOfSeparByRatiof) //fRatioOfSeparOfArr1stAndArr2ndf

{
void nNumArr1AboSepar_Arr2AboSepar(
					const int nDim1stf,
					const int nDim2ndf,
					const  float fepsf,

					const float fArr1stf[], //0 -- 1.0
					const  float fArr2ndf[], //0 -- 1.0

					const float fPosSeparf,

					int &nNumArr1stAboSeparf,
					int &nNumArr2ndAboSeparf,

					float &fNumArr1stAboSeparf,
					float &fNumArr2ndAboSeparf);


int
	nNumArr1stAboSeparf = -1,
	nNumArr2ndAboSeparf = -1;

float
	fDiffAbof,
	fDiffBelf,

	fEfficf,
	fEfficTemp1f,
	fEfficTemp2f,

	fRatioAbof,
	fRatioBelf,

	fNumArr1stBelSeparf,
	fNumArr2ndBelSeparf,

	fNumArr1stAboSeparf = -1.0,
	fNumArr2ndAboSeparf = -1.0;

//nResf = nNumArr1AboSepar_Arr2AboSepar(
	 nNumArr1AboSepar_Arr2AboSepar(
				nDim1stf, //const int nDim1stf,
					nDim2ndf, //const int nDim2ndf,
					fepsf, //const  float fepsf,

					fArr1stf, //const float fArr1stf[], //0 -- 1.0
					fArr2ndf, //const  float fArr2ndf[], //0 -- 1.0

					fPosSeparf, //const float fPosSeparf,

					nNumArr1stAboSeparf, //int &nNumArr1stAboSeparf,
					nNumArr2ndAboSeparf, //int &nNumArr2ndAboSeparf,

					fNumArr1stAboSeparf, //float &fNumArr1stAboSeparf,
					fNumArr2ndAboSeparf); //float &fNumArr2ndAboSeparf);

if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || fNumArr1stAboSeparf < nNumArr1stAboSeparf)
{  
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\nAn error in 'EfficiencyOfSeparByRatio': nNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByRatio': nnNumArr1stAboSeparf = %d < 0 ||..., nDim1stf = %d, fNumArr1stAboSeparf = %E, nNumArr1stAboSeparf = %d",
						nNumArr1stAboSeparf,nDim1stf,fNumArr1stAboSeparf,nNumArr1stAboSeparf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//getchar();	exit(1);
return UNSUCCESSFUL_RETURN;
} //if (nNumArr1stAboSeparf < 0 || nNumArr1stAboSeparf > nDim1stf || ...)

if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || fNumArr2ndAboSeparf < nNumArr2ndAboSeparf)
{  
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\nAn error in 'EfficiencyOfSeparByRatio': nNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);

fprintf(fout,"\n\nAn error in 'EfficiencyOfSeparByRatio': nnNumArr2ndAboSeparf = %d < 0 ||..., nDim2ndf = %d, fNumArr2ndAboSeparf = %E, nNumArr2ndAboSeparf = %d",
						nNumArr2ndAboSeparf,nDim2ndf,fNumArr2ndAboSeparf,nNumArr2ndAboSeparf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//getchar();	exit(1);
return UNSUCCESSFUL_RETURN;
} //if (nNumArr2ndAboSeparf < 0 || nNumArr2ndAboSeparf > nDim2ndf || ...)

fNumArr1stBelSeparf = nDim1stf - fNumArr1stAboSeparf;
fNumArr2ndBelSeparf = nDim2ndf - fNumArr2ndAboSeparf;

fDiffAbof = fNumArr1stAboSeparf - fNumArr2ndAboSeparf;

if (fNumArr2ndAboSeparf > fepsf)
	fRatioAbof = fNumArr1stAboSeparf/fNumArr2ndAboSeparf; //(float)(nDim1stf); //fNumArr2ndAboSeparf;
else
	fRatioAbof = (float)( fNumArr1stAboSeparf + 1.0); //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

fDiffBelf = fNumArr1stBelSeparf - fNumArr2ndBelSeparf;

if (fNumArr2ndBelSeparf > fepsf)
	fRatioBelf = fNumArr1stBelSeparf/fNumArr2ndBelSeparf; //(float)(nDim1stf;
else
	fRatioBelf = fNumArr1stBelSeparf; //(float)(2*nDim1stf) + 1.0; //more than otherwise the max possible value 'nDim1stf/0.5'

if (fDiffAbof > fDiffBelf)
{
nSeparDiffDirectionf = 1;
fDiffSeparOfArr1stAndArr2ndf = fDiffAbof;

} //if (fDiffAbof > fDiffBelf)
else
{
nSeparDiffDirectionf = -1;
fDiffSeparOfArr1stAndArr2ndf = fDiffBelf;
} //else

if (fRatioAbof > fRatioBelf)
{
nSeparRatioDirectionf = 1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioAbof;

} //if (fRatioAbof > fRatioBelf)
else
{
nSeparRatioDirectionf = -1;
fRatioOfSeparOfArr1stAndArr2ndf = fRatioBelf;
} //else

fEfficTemp1f = (fNumArr1stAboSeparf/nDim1stf) + (fNumArr2ndBelSeparf/nDim2ndf);

fEfficTemp2f = (fNumArr1stBelSeparf/nDim1stf) + (fNumArr2ndAboSeparf/nDim2ndf);

if (fEfficTemp1f >= fEfficTemp2f)
{
fEfficf = -fEfficTemp1f;
fNumArr1stSeparf = fNumArr1stAboSeparf;
fNumArr2ndSeparf = fNumArr2ndBelSeparf;

} //if (fEfficTemp1f >= fEfficTemp2f)
else
{
fEfficf = -fEfficTemp2f;
fNumArr1stSeparf = fNumArr1stBelSeparf;
fNumArr2ndSeparf = fNumArr2ndAboSeparf;

} //else

//fNumArr1stSeparf


//fEfficf = -( fRatioOfSeparOfArr1stAndArr2ndf + ( fDiffSeparOfArr1stAndArr2ndf/(nDim1stf + nDim2ndf) ) );
/*
printf("\n///////////////////////////////////////////////");
printf("\n\nEfficiencyOfSeparByRatio: fPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

printf("\n\nEfficiencyOfSeparByRatio: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

printf("\n\nEfficiencyOfSeparByRatio: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); getchar();

fprintf(fout,"\n///////////////////////////////////////////////");
fprintf(fout,"\n\nfPosSeparf = %E, fNumArr1stAboSeparf = %E, fNumArr2ndAboSeparf = %E, fDiffAbof = %E, fRatioAbof = %E",
	   fPosSeparf,fNumArr1stAboSeparf, fNumArr2ndAboSeparf, fDiffAbof, fRatioAbof);

fprintf(fout,"\n\nEfficiencyOfSeparByRatio: fNumArr1stBelSeparf = %E, fNumArr2ndBelSeparf = %E, fDiffBelf = %E, fRatioBelf = %E",
	   fNumArr1stBelSeparf, fNumArr2ndBelSeparf, fDiffBelf, fRatioBelf);

fprintf(fout,"\n\nEfficiencyOfSeparByRatio: nSeparDiffDirectionf = %d, nSeparRatioDirectionf = %d, fDiffSeparOfArr1stAndArr2ndf = %E, fRatioOfSeparOfArr1stAndArr2ndf = %E, fEfficf = %E",
	   nSeparDiffDirectionf, nSeparRatioDirectionf, fDiffSeparOfArr1stAndArr2ndf, fRatioOfSeparOfArr1stAndArr2ndf, fEfficf); 

fflush(fout);
*/

fEfficiencyOfSeparByRatiof = fEfficf;

return SUCCESSFUL_RETURN;
} // float EfficiencyOfSeparByRatio(...
///////////////////////////////////////////////

#ifndef ONLY_ANNAS_PART_REMAINS
int SelectingBestFeas(
					const float fLargef,
					const  float fepsf,

					const int nDimf,
					const int nDimSelecMaxf,

					const int nNumVec1stf,
					const int nNumVec2ndf,

					const float fPrecisionOf_G_Const_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
					const float fEfficBestSeparOfOneFeaAcceptMaxf,

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

					int &nDimSelecf,
					//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
					//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

					float &fRatioBestMinf,

					int &nPosOneFeaBestMaxf,

					int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestMaxf,
					float &fNumArr2ndSeparBestMaxf,

					float fRatioBestArrf[], //nDimSelecMaxf

					int nPosFeaSeparBestArrf[]) //nDimSelecMaxf
{
int FindingBestSeparByRatioOfOneFea(
					const float fLargef,
					const int nDim1stf,
					const int nDim2ndf,

					const  float fepsf,

					const float fPrecisionOf_G_Const_Searchf,

					const  float fBorderBelf,
					const  float fBorderAbof,

					const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					const float fArr1stf[], //0 -- 1
					const  float fArr2ndf[], //0 -- 1

					float &fEfficBestSeparOfOneFeaf,

					float &fPosSeparOfOneFeaBestf,

					int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
					int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

					float &fNumArr1stSeparBestf,
					float &fNumArr2ndSeparBestf,

					float &fDiffSeparOfArr1stAndArr2ndBestf,
					float &fRatioOfSeparOfArr1stAndArr2ndBestf); 					

int InsertSortOf_FloatArrWithPosit(const int nDimf,

	int nPositionArrf[], //[nDimf]
	float fArrf[]); //[nDimf]

int
	nResf,
	nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestff, //1 -- Abof, -1 -- Bel
	nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestff, //1 -- Abof, -1 -- Bel

	iFeaf,
	iVecf;

float
		fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaff,

		fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestff,

		fNumArr1stSeparBestf, //float &fNumArr1stSeparBestff,
		fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestff,

		fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestff,
		fRatioOfSeparOfArr1stAndArr2ndBestf; //

//Initialization
nDimSelecf = 0;

fRatioBestMinf = fLargef;

nPosOneFeaBestMaxf = -1;

for (iFeaf = 0; iFeaf < nDimSelecMaxf; iFeaf++)
{
nPosFeaSeparBestArrf[iFeaf]  = -1; //nDimSelecMaxf
fRatioBestArrf[iFeaf]  = -1.0;
} // for (iFeaf = 0; iFeaf < nDimSelecMaxf; iFeaf++)

//////////////////////////////////////
float *fOneFea1stArrf = new float[nNumVec1stf];
if (fOneFea1stArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf == NULL");
fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//getchar();	exit(1);
return UNSUCCESSFUL_RETURN;
} //if (fOneFea1stArrf == NULL)

float *fOneFea2ndArrf = new float[nNumVec2ndf];
if (fOneFea2ndArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf == NULL");
	fprintf(fout, "\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fOneFea1stArrf;
	//getchar();	exit(1);
	return UNSUCCESSFUL_RETURN;
} //if (fOneFea2ndArrf == NULL)

float *fEfficBestSeparOf_AllFeasArrf = new float[nDimf];
if (fEfficBestSeparOf_AllFeasArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\nAn error in 'SelectingBestFeas': fEfficBestSeparOf_AllFeasArrf == NULL");
	fprintf(fout, "\n\nAn error in 'SelectingBestFeas': fEfficBestSeparOf_AllFeasArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fOneFea1stArrf;
	delete[] fOneFea2ndArrf;
	//getchar();	exit(1);
	return UNSUCCESSFUL_RETURN;
} //if (fEfficBestSeparOf_AllFeasArrf == NULL)

int *nPositionOfFeaArrf = new int[nDimf];
if (nPositionOfFeaArrf == NULL)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\nAn error in 'SelectingBestFeas': nPositionOfFeaArrf == NULL");
	fprintf(fout, "\n\nAn error in 'SelectingBestFeas': nPositionOfFeaArrf == NULL");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	delete[] fOneFea1stArrf;
	delete[] fOneFea2ndArrf;
	delete[] fEfficBestSeparOf_AllFeasArrf;
	//getchar();	exit(1);
	return UNSUCCESSFUL_RETURN;
} //if (nPositionOfFeaArrf == NULL)

///////////////////////////////////////////
for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
	nPositionOfFeaArrf[iFeaf] = iFeaf;

	iFea_Glob = iFeaf;
	for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
	{
	fOneFea1stArrf[iVecf] = fArr1stf[iFeaf + iVecf*nDim];
/*		
		if (fOneFea1stArrf[iVecf] < -fepsf || fOneFea1stArrf[iVecf] > 1.0 + fepsf)
		{
		printf("\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea1stArrf[iVecf],iFeaf);

		fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea1stArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea1stArrf[iVecf],iFeaf);

		getchar();	exit(1);
		} //if (fOneFea1stArrf[iVecf] < -fepsf || fOneFea1stArrf[iVecf] > 1.0 + fepsf)
*/
	} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

	for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
	{
	fOneFea2ndArrf[iVecf] = fArr2ndf[iFeaf + iVecf*nDim]; //	fOneFeaTrain_2ndArr[nNumVecTrain_2nd],
/*
		if (fOneFea2ndArrf[iVecf] < -fepsf || fOneFea2ndArrf[iVecf] > 1.0 + fepsf)
		{
		printf("\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea2ndArrf[iVecf],iFeaf);

		fprintf(fout,"\n\nAn error in 'SelectingBestFeas': fOneFea2ndArrf[%d] = %E, iFeaf = %d", 
						iVecf,fOneFea2ndArrf[iVecf],iFeaf);

		getchar();	exit(1);
		} //if (fOneFea2ndArrf[iVecf] < -fepsf || fOneFea2ndArrf[iVecf] > 1.0 + fepsf)
*/
	} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

nResf = FindingBestSeparByRatioOfOneFea(
					fLargef, //const float fLargef,

					nNumVec1stf, //const int nDim1stf,
					nNumVec2ndf, //const int nDim2ndf,

					fepsf, //const  float fepsf,

					fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

					0.0, //const  float fBorderBelf,
					1.0, //const  float fBorderAbof,

					nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, 

					fOneFea1stArrf, //const float fArr1stf[], //0 -- 1
					fOneFea2ndArrf, //const  float fArr2ndf[], //0 -- 1

					fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaff,

					fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestff,

					nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestff, //1 -- Abof, -1 -- Bel
					nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestff, //1 -- Abof, -1 -- Bel

					fNumArr1stSeparBestf, //float &fNumArr1stSeparBestff,
					fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestff,

					fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestff,
					fRatioOfSeparOfArr1stAndArr2ndBestf); //float &fRatioOfSeparOfArr1stAndArr2ndBestf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n SelectingBestFeas': nResf == UNSUCCESSFUL_RETURN by 'FindingBestSeparByRatioOfOneFea' at iFeaf = %d", iFeaf);

		fprintf(fout, "\n\n SelectingBestFeas':nResf == UNSUCCESSFUL_RETURN by 'FindingBestSeparByRatioOfOneFea' at iFeaf = %d", iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		delete[] fOneFea1stArrf;
		delete[] fOneFea2ndArrf;

		delete[] fEfficBestSeparOf_AllFeasArrf;
		delete[] nPositionOfFeaArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	fEfficBestSeparOf_AllFeasArrf[iFeaf] = fEfficBestSeparOfOneFeaf;

	#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n SelectingBestFeas': fEfficBestSeparOfOneFeaf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E",
		fEfficBestSeparOfOneFeaf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf);
	printf( "\n nSeparRatioDirectionBestf = %d, iFeaf = %d", nSeparRatioDirectionBestf, iFeaf);

	fprintf(fout,"\n\n 'SelectingBestFeas': fEfficBestSeparOfOneFeaf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E",
		fEfficBestSeparOfOneFeaf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf);

	fprintf(fout, "\n nSeparRatioDirectionBestf = %d, iFeaf = %d", nSeparRatioDirectionBestf, iFeaf);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (iFeaf < nDimSelecMax)
	{
		fprintf(fout_PrintFeatures, "\n After 'FindingBestSeparByRatioOfOneFea': fEfficBestSeparOf_AllFeasArrf[%d] = %E, fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E",
			iFeaf, fEfficBestSeparOf_AllFeasArrf[iFeaf], fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf);
	} //if (iFeaf < nDimSelecMax)

	if (iFeaf == nPositionOf_TotFeaToPrint)
	{
		fprintf(fout_PrintFeatures,"\n\n iFeaf == nPositionOf_TotFeaToPrint = %d", iFeaf);

		fprintf(fout_PrintFeatures,"\n After 'FindingBestSeparByRatioOfOneFea': fEfficBestSeparOf_AllFeasArrf[%d] = %E, fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E",
			iFeaf, fEfficBestSeparOf_AllFeasArrf[iFeaf], fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf);

		//printf("\n\n Please press any key:"); getchar();
	} //if (iFeaf == nPositionOf_TotFeaToPrint)

	if (fEfficBestSeparOfOneFeaf <= fEfficBestSeparOfOneFeaAcceptMaxf)
	{
	nDimSelecf += 1;
/*
		if (nDimSelecf - 1 < nDimSelecMaxf)
		{
		nPosFeaSeparBestArrf[nDimSelecf - 1] = iFeaf;
		fRatioBestArrf[nDimSelecf - 1] = fEfficBestSeparOfOneFeaf;

		} //if (nDimSelecf - 1 < nDimSelecMaxf)
*/
		if (fEfficBestSeparOfOneFeaf < fRatioBestMinf)
		{
		fRatioBestMinf = fEfficBestSeparOfOneFeaf;
		nPosOneFeaBestMaxf = iFeaf;
		} // if (fEfficBestSeparOfOneFeaf < fRatioBestMinf)

	} //if (fEfficBestSeparOfOneFeaf <= fEfficBestSeparOfOneFeaAcceptMaxf)

} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

nResf = InsertSortOf_FloatArrWithPosit(
	nDimf, //const int nDimf,

	nPositionOfFeaArrf, //int nPositionArrf[], //[nDimf]
	fEfficBestSeparOf_AllFeasArrf); // float fArrf[]); //[nDimf]

if (nResf == UNSUCCESSFUL_RETURN)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n SelectingBestFeas': nResf == UNSUCCESSFUL_RETURN by 'InsertSortOf_FloatArrWithPosit'");

	fprintf(fout, "\n\n SelectingBestFeas': nResf == UNSUCCESSFUL_RETURN by 'InsertSortOf_FloatArrWithPosit'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fOneFea1stArrf;
	delete[] fOneFea2ndArrf;

	delete[] fEfficBestSeparOf_AllFeasArrf;
	delete[] nPositionOfFeaArrf;

	return UNSUCCESSFUL_RETURN;
}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout, "\n\n 'SelectingBestFeas': after sorting, nDimSelecf = %d", nDimSelecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n fEfficBestSeparOf_AllFeasArrf[%d] = %E, nPositionOfFeaArrf[%d] = %d",
		iFeaf, fEfficBestSeparOf_AllFeasArrf[iFeaf], iFeaf, nPositionOfFeaArrf[iFeaf]);

	fprintf(fout, "\n fEfficBestSeparOf_AllFeasArrf[%d] = %E, nPositionOfFeaArrf[%d] = %d",
		iFeaf, fEfficBestSeparOf_AllFeasArrf[iFeaf], iFeaf, nPositionOfFeaArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (iFeaf < nDimSelecMaxf)
	{
		nPosFeaSeparBestArrf[iFeaf] = nPositionOfFeaArrf[iFeaf];
		fRatioBestArrf[iFeaf] = fEfficBestSeparOf_AllFeasArrf[iFeaf];
	} //if (iFeaf < nDimSelecMaxf)

}// for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

delete [] fOneFea1stArrf;
delete [] fOneFea2ndArrf;

delete[] fEfficBestSeparOf_AllFeasArrf;
delete[] nPositionOfFeaArrf;

	if (nDimSelecf <= nDimSelecMaxf && nDimSelecf > 0)
		return SUCCESSFUL_RETURN;
	else if (nDimSelecf > nDimSelecMaxf)
		return 2;
	else if (nDimSelecf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n SelectingBestFeas': nResf == UNSUCCESSFUL_RETURN by nDimSelecf == 0");

		fprintf(fout, "\n\n SelectingBestFeas':nResf == UNSUCCESSFUL_RETURN  by nDimSelecf == 0");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //
} //int SelectingBestFeas(

#endif //#ifndef ONLY_ANNAS_PART_REMAINS

///////////////////////////////////////////////
int Converting_Arr_To_Selec(
					const int nDimf,
					const int nDimSelecf,

					const int nNumVecf,

					const int nPosFeaSeparBestArr[], //[nDimSelecf]

					const float fVecArr[],//[nProdTrain_1st] or [nProdTrain_2nd]
					float fVecSelecArr[]) //[[nNumVecTrain_1st*nDimSelec] or [nNumVecTrain_2nd*nDimSelec]]
{
int
	iFeaf,
	iVecf,
	nPosSelecCurf;
	
for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
{
nPosSelecCurf = nPosFeaSeparBestArr[iFeaf];
	if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\nAn error in 'Converting_Arr_To_Selec': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);
	
	fprintf(fout,"\n\nAn error in 'Converting_Arr_To_Selec': nPosSelecCurf = %d < 0 ||..., iFeaf = %d", 
					nPosSelecCurf,iFeaf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//getchar();	exit(1);
	return UNSUCCESSFUL_RETURN;
	} //if (nPosSelecCurf < 0 || nPosSelecCurf > nDimf - 1)

	for (iVecf = 0; iVecf < nNumVecf; iVecf++)
	{
	fVecSelecArr[iFeaf + iVecf*nDimSelecf] = fVecArr[nPosSelecCurf + iVecf*nDimf];
	} // for (iVecf = 0; iVecf < nNumVecf; iVecf++)

} //for (iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)

return SUCCESSFUL_RETURN;
} //int Converting_Arr_To_Selec(...
////////////////////////////////////////////////////////////////

int InsertSortOf_FloatArrWithPosit(const int nDimf,

	int nPositionArrf[], //[nDimf]
	float fArrf[]) //[nDimf]
{
	//From smaller to larger
	int
		nTempf,
		i1,
		i2;

	float
		fTempf;

	if (nDimf < 2)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\nAn error in 'InsertSortOf_FloatArrWithPosit': nDimf < 2");
		fprintf(fout, "\nAn error in 'InsertSortOf_FloatArrWithPosit': nDimf < 2");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//		getchar();	exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nDimf < 2)

	for (i1 = 0; i1 < nDimf; i1++)
		nPositionArrf[i1] = i1;

	for (i1 = 1; i1 < nDimf; i1++)
	{
		fTempf = fArrf[i1];
		nTempf = nPositionArrf[i1];

		i2 = i1;
		while ((i2 > 0) && fArrf[i2 - 1] > fTempf)
		{
			fArrf[i2] = fArrf[i2 - 1];
			nPositionArrf[i2] = nPositionArrf[i2 - 1];

			i2 = i2 - 1;
		} // while ( (i2 > 0) && fArrf[i2-1] > fTempf)

		fArrf[i2] = fTempf;
		nPositionArrf[i2] = nTempf;
	} // for (i1=1; i1 < nDimf; i1++)

	return SUCCESSFUL_RETURN;
} // int InsertSortOf_FloatArrWithPosit(const int nDimf, int fArrf[])

//printf("\n\nPlease press any key:"); getchar();