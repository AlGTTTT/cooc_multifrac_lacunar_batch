#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//#define ONLY_ANNAS_PART_REMAINS

#define nDim (nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot) //20 //100 

#define nNumOfFeasHighSeparability 100 // < nDim

#define nDimSelecMax 200 //10 //90 //20 //5

//#define fEfficBestSeparOfOneFeaAcceptMax (-1.55) //the min at the perfect separation is (-2.0) 
#define fEfficBestSeparOfOneFeaAcceptMax (-1.1) //the min at the perfect separation is (-2.0) 

#define nNumVecTrain_1st (nNumOf_TrainVecs_Normal) //10 

#define nNumVecTrain_2nd (nNumOf_TrainVecs_Malignant) //10 //(nNumOf_TrainVecs_Normal) //1000 //100

#define nProdTrain_1st (nDim*nNumOf_TrainVecs_Normal)

#define nProdTrain_2nd (nDim*nNumVecTrain_2nd)

#define nNumIterForDataGenerMax (nNumOf_TrainVecs_Normal*50)

#define nNumDistiguishFeasForInit 2
/////////////////////////////////////////////////

#define fThreshold_HighSeparability 0.5
#define fThreshold_LowSeparability_1 0.3
#define fThreshold_LowSeparability_2 0.7
/////////////////////////////////////////////////
#define fBorderBel 0.0
#define fBorderAbove 1.0

#define nNumIterMaxForFindingBestSeparByRatioOfOneFea 10 //100
#define nNumIterMaxForFindingBestSeparByDiffOfOneFea (nNumIterMaxForFindingBestSeparByRatioOfOneFea)

////////////////////////////////////
#define fC_Const 6.18034E-01 //( (-1.0 + sqrt(5.0) )/2.0 ) 
#define fR_Const 3.81966E-01 //(1.0 - fC_Const)

#define fEfficOfArr1stRequired 0.9

#define fFeaValueThreshold_ForWarning 1.0E+8 //6

const float fPrecisionOf_G_Const_Search = (float)(0.01); // 0.005

//////////////////////////////////////////////////////////////////