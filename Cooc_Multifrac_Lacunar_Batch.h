#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
//#include "median_filter.h"

#include <dirent.h>

#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.h"
//#include "Cooc_Multifrac_Lacunar_PassAgg_Shifted.h"

///////////////////////////////////////////////////////////
#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
//	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax_La (nDim_2pN_Max_La) //2048
#define nWidMax_La (nDim_2pN_Max_La) //2048 

#define nLenMin_La 50 
#define nWidMin_La 50

//#define nImageSizeMax (nLenMax_La*nWidMax_La)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535
//////////////////////////////////////////////////////
//the multifractal header
//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018

#define MASK_OUTPUT
#define COMMENT_OUT_ALL_PRINTS

#define nLenMax (nDim_2pN_Max_La) //6000
#define nWidMax (nDim_2pN_Max_La) //6000

#define nLenMin 50
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

//////////////////////////////////////////////////////
#define nLenSubimageToPrintMin  0 //342
#define nLenSubimageToPrintMax  4000  //2500

#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

#define nOneWidToPrint  226 //56 //371 //2476 //
#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
//#define nIntensityStatMin 10 //85
//#define nIntensityStatMinForEqualization 0 //85

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max 14

//#define nThresholdForIntensitiesMin 20 //5 //1 //110
//#define nThresholdForIntensitiesMin 70 //150 // all colors

//#define nThresholdForIntensitiesMax 255

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

//#define nNumOfSquareOccurrence_Intervals 4

//#define nLenOfSquareMin 2
#define nNumOfNonZero_ObjectSquaresTotMin 1

//#define nNumOf_Qs_Tot 21 //20 // including 0 -- 21
#define nNumOf_Qs_Tot 11 //21 //including 0 -- 10 //21
#define nQs_Min (-5) //(-10)
#define nQs_Max 5 //10

/////////////////////////////////////////////////////
//test
//for Victor's test image -- 5 copies
//#define nNumOfVecs_Normal 5 

//#define nNumOfVecs_Normal 60  // for D:\Imago\Images\spleen\Anna_March11\RawImages\test\normal
//#define nNumOfVecs_Malignant 15 // for D:\Imago\Images\spleen\Anna_March11\RawImages\test\malignant

//#define nNumOfVecs_Normal 397 //155  // for D:\Imago\Images\spleen\Normal_Tot
//#define nNumOfVecs_Malignant 76 //60 // for D:\Imago\Images\spleen\Malignant_Tot

//#define nNumOfVecs_Normal 10 // for D:\Imago\Images\spleen\Normal_Tot_Small
//#define nNumOfVecs_Malignant 10 // for D:\Imago\Images\spleen\Malignant_Tot_Small

///////////////////////////////
//#define nNumOfVecs_Normal 20 // for D:\Imago\Images\spleen\Normals_TestSet

//#define nNumOfVecs_Normal 3 // fD:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\3CopiesOfOneNormal\\
//#define nNumOfVecs_Normal 3 // for D:\Imago\Images\spleen\VictorsWholeImages
//#define nNumOfVecs_Malignant 20 // for D:\Imago\Images\spleen\Malignants_TestSet

/////////////////////////////////////////////////////////////////////
//train
//#define nNumOfVecs_Normal 313 //155  // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Normal\\AOI\\

//benign as nornmal
//#define nNumOfVecs_Normal 477 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\
//#define nNumOfVecs_Normal 472 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//#define nNumOfVecs_Normal 473 //after removing AoIs with dim > nDim_2pN_Max_La == 2048

//For D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_1_Test
#define nNumOfVecs_Normal 2 //after removing AoIs with dim > nDim_2pN_Max_La == 2048

//#define nNumOfVecs_Normal 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

//#define nNumOfVecs_Normal 2 ////D:\Imago\Images\spleen\Genas_Images_June26\Benign\AoI_2_Test

//#define nNumOfVecs_Malignant 576 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

#define nNumOfVecs_Malignant 575 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

////////////////////////////////////////////////////////////
//multifractal

//watch for '#define HISTOGRAM_EQUALIZATION_APPLIED'

//#define nWidthOfQ_Step ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )
//#define nNumOfIntensity_IntervalsForMultifractal 64 // 2^n
//#define nNumOfIntensity_IntervalsForMultifractal 8 // 2^n
#define nNumOfIntensity_IntervalsForMultifractal 4 // 2^n

//#define nNumOfIntensity_IntervalsForMultifractal 32 //16 // 2^n

// nLenOfIntensityIntervalf == 256 / nNumOfIntensity_IntervalsForMultifractal == 4

//2080
#define nNumOfIntensity_IntervalsForMultifractalTot ( ((nNumOfIntensity_IntervalsForMultifractal + 1)*nNumOfIntensity_IntervalsForMultifractal)/2)

//43680
#define nNumOfFeasForMultifractalTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot)

//131040
#define nNumOfMultifractalFeasFor_OneDimTot (3*nNumOfFeasForMultifractalTot)

//////////////////////////////////////////////////////////////
//#define nNumOfSelectedFea 1
//#define nNumOfSelectedFea 1100 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//#define nNumOfSelectedFea 1102 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelectedFea (-1) //2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelected_SpectrumFea (nNumOfSelectedFea - nNumOfFeasForMultifractalTot)

//////////////////////////////////////////////////////

//nNumOfProcessedFilesTot
//#define nLenSubimageToPrintMin  0 //342 
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470 

//#define nOneWidToPrint  226 //56 //371 //2476 //
//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970 

///////////////////////////////////////////////////////////////////////
//the lacunarity header
#define nIntensityStatForReadMax_La 65535

//#define nIntensityStatBlack 0 //85 
#define nIntensityStatMax_La 255 

//#define nIntensityStatWhite (nIntensityStatMax_La)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max_La 14

//#define nThresholdForIntensities_ForFractalDimMin_La 70 //80 // all colors
#define nThresholdForIntensities_ForFractalDimMin_La 10 //80 // all colors
#define nThresholdForIntensities_ForFractalDimMax_La 210 //255

#define fWeightOfRed_InStr_La 1.0 // 0.0
#define fWeightOfGreen_InStr_La 1.0
#define fWeightOfBlue_InStr_La 1.0 //0.0 //

//'nNumOfSquareOccurrence_Intervals_La' and 'nDivisorForLenOfSquareMax_Init_La' go together, in a tandem
#define nNumOfSquareOccurrence_Intervals_La 4 // 2*2 = 2^n
#define nDivisorForLenOfSquareMax_Init_La (nNumOfSquareOccurrence_Intervals_La/2) // initially, nLenOfSquareCurf = nDim_2pNf/ nDivisorForLenOfSquareMax_Init_La;

//'nNumOfSquareOccurrence_Intervals_La' and 'nDivisorForLenOfSquareMax_Init_La' go together, in a tandem
//#define nNumOfSquareOccurrence_Intervals_La 16 // 4*4 = 2^n
//#define nDivisorForLenOfSquareMax_Init_La (nNumOfSquareOccurrence_Intervals_La/8) // initially, nLenOfSquareCurf = nDim_2pNf/ nDivisorForLenOfSquareMax_Init_La;
/////////////////////////////////////////////////

#define fCloseToZeroLimitForMomentsOfSquares 1.0E-10
#define fCloseToZeroLimitForSumOfMomentsOfSquares  1.0E-8
//////////////////////////////////////////////////////////

#define nLenOfSquareMin_La 2
///////////////////////////////////////////////

//#define nNumOfLenOfSquareMax_La 7 //for nDim_2pN_Max_La == 512, starting from the 4 squares per the length
//#define nNumOfLenOfSquareMax_La 8 //for nDim_2pN_Max_La == 1024, starting from the 4 squares per the length
#define nNumOfLenOfSquareMax_La 9 //for nDim_2pN_Max_La == 2048, starting from the 4 squares per the length
//#define nNumOfLenOfSquareMax_La 10 //for nDim_2pN_Max_La == 4096, starting from the 4 squares per the lengt//!change both nDim_2pN_Max_La and nNumOfLenOfSquareMax_La in a tandem
//#define nDim_2pN_Max_La 1024 // == 2^(nNumOfLenOfSquareMax_La + 2)
#define nDim_2pN_Max_La 2048 // == 2^(nNumOfLenOfSquareMax_La + 2)
//#define nDim_2pN_Max_La 4096 // == 2^(nNumOfLenOfSquareMax_La + 2) //for the whole images
//#define nDim_2pN_Max_La  (2^(nNumOfLenOfSquareMax_La + 2))

//////////////////////////////////////
#define nNumOfFeasForLacunar_AtIntensity_Range (nNumOfLenOfSquareMax_La*3 + (nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La) ) // 9*3 + 9+16 == 171

//#define nNumOfIntensity_IntervalsForLacunar 64 // 2^n
//#define nNumOfIntensity_IntervalsForLacunar 32 // 2^n
#define nNumOfIntensity_IntervalsForLacunar 16 // 2^n //-1.46
//#define nNumOfIntensity_IntervalsForLacunar 8 // 2^n //-1.46

#define nNumOfIntensity_IntervalsForLacunarTot ( ((nNumOfIntensity_IntervalsForLacunar + 1)*nNumOfIntensity_IntervalsForLacunar)/2) //33*32/2 == 528

#define nNumOfLacunarFeasFor_OneDimTot (nNumOfFeasForLacunar_AtIntensity_Range*nNumOfIntensity_IntervalsForLacunarTot) //171*528 == 90,288 for 'nNumOfSquareOccurrence_Intervals_La' == 16

////////////////////////
#define nNumOfNonZero_ObjectSquaresTotMin_La 1

//not used
//#define nNumOfFeasForLacunarTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForLacunarTot)

//#define nNumOfSelectedLacunarityFea 2263 //1
#define nNumOfSelectedLacunarityFea (nNumOfLacunarFeasFor_OneDimTot - 1) //1
//#define nNumOfSelectedLacunarityFea 8 // < nNumOfLacunarFeasFor_OneDimTot == 2268
//#define nNumOfSelectedLacunarityFea 9 // < nNumOfLacunarFeasFor_OneDimTot == 2268

///////////////////////////////////////////////////////
//Cooc feas
//watch for '#define HISTOGRAM_EQUALIZATION_APPLIED'

#define nGrayLevelIntensityMax (nIntensityStatMax) //check it up with nNumOfGrayLevelIntensities = 256

	//#define nNumOfAnglesTot 8 //0, 45, 90, 135, 180, 225, 270, 315
#define nNumOfValidAnglesMin 2 //<= nNumOfAnglesTot

#define nNumOfAnglesWithInsufficientDiffOfIntensitiesMin 3 // <= nNumOfAnglesTot

#define nNumOfAnglesFor_CW_Or_CCW 5

#define nStep_IntensitiessWithLargestDescentDistances 20 //50 //10 //40 //30   //8

//#define nNumOfSelected_CoocFea 0 //3 //1 //0 // < 5  nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot
//#define nNumOfSelected_CoocFea (4 + nNumOfHistogramIntervals_ForCooc)
//#define nNumOfSelected_CoocFea ( 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) - 1)
//#define nNumOfSelected_CoocFea ( 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) )
//#define nNumOfSelected_CoocFea ( 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2)  )
//#define nNumOfSelected_CoocFea ( 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + 1 )

//#define nNumOfSelected_CoocFea ( (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax) )
//#define nNumOfSelected_CoocFea ( (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax) + 1)
#define nNumOfSelected_CoocFea ( nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot - 1 )

///////////////////////////////////////////////////////////////////////////////////////
//object
#define nNumOfGrayLevelIntensities 256 // (nGrayLevelIntensityMax + 1)

#define nCoocSizeMax (nNumOfGrayLevelIntensities * nNumOfGrayLevelIntensities)

////////////////////////////////////////////////////////////////////////////////
//#define nScaleCoocTot 3 //4
#define nScaleCoocTot 5 //3

//the actual dimension of the square could vary from 'nDimOfSquare_ScaleCoocMin' to 'nDimOfSquare_ScaleCoocMax'
//#define nDimOfSquare_ScaleCoocMin 16 //4 //2,4, ..., 2^n
#define nDimOfSquare_ScaleCoocMin 4 //4 //2,4, ..., 2^n

#define nDimOfSquare_ScaleCoocMax 64 // <= (nGrayLevelIntensityMax/2) !

//const int nDimOfSquare_ScaleCoocArr[nScaleCoocTot] = { nDimOfSquare_ScaleCoocMin, 32, nDimOfSquare_ScaleCoocMax };
const int nDimOfSquare_ScaleCoocArr[nScaleCoocTot] = { nDimOfSquare_ScaleCoocMin, 8, 16, 32, nDimOfSquare_ScaleCoocMax };


////////////////////////////////////////////////////////////////////////////////////////////////
#define nGrayIntensity_ScaleCoocMax (nNumOfGrayLevelIntensities/nDimOfSquare_ScaleCoocMin) // == 256/4 == 64 //16

//(nNumOfDirecAndDistTot*nScaleCoocSizeMax) must be divisible by 16!
#define nScaleCoocSizeMax (nGrayIntensity_ScaleCoocMax * nGrayIntensity_ScaleCoocMax) // 64*64 = 4096
/////////////////////////////////////////////////////////////

//For all cooc features
//nNumOfAnglesTot
#define nNumOfDirectionsTot 4

///////////////////////////////////////////////////////////////////////////////////////
//const int nIndicOfDirectionArr[nNumOfAnglesTot] = { 0,1,2,3,4,5,6,7 };
const int nIndicOfDirectionArr[nNumOfDirectionsTot] = { 0,1,2,3 };

#define nNumOfDistances 5

//check the nDistOfCoocMin == nDistancesArr[0] and nDistOfCoocMax == nDistancesArr[nNumOfDistances - 1];
const int nDistancesArr[nNumOfDistances] = { 10,15,20,25,30 }; //in pixels

//(nNumOfDirecAndDistTot*nScaleCoocSizeMax) must be divisible by 16!
#define nNumOfDirecAndDistTot (nNumOfDirectionsTot * nNumOfDistances)

#define nNumOfScalesAndDistTot (nScaleCoocTot*nNumOfDistances)
// nDimOfSquare_ScaleCoocArr[0] = nDimOfSquare_ScaleCoocMin = 4! nDimOfSquare_ScaleCoocArr[nScaleCoocTot - 1] = nDimOfSquare_ScaleCoocMax

////////////////////////////////////////////////////////////////////////////////////////////
#define nNumOfDirecAndDistAndScaleTot (nNumOfDirecAndDistTot * nScaleCoocTot)
//The actual total number of features is nNumOfDirecAndDistAndScaleTot + nNumOfScalesAndDistTot (the average of directions)

/////////////////////////////////////////////////////
//(nNumOfDirecAndDistTot*nScaleCoocSizeMax) must be divisible by 16!

//nHist_and_AllScales_andCoocSizeMax = nNumOfDirecAndDistTot *(16*16 + 8*8 + 4*4)
//#define nHist_and_AllScales_andCoocSizeMax (  nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistTot*nScaleCoocSizeMax) + (nNumOfDirecAndDistTot*nScaleCoocSizeMax/4) + (nNumOfDirecAndDistTot*nScaleCoocSizeMax/16) )
#define nAllScales_andCoocSizeMax ( nNumOfDirecAndDistTot*( nScaleCoocSizeMax + (nScaleCoocSizeMax/4) + (nScaleCoocSizeMax/16) + (nScaleCoocSizeMax/64) + (nScaleCoocSizeMax/256) ) )

//#define nHist_and_AllScales_andCoocSizeMax ( 4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + (nNumOfDirecAndDistTot*nScaleCoocSizeMax) + (nNumOfDirecAndDistTot*nScaleCoocSizeMax/4) + (nNumOfDirecAndDistTot*nScaleCoocSizeMax/16) )
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define nNumOfFeas_FixedDirecDistCoocScale 10 //fEnergy, fContrast, etc, in total //11

//The average?
#define nNumOfFeasTot (nNumOfDirecAndDistTot * nScaleCoocTot * nNumOfFeas_FixedDirecDistCoocScale) //4 * 5 * 4 * 11 == 880

//The actual total number of features is nNumOfFeasTot + (nNumOfScalesAndDistTot*nNumOfFeas_FixedDirecDistCoocScale) (the average of directions)

//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 10) + (nNumOfScalesAndDistTot*10) )

//the total number of 
#define nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)

//#define nPositionOfCoocFeaToPrint 6879
#define nPositionOfCoocFeaToPrint 6875
#define nPositionOf_TotFeaToPrint (nPositionOfCoocFeaToPrint + nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot) //6875 + 4536 = 11411

///////////////////////////////////////////////////////////
const int nIndicOfDirectionMin = 0;
const int nDistOfCoocMin = 5; // == nDistancesArr[0];

const int nIndicOfDirectionMax = 3;
const int nDistOfCoocMax = 100; // == nDistancesArr[nNumOfDistances - 1];

const int nGrayLevel_HighIntensityThreshold = 190;

///////////////////////////////////////////////////////////////////
//histograms
	//#define HISTOGRAM_EQUALIZATION_APPLIED

#define nIntensityOfBackground 255 // or 0 --check it up

#define nNumOfHistogramIntervals_ForCooc 16
///////////////////////////////////////////////////

//#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255 

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

//#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax
//#define fHueUndefinedValue (-1.0)
//////////////////////////////////////

#define nIntensityOfBackground 254

typedef struct
{
	float
		fWeightOfRed; //<= 1.0
	float
		fWeightOfGreen; //<= 1.0
	float
		fWeightOfBlue; //<= 1.0

} WEIGHTES_OF_RGB_COLORS;

/////////////////////////////////////////////////////////

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

////////////////////////////////////////////////////////////////
typedef struct
{

	int nWidth;
	int nLength;

	int *nEmbeddedImage_Arr; //0-object, 1-background

} EMBEDDED_IMAGE_BLACK_WHITE;
/////////////////////////////////////////////////////////////////////////////////////
//cooc feas
typedef struct
{
	int nIndicOfClass;

	int nLength; // nLenCur; //<= nLenMax
	int nWidth; // nWidCur; //<= nWidMax

	//int nPixelArr[nImageSizeMax];
	int *nPixelArr; //Grayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef)/3;

} GRAYSCALE_IMAGE;

typedef struct
{
	int nIndicOfClass;

	int nIndicOfDirection; // 0-- (-45 degrees from horizontal,
	//1--0 degrees, 2--45 degrees, 3--90 degrees

	int nDistOfCooc; //distance of cooccurrence

	/////////////////////////////////////////////////
	int nLen_IntensityCoocCur; //<= nLenMax
	int nWid_IntensityCoocCur; //<= nWidMax

	/////////////////////////////////////////////////
	//Output
	int nCoocOfOrigImageArr[nCoocSizeMax];
	float fAverOfCoocArr[nCoocSizeMax];

} COOC_OF_ORIG_IMAGE;

typedef struct
{
	//The number of feas is nNumOfFeas_FixedDirecDistCoocScale = 10 (ends on fMaxProb)
	float fEnergy;
	float fContrast;
	float fCorrelation;

	float fHomogeneity;
	float fEntropy;
	float fAutocorrelation;

	float fDissimilarity;
	float fClusterShade;
	float fClusterProminence;

	float fMaxProb;
	//float fMaxProb_At_HighIntensities; // At pixel intensity >= nGrayLevel_HighIntensityThreshold

	int nIntensityLength_MaxProb;
	int nIntensityWidth_MaxProb;

} FEATURES_OF_COOC;


//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 10) + (nNumOfScalesAndDistTot*10) )

typedef struct
{
	float fMeanf;
	float fStDevf;
	float fSkewnessf;
	float fKurtosisf;

	float fPercentsOfHistogramIntervalsArrf[nNumOfHistogramIntervals_ForCooc];

	float fPercentagesAboveThresholds_InHistogramArrf[nNumOfHistogramIntervals_ForCooc - 2]; // [nNumOfHistogramIntervals_ForCooc - 1]
//////////////////////////////////////////////////////////////
	float fAverOf_AllScalesCoocArr[nAllScales_andCoocSizeMax];// [nHist_and_AllScales_andCoocSizeMax]

///////////////////////////////////////////////////////
	float fEnergyArr[nNumOfDirecAndDistAndScaleTot];
	float fContrastArr[nNumOfDirecAndDistAndScaleTot];
	float fCorrelationArr[nNumOfDirecAndDistAndScaleTot];
	float fHomogeneityArr[nNumOfDirecAndDistAndScaleTot];
	float fEntropyArr[nNumOfDirecAndDistAndScaleTot];
	float fAutocorrelationArr[nNumOfDirecAndDistAndScaleTot];
	float fDissimilarityArr[nNumOfDirecAndDistAndScaleTot];

	float fClusterShadeArr[nNumOfDirecAndDistAndScaleTot];

	float fClusterProminenceArr[nNumOfDirecAndDistAndScaleTot];
	float fMaxProbArr[nNumOfDirecAndDistAndScaleTot];

	//float fMaxProb_At_HighIntensitiesArr[nNumOfDirecAndDistAndScaleTot]; // At pixel intensity >= nGrayLevel_HighIntensityThreshold
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//int nIntensityLength_MaxProbArr[nNumOfFeasTot];
	//int nIntensityWidth_MaxProbArr[nNumOfFeasTot];
	float fEnergyAverOverDirecArr[nNumOfScalesAndDistTot];
	float fContrastAverOverDirecArr[nNumOfScalesAndDistTot];

	float fCorrelationAverOverDirecArr[nNumOfScalesAndDistTot];
	float fHomogeneityAverOverDirecArr[nNumOfScalesAndDistTot];

	float fEntropyAverOverDirecArr[nNumOfScalesAndDistTot];
	float fAutocorrelationAverOverDirecArr[nNumOfScalesAndDistTot];

	float fDissimilarityAverOverDirecArr[nNumOfScalesAndDistTot];
	float fClusterShadeAverOverDirecArr[nNumOfScalesAndDistTot];

	float fClusterProminenceAverOverDirecArr[nNumOfScalesAndDistTot];
	float fMaxProbAverOverDirecArr[nNumOfScalesAndDistTot];
	//float fMaxProb_At_HighIntensitiesAverOverDirecArr[nNumOfScalesAndDistTot];

} FEATURES_ALL_OF_COOC;
//
/////////////////////////////////////////////////////////////

//#define nCoocSizeMax (nNumOfGrayLevelIntensities * nNumOfGrayLevelIntensities) == 256*256

//#define nDimOfSquare_ScaleCooc 8

//#define nGrayIntensity_ScaleCoocMax (nNumOfGrayLevelIntensities/nDimOfSquare_ScaleCoocMin) // == 16

//#define nScaleCoocSizeMax (nGrayIntensity_ScaleCoocMax * nGrayIntensity_ScaleCoocMax) == 16*16

typedef struct
{
	int nIndicOfClass;

	int nIndicOfDirection; // 0-- (-45 degrees from horizontal, 
						   //1--0 degrees, 2--45 degrees, 3--90 degrees

	int nDistOfCooc; //distance of cooccurrence

////////////////////////////////////////

	int nDimOfSquare; //== nDimOfSquare_ScaleCooc
					  //int nScale; //1,2,4, ..., 2^n

 /////////////////////////////////////////////////
	int nLen_IntensityScaleCoocCur; //== nGrayIntensity_ScaleCoocMax
	int nWid_IntensityScaleCoocCur; //== nGrayIntensity_ScaleCoocMax

/////////////////////////////////////////////////
						  //Output
	int nNumOfScaleCooc_OrigImage_FeasCurf;

	int nScaleCoocOfOrigImageArr[nScaleCoocSizeMax]; //16*16

	float fAverOfScaleCoocArr[nScaleCoocSizeMax];

} SCALE_COOC_OF_ORIG_IMAGE;



/*

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018

#define MASK_OUTPUT
	//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

//////////////////////////////////////////////////////
#define nLenSubimageToPrintMin  0 //342
#define nLenSubimageToPrintMax  4000  //2500

#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

#define nOneWidToPrint  226 //56 //371 //2476 //
#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
#define nIntensityStatMin 10 //85
#define nIntensityStatMinForEqualization 0 //85

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nNumOfIters_ForDim_2powerN_Max 14

//#define nThresholdForIntensitiesMin 20 //5 //1 //110
#define nThresholdForIntensitiesMin 70 //80 // all colors
//#define nThresholdForIntensitiesMin 100
//#define nThresholdForIntensitiesMin 150

#define nThresholdForIntensitiesMax 255

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

#define nNumOfSquareOccurrence_Intervals 4

#define nLenOfSquareMin 2
#define nNumOfNonZero_ObjectSquaresTotMin 1

#define nNumOf_Qs_Tot 21 //20 // including 0 -- 21
#define nQs_Min (-10)
#define nQs_Max 10

//#define nWidthOfQ_Step ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )
#define nNumOfIntensity_IntervalsForMultifractal 8 // 2^n

#define nNumOfIntensity_IntervalsForMultifractalTot ( ((nNumOfIntensity_IntervalsForMultifractal + 1)*nNumOfIntensity_IntervalsForMultifractal)/2)

#define nNumOfFeasForMultifractalTot (nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot)

#define nNumOfMultifractalFeasFor_OneDimTot (3*nNumOfFeasForMultifractalTot)

//#define nNumOfSelectedFea 1
//#define nNumOfSelectedFea 1100 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//#define nNumOfSelectedFea 1102 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelectedFea 2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
#define nNumOfSelected_SpectrumFea (nNumOfSelectedFea - nNumOfFeasForMultifractalTot)

*/


