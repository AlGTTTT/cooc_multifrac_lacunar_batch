
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

int
nNumOfProcessedFiles_NormalTot_Glob;

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
#include "Cooc_Multifrac_Lacunar_Batch_function.cpp"

int main()
{
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);

	int doMultifractalSpectrumOf_ColorImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,
		COLOR_IMAGE *sColor_Image,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]);

	int doOneFea_OfMultifractal(
		const int nNumOfOneFeaf,

		//const Image& image_in,

		const COLOR_IMAGE *sColor_Image,

		//	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
		float &fOneFeaf);

	int doLacunarityOf_ColorImage(
		//const Image& image_in,

		const COLOR_IMAGE *sColor_Imagef,
		float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

		float &fFractal_Dimension);

	int doOne_Fea_OfLacunarityOf_ColorImage(
		const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

		const Image& image_in,

		const COLOR_IMAGE *sColor_Imagef,
		float &fOne_SelectedLacunar_Feaf); //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax_La*3 + nNumOfLenOfSquareMax_La*nNumOfSquareOccurrence_Intervals_La]

	int doOneCoocFeature(
		const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

		const int nPosit_OfCoocFeaf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot // - 1

		const int nNumOfDirectionsf,
		const int nIndicOfDirectionArrf[],

		const int nNumOfDistancesf,
		const int nDistancesArrf[],

		const int nScaleCoocTotf,
		const int nDimOfSquare_ScaleCoocArrf[],

		//const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

		const COLOR_IMAGE *sColor_Imagef,

		float &fOneFeaf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]
/////////////////////////////////////////////////////

	int doGenerating_CoocFeaturesInOneRow(
		const COLOR_IMAGE *sColor_Imagef,

		FEATURES_ALL_OF_COOC *sFeasAllOfCoocf);

	int Copying_CoocFeaturesAsAVector(
		const int nDimf, // == nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot

		const int nPositInitf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot - 1
		const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

		float fOneDim_Multifrac_Lacunar_CoocArrf[]); //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

	void Initializing_AFloatVec_To_Zero(
		const int nDimf,
		float fVecArrf[]); //[nDimf]

	int ConvertingVecs_ToBest_SeparableFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		float fArr1stf[], //[nDimf*nNumVec1stf] // after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		float fArr2ndf[], //[nDimf*nNumVec2ndf] //after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	///////////////////////////////////

		int &nDimSelecf,

		float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

		float &fRatioBestMinf,

		int &nPosOneFeaBestMaxf,

		int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestMaxf,
		float &fNumArr2ndSeparBestMaxf,

		float fRatioBestArrf[], //nDimSelecMaxf

		int nPosFeaSeparBestArrf[]);	//nDimSelecMaxf

///////////////////////////////////////////////////
	int
		iFea,
		nIndex,

		nIndex_AllVecs,
		nInitFea_ForAllVecs,
		nTempf,

		nDimSelec,
		nNumOfProcessedFiles_NormalTot = 0,
		nNumOfProcessedFiles_MalignantTot = 0,

		nNumOfFeas_ForAll_Normal_VecsTotf,
		nNumOfFeas_ForAll_Malignant_VecsTotf,

		nNumOfInvalid_MultifractalFeasTot = 0,
		nNumOfInvalid_LacunarityFeasTot = 0,

		nPosOneFeaBestMax, //
		nSeparDirectionBestMax,
		nPosFeaSeparBestArr[nDimSelecMax],

		nPosOfSelectedFeaCur,
		//nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot,

		//nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)
		nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot], // 1--valid, 0--invalid

		nPositOfOneSelecCoocFeaInAllFeas = (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot) + nNumOfSelected_CoocFea,
		iVec,
		nRes;

	float
		fFractal_Dimension,
		fFeaCur,
		fNumOfFeas_ForAll_Normal_VecsTotf = (float)(nNumOfVecs_Normal)*(float)(nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot),
		fNumOfFeas_ForAll_Malignant_VecsTotf = (float)(nNumOfVecs_Malignant)*(float)(nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot),

		fAll_SelecFeasForAll_NormalVecs_Arr[nDimSelecMax*nNumOfVecs_Normal], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		fAll_SelecFeasForAll_MalignantVecs_Arr[nDimSelecMax*nNumOfVecs_Malignant], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

		fOne_SelectedLacunar_Fea,

		fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot],
		fOne_Multifractal_SelectedFea,

		fNumOf_NormalVecsSeparBestMax,
		fNumOf_MalignantVecsSeparBestMax,
		fRatioBestMin,

		fOne_SelectedCoocFea,
		fRatioBestArr[nDimSelecMax],

		fOneDim_Multifractal_Arr[nNumOfMultifractalFeasFor_OneDimTot]; //[nNumOfMultifractalFeasFor_OneDimTot]

	clock_t
		tStartMultifractal,
		tEndMultifractal,

		tStartLacunarity,
		tEndLacunarity,

		tStartCooc,
		tEndCooc;

	double
		dCPU_Time_Multifractal,
		dCPU_Time_Lacunarity,
		dCPU_Time_Cooc;
	/////////////////////////////////////////////////
		//'sColor_Image' is read in 'doMultifractalSpectrumOf_ColorImage'
	//////////////////////////////////////////////////////

		//fout_PrintFeatures = fopen("wVecsOfFeas_397Norm_76Malig.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_3TheSameTrainNormal.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_Test.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_337Norm_84Malig_HighFreq_train.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeas_Victors_5TestImages.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeas_Annass_5TestImages.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeas_337Norm_84Malig_HighFreq_test.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_Fea_11415.txt", "w");
	//	fout_PrintFeatures = fopen("wVecsOfFeas_Fea_11411_6875.txt", "w");
	////////////////////////////////////////////////////////////////////
		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Normal313_Malignant576.txt", "w");
		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Benign50_Malignant50_NoHistEqual.txt", "w");

		//fout_PrintFeatures = fopen("wVecsOfFeasSpleen_Benign472_Malignant575.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Ben50_Mal50_NoHistEqual.txt", "w");
		//fout_PrintFeatures = fopen("wVecsSpleen_Ben50_Mal50_WithHistEqual.txt", "w");

	//fout_PrintFeatures = fopen("wVecsSpleen_CustomProces_Ben472_Mal575.txt.txt", "w");
	//fout_PrintFeatures = fopen("wVecsSpleen_HighFreqProces_Ben472_Mal575.txt.txt", "w");
	fout_PrintFeatures = fopen("wVecsSpleen_2RawTheSame.txt", "w");

	if (fout_PrintFeatures == NULL)
	{
		printf("\n\n fout_PrintFeatures == NULL");
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_PrintFeatures == NULL)

	//fOneDim_Multifrac_Lacunar_CoocArr[nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot];
		////////////////////////////////////////////////////////////////////////////////////////////////
	//printf("\n\n INT_MAX = %ld", INT_MAX); getchar();

	if (fNumOfFeas_ForAll_Normal_VecsTotf >= (float)(INT_MAX))
	{
		printf("\n\n An error: fNumOfFeas_ForAll_Normal_VecsTotf = %E >= (float)(INT_MAX) = %E", fNumOfFeas_ForAll_Normal_VecsTotf, (float)(INT_MAX));
		printf("\n\n Please decrease the number of feas and/or vecs");
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error: fNumOfFeas_ForAll_Normal_VecsTotf = %E >= (float)(INT_MAX) = %E", fNumOfFeas_ForAll_Normal_VecsTotf, (float)(INT_MAX));
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fNumOfFeas_ForAll_Normal_VecsTotf >= (float)(INT_MAX) )

//#define nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)

	nNumOfFeas_ForAll_Normal_VecsTotf = nNumOfVecs_Normal * (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);
	/////////////////////////////////////

	if (fNumOfFeas_ForAll_Malignant_VecsTotf >= (float)(INT_MAX))
	{
		printf("\n\n An error: fNumOfFeas_ForAll_Malignant_VecsTotf = %E >= (float)(INT_MAX) = %E", fNumOfFeas_ForAll_Malignant_VecsTotf, (float)(INT_MAX));
		printf("\n\n Please decrease the number of feas and/or vecs");
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		(fout_lr, "\n\n An error: fNumOfFeas_ForAll_Malignant_VecsTotf = %E >= (float)(INT_MAX) = %E", fNumOfFeas_ForAll_Malignant_VecsTotf, (INT_MAX));
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fNumOfFeas_ForAll_Malignant_VecsTotf >= (float)(INT_MAX) )

	//////////////////////////////////////////////////////////////////////////////////
	//nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)
	nNumOfFeas_ForAll_Malignant_VecsTotf = nNumOfVecs_Malignant * (nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	if (nPositOfOneSelecCoocFeaInAllFeas >= nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot) // == nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot
	{
		printf("\n\n An error in 'main': nPositOfOneSelecCoocFeaInAllFeas = %d >= nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d", nPositOfOneSelecCoocFeaInAllFeas, nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error: fNumOfFeas_ForAll_Malignant_VecsTotf = %E >= (float)(INT_MAX) = %E", fNumOfFeas_ForAll_Malignant_VecsTotf, (INT_MAX));
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nPositOfOneSelecCoocFeaInAllFeas >= nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot + nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)

	printf("\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d",
		nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	printf("\n\n nNumOfDirecAndDistTot = %d, nNumOfScalesAndDistTot = %d, nScaleCoocSizeMax = %d, nNumOfDirecAndDistAndScaleTot = %d, nAllScales_andCoocSizeMax = %d",
		nNumOfDirecAndDistTot, nNumOfScalesAndDistTot, nScaleCoocSizeMax, nNumOfDirecAndDistAndScaleTot, nAllScales_andCoocSizeMax);

	printf("\n\n The total # of feas: nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d", nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);


	fprintf(fout_PrintFeatures, "\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d",
		nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	fprintf(fout_PrintFeatures, "\n\n nNumOfDirecAndDistTot = %d, nNumOfScalesAndDistTot = %d, nScaleCoocSizeMax = %d, nNumOfDirecAndDistAndScaleTot = %d, nAllScales_andCoocSizeMax = %d",
		nNumOfDirecAndDistTot, nNumOfScalesAndDistTot, nScaleCoocSizeMax, nNumOfDirecAndDistAndScaleTot, nAllScales_andCoocSizeMax);

	fprintf(fout_PrintFeatures, "\n\n The total # of feas: nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d", nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);
	printf("\n\n Please press any key to continue"); 	fflush(fout_PrintFeatures);	getchar();

	///////////////////////////////////////////////////////////////////////////////////////
	Image image_in;

	char cFileName_iinDirectoryOf_Normal_Images[150];
	char cFileName_iinDirectoryOf_Malignant_Images[150];
	//image_in.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIR;
	struct dirent *entry;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//char cInput_DirectoryOf_Normal_Images[150] = "C:\\Images_WoodyBreast_Normal\\"; //copy to if (pDIR = opendir(.. as well
	//test
		//5
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\VictorsTestImage\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\AnnasTestImage\\"; //copy to if (pDIR = opendir(.. as well


	//60 vecs
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\test\\normal\\"; //copy to if (pDIR = opendir(.. as well
	//15 vecs
		//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\test\\malignant\\"; //copy to if (pDIR = opendir(.. as well

	//60 vecs
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\test\\normal\\"; //copy to if (pDIR = opendir(.. as well
	//15 vecs
		//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\test\\malignant\\"; //copy to if (pDIR = opendir(.. as well


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//train
			//20 vecs only -- change 'nNumOfVecs_Normal' 
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Normals_TestSet\\"; //copy to if (pDIR = opendir(.. as well

	//3 vecs only -- change 'nNumOfVecs_Normal' 
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\3CopiesOfOneNormal\\";

		//337 
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\train\\normal\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\"; //copy to if (pDIR = opendir(.. as well

		//397 vecs  -- change 'nNumOfVecs_Normal' 
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Normal_Tot\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\normal_processed\\custom\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\normal_processed\\Relief1\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\normal_processed\\Relief2\\"; //copy to if (pDIR = opendir(.. as well
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\normal_processed\\HighFrequency\\"; //copy to if (pDIR = opendir(.. as well
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//check out '#define nNumOfVecs_Normal'!

		//313
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Normal\\AOI\\"; //copy to if (pDIR = opendir(.. as well

		//472 //477; benign as normal
		//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\"; //copy to if (pDIR = opendir(.. as well

		//473 //477; benign as normal //D:\Imago\Images\spleen\Genas_Images_June26\2_Processes_AOI_Mal_Ben\Custom\Benign_Custom
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Benign_Custom\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\HighFrequency\\Benign_HighFrequency\\"; //copy to if (pDIR = opendir(.. as well

 //check out '#define nNumOfVecs_Normal'!
//50
//	char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"; //copy to if (pDIR = opendir(.. as well

	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_2_Test\\"; //copy to if (pDIR = opendir(.. as well
//2 the same
//D:\Imago\Images\spleen\Genas_Images_June26\Benign\AoI_1_Test
	char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_1_Test\\"; //copy to if (pDIR = opendir(.. as well

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//malignant

	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\normal_processed\\HighFrequency_inv\\"; //copy to if (pDIR = opendir(.. as well

//84 --D:\Imago\Images\spleen\Anna_March11\RawImages\train\malignant
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\train\\malignant\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\malignant\\"; //copy to if (pDIR = opendir(.. as well

//76 vecs
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Malignant_Tot\\"; //copy to if (pDIR = opendir(.. as well

//	char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\abnormal_processed\\custom\\"; //copy to if (pDIR = opendir(.. as well
//	char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\abnormal_processed\\Relief1\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\abnormal_processed\\Relief2\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\abnormal_processed\\HighFrequency\\"; //copy to if (pDIR = opendir(.. as well

	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\abnormal_processed\\HighFrequency_inv\\"; //copy to if (pDIR = opendir(.. as well

	//10 vecs only -- change 'nNumOfVecs_Normal' 
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Normal_Tot_Small\\"; //copy to if (pDIR = opendir(.. as well
	//20 vecs only
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Malignant_Tot_Small\\"; //copy to if (pDIR = opendir(.. as well


	//3 for testing  -- change 'nNumOfVecs_Normal' 
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\VictorsWholeImages\\"; //copy to if (pDIR = opendir(.. as well

	//20 vecs only  -- change 'nNumOfVecs_Malignant' 
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Malignants_TestSet\\"; //copy to if (pDIR = opendir(.. as well

	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Annas_ProblemImages\\"; //copy to if (pDIR = opendir(.. as well
	//char cInput_DirectoryOf_Normal_Images[150] = "D:\\Imago\\Images\\spleen\\Annas_ProblemImages\\"; //copy to if (pDIR = opendir(.. as well
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//check out '#define nNumOfVecs_Malignant'!
//575 //576
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\"; //copy to if (pDIR = opendir(.. as well

	//D:\Imago\Images\spleen\Genas_Images_June26\2_Processes_AOI_Mal_Ben\Custom\Malignant_Custom
//	char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Malignant_Custom\\"; //copy to if (pDIR = opendir(.. as well
	
	char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\HighFrequency\\Malignant_HighFrequency\\"; //copy to if (pDIR = opendir(.. as well
//50
	//char cInput_DirectoryOf_Malignant_Images[150] = "D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI_50_Selec\\"; //copy to if (pDIR = opendir(.. as well
//////////////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //
		//'sColor_Image' is read in 'doMultifractalSpectrumOf_ColorImage'

	FEATURES_ALL_OF_COOC sFeasAllOfCooc;
	/////////////////////////////////////////////////////
	float *fOneDim_Multifrac_Lacunar_CoocArr;    //nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot
	fOneDim_Multifrac_Lacunar_CoocArr = new float[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot];

	if (fOneDim_Multifrac_Lacunar_CoocArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'main'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Please press any key to exit"); getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} //if (fOneDim_Multifrac_Lacunar_CoocArr == nullptr)
/////////////////////////////////////////////////////////////////////////
	float *fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
	fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr = new float[nNumOfFeas_ForAll_Normal_VecsTotf];

	if (fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr' in 'main'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr == nullptr)

////////////////////////////////////////////////////////////////////////////
/*
	if (nNumOfSelectedFea < 0 || nNumOfSelectedFea >= nNumOfMultifractalFeasFor_OneDimTot)
	{
		printf("\n\n Please specify the feature number correctly: nNumOfSelectedFea = %d, nNumOfMultifractalFeasFor_OneDimTot = %d",
			nNumOfSelectedFea, nNumOfMultifractalFeasFor_OneDimTot);

		delete[] fOneDim_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		printf("\n\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN; //
	} //if (nNumOfSelectedFea < 0 || nNumOfSelectedFea >= nNumOfMultifractalFeasFor_OneDimTot)
*/

/////////////////////////////////////////////////////////////////////////////////
//Normal 
	//////////////////////////////////////////////////////////////
//test
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\VictorsTestImage\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\AnnasTestImage\\"))
			//60 vecs
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\test\\normal\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\test\\normal\\"))

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//train
	//337
//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\train\\normal\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\"))


	//20 vecs  -- change 'nNumOfVecs_Normal' 
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Normals_TestSet\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\normal\\3CopiesOfOneNormal\\"))
	//397  -- change 'nNumOfVecs_Normal' 
//	if (pDIR = opendir("D:\\Imago\\Images\\spleen\\normal_processed\\Custom\\"))
//	if (pDIR = opendir("D:\\Imago\\Images\\spleen\\normal_processed\\Relief1\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\normal_processed\\Relief2\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\normal_processed\\HighFrequency\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\normal_processed\\HighFrequency_inv\\"))

		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\VictorsWholeImages\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Annas_ProblemImages\\"))
			//10 vecs only
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Normal_Tot_Small\\"))
	///////////////////////////////////////////////////////////////////////////

//check out '#define nNumOfVecs_Normal'!
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI_50_Selec\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_2_Test\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Benign_Custom\\"))
		//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\HighFrequency\\Benign_HighFrequency\\"))
		if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AoI_1_Test\\"))
		{
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_NormalTot += 1;

				nNumOfProcessedFiles_NormalTot_Glob = nNumOfProcessedFiles_NormalTot;

				if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					printf("\n\n Please press any key to exit"); getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)

				Initializing_AFloatVec_To_Zero(
					nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, //const int nDimf,
					fOneDim_Multifrac_Lacunar_CoocArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Normal_Images, '\0', sizeof(cFileName_iinDirectoryOf_Normal_Images));

				strcpy(cFileName_iinDirectoryOf_Normal_Images, cInput_DirectoryOf_Normal_Images);

				strcat(cFileName_iinDirectoryOf_Normal_Images, entry->d_name);

				printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
				fprintf(fout_PrintFeatures, "\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Normal_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();

				image_in.read(cFileName_iinDirectoryOf_Normal_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n (normal) nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d",
					nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

				printf("\n nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d, nNumOfProcessedFiles_NormalTot = %d", nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, nNumOfProcessedFiles_NormalTot);

				fprintf(fout_PrintFeatures, "\n\n (normal) nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d",
					nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

				fprintf(fout_PrintFeatures, "\n nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d, nNumOfProcessedFiles_NormalTot = %d", nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, nNumOfProcessedFiles_NormalTot);
				//printf("\n\n The sum nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot = %d", nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot);

				//nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot
				//printf("\n\n Press any key to ontinue");	getchar();
			//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
				{
					nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[iFea] = 1; //all valid initially
				}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

		//'sColor_Image' is read in 'doMultifractalSpectrumOf_ColorImage'
				tStartMultifractal = clock();
				nRes = doMultifractalSpectrumOf_ColorImage(
					image_in, // Image& image_in,

					&sColor_Image, //COLOR_IMAGE *sColor_Image,
					fOneDim_Multifractal_Arr); // 

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					//#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error: The image has not been processed properly by 'doMultifractalSpectrumOf_ColorImage' (normal), nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);

					//fprintf(fout_lr,"\n\n The image has not been processed properlyby 'doMultifractalSpectrumOf_ColorImage', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
					//fflush(fout_lr); //debugging;
			//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit"); getchar(); exit(1);
					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				tEndMultifractal = clock();

				dCPU_Time_Multifractal = ((double)(tEndMultifractal - tStartMultifractal)) / CLOCKS_PER_SEC;
				printf("\n\n /////////////// dCPU_Time_Multifractal = %E", dCPU_Time_Multifractal);
				//fprintf(fout_PrintFeatures, "\n\n /////////////// dCPU_Time_Multifractal = %E", dCPU_Time_Multifractal);

				//printf("\n\n After 'doMultifractalSpectrumOf_ColorImage': please press any key to continue"); fflush(fout_PrintFeatures);  getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d", nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot);
				fprintf(fout_lr, "\n nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);
				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////

//writing to 'fOneDim_Multifrac_Lacunar_CoocArr[]' // not fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[]
				nInitFea_ForAllVecs = 0;
				for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)
				{
					nIndex_AllVecs = (nInitFea_ForAllVecs + iFea) + ((nNumOfProcessedFiles_NormalTot - 1)*nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);

					if (nIndex_AllVecs >= nNumOfFeas_ForAll_Normal_VecsTotf)
					{
						printf("\n\n An error: Normal (multifractal): nIndex_AllVecs = %d > nNumOfFeas_ForAll_Normal_VecsTotf = %d, iFea = %d, nNumOfProcessedFiles_NormalTot = %d",
							nIndex_AllVecs, nNumOfFeas_ForAll_Normal_VecsTotf, iFea, nNumOfProcessedFiles_NormalTot);

						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex_AllVecs >= nNumOfFeas_ForAll_Normal_VecsTotf)

					fOneDim_Multifrac_Lacunar_CoocArr[iFea] = fOneDim_Multifractal_Arr[iFea];
					//	fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex_AllVecs] = fOneDim_Multifractal_Arr[iFea];

				}//for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)

			//////////////////////////////////////////////

				nNumOfSpectrumFea_Glob = -1;
				/*		//printf("\n\n Before 'doOneFea_OfMultifractal': please press any key"); getchar();
						printf("\n\n Before 'doOneFea_OfMultifractal' (normal):");


						nRes = doOneFea_OfMultifractal(
							nNumOfSelectedFea, //const int nNumOfOneFeaf,

							//image_in, //const Image& image_in,

							&sColor_Image, //const COLOR_IMAGE *sColor_Image,

							fOne_Multifractal_SelectedFea); // float &fOneFeaf);

						if (nRes == UNSUCCESSFUL_RETURN) // -1
						{
							printf("\n\n An error: (normal) The image has not been processed properly by 'doOneFea_OfMultifractal', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
							delete[] fOneDim_Multifrac_Lacunar_CoocArr;
							delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

							printf("\n\n Please press any key to exit");	getchar(); exit(1);
					#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
					#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							return UNSUCCESSFUL_RETURN; //
						} // if (nRes == UNSUCCESSFUL_RETURN)

						if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
						{
							printf("\n\n nNumOfSelectedFea = %d, fOne_Multifractal_SelectedFea = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E",
								nNumOfSelectedFea, fOne_Multifractal_SelectedFea, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1],
								nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
								nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);

					#ifndef COMMENT_OUT_ALL_PRINTS

							fprintf(fout_lr, "\n\n (normal) nNumOfSelectedFea = %d, fOne_Multifractal_SelectedFea = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E",
								nNumOfSelectedFea, fOne_Multifractal_SelectedFea, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1],
								nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
								nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);
							//fflush(fout_lr); //debugging;
					#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						} //if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
				*/

				for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)
				{
					if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
					{
						nNumOfInvalid_MultifractalFeasTot += 1;
						nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[iFea] = 0; // invalid 
					} //if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
				}//for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nNumOfInvalid_MultifractalFeasTot = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nNumOfInvalid_MultifractalFeasTot, nNumOfMultifractalFeasFor_OneDimTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n (normal) nNumOfInvalid_MultifractalFeasTot = %d, nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfProcessedFiles_NormalTot = %d",
					nNumOfInvalid_MultifractalFeasTot, nNumOfMultifractalFeasFor_OneDimTot, nNumOfProcessedFiles_NormalTot);

				//printf("\n\n The end of 'doOneFea_OfMultifractal': please press any key to proceed to 'doLacunarityOf_ColorImage'");  getchar(); //exit(1);
				printf("\n\n The end of 'doOneFea_OfMultifractal' (normal):");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				////////////////////////
				//	printf("\n\n Before 'doLacunarityOf_ColorImage' (normal): please press any key to continue"); fflush(fout_PrintFeatures);  getchar();

				tStartLacunarity = clock();

				nRes = doLacunarityOf_ColorImage(
					//image_in, // Image& image_in,
					&sColor_Image, //COLOR_IMAGE *sColor_Image,

					fOneDim_Lacunar_Arr, //[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
					fFractal_Dimension); // 

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n (normal) The image has not been processed properly by 'doLacunarityOf_ColorImage', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);

					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doLacunarityOf_ColorImage'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				tEndLacunarity = clock();

				dCPU_Time_Lacunarity = ((double)(tEndLacunarity - tStartLacunarity)) / CLOCKS_PER_SEC;
				printf("\n\n /////////////// dCPU_Time_Lacunarity = %E", dCPU_Time_Lacunarity);
			//	fprintf(fout_PrintFeatures, "\n\n /////////////// dCPU_Time_Lacunarity = %E", dCPU_Time_Lacunarity);

				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					if (fOneDim_Lacunar_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Lacunar_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
					{
						nNumOfInvalid_LacunarityFeasTot += 1;
						nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[nNumOfMultifractalFeasFor_OneDimTot + iFea] = 0; // invalid 
					} //if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)

				printf("\n\n After 'doLacunarityOf_ColorImage' (normal): fFractal_Dimension = %E, nNumOfInvalid_LacunarityFeasTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d",
					fFractal_Dimension, nNumOfInvalid_LacunarityFeasTot, nNumOfLacunarFeasFor_OneDimTot);

				//printf("\n\n After 'doLacunarityOf_ColorImage' (normal): please press any key to continue"); fflush(fout_PrintFeatures);  getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage': fFractal_Dimension = %E, nNumOfInvalid_LacunarityFeasTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d",
					fFractal_Dimension, nNumOfInvalid_LacunarityFeasTot, nNumOfLacunarFeasFor_OneDimTot);

				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					fprintf(fout_lr, "\n fOneDim_Lacunar_Arr[%d] = %E", iFea, fOneDim_Lacunar_Arr[iFea]);
				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)

				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////

//writing to fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[]
				nInitFea_ForAllVecs = nNumOfMultifractalFeasFor_OneDimTot;

				//fprintf(fout_PrintFeatures, "\n\n Lacunarity features: nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfSelectedLacunarityFea = %d\n",
					//nNumOfLacunarFeasFor_OneDimTot, nNumOfSelectedLacunarityFea);

				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					nIndex_AllVecs = (nInitFea_ForAllVecs + iFea) + ((nNumOfProcessedFiles_NormalTot - 1)*nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);

					if (nIndex_AllVecs >= nNumOfFeas_ForAll_Normal_VecsTotf)
					{
						printf("\n\n Normal (lacunarity): nIndex_AllVecs = %d > nNumOfFeas_ForAll_Normal_VecsTotf = %d, iFea = %d, nNumOfProcessedFiles_NormalTot = %d",
							nIndex_AllVecs, nNumOfFeas_ForAll_Normal_VecsTotf, iFea, nNumOfProcessedFiles_NormalTot);

						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex_AllVecs >= nNumOfFeas_ForAll_Normal_VecsTotf)

					fFeaCur = fOneDim_Lacunar_Arr[iFea];
					//fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

					fOneDim_Multifrac_Lacunar_CoocArr[nNumOfMultifractalFeasFor_OneDimTot + iFea] = fOneDim_Lacunar_Arr[iFea];

					//fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex_AllVecs] = fOneDim_Lacunar_Arr[iFea];

				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
			//////////////////////////////////////////////

				fflush(fout_PrintFeatures);

				printf("\n\n Before 'doOne_Fea_OfLacunarityOf_ColorImage' (normal): nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);
				//printf("\n Please press any key to proceed to 'doOne_Fea_OfLacunarityOf_ColorImage'"); fflush(fout_lr); getchar(); //exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Before 'doLacunarityOf_ColorImage':   nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);
				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
/*
		nRes = doOne_Fea_OfLacunarityOf_ColorImage(
			nNumOfSelectedLacunarityFea, //const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

			image_in, //const Image& image_in,

			&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

			//float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
			fOne_SelectedLacunar_Fea); // float &fOne_SelectedLacunar_Feaf); //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

		if (nRes == UNSUCCESSFUL_RETURN) // -1
		{
			printf("\n\n The image has not been processed properly by 'doOne_Fea_OfLacunarityOf_ColorImage' (normal), nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
			delete[] fOneDim_Multifrac_Lacunar_CoocArr;
			delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

			printf("\n\n Please press any key to exit");	getchar(); exit(1);

	#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOne_Fea_OfLacunarityOf_ColorImage'.");		fflush(fout_lr); //debugging;
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN; //
		} // if (nRes == UNSUCCESSFUL_RETURN)

		printf("\n\n After 'doOne_Fea_OfLacunarityOf_ColorImage' (normal): fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E",
			fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea, fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);

		printf( "\n\n nNumOfLacunarFeasFor_OneDimTot = %d, the number of valid lacunarity features: %d", nNumOfLacunarFeasFor_OneDimTot,nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

	#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage':  fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E",
			fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea, fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);

		fprintf(fout_lr,"\n\n The number of valid multifractal features: %d", nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot);

		fprintf(fout_lr,"\n The number of valid lacunarity features: %d", nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

		fprintf(fout_lr,"\n\n The total number of valid multifractal and lacunarity features: %d",
			nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot + nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

		fflush(fout_lr); //;
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//image_out.write("01-144_LCC_mask_NoLabels.png");
	//#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n The end of 'doLacunarityOf_ColorImage': please press any key to exit"); getchar(); exit(1);
		//printf("\n\n The end of 'doLacunarityOf_ColorImage' (normal); continuing to 'doGenerating_CoocFeaturesInOneRow' ");
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	*/
	////////////////////////////////////////////////////////////////////////////

				printf("\n\n The number of valid multifractal features (normal): %d", nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot);

				printf("\n The number of valid lacunarity features: %d", nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

				printf("\n\n The total number of valid multifractal and lacunarity features is %d, nNumOfProcessedFiles_NormalTot = %d",
					nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot + nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot, nNumOfProcessedFiles_NormalTot);

				if (nPositionOfCoocFeaToPrint > nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot - 1)
				{
					printf("\n\n An error: nPositionOfCoocFeaToPrint = %d > nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot - 1 = %d, nNumOfProcessedFiles_NormalTot = %d",
						nPositionOfCoocFeaToPrint, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot - 1, nNumOfProcessedFiles_NormalTot);

					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

					//return UNSUCCESSFUL_RETURN; //

				} //if (nPositionOfCoocFeaToPrint > nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot)

				tStartCooc = clock();

				nRes = doGenerating_CoocFeaturesInOneRow(
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					&sFeasAllOfCooc); // the total of feas is nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'doGenerating_CoocFeaturesInOneRow', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doGenerating_CoocFeaturesInOneRow'.");	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				tEndCooc = clock();

				dCPU_Time_Cooc = ((double)(tEndCooc - tStartCooc)) / CLOCKS_PER_SEC;
				printf("\n\n /////////////// dCPU_Time_Cooc = %E", dCPU_Time_Cooc);
				//fprintf(fout_PrintFeatures, "\n\n /////////////// dCPU_Time_Cooc = %E", dCPU_Time_Cooc);

				//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n The end of 'doGenerating_CoocFeaturesInOneRow', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = Copying_CoocFeaturesAsAVector(
					nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, //const int nDimf, // == nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot

					nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot, //const int nPositInitf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot
					&sFeasAllOfCooc, //const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

					fOneDim_Multifrac_Lacunar_CoocArr); // float fOneDim_Multifrac_Lacunar_CoocArrf[]); //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'Copying_CoocFeaturesAsAVector'.");	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				//fprintf(fout_PrintFeatures, "\n\n Normal (orig) -- 1st iter:  nNumOfProcessedFiles_NormalTot = %d, nAllScales_andCoocSizeMax = %d\n", nNumOfProcessedFiles_NormalTot, nAllScales_andCoocSizeMax);

				//fprintf(fout_PrintFeatures, "\n 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2 = %d\n\n", 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2));
				printf("\n 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2 = %d\n\n", 4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2));

				for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
				{
					//nIndex = iFea + nTempf;
					fFeaCur = fOneDim_Multifrac_Lacunar_CoocArr[iFea];
					fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

				}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

		///////////////////////
				nRes = doOneCoocFeature(
					nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot, //const int nDimCoocf, // == nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

					nNumOfSelected_CoocFea, //const int nPosit_OfCoocFeaf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot // - 1

					nNumOfDirectionsTot, //const int nNumOfDirectionsf,
					nIndicOfDirectionArr, //const int nIndicOfDirectionArrf[],

					nNumOfDistances, //const int nNumOfDistancesf,
					nDistancesArr, //const int nDistancesArrf[],

					nScaleCoocTot, //const int nScaleCoocTotf,
					nDimOfSquare_ScaleCoocArr, //const int nDimOfSquare_ScaleCoocArrf[],

					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					fOne_SelectedCoocFea); // float &fOneFeaf); //fOneDim_Multifrac_Lacunar_CoocArrf[]) //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'doOneCoocFeature' (benign), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					//delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneCoocFeature' (benign),");	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				printf("\n\n After 'doOneCoocFeature':  nNumOfSelected_CoocFea = %d, fOne_SelectedCoocFea = %E, fOneDim_Multifrac_Lacunar_CoocArr[%d] = %E",
					nNumOfSelected_CoocFea, fOne_SelectedCoocFea, nPositOfOneSelecCoocFeaInAllFeas, fOneDim_Multifrac_Lacunar_CoocArr[nPositOfOneSelecCoocFeaInAllFeas]);

				printf("\n\n nNumOfSelected_CoocFea = %d, nPosEnergyMinf = (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax) = %d",
					nNumOfSelected_CoocFea, (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax));


				//fprintf(fout_PrintFeatures, "\n\n After 'doOneCoocFeature':  nNumOfSelected_CoocFea = %d, fOne_SelectedCoocFea = %E, fOneDim_Multifrac_Lacunar_CoocArr[%d] = %E",
					//nNumOfSelected_CoocFea, fOne_SelectedCoocFea, nPositOfOneSelecCoocFeaInAllFeas, fOneDim_Multifrac_Lacunar_CoocArr[nPositOfOneSelecCoocFeaInAllFeas]);

				//fprintf(fout_PrintFeatures, "\n\n nNumOfSelected_CoocFea = %d, nPosEnergyMinf = (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax) = %d",
					//nNumOfSelected_CoocFea, (4 + nNumOfHistogramIntervals_ForCooc + (nNumOfHistogramIntervals_ForCooc - 2) + nAllScales_andCoocSizeMax));

				//printf("\n\n Please press any key to continue");	fflush(fout_PrintFeatures);  getchar(); 
				//printf("\n\n Please press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

		//////////////////////////////////////////////
				nTempf = (nNumOfProcessedFiles_NormalTot - 1)* nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
				for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
				{
					nIndex = iFea + nTempf;
					if (nIndex < 0 || nIndex >= nNumOfFeas_ForAll_Normal_VecsTotf)
					{
						printf("\n\n An error in 'main' (normal): nIndex = %d >= nNumOfFeas_ForAll_Normal_VecsTotf = %d", nIndex, nNumOfFeas_ForAll_Normal_VecsTotf);
						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'main': nIndex = %d >= nNumOfFeas_ForAll_Normal_VecsTotf = %d", nIndex, nNumOfFeas_ForAll_Normal_VecsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex < 0 || nIndex >= nNumOfFeas_ForAll_Normal_VecsTotf)

		//		fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr = new float[nNumOfFeas_ForAll_Normal_VecsTotf];

					fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex] = fOneDim_Multifrac_Lacunar_CoocArr[iFea];
				} //for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

		////////////////////////////////////////////////
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				printf("\n\n Normal: the end of nNumOfProcessedFiles_NormalTot = %d, nNumOfVecs_Normal = %d; going to to the next image", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("C:\\Images_WoodyBreast_Normal\\") //C:\\Malignant_cases\\"))

	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of normal images can not be opened");

		delete[] fOneDim_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

		printf("\n\n Press any key to exit");	getchar(); exit(1);

		perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Original feas: normal images");
	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Normal (orig):  iVec = %d, ", iVec);
		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;

		for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

printf("\n\n The end of normal inages: press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);
	//printf("\n\n The end of normal images: press any key to continue to the malignant ones");	fflush(fout_PrintFeatures);  getchar();

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Malignant 

	float *fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;
	fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr = new float[nNumOfFeas_ForAll_Malignant_VecsTotf];

	if (fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit");	getchar(); exit(1);

		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		return UNSUCCESSFUL_RETURN;
	} //if (fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr == nullptr)
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Malignant

	//test
	//15 vecs
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\test\\malignant\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\test\\malignant\\"))

		//////////////////////////////////////////////////////////////
	//train
	//20 vecs  -- change 'nNumOfVecs_Malignant' 
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignants_TestSet\\"))

	//84
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\RawImages\\train\\malignant\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Anna_March11\\HighFrequency\\train\\malignant\\"))
	//76
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\abnormal_processed\\Custom\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\abnormal_processed\\Relief1\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\abnormal_processed\\Relief2\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\abnormal_processed\\HighFrequency\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\abnormal_processed\\HighFrequency_inv\\"))

	//10 vecs only
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignant_Tot_Small\\"))

	//check out '#define nNumOfVecs_Malignant'!
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI_50_Selec\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\Custom\\Malignant_Custom\\"))
		if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Genas_Images_June26\\2_Processes_AOI_Mal_Ben\\HighFrequency\\Malignant_HighFrequency\\"))
		{
		nNumOfProcessedFiles_MalignantTot = 0;
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_MalignantTot += 1;

				if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)

				Initializing_AFloatVec_To_Zero(
					nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, //const int nDimf,
					fOneDim_Multifrac_Lacunar_CoocArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Malignant_Images, '\0', sizeof(cFileName_iinDirectoryOf_Malignant_Images));

				strcpy(cFileName_iinDirectoryOf_Malignant_Images, cInput_DirectoryOf_Malignant_Images);

				strcat(cFileName_iinDirectoryOf_Malignant_Images, entry->d_name);

				printf("\n Concatenated input file: %s, nNumOfProcessedFiles_MalignantTot = %d\n", cFileName_iinDirectoryOf_Malignant_Images, nNumOfProcessedFiles_MalignantTot);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Malignant_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();

				image_in.read(cFileName_iinDirectoryOf_Malignant_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d", nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot);
				printf("\n nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d, nNumOfProcessedFiles_MalignantTot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot, nNumOfProcessedFiles_MalignantTot);

				//printf("\n\n Press any key to ontinue");	getchar();
			//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
				{
					nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[iFea] = 1; //all valid initially
				}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

				nRes = doMultifractalSpectrumOf_ColorImage(
					image_in, // Image& image_in,

					&sColor_Image, //COLOR_IMAGE *sColor_Image,
					fOneDim_Multifractal_Arr); // 

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					//#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error: the image has not been processed properly by 'doMultifractalSpectrumOf_ColorImage', nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);

					//fprintf(fout_lr,"\n\n The image has not been processed properlyby 'doMultifractalSpectrumOf_ColorImage', nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
					//fflush(fout_lr); //debugging;
			//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d", nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot);
				fprintf(fout_lr, "\n nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);
				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//writing to fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[]
				nInitFea_ForAllVecs = 0;
				for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)
				{
					nIndex_AllVecs = (nInitFea_ForAllVecs + iFea) + ((nNumOfProcessedFiles_MalignantTot - 1)*nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);

					if (nIndex_AllVecs >= nNumOfFeas_ForAll_Malignant_VecsTotf)
					{
						printf("\n\n An error: malignant (multifractal): nIndex_AllVecs = %d > nNumOfFeas_ForAll_Malignant_VecsTotf = %d, iFea = %d, nNumOfProcessedFiles_MalignantTot = %d",
							nIndex_AllVecs, nNumOfFeas_ForAll_Malignant_VecsTotf, iFea, nNumOfProcessedFiles_MalignantTot);

						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex_AllVecs >= nNumOfFeas_ForAll_Malignant_VecsTotf)

					fOneDim_Multifrac_Lacunar_CoocArr[iFea] = fOneDim_Multifractal_Arr[iFea];
					//	fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[nIndex_AllVecs] = fOneDim_Multifractal_Arr[iFea];

				}//for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)

//////////////////////////////////////////////

				nNumOfSpectrumFea_Glob = -1;
				/*
								//printf("\n\n Before 'doOneFea_OfMultifractal': please press any key"); getchar();
								printf("\n\n Before 'doOneFea_OfMultifractal' (malignant):");

								nRes = doOneFea_OfMultifractal(
									nNumOfSelectedFea, //const int nNumOfOneFeaf,

									//image_in, //const Image& image_in,

									&sColor_Image, //const COLOR_IMAGE *sColor_Image,

									fOne_Multifractal_SelectedFea); // float &fOneFeaf);

								if (nRes == UNSUCCESSFUL_RETURN) // -1
								{
									printf("\n\n An error: the image has not been processed properly by 'doOneFea_OfMultifractal' (malignant), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
									delete[] fOneDim_Multifrac_Lacunar_CoocArr;
									delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
									delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

									printf("\n\n Please press any key to exit");	getchar(); exit(1);
				#ifndef COMMENT_OUT_ALL_PRINTS
									fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
				#endif //#ifndef COMMENT_OUT_ALL_PRINTS

									return UNSUCCESSFUL_RETURN; //
								} // if (nRes == UNSUCCESSFUL_RETURN)

								if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
								{
									printf("\n\n (benign) nNumOfSelectedFea = %d, fOne_Multifractal_SelectedFea = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E",
										nNumOfSelectedFea, fOne_Multifractal_SelectedFea, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1],
										nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
										nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);

				#ifndef COMMENT_OUT_ALL_PRINTS

									fprintf(fout_lr, "\n\n nNumOfSelectedFea = %d, fOne_Multifractal_SelectedFea = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E",
										nNumOfSelectedFea, fOne_Multifractal_SelectedFea, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1],
										nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
										nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);
									//fflush(fout_lr); //debugging;
				#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								} //if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
				*/

				for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)
				{
					if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
					{
						nNumOfInvalid_MultifractalFeasTot += 1;
						nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[iFea] = 0; // invalid 
					} //if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
				}//for (iFea = 0; iFea < nNumOfMultifractalFeasFor_OneDimTot; iFea++)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nNumOfInvalid_MultifractalFeasTot = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nNumOfInvalid_MultifractalFeasTot, nNumOfMultifractalFeasFor_OneDimTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n (malignant) nNumOfInvalid_MultifractalFeasTot = %d, nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfProcessedFiles_MalignantTot = %d",
					nNumOfInvalid_MultifractalFeasTot, nNumOfMultifractalFeasFor_OneDimTot, nNumOfProcessedFiles_MalignantTot);

				//printf("\n\n The end of 'doOneFea_OfMultifractal': please press any key to proceed to 'doLacunarityOf_ColorImage'");  getchar(); //exit(1);
				printf("\n\n The end of 'doOneFea_OfMultifractal' (malignant):");
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				////////////////////////
				nRes = doLacunarityOf_ColorImage(
					//image_in, // Image& image_in,
					&sColor_Image, //COLOR_IMAGE *sColor_Image,

					fOneDim_Lacunar_Arr, //[nNumOfLacunarFeasFor_OneDimTot], //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
					fFractal_Dimension); // 

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'doLacunarityOf_ColorImage' (malignant), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);

					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doLacunarityOf_ColorImage'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					if (fOneDim_Lacunar_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Lacunar_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
					{
						nNumOfInvalid_LacunarityFeasTot += 1;
						nOneDim_Multifrac_Lacunar_Cooc_ValidOrNotArr[nNumOfMultifractalFeasFor_OneDimTot + iFea] = 0; // invalid 
					} //if (fOneDim_Multifractal_Arr[iFea] < fCloseToZeroLimitForMomentsOfSquares && fOneDim_Multifractal_Arr[iFea] > fCloseToZeroLimitForMomentsOfSquares)
				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)

				printf("\n\n After 'doLacunarityOf_ColorImage' (malignant): fFractal_Dimension = %E, nNumOfInvalid_LacunarityFeasTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d",
					fFractal_Dimension, nNumOfInvalid_LacunarityFeasTot, nNumOfLacunarFeasFor_OneDimTot);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage': fFractal_Dimension = %E, nNumOfInvalid_LacunarityFeasTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d",
					fFractal_Dimension, nNumOfInvalid_LacunarityFeasTot, nNumOfLacunarFeasFor_OneDimTot);

				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					fprintf(fout_lr, "\n fOneDim_Lacunar_Arr[%d] = %E", iFea, fOneDim_Lacunar_Arr[iFea]);
				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)

				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////

//writing to fAllFeasForAll_MalignantlVecs_Multifrac_Lacunar_CoocArr[]
				nInitFea_ForAllVecs = nNumOfMultifractalFeasFor_OneDimTot;
				for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
				{
					nIndex_AllVecs = (nInitFea_ForAllVecs + iFea) + ((nNumOfProcessedFiles_MalignantTot - 1)*nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot);

					if (nIndex_AllVecs >= nNumOfFeas_ForAll_Malignant_VecsTotf)
					{
						printf("\n\n An error in malignant (lacunarity): nIndex_AllVecs = %d > nNumOfFeas_ForAll_Malignant_VecsTotf = %d, iFea = %d, nNumOfProcessedFiles_MalignantTot = %d",
							nIndex_AllVecs, nNumOfFeas_ForAll_Malignant_VecsTotf, iFea, nNumOfProcessedFiles_MalignantTot);

						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOneFea_OfMultifractal'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex_AllVecs >= nNumOfFeas_ForAll_Malignant_VecsTotf)

					fOneDim_Multifrac_Lacunar_CoocArr[nInitFea_ForAllVecs + iFea] = fOneDim_Lacunar_Arr[iFea];

					//fAllFeasForAll_MalignantlVecs_Multifrac_Lacunar_CoocArr[nIndex_AllVecs] = fOneDim_Lacunar_Arr[iFea];

				}//for (iFea = 0; iFea < nNumOfLacunarFeasFor_OneDimTot; iFea++)
//////////////////////////////////////////////
				printf("\n\n Before 'doOne_Fea_OfLacunarityOf_ColorImage' (malignant): nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);
				//printf("\n Please press any key to proceed to 'doOne_Fea_OfLacunarityOf_ColorImage'"); fflush(fout_lr); getchar(); //exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Before 'doLacunarityOf_ColorImage':   nNumOfSelectedLacunarityFea = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfIntensity_IntervalsForLacunarTot = %d",
					nNumOfSelectedLacunarityFea, nNumOfLacunarFeasFor_OneDimTot, nNumOfIntensity_IntervalsForLacunarTot);
				fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = doOne_Fea_OfLacunarityOf_ColorImage(
					nNumOfSelectedLacunarityFea, //const int nNumOfSelectedLacunarityFeaf, //< nNumOfLacunarFeasFor_OneDimTot

					image_in, //const Image& image_in,

					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					//float fOneDim_Lacunar_Arrf[], //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]
					fOne_SelectedLacunar_Fea); // float &fOne_SelectedLacunar_Feaf); //[nNumOfLacunarFeasFor_OneDimTot] //[nNumOfFeasForLacunar_AtIntensity_Range] = [nNumOfLenOfSquareMax*3 + nNumOfLenOfSquareMax*nNumOfSquareOccurrence_Intervals]

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'doOne_Fea_OfLacunarityOf_ColorImage' (benign), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doOne_Fea_OfLacunarityOf_ColorImage'.");		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

				printf("\n\n After 'doOne_Fea_OfLacunarityOf_ColorImage' (malignant): fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E",
					fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea, fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n After 'doLacunarityOf_ColorImage':  fOne_SelectedLacunar_Fea = %E, nNumOfSelectedLacunarityFea = %d, fOneDim_Lacunar_Arr[%d] = %E",
					fOne_SelectedLacunar_Fea, nNumOfSelectedLacunarityFea, nNumOfSelectedLacunarityFea, fOneDim_Lacunar_Arr[nNumOfSelectedLacunarityFea]);

				fprintf(fout_lr, "\n\n The number of valid multifractal features: %d", nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot);

				fprintf(fout_lr, "\n The number of valid lacunarity features: %d", nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

				fprintf(fout_lr, "\n\n The total number of valid multifractal and lacunarity features: %d",
					nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot + nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

				fflush(fout_lr); //;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n The number of valid multifractal features (malignant): %d", nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot);

				printf("\n The number of valid lacunarity features (malignant): %d", nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot);

				printf("\n\n (malignant) The total number of valid multifractal and lacunarity features is %d, nNumOfProcessedFiles_MalignantTot = %d",
					nNumOfMultifractalFeasFor_OneDimTot - nNumOfInvalid_MultifractalFeasTot + nNumOfLacunarFeasFor_OneDimTot - nNumOfInvalid_LacunarityFeasTot, nNumOfProcessedFiles_MalignantTot);

				//image_out.write("01-144_LCC_mask_NoLabels.png");
			//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'doLacunarityOf_ColorImage': please press any key to continue to 'doGenerating_CoocFeaturesInOneRow' "); getchar(); //exit(1);
				printf("\n\n The end of 'doLacunarityOf_ColorImage' (malignant); continuing to 'doGenerating_CoocFeaturesInOneRow' ");
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				////////////////////////////////////////////////////////////////////////////

				nRes = doGenerating_CoocFeaturesInOneRow(
					&sColor_Image, //const COLOR_IMAGE *sColor_Imagef,

					&sFeasAllOfCooc); // the total of feas is nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'doGenerating_CoocFeaturesInOneRow' (malignant), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'doGenerating_CoocFeaturesInOneRow'.");	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)

			//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n The end of 'doGenerating_CoocFeaturesInOneRow' (malignant), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = Copying_CoocFeaturesAsAVector(
					nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, //const int nDimf, // == nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot

					nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot, //const int nPositInitf, //== nNumOfMultifractalFeasFor_OneDimTot + nNumOfLacunarFeasFor_OneDimTot
					&sFeasAllOfCooc, //const FEATURES_ALL_OF_COOC *sFeasAllOfCoocf,

					fOneDim_Multifrac_Lacunar_CoocArr); // float fOneDim_Multifrac_Lacunar_CoocArrf[]); //[nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot]

				if (nRes == UNSUCCESSFUL_RETURN) // -1
				{
					printf("\n\n An error: the image has not been processed properly by 'Copying_CoocFeaturesAsAVector' (malignant), nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
					delete[] fOneDim_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
					delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

					printf("\n\n Please press any key to exit");	getchar(); exit(1);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The image has not been processed properly by 'Copying_CoocFeaturesAsAVector'.");	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN; //
				} // if (nRes == UNSUCCESSFUL_RETURN)
///////////////////////////////////////////////////////////////////////////////////////

				nTempf = (nNumOfProcessedFiles_MalignantTot - 1)* nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
				for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
				{
					nIndex = iFea + nTempf;
					if (nIndex < 0 || nIndex >= nNumOfFeas_ForAll_Malignant_VecsTotf)
					{
						printf("\n\n An error in 'main' (malignant): nIndex = %d >= nNumOfFeas_ForAll_Malignant_VecsTotf = %d", nIndex, nNumOfFeas_ForAll_Malignant_VecsTotf);
						delete[] fOneDim_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
						delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

						printf("\n\n Please press any key to exit");	getchar(); exit(1);
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An error in 'main': nIndex = %d >= nNumOfFeas_ForAll_Malignant_VecsTotf = %d", nIndex, nNumOfFeas_ForAll_Malignant_VecsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN; //
					} //if (nIndex < 0 || nIndex >= nNumOfFeas_ForAll_Malignant_VecsTotf)

		//		fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr = new float[nNumOfFeas_ForAll_Malignant_VecsTotf];

					fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[nIndex] = fOneDim_Multifrac_Lacunar_CoocArr[iFea];
				} //for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

		////////////////////////////////////////////////
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_MalignantTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_MalignantTot);  getchar(); //exit(1);
				printf("\n\n Malignant: the end of nNumOfProcessedFiles_MalignantTot = %d, nNumOfVecs_Malignant = %d; going to to the next image", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("C:\\Images_WoodyBreast_Malignant\\") //C:\\Malignant_cases\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n The directory of malignant images can not be opened");

		delete[] fOneDim_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

		printf("\n\n Press any key to exit");	getchar();
		perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

////////////////////////////////////////////////////////////////////

	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");

	fprintf(fout_PrintFeatures, "\n\n Original feas: normal images");
	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Normal (orig):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n1, ");
		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;

		for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n Original feas: images with malignancies");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Malig (orig):  iVec = %d, ", iVec);;

		fprintf(fout_PrintFeatures, "\n0, ");

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
/////////////////////////////////////////////////////////////////////////

	float *fAllFeasForAll_NormalVecs_Copy_Arr;
	fAllFeasForAll_NormalVecs_Copy_Arr = new float[nNumOfFeas_ForAll_Normal_VecsTotf];

	if (fAllFeasForAll_NormalVecs_Copy_Arr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllFeasForAll_NormalVecs_Copy_Arr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fAllFeasForAll_NormalVecs_Copy_Arr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit");	getchar(); exit(1);

		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;

		return UNSUCCESSFUL_RETURN;
	} //if (fAllFeasForAll_NormalVecs_Copy_Arr == nullptr)

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Copying malig (orig):  iVec = %d, ", iVec);;

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex];
			//fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

			fAllFeasForAll_NormalVecs_Copy_Arr[nIndex] = fFeaCur;
		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
/////////////////////////////////////////////////////////////////////////

	float *fAllFeasForAll_MalignantVecs_Copy_Arr;
	fAllFeasForAll_MalignantVecs_Copy_Arr = new float[nNumOfFeas_ForAll_Malignant_VecsTotf];

	if (fAllFeasForAll_MalignantVecs_Copy_Arr == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fAllFeasForAll_MalignantVecs_Copy_Arr' in 'main'");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'fAllFeasForAll_MalignantVecs_Copy_Arr' in 'main'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Press any key to exit");	getchar(); exit(1);

		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

		delete[] fAllFeasForAll_NormalVecs_Copy_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (fAllFeasForAll_MalignantVecs_Copy_Arr == nullptr)

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n Copying malig (orig):  iVec = %d, ", iVec);;

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[nIndex];
			//fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

			fAllFeasForAll_MalignantVecs_Copy_Arr[nIndex] = fFeaCur;
		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

//////////////////////////////////////////////////////////////////////////////////////////////////
/*// A selected fea

	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");

	fprintf(fout_PrintFeatures, "\n\n Original fea for nPositionOf_TotFeaToPrint = %d (normal)", nPositionOf_TotFeaToPrint);

	iFea = nPositionOf_TotFeaToPrint;

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Normal (orig) for nPositionOf_TotFeaToPrint = %d:  iVec = %d, ", nPositionOf_TotFeaToPrint, iVec);

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;

		//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n Original fea for nPositionOf_TotFeaToPrint = %d (malignant)", nPositionOf_TotFeaToPrint);

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n Malignant (orig) for nPositionOf_TotFeaToPrint = %d:  iVec = %d, ", nPositionOf_TotFeaToPrint,iVec);

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
*/
	fflush(fout_PrintFeatures);

	printf("\n\n The end of original feas for nPositionOf_TotFeaToPrint = %d, nNumOfProcessedFiles_MalignantTot = %d",
		nPositionOf_TotFeaToPrint, nNumOfProcessedFiles_MalignantTot);  //getchar(); //exit(1);

	printf("\n\n nNumOfMultifractalFeasFor_OneDimTot = %d, nNumOfLacunarFeasFor_OneDimTot = %d, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d",
		nNumOfMultifractalFeasFor_OneDimTot, nNumOfLacunarFeasFor_OneDimTot, nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);

	printf("\n nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot = %d, nNumOfProcessedFiles_NormalTot = %d", nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, nNumOfProcessedFiles_NormalTot);

	printf("\n\n Before 'ConvertingVecs_ToBest_SeparableFeas', nNumOfProcessedFiles_NormalTot = %d, nNumOfProcessedFiles_MalignantTot = %d, please press any key",
		nNumOfProcessedFiles_NormalTot, nNumOfProcessedFiles_MalignantTot); getchar();
	///////////////////////////////////////////////////////////////////////////
	nRes = ConvertingVecs_ToBest_SeparableFeas(
		fLarge, //const float fLargef,
		feps, //const  float fepsf,

		nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot, //const int nDimf,
		nDimSelecMax, //const int nDimSelecMaxf, //20

		nNumOfVecs_Normal, //const int nNumVec1stf,
		nNumOfVecs_Malignant, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMax, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr, //float fArr1stf[], //[nDimf*nNumVec1stf] // after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
		fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr, //float fArr2ndf[], //[nDimf*nNumVec2ndf] //after 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	///////////////////////////////////

		nDimSelec, //int &nDimSelecf,

		fAll_SelecFeasForAll_NormalVecs_Arr, //float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
		fAll_SelecFeasForAll_MalignantVecs_Arr, //float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

		fRatioBestMin, //float &fRatioBestMinf,

		nPosOneFeaBestMax, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMax, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumOf_NormalVecsSeparBestMax, //float &fNumArr1stSeparBestMaxf,
		fNumOf_MalignantVecsSeparBestMax, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArr, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArr); // int nPosFeaSeparBestArrf[])	//nDimSelecMaxf

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'main' for 'ConvertingVecs_ToBest_SeparableFeas' ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'main' for 'ConvertingVecs_ToBest_SeparableFeas' ");
		//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
		delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;
		delete[] fOneDim_Multifrac_Lacunar_CoocArr;

		delete[] fAllFeasForAll_NormalVecs_Copy_Arr;
		delete[] fAllFeasForAll_MalignantVecs_Copy_Arr;

		printf("\n\n Press any key to exit");	getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nRes == UNSUCCESSFUL_RETURN)
	///////////////////////////////////////////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n///////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (normalized) according to separability: normal");
	printf("\n\n Best feas (normalized) according to separability: normal");

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n1 (best feas, normalized):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n1, ");

		nTempf = iVec * nDimSelecMax;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAll_SelecFeasForAll_NormalVecs_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)

	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	/////////////////////////////////////////////////////////////////////////////////////////////

	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (orig) according to separability: normal");

	for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n1, ");

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{
			nPosOfSelectedFeaCur = nPosFeaSeparBestArr[iFea];
			if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot - 1)
			{
				printf("\n\n An error in 'main' (malignant cases): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'main' (malignant data): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);
				//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
				delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;
				delete[] fOneDim_Multifrac_Lacunar_CoocArr;

				delete[] fAllFeasForAll_NormalVecs_Copy_Arr;
				delete[] fAllFeasForAll_MalignantVecs_Copy_Arr;

				return UNSUCCESSFUL_RETURN;
			} //if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot - 1)

			nIndex = nPosOfSelectedFeaCur + nTempf;

			fFeaCur = fAllFeasForAll_NormalVecs_Copy_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Normal; iVec++)

	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (normalized) according to separability: malignant");
	printf("\n\n Best feas (normalized) according to separability: malignant");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		//fprintf(fout_PrintFeatures, "\n2 (best feas, normalized ):  iVec = %d, ", iVec);
		fprintf(fout_PrintFeatures, "\n0, ");

		//nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		nTempf = iVec * nDimSelecMax;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{

			nIndex = iFea + nTempf;
			fFeaCur = fAll_SelecFeasForAll_MalignantVecs_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

	fflush(fout_PrintFeatures);

	/////////////////////////////////////////////////////////////////////////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n/////////////////////////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_PrintFeatures, "\n\n Best feas (orig) according to separability: malignant");

	for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)
	{
		fprintf(fout_PrintFeatures, "\n0, ");

		nTempf = iVec * nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot;
		for (iFea = 0; iFea < nDimSelecMax; iFea++)
		{
			nPosOfSelectedFeaCur = nPosFeaSeparBestArr[iFea];
			if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot - 1)
			{
				printf("\n\n An error in 'main' (malignant cases): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'main' (malignant data): nPosOfSelectedFeaCur = %d, iFea = %d", nPosOfSelectedFeaCur, iFea);
				//printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Press any key to exit");	getchar(); exit(1);

				delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
				delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;
				delete[] fOneDim_Multifrac_Lacunar_CoocArr;

				delete[] fAllFeasForAll_NormalVecs_Copy_Arr;
				delete[] fAllFeasForAll_MalignantVecs_Copy_Arr;

				return UNSUCCESSFUL_RETURN;
			} //if (nPosOfSelectedFeaCur < 0 || nPosOfSelectedFeaCur > nNumOf_Multifrac_Lacunar_CoocFeas_OneDimTot - 1)

			nIndex = nPosOfSelectedFeaCur + nTempf;

			fFeaCur = fAllFeasForAll_MalignantVecs_Copy_Arr[nIndex];
			fprintf(fout_PrintFeatures, "%d:%E ", iFea, fFeaCur);

		}//for (iFea = 0; iFea < nDimSelecMax; iFea++)
	} // for (iVec = 0; iVec < nNumOfVecs_Malignant; iVec++)

	fflush(fout_PrintFeatures);

	///////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fclose(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	delete[] fAllFeasForAll_NormalVecs_Multifrac_Lacunar_CoocArr;
	delete[] fAllFeasForAll_MalignantVecs_Multifrac_Lacunar_CoocArr;

	delete[] fOneDim_Multifrac_Lacunar_CoocArr;

	delete[] fAllFeasForAll_NormalVecs_Copy_Arr;
	delete[] fAllFeasForAll_MalignantVecs_Copy_Arr;

	printf("\n\n The end of 'main', nNumOfProcessedFiles_NormalTot = %d, nNumOfProcessedFiles_MalignantTot = %d, please press any key to exit", nNumOfProcessedFiles_NormalTot, nNumOfProcessedFiles_MalignantTot);  getchar(); exit(1);

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();